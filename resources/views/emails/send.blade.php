
<!DOCTYPE html>
<html lang="en">
<head>
<title>Roundcube Webmail :: WMS++ 1.2 Release</title>
<meta name="viewport" content="" id="viewport" />
<link rel="shortcut icon" href="skins/larry/images/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="skins/larry/styles.min.css?s=1450862226" />
<link rel="stylesheet" type="text/css" href="skins/larry/mail.min.css?s=1450862226" />
<!--[if IE 9]><link rel="stylesheet" type="text/css" href="skins/larry/svggradients.min.css?s=1450862226" /><![endif]-->
<link rel="stylesheet" type="text/css" href="plugins/jqueryui/themes/larry/jquery-ui-1.10.4.custom.css?s=1450862226">
<script type="text/javascript" src="skins/larry/ui.min.js?s=1450862226"></script>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<script src="program/js/jquery.min.js?s=1450862226" type="text/javascript"></script>
<script src="program/js/common.min.js?s=1450862226" type="text/javascript"></script>
<script src="program/js/app.min.js?s=1450862226" type="text/javascript"></script>
<script src="program/js/treelist.min.js?s=1450862226" type="text/javascript"></script>
<script type="text/javascript">

/*
        @licstart  The following is the entire license notice for the 
        JavaScript code in this page.

        Copyright (C) 2005-2014 The Roundcube Dev Team

        The JavaScript code in this page is free software: you can redistribute
        it and/or modify it under the terms of the GNU General Public License
        as published by the Free Software Foundation, either version 3 of
        the License, or (at your option) any later version.

        The code is distributed WITHOUT ANY WARRANTY; without even the implied
        warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU GPL for more details.

        @licend  The above is the entire license notice
        for the JavaScript code in this page.
*/
</script>

<script type="text/javascript" src="plugins/jqueryui/js/jquery-ui-1.10.4.custom.min.js?s=1450862226"></script>
</head>

<div id="messagebody"><div class="message-htmlpart" id="message-htmlpart1"><!-- html ignored --><!-- head ignored --><!-- meta ignored --><!-- meta ignored --><!-- meta ignored --><style type="text/css">
@media only screen and (min-width: 620px){* [lang=x-wrapper] h1{}#message-htmlpart1 div.rcmBody * [lang=x-wrapper] h1{font-size:26px !important;
line-height:34px !important}#message-htmlpart1 div.rcmBody * [lang=x-wrapper] h2{}#message-htmlpart1 div.rcmBody * [lang=x-wrapper] h2{font-size:20px !important;
line-height:28px !important}#message-htmlpart1 div.rcmBody * [lang=x-wrapper] h3{}#message-htmlpart1 div.rcmBody * [lang=x-layout__inner] p,#message-htmlpart1 div.rcmBody * [lang=x-layout__inner] ol,#message-htmlpart1 div.rcmBody * [lang=x-layout__inner] ul{}#message-htmlpart1 div.rcmBody * div [lang=x-size-8]{font-size:8px !important;
line-height:14px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-9]{font-size:9px !important;
line-height:16px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-10]{font-size:10px !important;
line-height:18px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-11]{font-size:11px !important;
line-height:19px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-12]{font-size:12px !important;
line-height:19px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-13]{font-size:13px !important;
line-height:21px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-14]{font-size:14px !important;
line-height:21px !important}#message-htmlpart1 div.rcmBody * div 
[lang=x-size-15]{font-size:15px !important;
line-height:23px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-16]{font-size:16px !important;
line-height:24px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-17]{font-size:17px !important;
line-height:26px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-18]{font-size:18px !important;
line-height:26px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-18]{font-size:18px !important;
line-height:26px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-20]{font-size:20px !important;
line-height:28px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-22]{font-size:22px !important;
line-height:31px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-24]{font-size:24px !important;
line-height:32px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-26]{font-size:26px !important;
line-height:34px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-28]{font-size:28px !important;
line-height:36px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-30]{font-size:30px !important;
line-height:38px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-32]{font-size:32px 
!important;
line-height:40px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-34]{font-size:34px !important;
line-height:43px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-36]{font-size:36px !important;
line-height:43px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-40]{font-size:40px !important;
line-height:47px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-44]{font-size:44px !important;
line-height:50px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-48]{font-size:48px !important;
line-height:54px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-56]{font-size:56px !important;
line-height:60px !important}#message-htmlpart1 div.rcmBody * div [lang=x-size-64]{font-size:64px !important;
line-height:63px !important}}
</style>
<style type="text/css">
#message-htmlpart1 div.rcmBody {
  margin: 0;
  padding: 0}
#message-htmlpart1 div.rcmBody table {
  border-collapse: collapse;
  table-layout: fixed}
#message-htmlpart1 div.rcmBody * {
  line-height: inherit}
[x-apple-data-detectors],
[href^="tel"],
[href^="sms"] {
  color: inherit !important;
  text-decoration: none !important}
#message-htmlpart1 div.rcmBody .wrapper .footer__share-button a:hover,
#message-htmlpart1 div.rcmBody .wrapper .footer__share-button a:focus {
  color: #ffffff !important}
#message-htmlpart1 div.rcmBody .btn a:hover,
#message-htmlpart1 div.rcmBody .btn a:focus,
#message-htmlpart1 div.rcmBody .footer__share-button a:hover,
#message-htmlpart1 div.rcmBody .footer__share-button a:focus,
#message-htmlpart1 div.rcmBody .email-footer__links a:hover,
#message-htmlpart1 div.rcmBody .email-footer__links a:focus {
  opacity: 0.8}
#message-htmlpart1 div.rcmBody .ie .btn {
  width: 100%}
#message-htmlpart1 div.rcmBody .ie .column,
[owa] .column,
#message-htmlpart1 div.rcmBody .ie .gutter,
[owa] .gutter {
  display: table-cell;
  float: none !important;
  vertical-align: top}
#message-htmlpart1 div.rcmBody .ie .preheader,
[owa] .preheader,
#message-htmlpart1 div.rcmBody .ie .email-footer,
[owa] .email-footer {
  width: 560px !important}
#message-htmlpart1 div.rcmBody .ie .snippet,
[owa] .snippet,
#message-htmlpart1 div.rcmBody .ie .webversion,
[owa] .webversion {
  width: 280px !important}
#message-htmlpart1 div.rcmBody .ie .header,
[owa] .header,
#message-htmlpart1 div.rcmBody .ie .layout,
[owa] .layout,
#message-htmlpart1 div.rcmBody .ie .one-col .column,
[owa] .one-col .column {
  width: 600px !important}
#message-htmlpart1 div.rcmBody .ie .fixed-width.has-border,
[owa] .fixed-width.has-border,
#message-htmlpart1 div.rcmBody .ie .has-gutter.has-border,
[owa] .has-gutter.has-border {
  width: 602px !important}
#message-htmlpart1 div.rcmBody .ie .two-col .column,
[owa] .two-col .column {
  width: 300px !important}
#message-htmlpart1 div.rcmBody .ie .three-col .column,
[owa] .three-col .column,
#message-htmlpart1 div.rcmBody .ie .narrow,
[owa] .narrow {
  width: 200px !important}
#message-htmlpart1 div.rcmBody .ie .wide,
[owa] .wide {
  width: 400px !important}
#message-htmlpart1 div.rcmBody .ie .two-col.has-gutter .column,
[owa] .two-col.x_has-gutter .column {
  width: 290px !important}
#message-htmlpart1 div.rcmBody .ie .three-col.has-gutter .column,
[owa] .three-col.x_has-gutter .column,
#message-htmlpart1 div.rcmBody .ie .has-gutter .narrow,
[owa] .has-gutter .narrow {
  width: 188px !important}
#message-htmlpart1 div.rcmBody .ie .has-gutter .wide,
[owa] .has-gutter .wide {
  width: 394px !important}
#message-htmlpart1 div.rcmBody .ie .two-col.has-gutter.has-border .column,
[owa] .two-col.x_has-gutter.x_has-border .column {
  width: 292px !important}
#message-htmlpart1 div.rcmBody .ie .three-col.has-gutter.has-border .column,
[owa] .three-col.x_has-gutter.x_has-border .column,
#message-htmlpart1 div.rcmBody .ie .has-gutter.has-border .narrow,
[owa] .has-gutter.x_has-border .narrow {
  width: 190px !important}
#message-htmlpart1 div.rcmBody .ie .has-gutter.has-border .wide,
[owa] .has-gutter.x_has-border .wide {
  width: 396px !important}
#message-htmlpart1 div.rcmBody .ie .fixed-width .layout__inner {
  border-left: 0 none white !important;
  border-right: 0 none white !important}
#message-htmlpart1 div.rcmBody .ie .layout__edges {
  display: none}
#message-htmlpart1 div.rcmBody .mso .layout__edges {
  font-size: 0}
#message-htmlpart1 div.rcmBody .layout-fixed-width,
#message-htmlpart1 div.rcmBody .mso .layout-full-width {
  background-color: #ffffff}
@media only screen and (min-width: 620px) {
  #message-htmlpart1 div.rcmBody .column,
  #message-htmlpart1 div.rcmBody .gutter {
    display: table-cell;
    Float: none !important;
    vertical-align: top;
  }
  #message-htmlpart1 div.rcmBody .preheader,
  #message-htmlpart1 div.rcmBody .email-footer {
    width: 560px !important;
  }
  #message-htmlpart1 div.rcmBody .snippet,
  #message-htmlpart1 div.rcmBody .webversion {
    width: 280px !important;
  }
  #message-htmlpart1 div.rcmBody .header,
  #message-htmlpart1 div.rcmBody .layout,
  #message-htmlpart1 div.rcmBody .one-col .column {
    width: 600px !important;
  }
  #message-htmlpart1 div.rcmBody .fixed-width.has-border,
  #message-htmlpart1 div.rcmBody .fixed-width.ecxhas-border,
  #message-htmlpart1 div.rcmBody .has-gutter.has-border,
  #message-htmlpart1 div.rcmBody .has-gutter.ecxhas-border {
    width: 602px !important;
  }
  #message-htmlpart1 div.rcmBody .two-col .column {
    width: 300px !important;
  }
  #message-htmlpart1 div.rcmBody .three-col .column,
  #message-htmlpart1 div.rcmBody .column.narrow {
    width: 200px !important;
  }
  #message-htmlpart1 div.rcmBody .column.wide {
    width: 400px !important;
  }
  #message-htmlpart1 div.rcmBody .two-col.has-gutter .column,
  #message-htmlpart1 div.rcmBody .two-col.ecxhas-gutter .column {
    width: 290px !important;
  }
  #message-htmlpart1 div.rcmBody .three-col.has-gutter .column,
  #message-htmlpart1 div.rcmBody .three-col.ecxhas-gutter .column,
  #message-htmlpart1 div.rcmBody .has-gutter .narrow {
    width: 188px !important;
  }
  #message-htmlpart1 div.rcmBody .has-gutter .wide {
    width: 394px !important;
  }
  #message-htmlpart1 div.rcmBody .two-col.has-gutter.has-border .column,
  #message-htmlpart1 div.rcmBody .two-col.ecxhas-gutter.ecxhas-border .column {
    width: 292px !important;
  }
  #message-htmlpart1 div.rcmBody .three-col.has-gutter.has-border .column,
  #message-htmlpart1 div.rcmBody .three-col.ecxhas-gutter.ecxhas-border .column,
  #message-htmlpart1 div.rcmBody .has-gutter.has-border .narrow,
  #message-htmlpart1 div.rcmBody .has-gutter.ecxhas-border .narrow {
    width: 190px !important;
  }
  #message-htmlpart1 div.rcmBody .has-gutter.has-border .wide,
  #message-htmlpart1 div.rcmBody .has-gutter.ecxhas-border .wide {
    width: 396px !important;
  }
}
@media only screen and (-webkit-min-device-pixel-ratio: 2), #message-htmlpart1 div.rcmBody only screen and (min--moz-device-pixel-ratio: 2), #message-htmlpart1 div.rcmBody only screen and (-o-min-device-pixel-ratio: 2/1), #message-htmlpart1 div.rcmBody only screen and (min-device-pixel-ratio: 2), #message-htmlpart1 div.rcmBody only screen and (min-resolution: 192dpi), #message-htmlpart1 div.rcmBody only screen and (min-resolution: 2dppx) {
  #message-htmlpart1 div.rcmBody .fblike {/* evil! */}
  #message-htmlpart1 div.rcmBody .tweet {/* evil! */}
  #message-htmlpart1 div.rcmBody .linkedinshare {/* evil! */}
  #message-htmlpart1 div.rcmBody .forwardtoafriend {/* evil! */}
}
@media (max-width: 321px) {
  #message-htmlpart1 div.rcmBody .fixed-width.has-border .layout__inner {
    border-width: 1px 0 !important;
  }
  #message-htmlpart1 div.rcmBody .layout,
  #message-htmlpart1 div.rcmBody .column {
    min-width: 320px !important;
    width: 320px !important;
  }
  #message-htmlpart1 div.rcmBody .border {
    display: none;
  }
}
#message-htmlpart1 div.rcmBody .mso div {
  border: 0 none white !important}
#message-htmlpart1 div.rcmBody .mso .w560 .divider {
  Margin-left: 260px !important;
  Margin-right: 260px !important}
#message-htmlpart1 div.rcmBody .mso .w360 .divider {
  Margin-left: 160px !important;
  Margin-right: 160px !important}
#message-htmlpart1 div.rcmBody .mso .w260 .divider {
  Margin-left: 110px !important;
  Margin-right: 110px !important}
#message-htmlpart1 div.rcmBody .mso .w160 .divider {
  Margin-left: 60px !important;
  Margin-right: 60px !important}
#message-htmlpart1 div.rcmBody .mso .w354 .divider {
  Margin-left: 157px !important;
  Margin-right: 157px !important}
#message-htmlpart1 div.rcmBody .mso .w250 .divider {
  Margin-left: 105px !important;
  Margin-right: 105px !important}
#message-htmlpart1 div.rcmBody .mso .w148 .divider {
  Margin-left: 54px !important;
  Margin-right: 54px !important}
#message-htmlpart1 div.rcmBody .mso .font-avenir,
#message-htmlpart1 div.rcmBody .mso .font-cabin,
#message-htmlpart1 div.rcmBody .mso .font-open-sans,
#message-htmlpart1 div.rcmBody .mso .font-ubuntu {
  font-family: sans-serif !important}
#message-htmlpart1 div.rcmBody .mso .font-bitter,
#message-htmlpart1 div.rcmBody .mso .font-merriweather,
#message-htmlpart1 div.rcmBody .mso .font-pt-serif {
  font-family: Georgia, serif !important}
#message-htmlpart1 div.rcmBody .mso .font-lato,
#message-htmlpart1 div.rcmBody .mso .font-roboto {
  font-family: Tahoma, sans-serif !important}
#message-htmlpart1 div.rcmBody .mso .font-pt-sans {
  font-family: "Trebuchet MS", sans-serif !important}
#message-htmlpart1 div.rcmBody .mso .footer__share-button p {
  margin: 0}
@media only screen and (min-width: 620px) {
  #message-htmlpart1 div.rcmBody .wrapper .size-8 {
    font-size: 8px !important;
    line-height: 14px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-9 {
    font-size: 9px !important;
    line-height: 16px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-10 {
    font-size: 10px !important;
    line-height: 18px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-11 {
    font-size: 11px !important;
    line-height: 19px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-12 {
    font-size: 12px !important;
    line-height: 19px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-13 {
    font-size: 13px !important;
    line-height: 21px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-14 {
    font-size: 14px !important;
    line-height: 21px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-15 {
    font-size: 15px !important;
    line-height: 23px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-16 {
    font-size: 16px !important;
    line-height: 24px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-17 {
    font-size: 17px !important;
    line-height: 26px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-18 {
    font-size: 18px !important;
    line-height: 26px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-20 {
    font-size: 20px !important;
    line-height: 28px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-22 {
    font-size: 22px !important;
    line-height: 31px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-24 {
    font-size: 24px !important;
    line-height: 32px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-26 {
    font-size: 26px !important;
    line-height: 34px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-28 {
    font-size: 28px !important;
    line-height: 36px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-30 {
    font-size: 30px !important;
    line-height: 38px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-32 {
    font-size: 32px !important;
    line-height: 40px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-34 {
    font-size: 34px !important;
    line-height: 43px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-36 {
    font-size: 36px !important;
    line-height: 43px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-40 {
    font-size: 40px !important;
    line-height: 47px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-44 {
    font-size: 44px !important;
    line-height: 50px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-48 {
    font-size: 48px !important;
    line-height: 54px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-56 {
    font-size: 56px !important;
    line-height: 60px !important;
  }
  #message-htmlpart1 div.rcmBody .wrapper .size-64 {
    font-size: 64px !important;
    line-height: 63px !important;
  }
}
#message-htmlpart1 div.rcmBody .mso .size-8,
#message-htmlpart1 div.rcmBody .ie .size-8 {
  font-size: 8px !important;
  line-height: 14px !important}
#message-htmlpart1 div.rcmBody .mso .size-9,
#message-htmlpart1 div.rcmBody .ie .size-9 {
  font-size: 9px !important;
  line-height: 16px !important}
#message-htmlpart1 div.rcmBody .mso .size-10,
#message-htmlpart1 div.rcmBody .ie .size-10 {
  font-size: 10px !important;
  line-height: 18px !important}
#message-htmlpart1 div.rcmBody .mso .size-11,
#message-htmlpart1 div.rcmBody .ie .size-11 {
  font-size: 11px !important;
  line-height: 19px !important}
#message-htmlpart1 div.rcmBody .mso .size-12,
#message-htmlpart1 div.rcmBody .ie .size-12 {
  font-size: 12px !important;
  line-height: 19px !important}
#message-htmlpart1 div.rcmBody .mso .size-13,
#message-htmlpart1 div.rcmBody .ie .size-13 {
  font-size: 13px !important;
  line-height: 21px !important}
#message-htmlpart1 div.rcmBody .mso .size-14,
#message-htmlpart1 div.rcmBody .ie .size-14 {
  font-size: 14px !important;
  line-height: 21px !important}
#message-htmlpart1 div.rcmBody .mso .size-15,
#message-htmlpart1 div.rcmBody .ie .size-15 {
  font-size: 15px !important;
  line-height: 23px !important}
#message-htmlpart1 div.rcmBody .mso .size-16,
#message-htmlpart1 div.rcmBody .ie .size-16 {
  font-size: 16px !important;
  line-height: 24px !important}
#message-htmlpart1 div.rcmBody .mso .size-17,
#message-htmlpart1 div.rcmBody .ie .size-17 {
  font-size: 17px !important;
  line-height: 26px !important}
#message-htmlpart1 div.rcmBody .mso .size-18,
#message-htmlpart1 div.rcmBody .ie .size-18 {
  font-size: 18px !important;
  line-height: 26px !important}
#message-htmlpart1 div.rcmBody .mso .size-20,
#message-htmlpart1 div.rcmBody .ie .size-20 {
  font-size: 20px !important;
  line-height: 28px !important}
#message-htmlpart1 div.rcmBody .mso .size-22,
#message-htmlpart1 div.rcmBody .ie .size-22 {
  font-size: 22px !important;
  line-height: 31px !important}
#message-htmlpart1 div.rcmBody .mso .size-24,
#message-htmlpart1 div.rcmBody .ie .size-24 {
  font-size: 24px !important;
  line-height: 32px !important}
#message-htmlpart1 div.rcmBody .mso .size-26,
#message-htmlpart1 div.rcmBody .ie .size-26 {
  font-size: 26px !important;
  line-height: 34px !important}
#message-htmlpart1 div.rcmBody .mso .size-28,
#message-htmlpart1 div.rcmBody .ie .size-28 {
  font-size: 28px !important;
  line-height: 36px !important}
#message-htmlpart1 div.rcmBody .mso .size-30,
#message-htmlpart1 div.rcmBody .ie .size-30 {
  font-size: 30px !important;
  line-height: 38px !important}
#message-htmlpart1 div.rcmBody .mso .size-32,
#message-htmlpart1 div.rcmBody .ie .size-32 {
  font-size: 32px !important;
  line-height: 40px !important}
#message-htmlpart1 div.rcmBody .mso .size-34,
#message-htmlpart1 div.rcmBody .ie .size-34 {
  font-size: 34px !important;
  line-height: 43px !important}
#message-htmlpart1 div.rcmBody .mso .size-36,
#message-htmlpart1 div.rcmBody .ie .size-36 {
  font-size: 36px !important;
  line-height: 43px !important}
#message-htmlpart1 div.rcmBody .mso .size-40,
#message-htmlpart1 div.rcmBody .ie .size-40 {
  font-size: 40px !important;
  line-height: 47px !important}
#message-htmlpart1 div.rcmBody .mso .size-44,
#message-htmlpart1 div.rcmBody .ie .size-44 {
  font-size: 44px !important;
  line-height: 50px !important}
#message-htmlpart1 div.rcmBody .mso .size-48,
#message-htmlpart1 div.rcmBody .ie .size-48 {
  font-size: 48px !important;
  line-height: 54px !important}
#message-htmlpart1 div.rcmBody .mso .size-56,
#message-htmlpart1 div.rcmBody .ie .size-56 {
  font-size: 56px !important;
  line-height: 60px !important}
#message-htmlpart1 div.rcmBody .mso .size-64,
#message-htmlpart1 div.rcmBody .ie .size-64 {
  font-size: 64px !important;
  line-height: 63px !important}
#message-htmlpart1 div.rcmBody .footer__share-button p {
  margin: 0}
</style>
<link href="./?_task=utils&amp;_action=modcss&amp;_u=tmp-c3eaac2f161db0d69075839985fc65f8.css&amp;_c=message-htmlpart1" rel="stylesheet" type="text/css" /><style type="text/css">
#message-htmlpart1 div.rcmBody{background-color:#fafafa}#message-htmlpart1 div.rcmBody .mso h1{}#message-htmlpart1 div.rcmBody .mso h2{}#message-htmlpart1 div.rcmBody .mso h3{}#message-htmlpart1 div.rcmBody .mso .column,#message-htmlpart1 div.rcmBody .mso .column__background td{}#message-htmlpart1 div.rcmBody .mso .column,#message-htmlpart1 div.rcmBody .mso .column__background td{font-family:Tahoma,sans-serif !important}#message-htmlpart1 div.rcmBody .mso .btn a{}#message-htmlpart1 div.rcmBody .mso .btn a{font-family:Tahoma,sans-serif !important}#message-htmlpart1 div.rcmBody .mso .webversion,#message-htmlpart1 div.rcmBody .mso .snippet,#message-htmlpart1 div.rcmBody .mso .layout-email-footer td,#message-htmlpart1 div.rcmBody .mso .footer__share-button p{}#message-htmlpart1 div.rcmBody .mso .webversion,#message-htmlpart1 div.rcmBody .mso .snippet,#message-htmlpart1 div.rcmBody .mso .layout-email-footer td,#message-htmlpart1 div.rcmBody .mso .footer__share-button p{font-family:Tahoma,sans-serif !important}#message-htmlpart1 div.rcmBody .mso .logo{}#message-htmlpart1 div.rcmBody .mso .logo{font-family:Tahoma,sans-serif !important}#message-htmlpart1 div.rcmBody .logo a:hover,#message-htmlpart1 div.rcmBody .logo a:focus{color:#859bb1 !important}#message-htmlpart1 div.rcmBody .mso .layout-has-border{border-top:1px solid #c7c7c7;
border-bottom:1px solid #c7c7c7}#message-htmlpart1 div.rcmBody .mso .layout-has-bottom-border{border-bottom:1px solid #c7c7c7}#message-htmlpart1 div.rcmBody .mso .border,#message-htmlpart1 div.rcmBody .ie .border{background-color:#c7c7c7}@media only screen and (min-width: 620px){.wrapper h1{}#message-htmlpart1 div.rcmBody .wrapper h1{font-size:26px !important;
line-height:34px 
!important}#message-htmlpart1 div.rcmBody .wrapper h2{}#message-htmlpart1 div.rcmBody .wrapper h2{font-size:20px !important;
line-height:28px !important}#message-htmlpart1 div.rcmBody .wrapper h3{}#message-htmlpart1 div.rcmBody .column p,#message-htmlpart1 div.rcmBody .column ol,#message-htmlpart1 div.rcmBody .column ul{}}#message-htmlpart1 div.rcmBody .mso h1,#message-htmlpart1 div.rcmBody .ie h1{}#message-htmlpart1 div.rcmBody .mso h1,#message-htmlpart1 div.rcmBody .ie h1{font-size:26px !important;
line-height:34px !important}#message-htmlpart1 div.rcmBody .mso h2,#message-htmlpart1 div.rcmBody .ie h2{}#message-htmlpart1 div.rcmBody .mso h2,#message-htmlpart1 div.rcmBody .ie h2{font-size:20px !important;
line-height:28px !important}#message-htmlpart1 div.rcmBody .mso h3,#message-htmlpart1 div.rcmBody .ie h3{}#message-htmlpart1 div.rcmBody .mso .layout__inner p,#message-htmlpart1 div.rcmBody .ie .layout__inner p,#message-htmlpart1 div.rcmBody .mso .layout__inner ol,#message-htmlpart1 div.rcmBody .ie .layout__inner ol,#message-htmlpart1 div.rcmBody .mso .layout__inner ul,#message-htmlpart1 div.rcmBody .ie .layout__inner ul{}
</style>
<div class="rcmBody" class="full-padding" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%">

    <div class="wrapper" style="min-width: 320px; background-color: #fafafa" lang="x-wrapper">
      <div class="preheader" style="Margin: 0 auto; max-width: 560px; min-width: 280px; width: 280px; width: calc(28000% - 173040px)">
        <div style="border-collapse: collapse; display: table; width: 100%">
        
          <div class="snippet" style="Float: left; font-size: 12px; line-height: 19px; max-width: 280px; min-width: 140px; width: 140px; width: calc(14000% - 78120px); padding: 10px 0 5px 0; color: #c2c2c2; font-family: Lato,Tahoma,sans-serif">
            
          </div>
        
          <div class="webversion" style="Float: left; font-size: 12px; line-height: 19px; max-width: 280px; min-width: 139px; width: 139px; width: calc(14100% - 78680px); padding: 10px 0 5px 0; text-align: right; color: #c2c2c2; font-family: Lato,Tahoma,sans-serif">
            
          </div>
        
        </div>
      </div>
      <div class="header" style="Margin: 0 auto; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 173000px)" id="emb-email-header-container">
      
        <div class="logo emb-logo-margin-box" style="font-size: 26px; line-height: 32px; Margin-top: 6px; Margin-bottom: 20px; color: #c3ced9; font-family: Roboto,Tahoma,sans-serif; Margin-left: 20px; Margin-right: 20px" align="center">
          <div class="logo-center" style="font-size: 0px !important; line-height: 0 !important" align="center" id="emb-email-header"><img style="height: auto; width: 100%; border: 0; max-width: 222px" src="http://i1.cmail2.com/ei/d/34/B8E/A77/063241/csfinal/Capture1.PNG" alt="" width="222" /></div>
        </div>
      
      </div>
      <div class="layout one-col fixed-width" style="Margin: 0 auto; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 173000px); overflow-wrap: break-word; word-wrap: break-word; word-break: break-word">
        <div class="layout__inner" style="border-collapse: collapse; display: table; width: 100%; background-color: #fafafa" lang="x-layout__inner">
        
          <div class="column" style="text-align: left; color: #595959; font-size: 14px; line-height: 21px; font-family: Lato,Tahoma,sans-serif; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 167400px)">
        
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-top: 24px">
      <h1 class="size-32" style="Margin-top: 0; Margin-bottom: 0; font-style: normal; font-weight: normal; color: #b8bdc9; font-size: 28px; line-height: 36px; text-align: center" lang="x-size-32">Introducing WMS++ 1.2</h1><p class="size-17" style="Margin-top: 20px; Margin-bottom: 20px; font-size: 17px; line-height: 26px" lang="x-size-17">We're pleased to announce a brand new updates to help you track and manage your warehouse.</p>
    </div>
        
        <div style="font-size: 12px; font-style: normal; font-weight: normal" align="center">
          <img style="border: 0; display: block; height: auto; width: 100%; max-width: 900px" alt="" width="600" src="http://i1.cmail2.com/ei/d/34/B8E/A77/063241/csfinal/landing-011.png" /></div>
      
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-top: 20px">
      <div style="line-height: 10px; font-size: 1px"> </div>
    </div>
        
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-bottom: 24px">
      <p class="size-17" style="Margin-top: 0; Margin-bottom: 0; font-size: 17px; line-height: 26px" lang="x-size-17">Next time you log in, you'll see a brand new experience in the work flow and data hierarchy where we have totally redesigned a more convenient work flow.</p><p class="size-17" style="Margin-top: 20px; Margin-bottom: 0; font-size: 17px; line-height: 26px" lang="x-size-17">User interface is more convenient, new tour guide for new comers and much more you will see inside. </p>
    </div>
        
          </div>
        
        </div>
      </div>
  
      <div style="line-height: 20px; font-size: 20px"> </div>
  
      <div class="layout fixed-width" style="Margin: 0 auto; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 173000px); overflow-wrap: break-word; word-wrap: break-word; word-break: break-word">
        <div class="layout__inner" style="border-collapse: collapse; display: table; width: 100%; background-color: #fafafa" lang="x-layout__inner">
        
          <div class="column wide" style="text-align: left; color: #595959; font-size: 14px; line-height: 21px; font-family: Lato,Tahoma,sans-serif; Float: left; max-width: 400px; min-width: 320px; width: 320px; width: calc(8000% - 47600px)">
          
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-top: 24px; Margin-bottom: 24px">
            <p style="Margin-top: 0; Margin-bottom: 0"><strong>Your Credentials</strong></p>
            <br/>
            <p style="Margin-top: 0; Margin-bottom: 0"><strong>Username: {{$username}}</strong></p>
            <p style="Margin-top: 0; Margin-bottom: 0"><strong>Password: {{$password}}</strong></p>
            <br/>
            <br/>
      <p style="Margin-top: 0; Margin-bottom: 0"><strong>What's New?</strong></p><p style="Margin-top: 20px; Margin-bottom: 0">1- Fixed bugs in WMS 1.0<br />

2-New options for location hierarchy.<br />
3- Transactions can be partially executed.<br />
4- Updated user roles.<br />
5- Storage and order strategies is added.<br />
6- Generate printable documents for orders.<br />
7-Day be day reporting.<br />
8- Step by step guide for each operation.<br />
9- New statistics is added to the admin home.<br />
10- Integrated barcode capabilities. </p>
    </div>
          
          </div>
        
          <div class="column narrow" style="text-align: left; color: #595959; font-size: 14px; line-height: 21px; font-family: Lato,Tahoma,sans-serif; Float: left; max-width: 320px; min-width: 200px; width: 320px; width: calc(72200px - 12000%)">
          
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-top: 24px">
      <div style="line-height: 20px; font-size: 1px"> </div>
    </div>
          
            <div style="Margin-left: 20px; Margin-right: 20px">
      <div style="line-height: 45px; font-size: 1px"> </div>
    </div>
          
            <div style="Margin-left: 20px; Margin-right: 20px">
      <blockquote style="Margin-top: 0; Margin-bottom: 20px; Margin-left: 0; Margin-right: 0; padding-left: 14px; border-left: 4px solid #c7c7c7"><p style="Margin-top: 0; Margin-bottom: 20px"><a style="text-decoration: underline; transition: opacity 0.1s ease-in; color: #6b7489" href="http://meti.cmail2.com/t/d-l-ihikdkt-uycuturr-r/" target="_blank" rel="noreferrer">Goods In Walk Through</a></p></blockquote>
    </div>
          
            <div style="Margin-left: 20px; Margin-right: 20px">
      <div style="line-height: 20px; font-size: 1px"> </div>
    </div>
          
            <div style="Margin-left: 20px; Margin-right: 20px">
      <blockquote style="Margin-top: 0; Margin-bottom: 20px; Margin-left: 0; Margin-right: 0; padding-left: 14px; border-left: 4px solid #c7c7c7"><p style="Margin-top: 0; Margin-bottom: 20px"><a style="text-decoration: underline; transition: opacity 0.1s ease-in; color: #6b7489" href="http://meti.cmail2.com/t/d-l-ihikdkt-uycuturr-j/" target="_blank" rel="noreferrer">Goods Out Walk Through</a></p></blockquote>
    </div>
          
            <div style="Margin-left: 20px; Margin-right: 20px">
      <div style="line-height: 16px; font-size: 1px"> </div>
    </div>
          
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-bottom: 24px">
      <div class="btn btn--flat btn--large" style="text-align: center">
        <a style="border-radius: 4px; display: inline-block; font-size: 14px; font-weight: bold; line-height: 24px; padding: 12px 24px; text-align: center; text-decoration: none !important; transition: opacity 0.1s ease-in; color: #fff; background-color: #6b7489; font-family: Lato, Tahoma, sans-serif" href="http://{{$warehouseName}}.me-wims.com/" target="_blank" rel="noreferrer">Try Now</a>
      </div>
    </div>
          
          </div>
        
        </div>
      </div>
  
      <div style="line-height: 20px; font-size: 20px"> </div>
  
      <div class="layout one-col fixed-width" style="Margin: 0 auto; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 173000px); overflow-wrap: break-word; word-wrap: break-word; word-break: break-word">
        <div class="layout__inner" style="border-collapse: collapse; display: table; width: 100%; background-color: #fafafa" lang="x-layout__inner">
        
          <div class="column" style="text-align: left; color: #595959; font-size: 14px; line-height: 21px; font-family: Lato,Tahoma,sans-serif; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 167400px)">
        
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-top: 24px; Margin-bottom: 24px">
      <p class="size-17" style="Margin-top: 0; Margin-bottom: 0; font-size: 17px; line-height: 26px" lang="x-size-17">This release was a lot influenced by customer feedback so we're truly eager to get this in the hands of our clients. A lot of helper documentation is available in our <a style="text-decoration: underline; transition: opacity 0.1s ease-in; color: #6b7489" href="http://meti.cmail2.com/t/d-l-ihikdkt-uycuturr-i/" target="_blank" rel="noreferrer">Support Center</a>. </p>
    </div>
        
          </div>
        
        </div>
      </div>
  
      <div style="line-height: 20px; font-size: 20px"> </div>
  
      <div class="layout email-footer" style="Margin: 0 auto; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 173000px); overflow-wrap: break-word; word-wrap: break-word; word-break: break-word">
        <div class="layout__inner" style="border-collapse: collapse; display: table; width: 100%" lang="x-layout__inner">
        
          <div class="column wide" style="text-align: left; font-size: 12px; line-height: 19px; color: #c2c2c2; font-family: Lato,Tahoma,sans-serif; Float: left; max-width: 400px; min-width: 320px; width: 320px; width: calc(8000% - 47600px)">
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-top: 10px; Margin-bottom: 10px">
              <table class="email-footer__links emb-web-links" style="border-collapse: collapse; table-layout: fixed"><tbody><tr><td class="emb-web-links" style="padding: 0; width: 26px"><a style="text-decoration: underline; transition: opacity 0.1s ease-in; color: #c2c2c2" href="http://meti.cmail2.com/t/d-l-ihikdkt-uycuturr-d/" target="_blank" rel="noreferrer"><img style="border: 0" src="http://i8.cmail2.com/static/eb/master/13-the-blueprint-3/images/facebook.png" width="26" height="26" /></a></td><td class="emb-web-links" style="padding: 0 0 0 3px; width: 26px"><a style="text-decoration: underline; transition: opacity 0.1s ease-in; color: #c2c2c2" href="http://meti.cmail2.com/t/d-l-ihikdkt-uycuturr-h/" target="_blank" rel="noreferrer"><img style="border: 0" src="http://i9.cmail2.com/static/eb/master/13-the-blueprint-3/images/twitter.png" width="26" height="26" /></a></td><td class="emb-web-links" style="padding: 0 0 0 3px; width: 26px"><a style="text-decoration: underline; transition: opacity 0.1s ease-in; color: #c2c2c2" href="http://meti.cmail2.com/t/d-l-ihikdkt-uycuturr-k/" target="_blank" rel="noreferrer"><img style="border: 0" src="http://i10.cmail2.com/static/eb/master/13-the-blueprint-3/images/youtube.png" width="26" height="26" /></a></td><td class="emb-web-links" style="padding: 0 0 0 3px; width: 26px"><a style="text-decoration: underline; transition: opacity 0.1s ease-in; color: #c2c2c2" href="http://meti.cmail2.com/t/d-l-ihikdkt-uycuturr-u/" target="_blank" rel="noreferrer"><img style="border: 0" src="http://i2.cmail2.com/static/eb/master/13-the-blueprint-3/images/linkedin.png" width="26" height="26" /></a></td>
              </tr></tbody></table><div style="Margin-top: 20px">
                <div>Micro Engineering Tech Inc.<br />
Suite 400, 1716-16 Ave NW<br />
Calgary, Alberta T2M 0L7<br />
Tel: +1 (403) 457-3112<br />
Fax: +1 (403) 457-3121</div>
              </div>
              <div style="Margin-top: 18px">
                
              </div>
            </div>
          </div>
        
          <div class="column narrow" style="text-align: left; font-size: 12px; line-height: 19px; color: #c2c2c2; font-family: Lato,Tahoma,sans-serif; Float: left; max-width: 320px; min-width: 200px; width: 320px; width: calc(72200px - 12000%)">
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-top: 10px; Margin-bottom: 10px">
              
            </div>
          </div>
        
        </div>
      </div>
      <div class="layout one-col email-footer" style="Margin: 0 auto; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 173000px); overflow-wrap: break-word; word-wrap: break-word; word-break: break-word">
        <div class="layout__inner" style="border-collapse: collapse; display: table; width: 100%" lang="x-layout__inner">
        
          <div class="column" style="text-align: left; font-size: 12px; line-height: 19px; color: #c2c2c2; font-family: Lato,Tahoma,sans-serif; max-width: 600px; min-width: 320px; width: 320px; width: calc(28000% - 167400px)">
            <div style="Margin-left: 20px; Margin-right: 20px; Margin-top: 10px; Margin-bottom: 10px">
              <div>
                <a style="text-decoration: underline; transition: opacity 0.1s ease-in; color: #c2c2c2" href="http://meti.cmail2.com/t/d-u-ihikdkt-uycuturr-o/" target="_blank" rel="noreferrer">Unsubscribe</a>
              </div>
            </div>
          </div>
        
        </div>
      </div>
      <div style="line-height: 40px; font-size: 40px"> </div>
    
  </div><img style="overflow: hidden; position: fixed; visibility: hidden !important; display: block !important; height: 1px !important; width: 1px !important; border: 0 !important; margin: 0 !important; padding: 0 !important" src="https://meti.cmail2.com/t/d-o-ihikdkt-uycuturr/o.gif" width="1" height="1" border="0" alt="" /></div></div>
</div>
<div id="messagestack"></div>

<script type="text/javascript">

// UI startup
var UI = new rcube_mail_ui();
$(document).ready(function(){
  UI.set('errortitle', 'An error occurred!');
  UI.set('toggleoptions', 'Toggle advanced options');
  UI.init();
});

</script>


<script type="text/javascript">

jQuery.extend(jQuery.ui.dialog.prototype.options.position, {
                using: function(pos) {
                    var me = jQuery(this),
                        offset = me.css(pos).offset(),
                        topOffset = offset.top - 12;
                    if (topOffset < 0)
                        me.css('top', pos.top - topOffset);
                    if (offset.left + me.outerWidth() + 12 > jQuery(window).width())
                        me.css('left', pos.left - 12);
                }
            });
$(document).ready(function(){ 
rcmail.init();
});
</script>

</body>
</html>