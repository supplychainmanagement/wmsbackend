<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SKUType extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'SKU_Type_OWN_id');
    }
    public function SKU()
    {
        return $this->hasMany('App\SKU', 'SKU_SKU_Type_id');
    }
    public function CapacityUnitload()
    {
        return $this->hasMany('App\Unitload', 'CU_SKU_Type_id');
    }
    protected $table = 'SKU_Type';
    protected $dates = ['deleted_at'];
}
