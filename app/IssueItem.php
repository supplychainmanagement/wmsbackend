<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IssueItem extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'ISI_OWN_id');
    }
    public function SKU()
    {
        return $this->belongsTo('App\SKU', 'ISI_SKU_id');
    }
    public function IssueOrder()
    {
        return $this->belongsTo('App\IssueOrder', 'ISI_ISO_id');
    }
    public function Strategy()
    {
        return $this->belongsTo('App\Strategy', 'ISI_STRAT_id');
    }
    public function PickRequest()
    {
        return $this->hasOne('App\PickRequest', 'PR_ISI_id');
    }
    public function Shipping()
    {
        return $this->hasOne('App\Shipping', 'SHIP_ISI_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\Site', 'ISI_SITE_id');
    }
    protected $table = 'Issue_Item';
    protected $dates = ['deleted_at'];
}
