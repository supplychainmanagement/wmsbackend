<?php
/**
 * Created by PhpStorm.
 * User: Bor3y
 * Date: 2/1/2016
 * Time: 3:02 PM
 */

namespace App\RoboVics\Transformers;


class UserTransformer extends Transformer
{

    private $userTransformer;
    private $lessonTransformer;

    /**
     * UserTransformer constructor.
     * @param $userTransformer
     * @param $lessonTransformer
     */
    public function __construct(UserTransformer $userTransformer,LessonTransformer $lessonTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->lessonTransformer = $lessonTransformer;
    }


    public function transform($user)
    {
        return [
            'id' => (int) $user['id'],
            'fname' => $user['fname'],
            'lname' => $user['lname'],
            'email' => $user['email'],
            'gender' => $user['gender'],
            'start_at' => $user['start_at'],
            'image_link' => $this['image_link'],
            'created_at' => $user['created_at'],
            'updated_at' => $user['updated_at'],
            'expired_at' => $user['expired_at'],
            'limit' => $user['limit'],
            'qouta' => $user['qouta'],
            'type' => $user['type'],
            'status' => $user['status'],
            'parent' => $user['parent'],
        ];
    }

    public function requestTransform($request)
    {
        return [
            'fname' => $request['fname'],
            'lname' => $request['lname'],
            'email' => $request['email'],
            'password' => $request['password'],
            'gender' => $request['gender'],
        ];
    }

}