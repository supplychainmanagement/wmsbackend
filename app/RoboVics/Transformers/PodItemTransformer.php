<?php
/**
 * Created by PhpStorm.
 * User: Bor3y
 * Date: 2/2/2016
 * Time: 10:51 AM
 */

namespace App\RoboVics\Transformers;


use App\PurchaseOrderItems;

class PodItemTransformer extends Transformer
{
    public function transform( $pod_item)
    {
        return [
            'id' => $pod_item['id'],
            'poi_number' => $pod_item['POI_number'],
            'amount' => $pod_item['POI_amount'],
            'provider' => $pod_item['Provider'],
            'pod' => $pod_item['PurchaseOrder'],
            'item' => $pod_item['SKU'],
            'client' => $pod_item['Client'],
            'owner' => $pod_item['Owner'],
            'site' => $pod_item['Site'],
        ];
    }

    public function transformPodItemMajorData ( $pod_item){

        $pod_item_detailed =$this->transform($pod_item);
        return [
            'poi_id' => $pod_item_detailed['id'],
            'pod_id' => $pod_item_detailed['pod']->id,
            'provider_name' => $pod_item_detailed['provider']->PRV_name,
            'provider_id' => $pod_item_detailed['provider']->id, // to be updated.
            'order_sequense' => $pod_item_detailed['poi_number'], // to be updated.
            'item_id' => $pod_item_detailed['item']->SKU_name,
            'item_name' => $pod_item_detailed['item']->id,
            'item_serial' =>$pod_item_detailed['item']->SKU_item_no,
            'amount' => $pod_item_detailed['amount'],
            'proceeded' => 0, // to be updated.
            'item_order_number' => $pod_item_detailed['poi_number'],// to be updated.
        ];
    }

    public function transformPodItemsMajorData ($items){
        $pois = array();
        foreach ($items as $item) {
            array_push($pois,  $this->transformPodItemMajorData($item));
        }
        return $pois;
   }

    public function requestTransform($request)
    {
        return [
        ];
    }
}