<?php
/**
 * Created by PhpStorm.
 * User: Bor3y
 * Date: 11/12/2015
 * Time: 12:14 PM
 */

namespace App\RoboVics\Transformers;


Abstract class Transformer
{
    public function TransformCollection(array $items){
        return array_map([$this, 'transform'], $items);
    }

    public abstract function transform($item);
    public abstract function requestTransform($request);
}