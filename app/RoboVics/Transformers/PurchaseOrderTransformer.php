<?php
namespace App\RoboVics\Transformers;
use  App\PurchaseOrder ;
class PurchaseOrderTransformer extends Transformer
{
    protected  $poi_transformer ;

    public function __construct(PodItemTransformer $poi_transformer)
    {
        $this->poi_transformer = $poi_transformer;
    }
    public function transform($pod){
    
        return [
            'id' => $pod['id'],
            'title' => $pod['PO_number'],
            'desc' => $pod['Owner'],
            'client' => $pod->Client(),
            'created_at' => $pod['created_at'],
            'updated_at' => $pod['updated_at'],
            'lang' => $pod['PO_dateOfDelivery'],
        ];
    }

    public function transformToHandheld($pod){
        return [
            'id' => $pod['id'],
            'pod_num' => $pod['PO_number'],
            'date' => $pod['PO_dateOfDelivery'],
            'clientId' => $pod['Client']->id,
            'clientName' => $pod['Client']->CLI_name,
            'pod_items' =>$this->poi_transformer->transformPodItemsMajorData($pod['podItems']),

        ];
    }
    public function requestTransform($request){
        return [
            'title' => $request['title'],
            'description' => $request['desc'],
            'language_id' => (int) $request['lang_id'],
        ];
    }
}