<?php

namespace App\RoboVics\HttpRequest;

class HttpMethods {
    public static function  post($url,$headers,$fields){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $response = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $server_output = ['status'=> $info["http_code"] ,'body' =>$response];
        curl_close ($ch);
        return $server_output;
    }

    public static function  get($url,$headers){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $server_output = ['status'=> $info["http_code"] ,'body' =>$response];
        curl_close ($ch);
        return $server_output;
    }
}
