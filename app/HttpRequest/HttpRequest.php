<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/1/2016
 * Time: 10:11 AM
 */
use Illuminate\Support\Facades\Session;

class HttpRequest
{
    public static function  post($url, $fields)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        if (Session::has('Authorization')) {
            $temp = Session::get('Authorization');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:$temp"));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $body = curl_exec($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' => $body, 'info' => $info];
        curl_close($ch);
        return $server_output;
    }

    public static function  get($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (Session::has('Authorization')) {
            $temp = Session::get('Authorization');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:$temp"));
        }

        $body = curl_exec($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' => $body, 'info' => $info];
        curl_close($ch);
        return $server_output;
    }

    public static function  put($url, $fields = [])
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        if (Session::has('Authorization')) {
            $temp = Session::get('Authorization');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:$temp"));
        }

        $body = curl_exec($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' => $body, 'info' => $info];
        curl_close($ch);
        return $server_output;
    }

    public static function del($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (Session::has('Authorization')) {
            $temp = Session::get('Authorization');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:$temp"));
        }

        $body = curl_exec($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' => $body, 'info' => $info];
        //$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $server_output;
    }
}