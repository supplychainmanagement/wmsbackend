<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PickRequestItem extends Model
{
    use SoftDeletes;

    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'PRI_OWN_id');
    }

    public function Site()
    {
        return $this->belongsTo('App\Site', 'PRI_SITE_id');
    }

    public function PickRequest()
   	{
   		return $this->belongsTo('App\PickRequest', 'PRI_PR_id');
   	}

   	public function Unitload()
   	{
   		return $this->belongsTo('App\Unitload', 'PRI_UL');
   	}

   	public function PickJob()
   	{
   		return $this->hasOne('App\PickJob','PJ_PRI_id');
   	}

    protected $table = 'Pick_Request_Item';
    protected $dates = ['deleted_at'];
}
