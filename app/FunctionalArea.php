<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FunctionalArea extends ApiModel
{
	use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'FA_OWN_id');
    }
    public function StorageLocation()
    {
        return $this->hasMany('App\StorageLocation', 'STL_FA_id');
    }
    public function Bin()
    {
        return $this->hasMany('App\Bin', 'BIN_FA_id');
    }

    public function isGoodsInArea(){

            return $this->FA_GoodsIn;

    }

    public function isGoodsOutArea(){

        return $this->FA_GoodsOut;

    }
    public function isStorageArea(){

        return $this->FA_Storage;

    }
    protected $table = 'Functional_Area';
    protected $dates = ['deleted_at'];
}
