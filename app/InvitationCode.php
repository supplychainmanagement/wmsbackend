<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvitationCode extends ApiModel
{
    use SoftDeletes;

    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'INV_OWN_id');
    }

    protected $table = 'Invitation_Code';
    protected $dates = ['deleted_at'];
}
