<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'SITE_OWN_id');
    }
    public function Zone()
    {
    	return $this->hasMany('App\Zone','ZONE_SITE_id');
    }
    public function IssueOrder()
    {
        return $this->hasMany('App\IssueOrder','ISO_SITE_id');
    }
    public function IssueOrderItem()
    {
        return $this->hasMany('App\IssueItem','ISI_SITE_id');
    }
    public function StorageJob()
    {
        return $this->hasMany('App\StorageJob','STJ_SITE_id');
    }
    public function PickJob()
    {
        return $this->hasMany('App\PickJob','PJ_SITE_id');
    }
    public function PickRequest()
    {
        return $this->hasMany('App\PickRequest','PR_SITE_id');
    }
    public function PickRequestItem()
    {
        return $this->hasMany('App\PickRequestItem','PRI_SITE_id');
    }
    public function ReceivingJob()
    {
        return $this->hasMany('App\ReceivingJob','RCJ_SITE_id');
    }
    public function ReceivingJobUnitload()
    {
        return $this->hasMany('App\ReceivingJobUnitload','RJU_SITE_id');
    }
    public function PurchaseOrderItems()
    {
        return $this->hasMany('App\PurchaseOrderItems','POI_SITE_id');
    }
    public function StorageLocation()
    {
        return $this->hasMany('App\StorageLocation','STL_SITE_id');
    }

    public function Unitload()
    {
        return $this->hasMany('App\Unitload','UL_SITE_id');
    }
    public function User()
    {
        return $this->hasMany('App\User','USR_SITE_id');
    }
    public function Shipping()
    {
        return $this->hasMany('App\Shipping','SHIP_SITE_id');
    }
    public function PurchaseOrder()
    {
        return $this->hasMany('App\PurchaseOrder','PO_SITE_id');
    }
    public function Client()
    {
        return $this->belongsToMany('App\Client','Site_Client_Contract','SCC_SITE_id','SCC_CLI_id');
    }

    protected $table = 'Site';
    protected $dates = ['deleted_at'];
}
