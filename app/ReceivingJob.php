<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceivingJob extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'RCJ_OWN_id');
    }
    public function PurchaseOrderItems()
    {
        return $this->belongsTo('App\PurchaseOrderItems', 'RCJ_POI_id');
    }
    public function ReceivingJobUnitload()
    {
        return $this->hasMany('App\ReceivingJobUnitload', 'RJU_RCJ_id');
    }
    protected $table = 'Receiving_Job';
    protected $dates = ['deleted_at'];
}
