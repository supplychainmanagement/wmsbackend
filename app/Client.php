<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Client extends ApiModel
{
	use SoftDeletes;

    public function User()
    {
        return $this->hasMany('App\User', 'USR_CLI_id');
    }
    public function Customer()
    {
        return $this->hasMany('App\Customer', 'CST_CLI_id');
    }
    public function Provider()
    {
        return $this->hasMany('App\Provider', 'PRV_CLI_id');
    }
    public function SKU()
    {
        return $this->hasMany('App\SKU', 'SKU_CLI_id');
    }
    public function Unitload()
    {
        return $this->hasMany('App\Unitload', 'UL_CLI_id');
    }
    public function PurchaseOrder()
    {
        return $this->hasMany('App\PurchaseOrder', 'PO_CLI_id');
    }
    public function PurchaseOrderItems()
    {
        return $this->hasMany('App\PurchaseOrderItems', 'POV_CLI_id');
    }
    public function IssueOrder()
    {
        return $this->hasMany('App\IssueOrder', 'ISO_CLI_id');
    }

    public function CLICustomer()
    {
        return $this->belongsToMany('App\Customer','Customer_Client','CC_CLI_id','CC_CST_id');
    }

    public function Site()
    {
        return $this->belongsToMany('App\Site','Site_Client_Contract','SCC_CLI_id','SCC_SITE_id');
    }
    public function CLIProvider()
    {
        return $this->belongsToMany('App\Provider','Provider_Client','PC_CLI_id','PC_PRV_id');
    }


    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'CLI_OWN_id');
    }
    protected $table = 'Client';
    protected $dates = ['deleted_at'];
}
