<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PickJob extends Model
{
  use SoftDeletes;

    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'PJ_OWN_id');
    }

    public function Site()
    {
        return $this->belongsTo('App\Site', 'PJ_SITE_id');
    }

    public function PickRequestItem()
   	{
   		return $this->belongsTo('App\PickRequestItem','PJ_PRI_id');
   	}

   	public function Unitload()
   	{
   		return $this->belongsTo('App\Unitload', 'PJ_UL');
   	}

    protected $table = 'Pick_Job';
    protected $dates = ['deleted_at'];
}
