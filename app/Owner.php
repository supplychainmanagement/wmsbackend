<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends ApiModel
{
    use SoftDeletes;

    public function Zone()
    {
        return $this->hasMany('App\Zone', 'ZONE_OWN_id');
    }
    public function CapacityLocation()
    {
        return $this->hasMany('App\CapacityLocation', 'CL_OWN_id');
    }
    public function CapacityUnitload()
    {
        return $this->hasMany('App\CapacityUnitload', 'CU_OWN_id');
    }
    public function UnitloadType()
    {
        return $this->hasMany('App\UnitloadType', 'ULT_Type_OWN_id');
    }
    public function Unitload()
    {
        return $this->hasMany('App\Unitload', 'UL_OWN_id');
    }
    public function Supplier()
    {
        return $this->hasMany('App\Supplier', 'SUP_OWN_id');
    }
    public function StorageLocationType()
    {
        return $this->hasMany('App\StorageLocationType', 'STL_Type_OWN_id');
    }
    public function StorageLocation()
    {
        return $this->hasMany('App\StorageLocation', 'STL_OWN_id');
    }
    public function StorageJob()
    {
        return $this->hasMany('App\StorageJob', 'STJ_OWN_id');
    }
    public function PickJob()
    {
        return $this->hasMany('App\PickJob', 'PJ_OWN_id');
    }
    public function PickRequest()
    {
        return $this->hasMany('App\PickRequest', 'PR_OWN_id');
    }
    public function PickRequestItem()
    {
        return $this->hasMany('App\PickRequestItem', 'PRI_OWN_id');
    }
    public function SKUType()
    {
        return $this->hasMany('App\SKUType', 'SKU_Type_OWN_id');
    }
    public function SKU()
    {
        return $this->hasMany('App\SKU', 'SKU_OWN_id');
    }
    public function Site()
    {
        return $this->hasMany('App\Site', 'SITE_OWN_id');
    }
    public function ReceivingJob()
    {
        return $this->hasMany('App\ReceivingJob', 'RCJ_OWN_id');
    }
    public function Rack()
    {
        return $this->hasMany('App\Rack', 'RACK_OWN_id');
    }
    public function PurchaseOrderItems()
    {
        return $this->hasMany('App\PurchaseOrderItems', 'POI_OWN_id');
    }
    public function PurchaseOrder()
    {
        return $this->hasMany('App\PurchaseOrder', 'PO_OWN_id');
    }
    public function Provider()
    {
        return $this->hasMany('App\Provider', 'PRV_OWN_id');
    }
    public function IssueOrder()
    {
        return $this->hasMany('App\IssueOrder', 'ISO_OWN_id');
    }
    public function IssueItem()
    {
        return $this->hasMany('App\IssueItem', 'ISI_OWN_id');
    }
    public function FunctionalArea()
    {
        return $this->hasMany('App\FunctionalArea', 'FA_OWN_id');
    }
    public function Customer()
    {
        return $this->hasMany('App\Customer', 'CST_OWN_id');
    }
    public function Client()
    {
        return $this->hasMany('App\Client', 'CLI_OWN_id');
    }
    public function Bin()
    {
        return $this->hasMany('App\Bin', 'BIN_OWN_id');
    }
    public function Missmatch()
    {
        return $this->hasMany('App\Mismatch', 'MISSMATCH_OWN_id');
    }

    public function InvitationCode()
    {
        return $this->hasOne('App\InvitationCode', 'INV_OWN_id');
    }
    

    protected $table = 'Owner';
    protected $dates = ['deleted_at'];
}
