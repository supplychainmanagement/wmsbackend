<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitloadType extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'ULT_Type_OWN_id');
    }

    public function Unitload()
    {
        return $this->hasMany('App\Unitload', 'UL_ULT_Type_id');
    }
    public function MismatchFirst()
    {
        return $this->hasMany('App\MismatchFirst', 'MISSMATCH_ULT_Type_id_first');
    }
    public function MismatchSecond()
    {
        return $this->hasMany('App\MismatchSecond', 'MISSMATCH_ULT_Type_id_second');
    }
    public function CapacityUnitload()
    {
        return $this->hasMany('App\CapacityUnitload', 'CU_ULT_Type_id');
    }
    public function CapacityLocation()
    {
        return $this->hasMany('App\CapacityLocation', 'CL_ULT_Type_id');
    }
    protected $table = 'Unitload_Type';
    protected $dates = ['deleted_at'];
}
