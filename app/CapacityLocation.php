<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class CapacityLocation extends ApiModel
{
    use SoftDeletes;
    public function StorageLocationType()
    {
        return $this->belongsTo('App\StorageLocationType', 'CL_STL_Type_id');
    }
    public function UnitloadType()
    {
        return $this->belongsTo('App\UnitloadType', 'CL_ULT_Type_id');
    }
    protected $table = 'Capacity_Location';
    protected $dates = ['deleted_at'];
}
