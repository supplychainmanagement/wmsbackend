<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'PO_OWN_id');
    }
    public function Client()
    {
        return $this->belongsTo('App\Client', 'PO_CLI_id');
    }

    public function Site()
    {
        return $this->belongsTo('App\Site', 'PO_SITE_id');
    }

    public function podItems()
    {
        return $this->hasMany('App\PurchaseOrderItems','POI_PO_id');
    }

    protected $table = 'Purchase_Order';
    protected $dates = ['deleted_at'];
}
