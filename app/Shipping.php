<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    use SoftDeletes;

    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'SHIP_OWN_id');
    }

    public function Site()
    {
        return $this->belongsTo('App\Site', 'SHIP_SITE_id');
    }

    public function IssueItem()
    {
        return $this->belongsTo('App\IssueItem', 'SHIP_ISI_id');
    }

    protected $table = 'Shipping';
    protected $dates = ['deleted_at'];
}
