<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StorageJob extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'STJ_OWN_id');
    }
    public function StorageLocation()
    {
        return $this->belongsTo('App\StorageLocation', 'STJ_STL_id');
    }
    public function Unitload()
    {
        return $this->belongsTo('App\Unitload', 'STJ_UL_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\Site', 'STJ_SITE_id');
    }
    protected $table = 'Storage_Job';
    protected $dates = ['deleted_at'];
}
