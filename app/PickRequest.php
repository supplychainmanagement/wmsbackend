<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PickRequest extends Model
{
    use SoftDeletes;

    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'PR_OWN_id');
    }

    public function Site()
    {
        return $this->belongsTo('App\Site', 'PR_SITE_id');
    }

    public function IssueItem()
    {
        return $this->belongsTo('App\IssueItem', 'PR_ISI_id');
    }

   	public function PickRequestItem()
   	{
   		return $this->hasMany('App\PickRequestItem', 'PRI_PR_id');
   	}

    protected $table = 'Pick_Request';
    protected $dates = ['deleted_at'];
}