<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends ApiModel
{
    use SoftDeletes;

    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'CST_OWN_id');
    }
    public function IssueOrder()
    {
        return $this->hasMany('App\IssueOrder', 'ISO_CST_id');
    }

    public function CLient()
    {
        return $this->belongsToMany('App\Client','Customer_Client','CC_CST_id','CC_CLI_id');
    }

    protected $table = 'Customer';
    protected $dates = ['deleted_at'];
}
