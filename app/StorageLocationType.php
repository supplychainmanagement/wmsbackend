<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StorageLocationType extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'STL_Type_OWN_id');
    }

    public function StorageLocation()
    {
        return $this->hasMany('App\StorageLocation', 'STL_STL_Type_id');
    }
    public function Mismatch()
    {
        return $this->hasMany('App\Mismatch', 'MISSMATCH_STL_Type_id');
    }
    public function CapacityLocation()
    {
        return $this->hasMany('App\CapacityLocation', 'Cl_STL_Type_id');
    }
    protected $table = 'Storage_Location_Type';
    protected $dates = ['deleted_at'];
}
