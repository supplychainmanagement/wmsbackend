<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Strategy extends ApiModel
{
    public function IssueOrderItem()
    {
        return $this->hasMany('App\IssueItem', 'ISI_STRAT_id');
    }

    protected $table = 'Strategy';
}
