<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Mismatch;

class StorageLocation extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'STL_OWN_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\Site', 'STL_SITE_id');
    }
    public function FunctionalArea()
    {
        return $this->belongsTo('App\FunctionalArea', 'STL_FA_id');
    }
    public function StorageLocationType()
    {
        return $this->belongsTo('App\StorageLocationType', 'STL_STL_Type_id');
    }
    public function Rack()
    {
        return $this->belongsTo('App\Rack', 'STL_RACK_id');
    }

    public function Unitload()
    {
        return $this->hasMany('App\Unitload', 'UL_STL_id');
    }
    public function StorageJob()
    {
        return $this->hasMany('App\StorageJob', 'STJ_STL_id');
    }

    public function UnitLoadTypes(){
        $unitloads = $this->Unitload;
        $ulTypes = array();
        foreach ($unitloads as $ul) {
            $unitLoadType = $ul->UnitloadType;

            if(!in_array($unitLoadType, $ulTypes, true)){
                array_push($ulTypes, $unitLoadType);
            }

        }

        return $ulTypes ;

    }
    //
    public function canTakeMoreUL($ul,$amount){

        //check for missmatch.
        //$results = Missmatch::where('CU_SKU_Type_id', $this->SKU->SKUType->id)->where('CU_ULT_Type_id', $this->UnitloadType->id)->get();
        //check for capacity.

        $results = CapacityLocation::where('CL_ULT_Type_id', $ul->UnitloadType->id)->where('CL_STL_Type_id', $this->StorageLocationType->id)->get();
        if ($results->count()==0) return false ;
        $allocation = $results[0]->CL_allocation;
        if(((intval($amount))*intval($allocation))<=100){
            return true;
        }else
            return false ;
    }

    //Fit One more UL // returns the new allocation
    public function fitOneMoreUL($ul){

        //check for missmatch.
        $results = Mismatch::where('MISSMATCH_STL_Type_id', $this->StorageLocationType->id)->get();
        if($results->count()>0){
            $ulTypes = $this->UnitLoadTypes();
            foreach ($ulTypes as $ult) {
                foreach($results as $result){
                    if(($result->UnitloadTypeSecond->id == $ult->id && $result->UnitloadTypeFirst->id == $ul->id)||($result->UnitloadTypeSecond->id ==  $ul->id && $result->UnitloadTypeFirst->id ==$ult->id) ){
                        return null;
                    }
                }
            }
        }
        //check for capacity.
        $results = CapacityLocation::where('CL_ULT_Type_id', $ul->UnitloadType->id)->where('CL_STL_Type_id', $this->StorageLocationType->id)->get();
        if ($results->count()==0) return null ;
        $allocation = $results[0]->CL_allocation;
        if(intval($allocation)<=100){
            return $allocation;
        }else
            return null ;
    }
    protected $table = 'Storage_Location';
    protected $dates = ['deleted_at'];
}
