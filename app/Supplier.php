<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends ApiModel
{
    use SoftDeletes;

    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'SUP_OWN_id');
    }
    public function SKU()
    {
        return $this->hasMany('App\SKU', 'SKU_SUP_id');
    }
    public function SUPSKU()
    {
        return $this->belongsToMany('App\SKU','SUP_SKU','SS_SUP_id','SS_SKU_id');
    }

    protected $table = 'Supplier';
    protected $dates = ['deleted_at'];
}
