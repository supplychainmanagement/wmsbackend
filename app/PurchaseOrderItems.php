<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrderItems extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'POI_OWN_id');
    }
    public function Client()
    {
        return $this->belongsTo('App\Client', 'POV_CLI_id');
    }
    public function SKU()
    {
        return $this->belongsTo('App\SKU', 'POI_SKU_id');
    }
    public function Provider()
    {
        return $this->belongsTo('App\Provider', 'POI_PRV_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\Site', 'POI_SITE_id');
    }
    public function PurchaseOrder()
    {
        return $this->belongsTo('App\PurchaseOrder', 'POI_PO_id');
    }
    public function ReceivingJob()
    {
        return $this->hasMany('App\ReceivingJob', 'RCJ_POI_id');
    }

    public function TotalAmountsubmitted()
    {
        $receivingJobs = $this->ReceivingJob;
        $totalamount = 0 ;
        foreach ($receivingJobs as $rcj) {
            $totalamount = $totalamount + intval($rcj->RCJ_quantity);
        }
        return $totalamount;

    }
    protected $table = 'Purchase_Order_items';
    protected $dates = ['deleted_at'];
}
