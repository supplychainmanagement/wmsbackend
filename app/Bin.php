<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Bin extends ApiModel
{
	use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'BIN_OWN_id');
    }
    public function FunctionalArea()
    {
        return $this->belongsTo('App\FunctionalArea', 'BIN_FA_id');
    }
    public function Zone()
    {
        return $this->belongsTo('App\Zone', 'BIN_ZONE_id');
    }
    protected $table = 'Bin';
    protected $dates = ['deleted_at'];
}
