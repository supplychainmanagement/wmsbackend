<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends ApiModel
{
    use SoftDeletes;

    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'PRV_OWN_id');
    }
    public function PurchaseOrderItems()
    {
        return $this->hasMany('App\PurchaseOrderItems', 'POI_PRV_id');
    }

    public function Client()
    {
        return $this->belongsToMany('App\Client','Provider_Client','PC_PRV_id','PC_CLI_id');
    }

    protected $table = 'Provider';
    protected $dates = ['deleted_at'];
}
