<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceivingJobUnitload extends ApiModel
{
    use SoftDeletes;
    public function SKU()
    {
        return $this->belongsTo('App\SKU', 'RJU_SKU_id');
    }
    public function Unitload()
    {
        return $this->belongsTo('App\Unitload', 'RJU_UL_id');
    }
    public function ReceivingJob()
    {
        return $this->belongsTo('App\ReceivingJob', 'RJU_RCJ_id');
    }
    protected $table = 'Receiving_Job_Unitload';
    protected $dates = ['deleted_at'];
}
