<?php

namespace App;
use Illuminate\Auth\Authenticatable; 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;  
 use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;  
 use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends ApiModel implements AuthenticatableContract, CanResetPasswordContract 
{
    use Authenticatable,Authorizable,CanResetPassword;

    use SoftDeletes;
    protected $table = 'users';
    protected $fillable = ['USR_id','USR_name', 'USR_email', 'password','USR_CLI_id','USR_SITE_id'];
    protected $hidden = ['remember_token'];
    public function Client()
    {
        return $this->belongsTo('App\Client', 'USR_CLI_id');
    }
    public function Role()
    {
        return $this->belongsToMany('App\Role','Users_Role','UR_USR_id','UR_ROLE_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\Site','USR_SITE_id');
    }
    public function isSystemClient()
    {
        $checkSystemClient = $this->client->CLI_systemClient;
        if($checkSystemClient == null)
            return false;
        else if($checkSystemClient== 1)
            return true;
           
    }
    public function isSiteAdmin()
    {
        $roles = $this->role;
        $checkrole = false;
        foreach($roles as $role)
        {
            if($role->ROLE_name == 'SiteAdmin')
                $checkrole = true;
        }
        return $checkrole;
    }
     public function isSystemAdmin()
    {
        $roles = $this->role;
        $checkrole = false;
        foreach($roles as $role)
        {
            if($role->ROLE_name == 'SystemAdmin')
                $checkrole = true;
        }
        return $checkrole;
    }
    public function isCheifOfLabours()
    {
        $roles = $this->role;
        $checkrole = false;
        foreach($roles as $role)
        {
            if($role->ROLE_name == 'CheifOfLabours')
                $checkrole = true;
        }
        return $checkrole;
    }
    public function isLabour()
    {
        $roles = $this->role;
        $checkrole = false;
        foreach($roles as $role)
        {
            if($role->ROLE_name == 'Labour')
                $checkrole = true;
        }
        return $checkrole;
    }
    protected $dates = ['deleted_at'];
}
