<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Response;
use Closure;
use Crypt;
class Decrypt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $encryptedValue = $request->header('Authorization');

        $payload = json_decode(base64_decode($encryptedValue), true);
        if($this->invalidPayload($payload)){
            return Response::json("payload is invalid",401);
        }

        $decryptedValue = Crypt::decrypt($encryptedValue);
        $decodedValue = base64_decode($decryptedValue);
        $pieces = explode(":", $decodedValue);
        $request->headers->set('PHP_AUTH_USER',$pieces[0]);
        $request->headers->set('PHP_AUTH_PW',$pieces[1]);

        return $next($request);
    }

    private function invalidPayload($data)
    {
        return ! is_array($data) || ! isset($data['iv']) || ! isset($data['value']) || ! isset($data['mac']);
    }
}
