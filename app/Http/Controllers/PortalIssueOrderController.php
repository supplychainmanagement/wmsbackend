<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use DB;
use App\Strategy;
use App\Client;
use App\Site;
use App\Customer;
use App\IssueOrder;
use App\IssueItem;
use App\SKU;

class PortalIssueOrderController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function getDataForIssueOrder()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }
        
        //$skus = $owner->SKU;
        $clients = $owner->Client;
        $sites = $owner->Site;
        $strategies = Strategy::where('STRAT_order',1)->get();
        $Data = array();
        // $sku_names = array();
        // $counter = 0;
        // foreach ($skus as $key => $sku) {
        //     $sku_names[$counter] = $sku->SKU_name;
        //     $counter++;
        // }

        $client_names = array();
        $counter = 0;
        foreach ($clients as $key => $client) {
            $client_names[$counter] = $client->CLI_name;
            $counter++;
        }

        $site_names = array();
        $counter = 0;
        foreach ($sites as $key => $site) {
            $site_names[$counter] = $site->SITE_name;
            $counter++;
        }

        $strategy_names = array();
        $counter = 0;
        foreach ($strategies as $key => $strategy) {
            $strategy_names[$counter] = $strategy->STRAT_name;
            $counter++;
        }

        //$Data['sku_names'] = $sku_names;
        $Data['client_names'] = $client_names;
        $Data['site_names'] = $site_names;
        $Data['strategy_names'] = $strategy_names;

        return $this->respond($Data);
    }

    public function getCustomersofClient(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'CLI_name'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
        if($client == null)
            return $this->badRequest(['Client Does not Exist']);

        $customers = $client->CLICustomer;
        $customer_names = array();
        $counter = 0;
        foreach ($customers as $key => $customer) {
            $customer_names[$counter] = $customer->CST_name;
            $counter++;
        }

        return $this->respond($customer_names); 
    }

    public function createIssueOrder(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        if($user->isSystemAdmin())
        {
            if(!$request->has('SITE_name'))
                return $this->badRequest(['Site is Missing']);
            else
            {
                $site_id = Site::getIdByName('SITE',$request->SITE_name);
                if($site_id == -1)
                    return $this->badRequest(['Site does not Exist']);
            }

        }
        else
            $site_id = $user->Site->id; 

        if($user->isSystemClient())
        {
            if (!$request->has('CLI_name')) {
                return $this->badRequest(['Client is Missing']);
            }
            else
            {
                $client_id = Client::getIdByName('CLI',$request->CLI_name);
                if($client_id == -1)
                    return $this->badRequest(['Client does not Exist']);
            }

        }
        else
            $client_id = $user->Client->id;

        $issueorder = new IssueOrder();
        $issueorder->ISO_number = 'ISO_'.((IssueOrder::where('ISO_OWN_id',$owner->id)->count())+1); 
        $issueorder->ISO_status = 'new';
        $issueorder->ISO_CLI_id = $client_id;
        $issueorder->ISO_OWN_id = $owner->id;
        $issueorder->ISO_SITE_id = $site_id;

        if($request->has('CST_name'))
        {
            $customer_id = Customer::getIdByName('CST',$request->CST_name);
            if($customer_id == -1)
                return $this->badRequest(['Customer does not Exist']);
            $issueorder->ISO_CST_id = $customer_id;
        }        

        if($issueorder->save())
        {
            return $this->resourceCreated([$issueorder->ISO_number]);
        }else{
            return $this->internalError(['Issue Order Creation has Failed']);
        }
    }

    public function createIssueOrderItem(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        $v = Validator::make($request->all(),[
            'ISO_number'=>'required',
            'ISI_amount'=>'required|numeric|integer',
            'SKU_name'=>'required',
            'ISI_shippingDate'=>'date_format:"Y-m-d"'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        if($request->ISI_amount <=0)
            return $this->badRequest(['Amount is Invalid']);

        $today = Carbon::now()->toDateString();
        if($request->ISI_shippingDate < $today)
        {
            return $this->badRequest(['Shipping Date is Invalid']);
        }        

        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }


        $issueorder = IssueOrder::where('ISO_OWN_id',$owner->id)->where('ISO_number',$request['ISO_number'])->first();
        if($issueorder == null)
            return $this->badRequest(['Issue Order Does not Exist']);

        $sku_id = SKU::getIdByName('SKU',$request->SKU_name);
        if($sku_id == -1)
            return $this->badRequest(['SKU does not Exist']);
        
        $issueorderitem = new IssueItem();
        $issueorderitem->ISI_number = $issueorder->ISO_number.'_'.(($issueorder->IssueItem->count())+1);
        $issueorderitem->ISI_amount = $request->ISI_amount;

        if($request->has('STRAT_name'))
        {
            $strategy = Strategy::where('STRAT_order',1)->where('STRAT_name',$request['STRAT_name'])->first();
            if($strategy == null)
                return $this->badRequest(['Strategy Does not Exist']);
            $issueorderitem->ISI_STRAT_id = $strategy->id;
        }

        if ($request->has('ISI_shippingDate'))
        {
            $issueorderitem->ISI_shippingDate = $request->ISI_shippingDate;
        }
        $issueorderitem->ISI_picked = 0;
        $issueorderitem->ISI_shipped = 0;
        $issueorderitem->ISI_ISO_id = $issueorder->id;
        $issueorderitem->ISI_SKU_id = $sku_id;
        $issueorderitem->ISI_OWN_id = $owner->id;
        $issueorderitem->ISI_SITE_id = $issueorder->Site->id;
        $issueorderitem->ISI_state = 'new';

        if($issueorderitem->save())
        {
            return $this->resourceCreated([$issueorderitem]);
        }else{
            return $this->internalError(['Issue Order Item Creation has Failed']);
        }
    }

    public function getAllIssueOrders()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $issueorders = $owner->IssueOrder;
        $issueorder_names = array();
        $counter = 0;
        foreach ($issueorders as $key => $issueorder) {
            $issueorder_names[$counter] = $issueorder->ISO_number;
            $counter++;
        }
        return $issueorder_names;
    }

    public function getIssueOrderByName(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'ISO_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $issueorder = IssueOrder::where('ISO_OWN_id',$owner->id)->where('ISO_number',$request['ISO_number'])->first();
        if($issueorder == null)
            return $this->badRequest(['Issue Order Does not Exist']);

        $issueorderitems = $issueorder->IssueItem;
        $issueorderitems_data = array();
        $counter = 0;
        if($request->has('type') && $request->type = 'shipping')
        {
            foreach ($issueorderitems as $key => $issueorderitem) {
                if($issueorderitem->ISI_amount != $issueorderitem->ISI_shipped && $issueorderitem->ISI_amount <= $issueorderitem->ISI_picked)
                {
                    $issueorderitems_data[$counter]['issueitem'] = $issueorderitem;
                    $issueorderitems_data[$counter]['SKU_name']= $issueorderitem->SKU->SKU_name;
                    $issueorderitems_data[$counter]['SKU_number']= $issueorderitem->SKU->SKU_item_no;
                    // $issueorderitems_data[$counter]['STRAT_name'] = $issueorderitem->Strategy->STRAT_name;
                    $counter++;
                }            
            }
        }
        elseif($request->has('pdf'))
        {
            foreach ($issueorderitems as $key => $issueorderitem) {
                    $issueorderitems_data[$counter]['issueitem'] = $issueorderitem;
                    $issueorderitems_data[$counter]['SKU_name']= $issueorderitem->SKU->SKU_name;
                    $issueorderitems_data[$counter]['SKU_number']= $issueorderitem->SKU->SKU_item_no;
                    // $issueorderitems_data[$counter]['STRAT_name'] = $issueorderitem->Strategy->STRAT_name;
                    $counter++;
            }
        }
        else
        {
            foreach ($issueorderitems as $key => $issueorderitem) {
                if($issueorderitem->ISI_state == 'new')
                {
                    $issueorderitems_data[$counter]['issueitem'] = $issueorderitem;
                    $issueorderitems_data[$counter]['SKU_name']= $issueorderitem->SKU->SKU_name;
                    $issueorderitems_data[$counter]['SKU_number']= $issueorderitem->SKU->SKU_item_no;
                    // $issueorderitems_data[$counter]['STRAT_name'] = $issueorderitem->Strategy->STRAT_name;
                    $counter++;
                }            
            }
        }

        $Data = array();
        $Data['issueorder'] = $issueorder;
        $Data['CLI_name'] = $issueorder->Client->CLI_name;
        $Data['CST_name'] = $issueorder->Customer->CST_name;
        $Data['Customer'] = $issueorder->Customer;
        $Data['SITE_name'] = $issueorder->Site->SITE_name;
        $Data['issueorderitems_data'] = $issueorderitems_data;

        return $this->respond($Data);
    }

    public function getIssueOrderItemByName(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'ISI_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $issueorderitem = IssueItem::where('ISI_OWN_id',$owner->id)->where('ISI_number',$request['ISI_number'])->first();
        if($issueorderitem == null)
            return $this->badRequest(['Issue Order Item Does not Exist']);

        $Data = array();
        $Data['ISI_amount'] = $issueorderitem->ISI_amount;
        $Data['STRAT_name'] = $issueorderitem->Strategy;
        $Data['ISI_picked'] = $issueorderitem->ISI_picked;
        $Data['SKU_name'] = $issueorderitem->SKU->SKU_name;        
        $Data['ISI_shippingDate'] = $issueorderitem->ISI_shippingDate;

        return $this->respond($Data);
    }


    public function issueOrderUpdate(Request $request)
    {$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'ISO_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());
        $issueorder = IssueOrder::where('ISO_OWN_id',$owner->id)->where('ISO_number',$request['ISO_number'])->first();
        if($issueorder == null)
            return $this->badRequest(['Issue Order Item Does not Exist']);

         if($user->isSystemAdmin())
        {
            if(!$request->has('SITE_name'))
                return $this->badRequest(['Site is Missing']);
            else
            {
                $site_id = Site::getIdByName('SITE',$request->SITE_name);
                if($site_id == -1)
                    return $this->badRequest(['Site does not Exist']);
                $issueorder->ISO_SITE_id = $site_id;
            }

        } 

        if($user->isSystemClient())
        {
            if (!$request->has('CLI_name')) {
                return $this->badRequest(['Client is Missing']);
            }
            else
            {
                $client_id = Client::getIdByName('CLI',$request->CLI_name);
                if($client_id == -1)
                    return $this->badRequest(['Client does not Exist']);
                 $issueorder->ISO_CLI_id = $client_id;
            }

        }

        if($request->has('CST_name'))
        {
            $customer_id = Customer::getIdByName('CST',$request->CST_name);
            if($customer_id == -1)
                return $this->badRequest(['Customer does not Exist']);
            $issueorder->ISO_CST_id = $customer_id;
        }


        if($issueorder->save())
        {
            return $this->respond([$issueorder->ISO_number]);
        }else{
            return $this->internalError(['Issue Order Creation has Failed']);
        }
    }

    public function issueOrderItemUpdate(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'id' => 'required|exists:Issue_Order,id',
            'ISO_number'=>'required',
            'ISI_amount'=>'required|numeric|integer',
            'SKU_name'=>'required',
            'ISI_shippingDate'=>'date_format:"Y-m-d"'
        ]);

        $id = intval($request['id']);
        $issueorderitem = IssueItem::findOrFail($id);
        if($v->fails()) return $this->badRequest($v->errors()->all());


        if($request->ISI_amount <=0)
            return $this->badRequest(['Amount is Invalid']);

        $today = Carbon::now()->toDateString();
        if($request->ISI_shippingDate < $today)
        {
            return $this->badRequest(['Shipping Date is Invalid']);
        }        


        $issueorder = IssueOrder::where('ISO_OWN_id',$owner->id)->where('ISO_number',$request['ISO_number'])->first();
        if($issueorder == null)
            return $this->badRequest(['Issue Order Does not Exist']);

        $sku_id = SKU::getIdByName('SKU',$request->SKU_name);
        if($sku_id == -1)
            return $this->badRequest(['SKU does not Exist']);

        $issueorderitem->ISI_amount = $request->ISI_amount;

        if($request->has('STRAT_name'))
        {
            $strategy = Strategy::where('STRAT_order',1)->where('STRAT_name',$request['STRAT_name'])->first();
            if($strategy == null)
                return $this->badRequest(['Strategy Does not Exist']);
            $issueorderitem->ISI_STRAT_id = $strategy->id;
        }

        if ($request->has('ISI_shippingDate'))
        {
            $issueorderitem->ISI_shippingDate = $request->ISI_shippingDate;
        }
        $issueorderitem->ISI_ISO_id = $issueorder->id;
        $issueorderitem->ISI_SKU_id = $sku_id;

        if($issueorderitem->save())
        {
            return $this->respond(['Issue Order Item Successfully Updated']);
        }else{
            return $this->internalError(['Issue Order Item Creation has Failed']);
        }
    }

    public function issueOrderIndex()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if($user->isSystemAdmin())
        {
            $issueorders = $owner->IssueOrder;
        }
        elseif(!$user->isSystemAdmin())
        {
            $issueorders = $user->site->IssueOrder;
        }
        
        $issueorders_array = array();
        $counter = 0;
        foreach ($issueorders as $key => $issueorder) {
            $issueorders_array[$counter]['issueorder'] = $issueorder;
            $issueorders_array[$counter]['client'] = $issueorder->Client;
            if($issueorder->Client != null)
                $issueorders_array[$counter]['client'] = $issueorder->Client->CLI_name;
            else
                $issueorders_array[$counter]['client'] = 'not set';
            if($issueorder->Customer != null)
                $issueorders_array[$counter]['customer'] = $issueorder->Customer->CST_name;
            else
                $issueorders_array[$counter]['customer'] = 'not set';
            if($issueorder->Site != null)
                $issueorders_array[$counter]['site'] = $issueorder->Site->SITE_name;
            else
                $issueorders_array[$counter]['site'] = 'not set';
            $counter++;
        }

        
        return $this->respond($issueorders_array);
    }

    public function issueOrderItemIndex()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
         if($user->isSystemAdmin())
        {
            $issueitems = $owner->IssueItem;
        }
        elseif(!$user->isSystemAdmin())
        {
            $issueitems = $user->site->IssueItem;
        }
        
        $issueitems_array = array();
        $counter = 0;
        foreach ($issueitems as $key => $issueitem) {
            $issueitems_array[$counter]['issueitem'] = $issueitem;
            if($issueitem->SKU != null)
                $issueitems_array[$counter]['sku'] = $issueitem->SKU->SKU_name;
            else
                $issueitems_array[$counter]['sku'] = 'not set';
            if($issueitem->IssueOrder != null)
                $issueitems_array[$counter]['issueorder'] = $issueitem->IssueOrder->ISO_number;
            else
                $issueitems_array[$counter]['issueorder'] = 'not set';
            if($issueitem->Strategy != null)
                $issueitems_array[$counter]['strategy'] = $issueitem->Strategy->STRAT_name;
            else
                $issueitems_array[$counter]['strategy'] = 'not set';
            if($issueitem->Site != null)
                $issueitems_array[$counter]['site'] = $issueitem->Site->SITE_name;
            else
                $issueitems_array[$counter]['site'] = 'not set';
            $counter++;
        }
        return $this->respond($issueitems_array);
    }
}

