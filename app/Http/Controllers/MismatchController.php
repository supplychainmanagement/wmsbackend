<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\CapacityLocation;
use App\UnitloadType;
use App\StorageLocationType;
use App\Mismatch;

class MismatchController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$MismatchData =array();
    	$mismatchdata =array();
        if($user->isSystemClient()){
			$MismatchData['systemClient']=true;
			$missmatches = $owner->Missmatch;
			$counter =0;
    		foreach ($missmatches as $key => $missmatch) {
    			$mismatchdata[$counter]['missmatch'] = $missmatch;
    			//return $value;
    			$mismatchdata[$counter]['MISSMATCH_ULT_Type_first'] = 
    				UnitloadType::where('id',$missmatch['MISSMATCH_ULT_Type_id_first'])->first()->ULT_Type_name;
				$mismatchdata[$counter]['MISSMATCH_ULT_Type_second'] = 
				UnitloadType::where('id',$missmatch['MISSMATCH_ULT_Type_id_second'])->first()->ULT_Type_name;
    			$mismatchdata[$counter]['MISSMATCH_STL_Type'] = StorageLocationType::where('id',$missmatch['MISSMATCH_STL_Type_id'])->first()->STL_Type_name;
    			$counter++;
    		}
        	$MismatchData['Missmatch']= $mismatchdata;
        	$MismatchData['ULT'] = $owner->unitloadtype;
        	$MismatchData['STLT'] = $owner->storagelocationtype;
        	if($user->isSystemAdmin() || $user->isSiteAdmin())
        	{
        		
        		$MismatchData['permission'] = true;
				$MismatchData['siteBased']=false;
				if($user->isSystemAdmin())
					$MismatchData['role'] = "systemAdmin";
				elseif($user->isSiteAdmin())
					$MismatchData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$MismatchData['permission'] = true;
				$MismatchData['siteBased'] = true;
				$MismatchData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$MismatchData['permission'] = false;
				$MismatchData['siteBased'] = true;
				$MismatchData['role'] ="labour";
			}
        }
       	 	//if(sizeof($MismatchData['Missmatch']) > 0){
                return $this->respond($MismatchData);
            // }else{
            //     return $this->respondNoContent();
            // }
    }

    /**
	* Create new Capacity Location
	* @param Attributes of Capacity Location to be Created
	* @return Approval of Creation
	*/

    public function store(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'MISSMATCH_ULT_Type_name_first'=>'required|exists:Unitload_Type,ULT_Type_name',
			'MISSMATCH_ULT_Type_name_second'=>'required|exists:Unitload_Type,ULT_Type_name','MISSMATCH_STL_Type_name'=>'required|exists:Storage_Location_Type,STL_Type_name'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$unitloadTypeNameFirst = $request['MISSMATCH_ULT_Type_name_first'];
		$unitloadTypeNameSecond = $request['MISSMATCH_ULT_Type_name_second'];
		$storageLocationTypeName = $request['MISSMATCH_STL_Type_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Missmatch rule"]);
		else
		{
			$owner = $user->client->owner;
			$unitloadtypefirst =UnitloadType::where('ULT_Type_name',$unitloadTypeNameFirst)->where('ULT_Type_OWN_id',$owner->id)->first();
			$unitloadtypesecond =UnitloadType::where('ULT_Type_name',$unitloadTypeNameSecond)->where('ULT_Type_OWN_id',$owner->id)->first();
			$storagelocationtype=StorageLocationType::where('STL_Type_name',$storageLocationTypeName)->where('STL_Type_OWN_id',$owner->id)->first();
			if($storagelocationtype == null || $unitloadtypefirst == null || $unitloadtypesecond == null)
				return $this->badRequest(["This Unitloadtype or this Storage Location Type doesn't Exists"]);
			if(!$this->isUniqueness($unitloadTypeNameFirst,$unitloadTypeNameSecond,$storageLocationTypeName, $owner->id,0))return $this->badRequest(['Missmatch rule Exists']);

			$missmatch = new Mismatch();
			$missmatch->MISSMATCH_ULT_Type_id_first = $unitloadtypefirst->id;
			$missmatch->MISSMATCH_STL_Type_id = $storagelocationtype->id;
			$missmatch->MISSMATCH_OWN_id = $owner->id;
			$missmatch->MISSMATCH_ULT_Type_id_second = $unitloadtypesecond->id;
			if($missmatch->save())
			{
				return $this->resourceCreated(['Missmatch rule Successfully Created']);
			}
			else{
				return $this->internalError(['Missmatch rule Creation has Failed']);
			}
		}
	}

	public function update(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'id' => 'required|exists:Missmatch,id'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create missmatch rule"]);
		else
		{
			$unitloadtypenamefirst = $request['ULT_Type_name_first'];
			$unitloadtypenamesecond = $request['ULT_Type_name_second'];
			$storagelocationtypename = $request['STL_Type_name'];

			$missmatch = Mismatch::findOrFail($id);
			$unitloadtypefirst =UnitloadType::where('ULT_Type_name',$unitloadtypenamefirst)->where('ULT_Type_OWN_id',$owner_id)->first();
			$unitloadtypesecond =UnitloadType::where('ULT_Type_name',$unitloadtypenamesecond)->where('ULT_Type_OWN_id',$owner_id)->first();
			if($unitloadtypefirst->id == $unitloadtypesecond->id)
				return $this->badRequest(["you cant make a missmatch rule between the same unitloadtype"]);
			$storagelocationtype=StorageLocationType::where('STL_Type_name',$storagelocationtypename)->where('STL_Type_OWN_id',$owner_id)->first();
			if($storagelocationtype == null || $unitloadtypefirst == null || $unitloadtypesecond == null)
				return $this->badRequest(["This Unitloadtype or this Storage Location Type doesn't Exists"]);


			$missmatch->MISSMATCH_ULT_Type_id_first = $unitloadtypefirst->id;
			$missmatch->MISSMATCH_STL_Type_id = $storagelocationtype->id;
			$missmatch->MISSMATCH_OWN_id = $owner_id;
			$missmatch->MISSMATCH_ULT_Type_id_second = $unitloadtypefirst->id;

			if(!$this->isUniqueness($unitloadtypenamefirst,$unitloadtypenamesecond,$storagelocationtypename,$owner_id,$id))return $this->badRequest(['missmatch rule Already Exists']);

			if($missmatch->save())
			{
				return $this->respond(['missmatch rule Successfully Updated']);
			}else{
				return $this->internalError(['missmatch rule Location Update Failed']);
			}
		}
	}


	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Missmatch,id'
		]);

		$user = \Illuminate\Support\Facades\Request::user();

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Mismatch rule"]);
		else
		{
			if($v->fails())return $this->badRequest($v->errors()->all());
			$id = intval($request['id']);
			$missmatch = Mismatch::findOrFail($id);
			if($missmatch->delete())
			{
				return $this->respond(['Mismatch rule  Deleted Succesfully']);
			}else{
				return $this->internalError(['Mismatch rule Delete Failed']);
			}
		}
	}


	public function isUniqueness($unitloadTypeNameFirst,$unitloadTypeNameSecond,$storageLocationTypeName,$owner_id,$id)
	{
		$field = array('id'=>$id);
		$ult1_id = UnitloadType::where('ULT_Type_name',$unitloadTypeNameFirst)->where('ULT_Type_OWN_id',$owner_id)->first()->id;
		$ult2_id = UnitloadType::where('ULT_Type_name',$unitloadTypeNameSecond)->where('ULT_Type_OWN_id',$owner_id)->first()->id;
		$stlt_id =StorageLocationType::where('STL_Type_name',$storageLocationTypeName)
		->where('STL_Type_OWN_id',$owner_id)->first()->id;
		$missmatch = Mismatch::where('MISSMATCH_ULT_Type_id_first',$ult1_id)
		->where('MISSMATCH_ULT_Type_id_second',$ult2_id)
		->where('MISSMATCH_STL_Type_id',$stlt_id)->whereNotIn('id',$field)->first();
		if($missmatch!=null)return false;
		else return true;
	}

}
