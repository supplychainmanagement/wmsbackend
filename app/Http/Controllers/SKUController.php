<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\SKUType;
use App\SKU;
use App\Client;
use App\Supplier;

class SKUController extends ApiController
{

	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$SKUData =array();
        if($user->isSystemClient()){
			$SKUData['systemClient']=true;
        	if($user->isSystemAdmin() || $user->isSiteAdmin())
        	{
        		$skus = $owner->SKU;
        		$skudata = array();
        		$counter = 0;
        		foreach ($skus as $key =>$sku) {
        			$client_name = $sku->Client->CLI_name;
        			$skuType_name = $sku->SKUType->SKU_Type_name;
        			$skudata[$counter]['sku'] = $sku;
        			$skudata[$counter]['client_name'] = $client_name;
        			$skudata[$counter]['skuType_name'] = $skuType_name;
        			$counter++;
        			
        		}
        		$SKUData['skus']=$skudata;
        		$all_clients = $owner->Client;
        		$all_skuTypes = $owner->SKUType;
        		$all_suppliers = $owner->Supplier;
        		$SKUData['all_clients']=$all_clients->all();
        		$SKUData['all_skuTypes']=$all_skuTypes->all();
        		$SKUData['all_suppliers']=$all_suppliers->all();
        		$SKUData['permission'] = true;
        	}
        	else
			{
        		$skus = $owner->SKU;
        		$skudata = array();
        		$counter = 0;
        		foreach ($skus as $key =>$sku) {
        			$client_name = $sku->Client->CLI_name;
        			$skuType_name = $sku->SKUType->SKU_Type_name;
        			$skudata[$counter]['sku'] = $sku;
        			$skudata[$counter]['client_name'] = $client_name;
        			$skudata[$counter]['skuType_name'] = $skuType_name;
        			$counter++;
        			
        		}
        		$SKUData['skus']=$skudata;
        		$SKUData['all_clients']=array();
        		$SKUData['all_skuTypes']=array();
        		$SKUData['all_suppliers']=array();
        		$SKUData['permission'] = false;
			}
        }
        
       	 	return $this->respond($SKUData);
            
    }


    public function create(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'SKU_name'=>'required',
			'SKU_SKU_Type'=>'required',
			'SKU_CLI'=>'required',
			'SKU_item_no'=>'required|integer|min:1',
			'SKU_stackingNumber'=>'integer|min:1',
			'SKU_lifeTime'=>'integer|min:1'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$owner = $user->client->owner;

		if(!SKU::isUniqueName('SKU','client',$request->SKU_name,0))return $this->badRequest(['SKU name already Exists']);
		$skuType_id = SKUType::getIdByName('SKU_Type',$request->SKU_SKU_Type);
		if($skuType_id == -1)
			return $this->badRequest(['SKU Type does not Exist']);
		$client_id = Client::getIdByName('CLI',$request->SKU_CLI);
		if($client_id == -1)
			return $this->badRequest(['Client does not Exist']);

		$sku = new SKU();
		$sku->SKU_name = $request->SKU_name;
		$sku->SKU_item_no = $request->SKU_item_no;
		$sku->SKU_lifeTime = $request->SKU_lifeTime;
		$sku->SKU_stackingNumber = $request->SKU_stackingNumber;
		$sku->SKU_SKU_Type_id = $skuType_id;
		$sku->SKU_CLI_id = $client_id;
		$sku->SKU_OWN_id = $owner->id;	

		if($sku->save())
		{
			return $this->resourceCreated(['SKU Successfully Created']);
		}else{
			return $this->internalError(['SKU Creation has Failed']);
		}

		if($request->SKU_suppliers != null)
		{
			$suppliers = $request->SKU_suppliers;
			foreach ($suppliers as $key => $value) {
				$supplier_id= Supplier::getIdByName('SUP',$value);
				if($supplier_id == -1)
				{
					return $this->badRequest(['One of the Suppliers does not Exist']);
				}
				$sku->SKUSupplier()->attach($supplier_id);
			}
		}
    }

    public function prop(Request $request)
    {
    	$v = Validator::make(['id'=> $request['id']],[
			'id' => 'integer|exists:SKU,id'
		]);

		if($v->fails()) return $this->badRequest($v->errors()->all());
		$id = intval($request['id']);

		$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;
		$sku = SKU::findOrFail($id);
		$permission = false;
		$attached = array();
		$respond = array();
		if($user->isSystemAdmin())
		{
			$permission = true;
			$respond['all_clients'] = $owner->Client->all();
			$respond['all_sku_types'] = $owner->SKUType->all();
			$respond['all_suppliers'] = $owner->Supplier->all();

		}else
        {
            $permission = false;
          	$respond['all_clients'] = $owner->Client->all();
			$respond['all_sku_types'] = $owner->SKUType->all();
			$respond['all_suppliers'] = $owner->Supplier->all();
        }
		$respond['permission'] = $permission;
		$respond['sku'] = $sku;
		$attachedSuppliers = $sku->SKUSupplier;
		foreach($attachedSuppliers as $attachedSupplier)
		{
			array_push($attached,$attachedSupplier['SUP_name']);
		}
		$respond['attached'] = $attached;
		$respond['sku_client'] = $sku->Client->CLI_name;
		$respond['sku_sku_type'] = $sku->SKUType->SKU_Type_name;
		$respond['sku_suppliers'] = null;
		//if($sku->Supplier!=null)$respond['sku_suppliers']=$sku->Supplier->SUP_name;

		json_encode($respond);
		return $this->respond($respond);
    }

    public function update(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'id' => 'required|exists:SKU,id',
			'SKU_name'=>'required',
			'SKU_SKU_Type'=>'required',
			'SKU_CLI'=>'required',
			'SKU_item_no'=>'required|integer|min:1',
			'SKU_stackingNumber'=>'integer|min:1',
			'SKU_lifeTime'=>'integer|min:1'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());

		$owner_id = $user->client->owner->id;

		if(!SKU::isUniqueName('SKU','client',$request->SKU_name,$id))return $this->badRequest(['SKU name already Exists']);

		$skuType_id = SKUType::getIdByName('SKU_Type',$request->SKU_SKU_Type);
		if($skuType_id == -1)
			return $this->badRequest(['SKU Type does not Exist']);
		$client_id = Client::getIdByName('CLI',$request->SKU_CLI);
		if($client_id == -1)
			return $this->badRequest(['Client does not Exist']);

		$sku = SKU::findOrFail($id);
		$sku->SKU_name = $request->SKU_name;
		$sku->SKU_item_no = $request->SKU_item_no;
		$sku->SKU_lifeTime = $request->SKU_lifeTime;
		$sku->SKU_stackingNumber = $request->SKU_stackingNumber;
		$sku->SKU_SKU_Type_id = $skuType_id;
		$sku->SKU_CLI_id = $client_id;
		$sku->SKU_OWN_id = $owner_id;
		$sku->SKUSupplier()->detach();

		if($sku->save())
		{
			if($request->SKU_suppliers != null)
			{
				$suppliers = $request->SKU_suppliers;
				foreach ($suppliers as $key => $value) {
					$supplier_id= Supplier::getIdByName('SUP',$value);
					if($supplier_id == -1)
					{
						return $this->badRequest(['One of the Suppliers does not Exist']);
					}
					$sku->SKUSupplier()->attach($supplier_id);
				}
			}
			return $this->respond(['SKU Successfully Updated']);
		}else{
			return $this->internalError(['SKU Update Failed']);
		}

    }

    public function delete(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:SKU,id'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());
		$id = intval($request['id']);
		$sku = SKU::findOrFail($id);

		$unitloads = $sku->Unitload;
		if(count($unitloads) > 0)
		{
			return $this->badRequest(['SKU selected is assotiated with unitloads']);
		}

		if($sku->delete())
		{
			return $this->respond(['SKU Deleted Succesfully']);
		}else{
			return $this->internalError(['SKU Delete Failed']);
		}
    }


}
