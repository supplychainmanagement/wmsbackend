<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use DB;
use App\PurchaseOrderItems;
use App\ReceivingJob;
use App\Unitload;
use App\CapacityUnitload;
use App\SKU;
use App\ReceivingJobUnitload;
use Carbon\Carbon;

class PortalReceivingJobController extends ApiController
{
	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function generateReceivingJobName()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$receivingjob_name = 'RCJ_'.((ReceivingJob::where('RCJ_OWN_id',$owner->id)->count())+1); 

        $time_start = microtime(true) * 10000;
        $receivingjob_name = $receivingjob_name.'_'. $time_start;
        return $this->respond([$receivingjob_name]);
    }

    public function createReceivingJob(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'POI_number'=>'required',
			'RCJ_amount'=>'required|numeric|integer'
		]);

    	if($v->fails())return $this->badRequest($v->errors()->all());

    	$purchaseorderitem = PurchaseOrderItems::where('POI_OWN_id',$owner->id)->where('POI_number',$request['POI_number'])->first();
		if($purchaseorderitem == null)
			return $this->badRequest(['Purchase Order Item Does not Exist']);

    	if($request->RCJ_amount <=0 || $request->RCJ_amount > $purchaseorderitem->POI_amount)
			return $this->badRequest(['Amount is Invalid']);

		$receivingjob = new ReceivingJob();
		$receivingjob->RCJ_number = 'RCJ_'.((ReceivingJob::where('RCJ_OWN_id',$owner->id)->count())+1); 
		$receivingjob->RCJ_amount = $request->RCJ_amount;
		$receivingjob->RCJ_POI_id = $purchaseorderitem->id;
		$receivingjob->RCJ_OWN_id = $owner->id;
		$receivingjob->RCJ_SITE_id = $purchaseorderitem->Site->id;

		if($receivingjob->save())
		{
			$purchaseorderitem->POI_receivedamount = ($purchaseorderitem->POI_receivedamount)
				+ ($receivingjob->RCJ_amount);

			if($purchaseorderitem->POI_receivedamount == $purchaseorderitem->POI_amount)
				$purchaseorderitem->POI_state = 'fully_received';
			else
				$purchaseorderitem->POI_state = 'partially_received';

			return $this->resourceCreated(['Receiving Job Successfully Created']);
		}else{
			return $this->internalError(['Receiving Job Creation has Failed']);
		}
    }

    public function getReceivingJobByName(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'RCJ_number'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$receivingjob = ReceivingJob::where('RCJ_OWN_id',$owner->id)->where('RCJ_number',$request['RCJ_number'])->first();
		if($receivingjob == null)
			return $this->badRequest(['Receiving Job Does not Exist']);

    	$Data = array();
    	$Data['RCJ_amount'] = $receivingjob->RCJ_amount;
    	$Data['SKU_name'] = $receivingjob->PurchaseOrderItems->SKU->SKU_name;

    	return $this->respond($Data);    	
    }

    public function getMatchedUnitloads(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'POI_number'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$purchaseorderitem = PurchaseOrderItems::where('POI_OWN_id',$owner->id)->where('POI_number',$request['POI_number'])->first();
		if($purchaseorderitem == null)
			return $this->badRequest(['Purchase Order Item Does not Exist']);

		$matchedunitloads_names = array();
		if($purchaseorderitem->POI_productiondate != null)
		{
			$lifetime = intval($purchaseorderitem->SKU->SKU_lifeTime);
			$expiryDate= Carbon::parse($purchaseorderitem->POI_productiondate)->addDays($lifetime);
			$matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
				$purchaseorderitem->Site->id)->where('UL_SKU_id',$purchaseorderitem->SKU->id)->Where(function ($query) use ($expiryDate) {
		                $query->where('UL_expiryDate', $expiryDate)
                      ->orwhereNull('UL_expiryDate')->orwhere('UL_expiryDate','');
            		})->get();

		}
		else
		{
			$matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
				$purchaseorderitem->Site->id)->where('UL_SKU_id',$purchaseorderitem->SKU->id)->Where(function ($query) {
		                $query->whereNull('UL_expiryDate')->orwhere('UL_expiryDate','');
            		})->get();
		}
		if(count($matchedunitloads) > 0)
			{
				$counter = 0;
				foreach ($matchedunitloads as $key => $matchedunitload) {
					if($matchedunitload->StorageLocation->FunctionalArea->FA_GoodsIn 
						&& $matchedunitload->SKU->SKUType->id == $purchaseorderitem->SKU->SKUType->id && $matchedunitload->UL_allocation < 100)
					{
						$matchedunitloads_names[$counter]['unitload'] = $matchedunitload;
						$matchedunitloads_names[$counter]['route'] = ($matchedunitload->StorageLocation->Rack->Zone->ZONE_name).' => '.($matchedunitload->StorageLocation->Rack->RACK_name).' => '.($matchedunitload->StorageLocation->STL_name);
						$counter++;
					}
				}
			}

		return $this->respond($matchedunitloads_names);
    }

    public function getMaximumUnitloadCapacity(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'UL_name'=>'required',
			'SKU_name'=>'required'
		]);

    	if($v->fails())return $this->badRequest($v->errors()->all());

    	$unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$request['UL_name'])->first();
		if($unitload == null)
			return $this->badRequest(['Unitload Does not Exist']);

		$sku= SKU::where('SKU_OWN_id',$owner->id)->where('SKU_name',$request['SKU_name'])->first();
		if($sku == null)
			return $this->badRequest(['SKU Does not Exist']);

		if($unitload->SKU->id != $sku->id)
			return $this->badRequest(['Not the Same SKU']);
		if($unitload->SKU->SKUType->id != $sku->SKUType->id)
			return $this->badRequest(['Not the Same SKU Type']);

		$old_unitload_amount= intval($unitload->UL_amount) + intval($unitload->UL_reserved);

		$allocation = CapacityUnitload::where('CU_OWN_id',$owner->id)
		->where('CU_ULT_Type_id',$unitload->UnitloadType->id)->where('CU_SKU_Type_id',$sku->SKUType->id);

		$max_capacity = 100 / intval($allocation) - ($old_unitload_amount);

		if($max_capacity < 0)
			return $this->internalError(['Max Capacity is Negative']);

		return $this->respond([$max_capacity]);
    }

    public function checkUnitloadCapacity(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'UL_name'=>'required',
			'SKU_name'=>'required',
			'UL_amount'=>'required|numeric|integer'
		]);

    	if($v->fails())return $this->badRequest($v->errors()->all());

    	$unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$request['UL_name'])->first();
		if($unitload == null)
			return $this->badRequest(['Unitload Does not Exist']);

		$sku= SKU::where('SKU_OWN_id',$owner->id)->where('SKU_name',$request['SKU_name'])->first();
		if($sku == null)
			return $this->badRequest(['SKU Does not Exist']);

    	if($request->UL_amount < 0)
			return $this->badRequest(['Amount is Invalid']);

		if($unitload->SKU->id != $sku->id)
			return $this->badRequest(['Not the Same SKU']);
		if($unitload->SKU->SKUType->id != $sku->SKUType->id)
			return $this->badRequest(['Not the Same SKU Type']);

		$old_unitload_amount = intval($unitload->UL_amount) + intval($unitload->UL_reserved);

		$allocation = CapacityUnitload::where('CU_OWN_id',$owner->id)->where('CU_ULT_Type_id',
			$unitload->UnitloadType->id)->where('CU_SKU_Type_id',$sku->SKUType->id);

		$total_amount = ($old_unitload_amount + intval($request->UL_amount))*intval($allocation);

		if($total_amount <= 100)
			return $this->respond([true]);
		else
			return $this->respond([false]);
    }

    public function submitReceivingJob(Request $request)
    {	
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'POI_number'=>'required',
			'RCJ_amount'=>'required',
			'RCJ_number'=>'required',
			'POI_productiondate'=>'date_format:"Y-m-d"',
			'Unitloads_amount_array'=>'required'
		]);

    	if($v->fails())return $this->badRequest($v->errors()->all());

    	$purchaseorderitem = PurchaseOrderItems::where('POI_OWN_id',$owner->id)->where('POI_number',$request['POI_number'])->first();
		if($purchaseorderitem == null)
			return $this->badRequest(['Purchase Order Item Does not Exist']);

		$today = Carbon::now()->toDateString();
		if($request->POI_productiondate > $today)
    	{
    		return $this->badRequest(['Production Date is Invalid']);
    	}

		if($purchaseorderitem->POI_state == 'fully_received')
			return $this->badRequest(['Purchaseorder Item has already been Received']);

    	if($request->RCJ_amount <=0 || $request->RCJ_amount > (($purchaseorderitem->POI_amount) - ($purchaseorderitem->POI_receivedamount)))
			return $this->badRequest(['Amount is Invalid']);

		DB::beginTransaction();

		$receivingjob = new ReceivingJob();
		$receivingjob->RCJ_number = $request->RCJ_number;
		$receivingjob->RCJ_amount = $request->RCJ_amount;
		$receivingjob->RCJ_POI_id = $purchaseorderitem->id;
		$receivingjob->RCJ_OWN_id = $owner->id;
		$receivingjob->RCJ_SITE_id = $purchaseorderitem->Site->id;
		if(!$receivingjob->save())
		{
			DB::rollBack();
			return $this->internalError(['Receiving Job Creation has Failed']);
		}

		$purchaseorderitem->POI_receivedamount = ($purchaseorderitem->POI_receivedamount)
				+ ($receivingjob->RCJ_amount);

		if($purchaseorderitem->POI_receivedamount == $purchaseorderitem->POI_amount)
			$purchaseorderitem->POI_state = 'fully_received';
		else
			$purchaseorderitem->POI_state = 'partially_received';

		if ($request->has('POI_productiondate'))
		{
			$purchaseorderitem->POI_productiondate = $request->POI_productiondate;
		}

		if(!$purchaseorderitem->save())
		{
			DB::rollBack();
			return $this->internalError(['Receiving Job Creation has Failed']);
		}


		if(count($request->Unitloads_amount_array) == 0)
		{
			DB::rollBack();
			return $this->badRequest(['Unitloads Array is Empty']);
		}

		$total_amount = 0;
		foreach ($request->Unitloads_amount_array as $key => $unitload_amount_pair) {
			if (!array_key_exists("unitload_name",$unitload_amount_pair))
			{
				DB::rollBack();
				return $this->badRequest(['Unitload name Index Error']);
			}
			if (!array_key_exists("unitload_amount",$unitload_amount_pair))
			{
				DB::rollBack();
				return $this->badRequest(['Unitload amount Index Error']);
			}
			if (!array_key_exists("unitload_status",$unitload_amount_pair))
			{
				DB::rollBack();
				return $this->badRequest(['Unitload status Index Error']);
			}
			$unitload_name = $unitload_amount_pair['unitload_name'];
			$unitload_amount = $unitload_amount_pair['unitload_amount'];
			$unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$unitload_name)->first();
			if($unitload == null)
			{
				DB::rollBack();
				return $this->badRequest(['Unitload Does not Exist']);
			}

			if($unitload_amount <= 0)
			{
				DB::rollBack();
				return $this->badRequest(['Amount is Invalid']);
			}


			if($unitload->SKU->id != $purchaseorderitem->SKU->id )
			{
				DB::rollBack();
				return $this->badRequest(['Not the Same SKU is Invalid']);
			}

			if($unitload->SKU->SKUType->id != $purchaseorderitem->SKU->SKUType->id )
			{
				DB::rollBack();
				return $this->badRequest(['Not the Same SKU Type is Invalid']);
			}

			if(!$unitload->StorageLocation->FunctionalArea->FA_GoodsIn)
			{
				DB::rollBack();
				return $this->badRequest(['Unitload is Not in Goods In Area']);
			}

			if($unitload->Site->id != $purchaseorderitem->Site->id)
			{
				DB::rollBack();
				return $this->badRequest(['Unitload is Not in the Same Site']);
			}

			$old_unitload_amount = intval($unitload->UL_amount) + intval($unitload->UL_reserved);

			$capacity = CapacityUnitload::where('CU_OWN_id',$owner->id)->where('CU_ULT_Type_id',$unitload->UnitloadType->id)->where('CU_SKU_Type_id',$purchaseorderitem->SKU->SKUType->id)->first();

			if($capacity == null)
			{
				DB::rollBack();
				return $this->badRequest(['Unitload Capacity Rule Not Found']);
			}

			$allocation = $capacity->CU_allocation;

			$max_amount=($old_unitload_amount + intval($unitload_amount))*intval($allocation);

			if($max_amount > 100)
			{
				DB::rollBack();
				return $this->badRequest(['Unitload Capacity is Not Enough']);
			}

			if($purchaseorderitem->POI_productiondate != null)
			{
				$lifetime = intval($purchaseorderitem->SKU->SKU_lifeTime);
				$expiryDate= Carbon::parse($purchaseorderitem->POI_productiondate)->addDays($lifetime);

				if($unitload->UL_expiryDate != null)
				{
					if($unitload->UL_expiryDate != $expiryDate)
					{
						DB::rollBack();
						return $this->badRequest(['Not the Same Expiry Date']);
					}

				}

				else
				{
					$unitload->UL_expiryDate = $expiryDate;
				}
			}

			$total_amount = $total_amount + $unitload_amount;

			$unitload->UL_amount = ($unitload->UL_amount) + $unitload_amount;
			$unitload->UL_allocation = ($unitload->UL_allocation) +(intval($unitload_amount) * intval($allocation));
			$unitload->UL_status = $unitload_amount_pair['unitload_status'];
			if(!$unitload->save())
				return $this->internalError(['Unitload Updating has Failed']);

			$receivingjobunitload = new ReceivingJobUnitload();
			$receivingjobunitload->RJU_SKU_id = $unitload->SKU->id;
			$receivingjobunitload->RJU_UL_id = $unitload->id;
 			$receivingjobunitload->RJU_RCJ_id = $receivingjob->id;
			$receivingjobunitload->RJU_SITE_id = $unitload->Site->id;
			if(!$receivingjobunitload->save())
			return $this->internalError(['Receiving Job Unitload Creation has Failed']);

		}

		if($total_amount > $receivingjob->RCJ_amount)
		{
			DB::rollBack();
			return $this->badRequest(['Total Amount is Larger than Receiving Job Amount']);
		}

		elseif($total_amount < $receivingjob->RCJ_amount)
		{
			DB::rollBack();
			return $this->badRequest(['Total Amount is Smaller than Receiving Job Amount']);
		}
		DB::commit();
		return $this->resourceCreated(['Receiving Job Successfully Submitted']);

    }

    public function receivingJobIndex()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;
		if($user->isSystemAdmin())
        {
            $receivingjobs = $owner->ReceivingJob;
        }
        elseif(!$user->isSystemAdmin())
        {
            $receivingjobs = $user->site->ReceivingJob;
        }
    	
    	$receivingjobs_array = array();
    	$counter = 0;
    	foreach ($receivingjobs as $key => $receivingjob) {
    		$receivingjobs_array[$counter]['receivingjob'] = $receivingjob;
    		if($receivingjob->PurchaseOrderItems != null)
    			$receivingjobs_array[$counter]['POI_name'] = $receivingjob->PurchaseOrderItems->POI_name;
    		else
    			$receivingjobs_array[$counter]['POI_name'] = '';
    		if($receivingjob->site != null)
    			$receivingjobs_array[$counter]['SITE_name'] = $receivingjob->Site->SITE_name;
    		else
    			$receivingjobs_array[$counter]['SITE_name'] = '';
    		$counter++;
    	}
    	return $this->respond($receivingjobs_array);
    }
}
