<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use DB;
use App\Site;
use App\Zone;
use App\Rack;
use App\FunctionalArea;
use App\Unitload;
use App\StorageLocation;
use App\StorageJob;
use App\Strategy;
use App\CapacityLocation;
use App\Mismatch;

class PortalStorageController extends ApiController
{
	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    } 

    public function generateStorageJobName()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$storage_name = 'STJ_'.((StorageJob::where('STJ_OWN_id',$owner->id)->count())+1);  
        $time_start = microtime(true) * 10000;
        $storage_name = $storage_name.'_'. $time_start;
        return $this->respond([$storage_name]);
    }

    public function getDataForStorageJob()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	if($user->isSystemAdmin())
    	{
    		$unitloads = $owner->Unitload;
    		$sites = Site::where('SITE_OWN_id',$owner->id)->lists('SITE_name')->toArray();
    	}
    	else
    	{
    		$unitloads = $user->Site->Unitload;
    		$sites = [$user->Site->SITE_name];
    	}

		$goodsinUnitload_names = array();
		$counter = 0;

    	foreach ($unitloads as $key => $unitload) {
    			if($unitload->StorageLocation->FunctionalArea->FA_GoodsIn)
					{
						$goodsinUnitload_names[$counter]['name'] = $unitload->UL_name;
						$goodsinUnitload_names[$counter]['route'] = ($unitload->StorageLocation->Rack->Zone->ZONE_name).' => '.($unitload->StorageLocation->Rack->RACK_name).' => '.($unitload->StorageLocation->STL_name);
						$counter++;
					}
    		}


    	$strategies = Strategy::all();
    	$strategy_names = array();
    	$counter = 0;

    	foreach ($strategies as $key => $strategy) {
    		if($strategy->STRAT_storage)
    		{
    			$strategy_names[$counter] = $strategy->STRAT_name;
    			$counter++;
    		}
    	}

    	$Data = array();
    	$Data['goodsinUnitload_names'] = $goodsinUnitload_names;
    	$Data['site_names'] = $sites;
    	$Data['strategy_names'] = $strategy_names;

    	return $this->respond($Data);
    }

    public function getStorageLocationsForStrategy(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->client->owner;
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'UL_name'=>'required',
			'SITE_name'=>'required',
			'ZONE_name'=>'required',
			'RACK_name'=>'required',
			'STRAT_name' =>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$request['UL_name'])->first();
		if($unitload == null)
			return $this->badRequest(['Unitload Does not Exist']);

		$strategy = Strategy::where('STRAT_name',$request['STRAT_name'])->where('STRAT_storage',1)->first();
		if($strategy == null)
			return $this->badRequest(['Storage Strategy Does not Exist']);
		 $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
        if($site == null)
            return $this->badRequest(['Site Does not Exist']);

         $zone = Zone::where('ZONE_OWN_id',$owner->id)->where('ZONE_SITE_id',$site->id)->where('ZONE_name',$request['ZONE_name'])->first();
        if($zone == null)
            return $this->badRequest(['Zone Does not Exist']);

        $rack = Rack::where('RACK_OWN_id',$owner->id)->where('RACK_ZONE_id',$zone->id)->where('RACK_name',$request['RACK_name'])->first();
        if($rack == null)
            return $this->badRequest(['Rack Does not Exist']);

		$location_names = array();
		$counter = 0;
		$locations = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_RACK_id',$rack->id)->whereIn('STL_FA_id', (FunctionalArea::where('FA_OWN_id',$owner->id)->where('FA_Storage',1)->lists('id')->toArray()))->get();

		if($strategy->STRAT_name == 'Same_UnitloadType_in_Rack')
		{			

			foreach ($locations as $key => $location) {
				if(count($location->Unitload) > 0)
				{
					$flag = true;
					$unitloadtype_id = $unitload->UnitloadType->id;
					foreach ($location->Unitload as $key => $location_unitload) {
						if($location_unitload->UnitloadType->id != $unitloadtype_id)
						{
							$flag = false;
							break;
						}
					}
					if($flag)
					{
						$location_names[$counter]['name'] = $location->STL_name;
						$location_names[$counter]['route'] = ($location->Rack->Zone->ZONE_name).' => '.($location->Rack->RACK_name);
						$counter++;
					}
				}
				else{
					$location_names[$counter]['name'] = $location->STL_name;
					$location_names[$counter]['route'] = ($location->Rack->Zone->ZONE_name).' => '.($location->Rack->RACK_name);
					$counter++;
				}

			}
		}
		elseif($strategy->STRAT_name == 'Same_SKU_in_Rack')
		{

			foreach ($locations as $key => $location) {
				if(count($location->Unitload) > 0)
				{
					$flag = true;
					$sku_id = $unitload->SKU->id;
					foreach ($location->Unitload as $key => $location_unitload) {
						if($location_unitload->SKU->id != $sku_id)
						{
							$flag = false;
							break;
						}
					}
					if($flag)
					{
						$location_names[$counter]['name'] = $location->STL_name;
						$location_names[$counter]['route'] = ($location->Rack->Zone->ZONE_name).' => '.($location->Rack->RACK_name);
						$counter++;
					}
				}
				else{
					$location_names[$counter]['name'] = $location->STL_name;
					$location_names[$counter]['route'] = ($location->Rack->Zone->ZONE_name).' => '.($location->Rack->RACK_name);
					$counter++;
				}


			}
		}

		return $this->respond($location_names);

    }

    public function createStorageJob(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->client->owner;
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'UL_name'=>'required',
			'STL_name' =>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$request['UL_name'])->first();
		if($unitload == null)
			return $this->badRequest(['Unitload Does not Exist']);

		$storagelocation = StorageLocation::where('UL_OWN_id',$owner->id)->where('STL_name',$request['STL_name'])->first();
		if($storagelocation == null)
			return $this->badRequest(['Storage Location Does not Exist']);

		if($unitload->Site->id != $storagelocation->Site->id)
			return $this->badRequest(['Unitload & Storage Location Are Not in the Same Site']);

		if(!$unitload->StorageLocation->FunctionalArea->FA_GoodsIn)
			return $this->badRequest(['Unitload is Not in Goods In Area']);

		if(!$storagelocation->FunctionalArea->FA_Storage)
			return $this->badRequest(['Location is Not Used For Storage']);

		if($request->has('STRAT_name'))
		{
			$strategy = Strategy::where('STRAT_name',$request['STRAT_name'])->where('STRAT_storage',1)->first();
			if($strategy == null)
				return $this->badRequest(['Storage Strategy Does not Exist']);

			$fields = array();
			$fields['UL_name'] = $request->UL_name;
			$fields['STRAT_name'] = $request->STRAT_name;
			$response = $this->getStorageLocationsForStrategy($fields);

			$flag = false;
			foreach ($response as $key => $location_name) {
				if($location_name == $request->UL_name)
				{
					$flag = true;
					break;
				}
			}
			if(!$flag)
				return $this->badRequest(['Storage Location Violates Storage Strategy']);
		}
		$capacity = CapacityLocation::where('CL_OWN_id',$owner->id)->where('CL_STL_Type_id',$storagelocation->StorageLocationType->id)->where('CL_ULT_Type_id',$unitload->UnitloadType->id)->first();
		if($capacity == null)
			return $this->badRequest(['No Capacity Rule Found For this Location']);

		$allocation = $capacity->CL_allocation;

		if(count($storagelocation->Unitload) > 0)
		{
			$flag_mismatch = true;
			$flag_capacity = true;
			$unitloadtype_id = $unitload->UnitloadType->id;
			$storagelocationtype_id = $storagelocation->StorageLocationType->id;
			$total_allocation = intval($allocation);

			foreach ($storagelocation->Unitload as $key => $location_unitload) {
				$location_unitload_type_id = $location_unitload->UnitloadType->id;
				$mismatches = Mismatch::where('MISSMATCH_OWN_id',$owner->id)->where('MISSMATCH_STL_Type_id',$storagelocationtype_id)->where('MISSMATCH_ULT_Type_id_first',$unitloadtype_id)->where('MISSMATCH_ULT_Type_id_second',$location_unitload_type_id)->get();
				if(count($mismatches) > 0)
					return $this->badRequest(['Unitload Type is Mismatched with other Unitload Type in this Storage Location']);

				$location_unitload_capacity = CapacityLocation::where('CL_OWN_id',$owner->id)->where('CL_STL_Type_id',$storagelocationtype_id)->where('CL_ULT_Type_id',$location_unitload_type_id)->first();
				if($location_unitload_capacity == null)
					return $this->badRequest(['No Capacity Rule Found For this Location']);

				$location_unitload_allocation = $location_unitload_capacity->CL_allocation;

				$total_allocation=$total_allocation + intval($location_unitload_allocation);
				if($total_allocation > 100)
					return $this->badRequest(['No Enough Capacity in this Storage Location']);

			}
			if($total_allocation > 100)
				return $this->badRequest(['No Enough Capacity in this Storage Location']);
		}
		else{
			if($allocation > 100)
				return $this->badRequest(['No Enough Capacity in this Storage Location']);
		}

		$storagejob = new StorageJob();
		$storagejob->STJ_name = 'STJ_'.((StorageJob::where('STJ_OWN_id',$owner->id)->count())+1); 
		$storagejob->STJ_state = 'new';
		$storagejob->STJ_UL_id = $unitload->id;
		$storagejob->STJ_STL_id = $storagelocation->id;
		$storagejob->STJ_OWN_id = $owner->id;
		$storagejob->STJ_SITE_id = $unitload->Site->id;

		if($storagejob->save())
		{
			return $this->resourceCreated(['Storage Job Successfully Created']);
		}else{
			return $this->internalError(['Storage Job Creation has Failed']);
		}


    }

    public function getAllStorageJobs()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$storagejobs = $owner->StorageJob;
    	$storagejob_names = array();
		$counter = 0;
    	foreach ($storagejobs as $key => $storagejob) {
    		$storagejob_names[$counter] = $storagejob->STJ_name;
    		$counter++;
    	}

    	return $this->respond($storagejob_names);
    }

    public function submitStorageJob(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->client->owner;
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours() && !$user->isLabor())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'UL_name'=>'required',
			'STL_name' =>'required',
			'STJ_name'=>'required',
			'SITE_name'=>'required',
			'ZONE_name'=>'required',
			'RACK_name'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$request['UL_name'])->first();
		if($unitload == null)
			return $this->badRequest(['Unitload Does not Exist']);

		 $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
        if($site == null)
            return $this->badRequest(['Site Does not Exist']);

         $zone = Zone::where('ZONE_OWN_id',$owner->id)->where('ZONE_SITE_id',$site->id)->where('ZONE_name',$request['ZONE_name'])->first();
        if($zone == null)
            return $this->badRequest(['Zone Does not Exist']);

        $rack = Rack::where('RACK_OWN_id',$owner->id)->where('RACK_ZONE_id',$zone->id)->where('RACK_name',$request['RACK_name'])->first();
        if($rack == null)
            return $this->badRequest(['Rack Does not Exist']);


		$storagelocation = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_RACK_id',$rack->id)->where('STL_name',$request['STL_name'])->first();
		if($storagelocation == null)
			return $this->badRequest(['Storage Location Does not Exist']);

		if($unitload->Site->id != $storagelocation->Site->id)
			return $this->badRequest(['Unitload & Storage Location Are Not in the Same Site']);

		if(!$unitload->StorageLocation->FunctionalArea->FA_GoodsIn)
			return $this->badRequest(['Unitload is Not in Goods In Area']);

		if(!$storagelocation->FunctionalArea->FA_Storage)
			return $this->badRequest(['Location is Not Used For Storage']);

		if($request->has('STRAT_name'))
		{
			$strategy = Strategy::where('STRAT_name',$request['STRAT_name'])->where('STRAT_storage',1)->first();
			if($strategy == null)
				return $this->badRequest(['Storage Strategy Does not Exist']);

			// return $strategy;

			// $fields = array();
			// $fields['UL_name'] = $request->UL_name;
			// $fields['STRAT_name'] = $request->STRAT_name;
			// $response = $this->getStorageLocationsForStrategy($fields);

			// return $response;

				
			$location_names = array();
			$counter = 0;
			$locations = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_RACK_id',$rack->id)->whereIn('STL_FA_id', (FunctionalArea::where('FA_OWN_id',$owner->id)->where('FA_Storage',1)->lists('id')->toArray()))->get();

			if($strategy->STRAT_name == 'Same_UnitloadType_in_Rack')
			{

				foreach ($locations as $key => $location) {
					if(count($location->Unitload) > 0)
						{
						$flag = true;
						$unitloadtype_id = $unitload->UnitloadType->id;
						foreach ($location->Unitload as $key => $location_unitload) {
							if($location_unitload->UnitloadType->id != $unitload->UnitloadType->id)
							{
								$flag = false;
								break;
							}
						}
						if($flag)
						{
							$location_names[$counter]['name'] = $location->STL_name;
							$location_names[$counter]['route'] = ($location->Rack->Zone->ZONE_name).' => '.($location->Rack->RACK_name);
							$counter++;
						}
					}
					else{
						$location_names[$counter]['name'] = $location->STL_name;
						$location_names[$counter]['route'] = ($location->Rack->Zone->ZONE_name).' => '.($location->Rack->RACK_name);
						$counter++;
					}


				}
			}
			elseif($strategy->STRAT_name == 'Same_SKU_in_Rack')
			{

				foreach ($locations as $key => $location) {
					if(count($location->Unitload) > 0)
					{
						$flag = true;
						$sku_id = $unitload->SKU->id;
						foreach ($location->Unitload as $key => $location_unitload) {
							if($location_unitload->SKU->id != $unitload->SKU->id)
							{
								$flag = false;
								break;
							}
						}
						if($flag)
						{
							$location_names[$counter]['name'] = $location->STL_name;
							$location_names[$counter]['route'] = ($location->Rack->Zone->ZONE_name).' => '.($location->Rack->RACK_name);
							$counter++;
						}
					}
					else{
						$location_names[$counter]['name'] = $location->STL_name;
						$location_names[$counter]['route'] = ($location->Rack->Zone->ZONE_name).' => '.($location->Rack->RACK_name);
						$counter++;
					}


				}
			}

			$flag = false;
			foreach ($location_names as $key => $location_name) {
				if($location_name['name'] == $request->STL_name)
				{
					$flag = true;
					break;
				}
			}
			if(!$flag)
				return $this->badRequest(['Storage Location Violates Storage Strategy']);
		}
		$capacity = CapacityLocation::where('CL_OWN_id',$owner->id)->where('CL_STL_Type_id',$storagelocation->StorageLocationType->id)->where('CL_ULT_Type_id',$unitload->UnitloadType->id)->first();
		if($capacity == null)
			return $this->badRequest(['No Capacity Rule Found For this Location']);

		$allocation = $capacity->CL_allocation;

		if(count($storagelocation->Unitload) > 0)
		{
			$flag_mismatch = true;
			$flag_capacity = true;
			$unitloadtype_id = $unitload->UnitloadType->id;
			$storagelocationtype_id = $storagelocation->StorageLocationType->id;
			$total_allocation = intval($allocation);

			foreach ($storagelocation->Unitload as $key => $location_unitload) {
				$location_unitload_type_id = $location_unitload->UnitloadType->id;
				$mismatches = Mismatch::where('MISSMATCH_OWN_id',$owner->id)->where('MISSMATCH_STL_Type_id',$storagelocationtype_id)->where('MISSMATCH_ULT_Type_id_first',$unitloadtype_id)->where('MISSMATCH_ULT_Type_id_second',$location_unitload_type_id)->get();
				if(count($mismatches) > 0)
					return $this->badRequest(['Unitload Type is Mismatched with other Unitload Type in this Storage Location']);

				$location_unitload_capacity = CapacityLocation::where('CL_OWN_id',$owner->id)->where('CL_STL_Type_id',$storagelocationtype_id)->where('CL_ULT_Type_id',$location_unitload_type_id)->first();
				if($location_unitload_capacity == null)
					return $this->badRequest(['No Capacity Rule Found For this Location']);

				$location_unitload_allocation = $location_unitload_capacity->CL_allocation;

				$total_allocation=$total_allocation + intval($location_unitload_allocation);
				if($total_allocation > 100)
					return $this->badRequest(['No Enough Capacity in this Storage Location']);

			}
			if($total_allocation > 100)
				return $this->badRequest(['No Enough Capacity in this Storage Location']);
		}
		else{
			if($allocation > 100)
				return $this->badRequest(['No Enough Capacity in this Storage Location']);
		}
		DB::beginTransaction();
		$storagejob = new StorageJob();
		$storagejob->STJ_name = $request->STJ_name;
		$storagejob->STJ_state = 'submitted';
		$storagejob->STJ_UL_id = $unitload->id;
		$storagejob->STJ_STL_id = $storagelocation->id;
		$storagejob->STJ_OWN_id = $owner->id;
		$storagejob->STJ_SITE_id = $unitload->Site->id;

		if(!$storagejob->save())
		{
			DB::rollBack();
			return $this->internalError(['Storage Job Creation has Failed']);
		}

		$unitload = $storagejob->Unitload;
		$old_location = $unitload->StorageLocation;

		// THE MOST IMPORTANT LINE IN THE WORLD
		$unitload->UL_STL_id = $storagejob->StorageLocation->id;

		if(!$unitload->save())
		{
			DB::rollBack();
			return $this->internalError(['Unitload Updating has Failed']);
		}

		$old_capacity = CapacityLocation::where('CL_OWN_id',$owner->id)->where('CL_STL_Type_id',$unitload->StorageLocation->StorageLocationType->id)->where('CL_ULT_Type_id',$unitload->UnitloadType->id)->first();
		if($old_capacity == null)
			return $this->badRequest(['No Capacity Rule Found For this Location']);

		$old_allocation = $old_capacity->CL_allocation;
		$old_location->STL_allocation = ($old_location->STL_allocation) - $old_allocation;

		if(!$old_location->save())
		{
			DB::rollBack();
			return $this->internalError(['Old Storage Location Allocation Updating has Failed']);
		}

		$storagelocation->STL_allocation = ($storagelocation->STL_allocation) 
			+ $allocation;

		if(!$storagelocation->save())
		{
			DB::rollBack();
			return $this->internalError(['New Storage Location Allocation Updating has Failed']);
		}

		DB::commit();
		return $this->respond(['Storage Job Successfully Submitted']);
    }

    public function storageJobIndex()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;
		 if($user->isSystemAdmin())
        {
            $storagejobs = $owner->StorageJob;
        }
        elseif(!$user->isSystemAdmin())
        {
            $storagejobs = $user->site->StorageJob;
        }
    	$storagejobs_array = array();
    	$counter = 0;
    	foreach ($storagejobs as $key => $storagejob) {
    		$storagejobs_array[$counter]['storagejob'] = $storagejob;
    		$storagejobs_array[$counter]['UL_name'] = $storagejob->Unitload->UL_name;
    		$storagejobs_array[$counter]['STL_name'] = $storagejob->StorageLocation->STL_name;
    		$storagejobs_array[$counter]['SITE_name'] = $storagejob->Site->SITE_name;
    		$counter++;
    	}

    	
    	return $this->respond($storagejobs_array);
    }
}
