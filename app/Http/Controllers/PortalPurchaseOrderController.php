<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use DB;
use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\Client;
use App\Site;
use App\SKU;
use App\Provider;

class PortalPurchaseOrderController extends ApiController
{
	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function getDataForPurchaseOrder()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$owner = $user->Client->Owner;
    	//$skus = $owner->SKU;
    	$clients = $owner->Client;
    	$sites = $owner->Site;
    	$Data = array();
    	// $sku_names = array();
    	// $counter = 0;
    	// foreach ($skus as $key => $sku) {
    	// 	$sku_names[$counter] = $sku->SKU_name;
    	// 	$counter++;
    	// }

    	$client_names = array();
    	$counter = 0;
    	foreach ($clients as $key => $client) {
    		$client_names[$counter] = $client->CLI_name;
    		$counter++;
    	}

    	$site_names = array();
    	$counter = 0;
    	foreach ($sites as $key => $site) {
    		$site_names[$counter] = $site->SITE_name;
    		$counter++;
    	}

    	//$Data['sku_names'] = $sku_names;
    	$Data['client_names'] = $client_names;
    	$Data['site_names'] = $site_names;

    	return $this->respond($Data);

    }

    public function getProvidersOfClient(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'CLI_name'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
		if($client == null)
			return $this->badRequest(['Client Does not Exist']);

		$providers = $client->CLIProvider;
		$provider_names = array();
		$counter = 0;
    	foreach ($providers as $key => $provider) {
    		$provider_names[$counter] = $provider->PRV_name;
    		$counter++;
    	}

    	return $this->respond($provider_names);	

    }

    public function getSKUsOfClient(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;
    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'CLI_name'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
		if($client == null)
			return $this->badRequest(['Client Does not Exist']);

		if($client->CLI_systemClient)
			$skus = $owner->sku;
		else
			$skus = $client->SKU;
		$sku_names = array();
		$counter = 0;
    	foreach ($skus as $key => $sku) {
    		$sku_names[$counter] = $sku->SKU_name;
    		$counter++;
    	}

    	return $this->respond($sku_names);	

    }

    public function createPurchaseOrder(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	$v = Validator::make($request->all(),[
			'PO_dateOfDelivery'=>'required|date_format:"Y-m-d"'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$today = Carbon::now()->toDateString();
		if($request->PO_dateOfDelivery < $today)
    	{
    		return $this->badRequest(['Delivery Date is Invalid']);
    	}

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	if($user->isSystemAdmin())
		{
			if(!$request->has('SITE_name'))
				return $this->badRequest(['Site is Missing']);
			else
			{
				$site_id = Site::getIdByName('SITE',$request->SITE_name);
				if($site_id == -1)
					return $this->badRequest(['Site does not Exist']);
			}

		}
		else
			$site_id = $user->Site->id;	

    	if($user->isSystemClient())
    	{
    		if (!$request->has('CLI_name')) {
    			return $this->badRequest(['Client is Missing']);
    		}
    		else
    		{
				$client_id = Client::getIdByName('CLI',$request->CLI_name);
				if($client_id == -1)
					return $this->badRequest(['Client does not Exist']);
    		}

    	}
    	else
    		$client_id = $user->Client->id;

    	$purchaseorder = new PurchaseOrder();
    	$purchaseorder->PO_number = 'PO_'.((PurchaseOrder::where('PO_OWN_id',$owner->id)->count())+1);
    	$purchaseorder->PO_OWN_id = $owner->id;
    	$purchaseorder->PO_CLI_id = $client_id;
    	$purchaseorder->PO_SITE_id = $site_id;
    	$purchaseorder->PO_dateOfDelivery = $request['PO_dateOfDelivery'];

    	if($purchaseorder->save())
		{
			return $this->resourceCreated([$purchaseorder->PO_number]);
		}else{
			return $this->internalError(['Purchase Order Creation has Failed']);
		}
    		

    }

    public function createPurchaseOrderItem(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	$v = Validator::make($request->all(),[
			'PO_number'=>'required',
			'POI_amount'=>'required|numeric|integer',
			'SKU_name'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		if($request->POI_amount <=0)
			return $this->badRequest(['Amount is Invalid']);
		

		if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}


		$purchaseorder = PurchaseOrder::where('PO_OWN_id',$owner->id)->where('PO_number',$request['PO_number'])->first();
		if($purchaseorder == null)
			return $this->badRequest(['Purchase Order Does not Exist']);

		$sku_id = SKU::getIdByName('SKU',$request->SKU_name);
		if($sku_id == -1)
			return $this->badRequest(['SKU does not Exist']);

		$provider_id = null;
		if ($request->has('PRV_name'))
		{
			$provider_id = Provider::getIdByName('PRV',$request->PRV_name);
			if($provider_id == -1)
				return $this->badRequest(['Provider does not Exist']);	
		}
		
		$purchaseorderitem = new PurchaseOrderItems();
		$purchaseorderitem->POI_number = $purchaseorder->PO_number.'_'.(($purchaseorder->podItems->count())+1);
		$purchaseorderitem->POI_amount = $request->POI_amount;
		$purchaseorderitem->POI_PO_id = $purchaseorder->id;
		$purchaseorderitem->POI_SKU_id = $sku_id;
		$purchaseorderitem->POI_PRV_id = $provider_id;
		$purchaseorderitem->POI_OWN_id = $owner->id;
		$purchaseorderitem->POI_SITE_id = $purchaseorder->Site->id;
		$purchaseorderitem->POI_state = 'new';

		if($purchaseorderitem->save())
		{
			return $this->resourceCreated([$purchaseorderitem]);
		}else{
			return $this->internalError(['Purchase Order Item Creation has Failed']);
		}

    }

    public function updatePurchaseOrderDate(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'POI_number'=>'required',
            'POI_productiondate' => 'required|date_format:"Y-m-d"'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $purchaseorderitem = PurchaseOrderItems::where('POI_OWN_id',$owner->id)->where('POI_number',$request['POI_number'])->first();
        if($purchaseorderitem == null)
            return $this->badRequest(['Purchase Order Item Does not Exist']);

        $purchaseorderitem->POI_productiondate = $request->POI_productiondate;

        if($purchaseorderitem->save())
        {
            return $this->respond(['Purchase Order Item Successfully Updated']);
        }else{
            return $this->internalError(['Purchase Order Item Updating has Failed']);
        }
    }

    public function getAllPurchaseOrdersNames()
	{
		$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;


    	$purchaseorders = $owner->PurchaseOrder;
    	$purchaseorder_names = array();
		$counter = 0;
    	foreach ($purchaseorders as $key => $purchaseorder) {
    		$purchaseorder_names[$counter] = $purchaseorder->PO_number;
    		$counter++;
    	}
		return $purchaseorder_names;

	}


	public function getPurchaseOrderItemData()
	{
		$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;
		$purchaseorders = $this->getAllPurchaseOrdersNames();
		$skus = SKU::where('SKU_OWN_id',$owner->id)->get();
		$data['purchaseorders'] = $purchaseorders;
		$data['skus'] = $skus;
		return $this->respond($data);
	}

	public function getAllPurchaseOrders()
	{
		$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;

    	$purchaseorders = $owner->PurchaseOrder->all();
    	$purchaseorders_array = array();
    	$counter = 0;
    	foreach ($purchaseorders as $key => $purchaseorder) {
    		$purchaseorders_array[$counter]['purchaseorder'] = $purchaseorder;
    		$purchaseorders_array[$counter]['client_name'] = $purchaseorder->Client->CLI_name;
    		$purchaseorders_array[$counter]['site_name'] = $purchaseorder->Site->SITE_name;
    		$counter++;
    	}
    	
    	return $this->respond($purchaseorders_array);
    	
	}

    public function getPurchaseOrderByName(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        $v = Validator::make($request->all(),[
            'PO_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $purchaseorder = PurchaseOrder::where('PO_OWN_id',$owner->id)->where('PO_number',$request['PO_number'])->first();
        if($purchaseorder == null)
            return $this->badRequest(['Purchase Order Does not Exist']);

        $purchaseorderitems = $purchaseorder->podItems;
        $purchaseorderitems_names = array();
        $counter = 0;
        foreach ($purchaseorderitems as $key => $purchaseorderitem) {
            if($purchaseorderitem->POI_state != 'fully_received')
            {
                $purchaseorderitems_names[$counter]['name']= $purchaseorderitem->POI_number;
                $purchaseorderitems_names[$counter]['amount']= intval($purchaseorderitem->POI_amount) - intval($purchaseorderitem->POI_receivedamount);
                $purchaseorderitems_names[$counter]['sku']= $purchaseorderitem->SKU->SKU_name;
                $purchaseorderitems_names[$counter]['skunumber']= $purchaseorderitem->SKU->SKU_item_no;
                if($purchaseorderitem->provider != null)
                    $purchaseorderitems_names[$counter]['provider']= $purchaseorderitem->provider->PRV_name;
                else
                    $purchaseorderitems_names[$counter]['provider']='not set';
                $counter++;
            }           
        }

        $Data = array();
        $Data['purchaseorder'] = $purchaseorder;
        $Data['PO_dateOfDelivery'] = $purchaseorder->PO_dateOfDelivery;
        $Data['purchaseorderitems_names'] = $purchaseorderitems_names;
        if($purchaseorder->site != null)
            $Data['sitename'] = $purchaseorder->site->SITE_name;
        if($purchaseorder->client != null)
            $Data['client'] = $purchaseorder->client;

        return $this->respond($Data);
    }
    public function getPurchaseOrderByNameCreate(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'PO_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $purchaseorder = PurchaseOrder::where('PO_OWN_id',$owner->id)->where('PO_number',$request['PO_number'])->first();
        if($purchaseorder == null)
            return $this->badRequest(['Purchase Order Does not Exist']);

        $purchaseorderitems = $purchaseorder->podItems;
        $purchaseorderitems_data = array();
        $counter = 0;
        foreach ($purchaseorderitems as $key => $purchaseorderitem) {
            if($purchaseorderitem->POI_state == 'new')
            {
                $purchaseorderitems_data[$counter]['purchaseitem'] = $purchaseorderitem;
                $purchaseorderitems_data[$counter]['SKU_name']= $purchaseorderitem->SKU->SKU_name;
                if($purchaseorderitem->provider != null)
                    $purchaseorderitems_data[$counter]['PRV_name'] = $purchaseorderitem->provider->PRV_name;
                $counter++;
            }            
        }
        $Data = array();
        $Data['CLI_name'] = $purchaseorder->Client->CLI_name;
        $Data['dateofdelivery'] = $purchaseorder->dateOfDelivery;
        $Data['SITE_name'] = $purchaseorder->Site->SITE_name;
        $Data['purchaseorderitems_data'] = $purchaseorderitems_data;

        return $this->respond($Data);
    }
    public function getPurchaseOrderItemByName(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	$v = Validator::make($request->all(),[
			'POI_number'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$purchaseorderitem = PurchaseOrderItems::where('POI_OWN_id',$owner->id)->where('POI_number',$request['POI_number'])->first();
		if($purchaseorderitem == null)
			return $this->badRequest(['Purchase Order Item Does not Exist']);

    	$Data = array();
    	$Data['POI_amount'] = $purchaseorderitem->POI_amount;
    	$Data['PRV_name'] = $purchaseorderitem->Provider;
    	$Data['SKU_name'] = $purchaseorderitem->SKU->SKU_name;
    	$Data['POI_productiondate'] = $purchaseorderitem->POI_productiondate;
		$Data['POI_received'] = $purchaseorderitem->POI_receivedamount;
		$Data['POI_client'] = $purchaseorderitem->PurchaseOrder->Client->CLI_name;;

    	return $this->respond($Data);
    }

    public function purchaseOrderIndex()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;
        if($user->isSystemAdmin())
        {
            $purchaseorders = $owner->PurchaseOrder;
        }
    	elseif(!$user->isSystemAdmin())
        {
            $purchaseorders = $user->site->PurchaseOrder;
        }
    	$purchaseorders_array = array();
    	$counter = 0;
    	foreach ($purchaseorders as $key => $purchaseorder) {
    		$purchaseorders_array[$counter]['purchaseorder'] = $purchaseorder;
    		$purchaseorders_array[$counter]['CLI_name'] = $purchaseorder->Client->CLI_name;
    		$purchaseorders_array[$counter]['SITE_name'] = $purchaseorder->Site->SITE_name;
    		$counter++;
    	}

    	
    	return $this->respond($purchaseorders_array);
    }


    public function purchaseOrderItemIndex()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;
        if($user->isSystemAdmin())
        {
            $purchaseorderitems = $owner->PurchaseOrderItems->all();
        }
        elseif(!$user->isSystemAdmin())
        {
            $purchaseorderitems = $user->site->PurchaseOrderItems->all();
        }
    	$purchaseorderitems_array = array();
    	$counter = 0;
    	foreach ($purchaseorderitems as $key => $purchaseorderitem) {
    		$purchaseorderitems_array[$counter]['purchaseorderitem'] = $purchaseorderitem;
    		$purchaseorderitems_array[$counter]['purchaseorder_name'] = $purchaseorderitem->PurchaseOrder->PO_number;
    		$purchaseorderitems_array[$counter]['sku_name'] = $purchaseorderitem->SKU->SKU_name;
    		$purchaseorderitems_array[$counter]['provider'] = $purchaseorderitem->Provider;
    		$counter++;
    	}

    	
    	return $this->respond($purchaseorderitems_array);
    }

    public function PurchaseOrderUpdate(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;

    	$v = Validator::make($request->all(),[
    		'PO_number'=>'required',
			'PO_dateOfDelivery'=>'required|date_format:"Y-m-d"'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$today = Carbon::now()->toDateString();
		if($request->PO_dateOfDelivery < $today)
    	{
    		return $this->badRequest(['Delivery Date is Invalid']);
    	}

    	if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}
        $purchaseorder = PurchaseOrder::where('PO_OWN_id',$owner->id)->where('PO_number',$request['PO_number'])->first();
        if($purchaseorder == null)
            return $this->badRequest(['Purchase Order Item Does not Exist']);
    	if($user->isSystemAdmin())
		{
			if(!$request->has('SITE_name'))
				return $this->badRequest(['Site is Missing']);
			else
			{
				$site_id = Site::getIdByName('SITE',$request->SITE_name);
				if($site_id == -1)
					return $this->badRequest(['Site does not Exist']);
				$purchaseorder->PO_SITE_id = $site_id;
			}

		}

    	if($user->isSystemClient())
    	{
    		if (!$request->has('CLI_name')) {
    			return $this->badRequest(['Client is Missing']);
    		}
    		else
    		{
				$client_id = Client::getIdByName('CLI',$request->CLI_name);
				if($client_id == -1)
					return $this->badRequest(['Client does not Exist']);
				$purchaseorder->PO_CLI_id = $client_id;
    		}

    	}

    	$purchaseorder->PO_dateOfDelivery = $request['PO_dateOfDelivery'];

    	if($purchaseorder->save())
		{
			return $this->respond(['Purchase Order Successfully Created']);
		}else{
			return $this->internalError(['Purchase Order Creation has Failed']);
		}    		

    }


}
