<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/3/2016
 * Time: 7:22 PM
 */
namespace App\Http\Controllers;

use App\Http\Requests;
use Validator;
use App\Provider;
use App\Client;
use Illuminate\Http\Request;


class ProviderController extends ApiController{

    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }


    /**
     * Retrieve All Providers
     * @param None
     * @return All Providers Attributes
     */
    public function index()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        $providers = array();
        $clients = array();
        if($user->isSystemClient())
        {
            if($user->isSystemAdmin())
            {
                $providers['providers'] = $owner->provider->all();
                $providers['clients'] = Client::where('CLI_OWN_id',$owner->id)->get();
                $providers['role'] = "systemAdmin";
                $providers['permission'] = true;
                $providers['siteBased'] = false;

            }elseif($user->isSiteAdmin())
            {
                $providers['role'] = "siteAdmin";
                $providers['permission'] = true;
                $providers['siteBased'] = true;
                $providers['providers'] = $owner->provider->all();
                $providers['clients'] = $user->site->client;

            }elseif($user->isCheifOfLabours())
            {
                $providers['role'] = "cheifOfLabours";
                $providers['siteBased'] = true;
                $providers['permission'] = false;
                $providers['providers'] = $owner->provider->all();
                $providers['clients'] = $user->site->client;

            }elseif($user->isLabour())
            {
                $providers['role'] = "labour";
                $providers['siteBased'] = true;
                $providers['permission'] = false;
                $providers['providers'] = $owner->provider->all();
                $providers['clients'] = $user->site->client;

            }
        }

        return $this->respond($providers);

    }

    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
           'PRV_name'=>'required'
        ]);
        if($v->fails())return $this->badRequest(['Missing Inputs']);
//        $v2 = Validator::make($request->all(),[
//            'PRV_name'=> 'unique_with:Provider,PRV_OWN_id'
//        ]);
//        if($v2->fails())return $this->badRequest(['This Provider Already Exists']);
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if($user->isCheifOfLabours()||$user->isLabour())
        {
            return $this->unauthorized(['Unauthorized Access']);
        }
        $old_provider = Provider::where('PRV_OWN_id',$owner->id)->where('PRV_name',$request->PRV_name)->get();
        if(sizeof($old_provider) > 0)
        {
            return $this->badRequest(['Provider Name Must Be Unique']);
        } 
        $provider_name = $request['PRV_name'];
        $phone = null;
        $address = null;
        $email = null;
        if($request->has('PRV_phone')) $phone = $request['PRV_phone'];
        if($request->has('PRV_address')) $address = $request['PRV_address'];
        if($request->has('PRV_email')) $email = $request['PRV_email'];


        $provider = new Provider();
        $provider->PRV_NAME = $provider_name;
        $provider->PRV_email = $email;
        $provider->PRV_address = $address;
        $provider->PRV_phone = $phone;
        $provider->PRV_OWN_id = $user->client->owner->id;

        if($provider->save())
        {
            if($request->has('PRV_clients'))
            {
                for($i = 0;$i<sizeof($request['PRV_clients']);$i++)
                {
                    $client_id = Client::where('CLI_name',$request['PRV_clients'][$i])->first()->id;
                    $provider->Client()->attach($client_id);
                }
            }
            return $this->resourceCreated(['Provider Created Successfully']);
        }
        return $this->internalError(['Provider Creation Failed']);

    }

    public function destroy(Request $request)
    {
        $v = Validator::make($request->all(),[
            'id' => 'required',
        ]);
        if($v->fails())return $this->badRequest(['Missing Index']);
        $id = intval($request['id']);
        $user = \Illuminate\Support\Facades\Request::user();
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())return $this->unauthorized(['Unauthorized Access']);
        $provider = Provider::findOrFail($id);
        //return $provider;
        $provider->client()->detach();
        if($provider->delete())return $this->respond(['Provider Deleted Successfully']);
        return $this->internalError(['Provider Deleting Failed']);
    }

    public function update(Request $request)
    {
        $v = Validator::make($request->all(),[
            'PRV_name' => 'required',
            'id'=>'required|exists:Provider,id'
        ]);
        if($v->fails())return $this->badRequest(['Missing Inputs']);
        $provider_name = $request['PRV_name'];
        $id = intval($request['id']);
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        $provider = Provider::findOrFail($id);

        $old_provider = Provider::where('PRV_OWN_id',$owner->id)->where('PRV_name',$request->PRV_name)->where('id','<>',$provider->id)->get();
        if(sizeof($old_provider) > 0)
        {
            return $this->badRequest(['Provider Name Must Be Unique']);
        } 
        $owner_id = \Illuminate\Support\Facades\Request::user()->client->owner->id;

        if($user->isCheifOfLabours()||$user->isLabour())
        {
            return $this->unauthorized(['Unauthorized Access']);
        }
        $provider->PRV_name = $provider_name;
        //return $request->PRV_clients;

        if($request->has('PRV_phone'))$provider->PRV_phone = $request['PRV_phone'];
        if($request->has('PRV_address'))$provider->PRV_address = $request['PRV_address'];
        if($request->has('PRV_email'))$provider->PRV_email = $request['PRV_email'];
        $provider->Client()->detach();
        if($request->has('PRV_clients'))
        {            
            for($i=0;$i<sizeof($request['PRV_clients']);$i++)
            {
                $client_id = Client::where('CLI_name',$request['PRV_clients'][$i])->where('CLI_OWN_id',$owner_id)->first()->id;
                $provider->Client()->attach($client_id);
            }
        }

        if(Provider::where('PRV_name',$provider_name)->where('PRV_OWN_id',$owner_id)->whereNotIn('id',[$id])->exists())
            return $this->badRequest(['This Provider Already Exists']);

        if($provider->save())return $this->respond(['Provider Updated Successfully']);
        return $this->internalError(['Updating Provider Failed']);


    }

    public function providerProp(Request $request)
    {
        $v = Validator::make($request->all(),[
            'id'=>'required|exists:Provider,id',
        ]);
        if($v->fails())return $this->badRequest(['Missing Index']);
        $id = intval($request['id']);
        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;
        $site = false;
        $admin = false;
        $provider = null;
        $clients = null;
        if($user->isSystemClient())
        {
            if($user->isSystemAdmin())
            {
                $admin = true;
                $site = false;
                $provider = Provider::findOrFail($id);
                $clients = Client::where('CLI_OWN_id',$owner_id)->get();
            }else
            {
                $admin = false;
                $site = true;
                $provider = Provider::findOrFail($id);
                $clients = $user->site->client;
            }
        }
        $respond = array();
        $respond['admin'] = $admin;
        $respond['site'] = $site;
        $respond['provider'] = $provider;
        $respond['clients'] = $clients;
        $respond['provider_clients'] = $provider->Client->all();
        json_encode($respond);
        return $this->respond($respond);
    }
}