<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\Zone;
use App\Site;

class ZoneController extends ApiController
{
	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }
   public function index()
    {
		/*
		 * View Zones
		 * @param none
		 * @return Zones Based on Permissions
		 */
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$ZoneData =array();
    	$zonedata = array();
        if($user->isSystemClient()){
			$ZoneData['systemClient']=true;
        	if($user->isSystemAdmin())
        	{
        		$zones = $owner->Zone;
        		$sites = $owner->Site;
//        		$ZoneData['zones']=$zones->all();
        		$ZoneData['clients']=$owner->Client->all();
        		$counter = 0;
        		foreach ($zones as $key => $value) {
        			$zonedata[$counter]['zone'] = $value;
        			$zonedata[$counter]['site_name'] = $value->Site->SITE_name;
        			$counter++;
        		}

        		$ZoneData['zones']=$zonedata;
        		$ZoneData['sites']= $sites->all();
        		$ZoneData['permission'] = true;
				$ZoneData['siteBased']=false;
				$ZoneData['role'] = "systemAdmin";
        	}
        	elseif($user->isSiteAdmin())
        	{
        		$site = $user->Site;
        		$zones = $site->Zone;
        		$counter = 0;
        		foreach ($zones as $zone) {
        			$zonedata[$counter]['zone'] = $zone;
        			$zonedata[$counter]['site_name'] = $zone->Site->SITE_name;
        			$counter++;
        		}
                $ZoneData['sites'] = $site->all();
        		$clients = $owner->Client;
//        		$ZoneData['zones']=$zones->all();
        		$ZoneData['clients']=$owner->Client->all();
        		$ZoneData['zones']=$zonedata;
        		$ZoneData['permission'] = true;
				$ZoneData['siteBased'] = true;
				$ZoneData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$site = $user->Site;
                // $site = array();
                // array_push($site, $site_object);
        		$zones = $site->Zone;
                $ZoneData['sites'] =$site->all();
                $counter = 0;
        		foreach ($zones as $zone) {
        			$zonedata[$counter]['zone'] = $zone;
        			$zonedata[$counter]['site_name'] = $zone->Site->SITE_name;
        			$counter++;
        		}
        		$ZoneData['zones']=$zonedata;
        		$ZoneData['permission'] = false;
				$ZoneData['siteBased'] = true;
                $ZoneData['clients']=$owner->Client->all();
				$ZoneData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$site = $user->Site;
				$zones = $site->Zone;
                $ZoneData['sites'] =$site->all();
                $ZoneData['clients']=$owner->Client->all();
//				$ZoneData['zones']=$zones->all();
				$counter = 0;
        		foreach ($zones as $zone) {
        			$zonedata[$counter]['zone'] = $zone;
        			$zonedata[$counter]['site_name'] = $zone->Site->SITE_name;
        			$counter++;
        		}
        		$ZoneData['zones']=$zonedata;
				$ZoneData['permission'] = false;
				$ZoneData['siteBased'] = true;
				$ZoneData['role'] ="labour";
			}
        }
   	 	return $this->respond($ZoneData);        
    }



	public function store(Request $request)
	{
		/*
		 * Create new Zone
		 * @param Attributes of Zone to be Created
		 * @return Approval of Creation
		 */
		$v = Validator::make($request->all(),[
			'ZONE_name'=>'required',
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$name = $request['ZONE_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->client->owner;
		$site = null;
		if($user->isSystemAdmin())
		{
			if(!$request->has('ZONE_site'))
			{
				return $this->badRequest(['Site is Missing']);
			}else{
				$site_name = $request['ZONE_site'];
				$site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$site_name)->first()->id;
			}
		}else{
			$site = $user->site;
			$site = $site->id;
		}

		if(!$this->isUniqueness($name, $owner->id,$site,0))return $this->badRequest(['Zone name already Exists']);

		$zone = new Zone();
		$zone->ZONE_name = $name;
		$zone->ZONE_OWN_id = $owner->id;
		$zone->ZONE_SITE_id = $site;

		if($zone->save())
		{
			return $this->resourceCreated(['Zone Successfully Created']);
		}else{
			return $this->internalError(['Zone Creation has Failed']);
		}


	}

	public function isUniqueness($name,$owner_id,$site_id,$id)
	{
		$field = array('id'=>$id);
		$zones = Zone::where('ZONE_name',$name)->where('ZONE_OWN_id',$owner_id)->where('ZONE_SITE_id',$site_id)->
		whereNotIn('id',$field)->first();
		if($zones!=null)return false;
		else return true;
	}

	public function zoneprob (Request $request)
	{
		/*
		 * View Properties of Zone to be Updated
		 * @param Zone id
		 * @return Zone Properties (Attributes)
		 */
		$v = Validator::make(['id'=> $request['id']],[
			'id' => 'integer|exists:Zone,id'
		]);

		if($v->fails()) return $this->badRequest($v->errors()->all());
		$id = intval($request['id']);

		$user = \Illuminate\Support\Facades\Request::user();
		$site_id =null;
        //return $id;
		$zones = Zone::where('id',$id)->first();
		$sites = null;
		$admin = false;
		$site = false;
		if($user->isSystemAdmin())
		{
			$admin = true;
			$site = false;
			$sites = $user->client->owner->site->all();

		}else if($user->isSiteAdmin())
		{
            $site_id = $user->Site->id;
			$admin = false;
			$site = true;
			$sites = Site::where('id',$site_id)->get();
		}else
        {
            $site_id = $user->Site->id;
            $admin = false;
            $site = false;
            $sites = Site::where('id',$site_id)->get();
        }
		$respond = array();
		$respond['site'] = $site;
		$respond['admin'] = $admin;
		$respond['zones'] = $zones;
		$respond['sites'] = $sites;
		$respond['zonesite'] = Site::where('id',$zones->ZONE_SITE_id)->first()->SITE_name; 
		json_encode($respond);
		return $this->respond($respond);

	}

	public function update(Request $request)
	{
		/*
		 * Update Zone
		 * @param Zone id to be Updated & New Attributes
		 * @return Update Approval
		 */
		$v = Validator::make(['id'=>$request['id'],'name' =>$request['ZONE_name'],'site_name'=>$request['ZONE_site']],[
			'id' => 'required|exists:Zone,id',
			'name'=>'required',
			'site_name'=>'required'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;
		$name = $request['ZONE_name'];
		$site_name = $request['ZONE_site'];
		$site_id = Site::where('SITE_name',$site_name)->first()->id;

		$zone = Zone::findOrFail($id);
		$zone->ZONE_name = $name;
		$zone->ZONE_SITE_id = $site_id;
		$zone->ZONE_OWN_id = $owner_id;

		if(!$this->isUniqueness($name,$owner_id,$site_id,$id))return $this->badRequest(['Zone Name Already Exists']);

		if($zone->save())
		{
			return $this->respond(['Zone Successfully Updated']);
		}else{
			return $this->internalError(['Zone Update Failed']);
		}


	}

	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Zone,id'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());
		$id = intval($request['id']);
		$zone = Zone::findOrFail($id);
		if(sizeof($zone->bin)>0 || sizeof($zone->rack))
			return $this->badRequest(['This Zone is attached to a bin or a rack please delete this rack or bin first']);
		if($zone->delete())
		{
			return $this->respond(['Zone Deleted Succesfully']);
		}else{
			return $this->internalError(['Zone Delete Failed']);
		}
	}



}
