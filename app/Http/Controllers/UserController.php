<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/1/2016
 * Time: 12:07 PM
 */
namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\User;
use App\Role;
use App\Site;
use Hash;
use Auth;
use Crypt;
use DB;
class UserController extends ApiController{



    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }


    /**
     * View Users of Warehouse
     * @param None
     * @return Users, Clients, Roles, Sites
     */

    public function index()
    {

        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;
        $users = array();
        $response = array();
        $response['users'] = array();
        $userClient = null;
        if($user->isSystemClient())
        {
            if($user->isSystemAdmin())
            {
                $clients = Client::where('CLI_OWN_id',$owner_id)->get();
                foreach($clients as $key => $client)
                {
                    $clientUsers = $client->User->all();
                    $counter = 0;
                    foreach($clientUsers as $clientUser)
                    {
                        $users['user']=$clientUser;
                        $users['client']=$client;
                        $users['role']=$clientUser->role[0];
                        $users['site']=$clientUser->site;
                        array_push($response['users'],$users);
                    }
                }
                $response['siteBased'] = false;
                $response['permission'] = true;
                $response['role'] = "SystemAdmin";

            }elseif($user->isSiteAdmin())
            {
                $client = Client::where('CLI_OWN_id',$owner_id)->where('CLI_systemClient',1)->first();
                $mainClientUsers = $client->user;
                $counter =0;
                foreach ($mainClientUsers as $key => $userx) {
                        if($userx->site == null || $userx->site->id == $user->site->id)
                        {
                             $users['user']=$userx;
                            $users['client']=$userx->client;
                            $users['role']=$userx->role[0];
                            if($userx->site!=null)
                                $users['site']=$userx->site;
                            else
                                $users['site']=null;
                            $response['users'][$counter] = $users;
                            $counter++;
                        }
                           
                 } 
                // $site = $user->site;
                // $siteClients = $site->client;
                // foreach($siteClients as $siteClient)
                // {
                //     $clientUsers = $siteClient->user->all();
                //     $counter =0;
                //     foreach($clientUsers as $clientUser)
                //     {
                //         $users['user']=$clientUser;
                //         $users['client']=$client;
                //         $users['role']=$clientUser->role[0];
                //         $users['site']=$clientUser->site;
                //         array_push($response['users'],$users);
                //     }
                // }
                $response['siteBased'] = true;
                $response['permission'] = false;
                $response['role'] = "SiteAdmin";
            }elseif($user->isCheifOfLabours())
            {
                $client = Client::where('CLI_OWN_id',$owner_id)->where('CLI_systemClient',1)->first();
                $mainClientUsers = $client->user;
                $counter =0;
                foreach ($mainClientUsers as $key => $userx) {
                        if($userx->site == null || $userx->site->id == $user->site->id)
                        {
                             $users['user']=$userx;
                            $users['client']=$userx->client;
                            $users['role']=$userx->role[0];
                            if($userx->site!=null)
                                $users['site']=$userx->site;
                            else
                                $users['site']=null;
                            $response['users'][$counter] = $users;
                            $counter++;
                        }
                           
                 } 
                // $site_id = $user->site->id;
                // foreach($clients as $client)
                // {
                //     $sites = $client->site;
                //     foreach ($sites as $site) {
                //         if($site->id == $site_id)
                //         {
                //             $clientUsers = $client->user->all();
                //             json_decode($clientUsers,true);
                //             $counter =0;
                //             foreach($clientUsers as $clientUser)
                //             {
                //                 $users['user']=$clientUser;
                //                 $users['client']=$client;
                //                 $users['role']=$clientUser->role[0];
                //                 $users['site']=$clientUser->site;
                //                 array_push($response['users'],$users);
                //             }

                //         }
                //     }
                // }
                $response['siteBased'] = true;
                $response['permission'] = false;
                $response['role'] = "CheifOfLabours";
            }elseif($user->isLabour())
            {
                $client = Client::where('CLI_OWN_id',$owner_id)->where('CLI_systemClient',1)->first();
                $mainClientUsers = $client->user;
                $counter =0;
                foreach ($mainClientUsers as $key => $userx) {
                        if($userx->site == null || $userx->site->id == $user->site->id)
                        {
                             $users['user']=$userx;
                            $users['client']=$userx->client;
                            $users['role']=$userx->role[0];
                            if($userx->site!=null)
                                $users['site']=$userx->site;
                            else
                                $users['site']=null;
                            $response['users'][$counter] = $users;
                            $counter++;
                        }
                           
                 } 
                // $site_id = $user->site->id;
                // foreach($clients as $client)
                // {
                //     $sites = $client->site;
                //     foreach ($sites as $site) {
                //         if($site->id == $site_id)
                //         {
                //             $clientUsers = $client->user->all();
                //             json_decode($clientUsers,true);
                //             $counter =0;
                //             foreach($clientUsers as $clientUser)
                //             {
                //                 $users['user']=$clientUser;
                //                 $users['client']=$client;
                //                 $users['role']=$clientUser->role[0];
                //                 $users['site']=$clientUser->site;
                //                 array_push($response['users'],$users);
                //             }

                //         }
                //     }
                // }
                $response['siteBased'] = true;
                $response['permission'] = false;
                $response['role'] = "Labour";
            }
        }
        $owner = $user->client->owner;
        $response['clients'] = $owner->Client->all();
        $response['roles'] = Role::all();
        $response['client'] = $user->Client->CLI_name;
        if($user->Site!=null) $response['site'] = $user->Site->SITE_name;
        else $response['site'] = null;
        $response['sites'] = Site::where('SITE_OWN_id',$owner_id)->get();
        return $this->respond($response);

    }


    /**
     * Create New User
     * @param User Attributes
     * @return Creation Approval
     */

    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'USR_name' => 'required',
            'password' => 'required',
            'USR_ROLE' => 'required'
        ]);

        if($v->fails())return $this->badRequest(['Missing Inputs or This User Already Exists']);

        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;

        $name = $request['USR_name'];
        $password = $request['password'];
        // $client_name = $request['USR_CLI'];
        // $client_id = Client::where('CLI_name',$client_name)->where('CLI_OWN_id',$owner_id)->first()->id;
        $flag = User::where('USR_name',$request['USR_name'])
            ->where('USR_CLI_id',$user->client->id)->exists();

        if($flag)return $this->badRequest(['This User Already Exists']);

        $newUser = new User();
        $newUser->USR_name = $name;
        $newUser->password = Hash::make($password);
        $newUser->USR_CLI_id = $user->Client->id;
        $role_id = Role::where('ROLE_name',$request['USR_ROLE'])->first()->id;
        $newUser->USR_SITE_id = null;
        $newUser->USR_email = null;
        if($request['USR_ROLE'] != "SystemAdmin")
        {
            if($request['USR_SITE'] != null)
            {
                $site_id = Site::where('SITE_name',$request['USR_SITE'])->where('SITE_OWN_id',$owner_id)->first()->id;
                $newUser->USR_SITE_id = $site_id;
            }
            else
            {
                return $this->badRequest(['Any Non SystemAdmin User must have a site attached to Him/Her']);
            }
        }
        if($request->has('USR_email'))$newUser->USR_email = $request['USR_email'];

        if($newUser->save())
        {
            $newUser->role()->attach($role_id);
            return $this->resourceCreated(['User Created Successfully']);
        }
        else return $this->internalError(['User Creation Failed']);


    }


    /**
     * Update User
     * @param User Attributes
     * @return Update Approval
     */

    public function update(Request $request)
    {
        $v = Validator::make($request->all(),[
            'id'=>'required|exists:users,id',
            'USR_name'=>'required',
            'USR_ROLE'=>'required',
        ]);

        if($v->fails())return $this->badRequest(['Missing Inputs']);
        $id = intval($request['id']);
        $name = $request['USR_name'];

        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;
        $target = User::findOrFail($id);
        DB::beginTransaction();
        if(!$user->isSystemAdmin())return $this->unauthorized(['Unauthorized Access']);
        $target->USR_name = $name;
        if($request->has('USR_email'))$target->USR_email = $request['USR_email'];
        // if($request->has('USR_CLI'))
        // {
        //     $client_id = Client::where('CLI_name',$request['USR_CLI'])->where('CLI_OWN_id',$owner_id)->first()->id;
        //     $target->USR_CLI_id = $client_id;
        // }
        $role_id = Role::where('ROLE_name',$request['USR_ROLE'])->first()->id;
        $target->role()->detach();
        $target->role()->attach($role_id);
        if($request['USR_ROLE']!='SystemAdmin')
        {
            if($request['USR_SITE'] != null)
            {
                $siteId = Site::where('SITE_name',$request['USR_SITE'])->where('SITE_OWN_id',$owner_id)->first()->id;
                $target->USR_SITE_id = $siteId;
            }
            else
            {
                 DB::rollBack();
                return $this->badRequest(['User Updating Failed Because Site was not set']);
            }
            
        }else{
           $target->USR_SITE_id = null;
        }

        if($target->save())
        {
            DB::commit();
            return $this->respond(['User Updated Successfully']);
        }
        else
        {
            DB::rollBack();
            return $this->badRequest(['User Updating Failed']);     
        }
    }



    /**
     * Delete User
     * @param User Id
     * @return Destroy Approval
     */

    public function destroy(Request $request)
    {
        $v = Validator::make($request->all(),[
           'id' => 'required|exists:users,id'
        ]);
        if($v->fails()) return $this->badRequest(['User doesn\'t exist']);
        $id = intval($request['id']);
        $user = \Illuminate\Support\Facades\Request::user();
        if(!$user->isSystemAdmin())return $this->unauthorized(['Unauthorized Access']);
        $target = User::findOrFail($id);
        $target->role()->detach();
        if($target->delete()) return $this->respond(['User Deleted Successfully']);
        return $this->internalError(['User Deleting Failed']);
    }



    /**
     * @param User id
     * @return User Attributes
     */

    public function userProp(Request $request)
    {
        $v = Validator::make($request->all(),[
           'id'=>'required|exists:users,id'
        ]);
        if($v->fails())return $this->badRequest(['Wrong Index']);
        $id = intval($request['id']);
        $target = User::findOrFail($id);
        $user = \Illuminate\Support\Facades\Request::user();
        $site = false;
        $admin = false;
        $role = null;
        $client = null;
        if($user->isSystemClient())
        {
            if($user->isSystemAdmin())
            {
                $site = false;
                $admin = true;
                $role = $target->role[0]->ROLE_name;
                $client = $target->client->CLI_name;
            }elseif($user->isSiteAdmin())
            {
                $site = true;
                $admin = true;
                $role = $target->role[0]->ROLE_name;
                $client = $target->client->CLI_name;
            }else{
                $site = true;
                $admin = false;
                $role = $user->role[0]->ROLE_name;
                $client = $user->client->CLI_name;
            }
        }
        $respond = array();
        $respond['site'] = $site;
        $respond['admin'] = $admin;
        $respond['user'] = $target;
        $respond['user_role'] = $role;
        $respond['user_client'] = $client;
        $respond['clients'] = $user->Client->all();
        $respond['roles'] = Role::all();
        $owner = $user->client->owner;
        $respond['sites'] = Site::where('SITE_OWN_id',$owner->id)->get();
        if($target->site!=null) $respond['user_site'] = $target->site->SITE_name;
        else $respond['user_site'] = null;
        json_encode($respond);
        return $this->respond($respond);
    }



  
}


