<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use DB;
use App\Client;
use App\Site;
use App\SKU;
use App\ReceivingJob;
use App\Shipping;
use App\PurchaseOrder;
use App\StorageJob;
use App\IssueOrder;
use App\PickRequest;
use App\PickJob;
use App\Unitload;
use App\PickRequestItem;

class ReportsController extends ApiController
{
	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function getTransactionReport(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to View This Page']);
        }

        $v = Validator::make($request->all(),[
            'start_date'=>'date_format:"Y-m-d"',
            'end_date'=>'date_format:"Y-m-d"',
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $today = Carbon::now()->toDateString();
        if($request->has('start_date'))
        {
        	$today = Carbon::now()->toDateString();
	        if($request->start_date > $today)
	        {
	            return $this->badRequest(['Selected Day is Invalid']);
	        } 
	        $start_date = $request->start_date; 
        }
        else
        	$start_date = '1111-01-01';

        if($request->has('end_date'))
        {
        	$today = Carbon::now()->toDateString();
	        if($request->end_date > $today)
	        {
	            return $this->badRequest(['Selected Day is Invalid']);
	        } 
	        $end_date = $request->end_date; 
        }
        else
        	$end_date = '8888-01-01';
        
        if($end_date < $start_date)
        {
            return $this->badRequest(['End Date is Before Start Date']);
        }

        $purchaseorders = PurchaseOrder::where('PO_OWN_id',$owner->id)->where('created_at','>=',$start_date)->where('created_at','<',Carbon::parse($end_date)->addDays(1))->get();

        $issueorders = IssueOrder::where('ISO_OWN_id',$owner->id)->where('created_at','>=',$start_date)->where('created_at','<',Carbon::parse($end_date)->addDays(1))->get();

        $orders_array = array();
        $counter = 0;

        if($request->has('CLI_name'))
        {
        	$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
	        if($client == null)
	            return $this->badRequest(['Client Does not Exist']);

	        foreach ($purchaseorders as $key => $purchaseorder) {
	        	if($purchaseorder->Client->id == $client->id)
	        	{
	        		$purchaseorderitems = $purchaseorder->podItems;
		        	foreach ($purchaseorderitems as $key => $purchaseorderitem) {
		        		
		        		$orders_array[$counter]['order'] = $purchaseorder->PO_number;
		        		$orders_array[$counter]['client']= $purchaseorder->Client->CLI_name;
		        		$orders_array[$counter]['site'] = $purchaseorder->Site->SITE_name;
		        		$orders_array[$counter]['type'] = 'IN';	
		        		$orders_array[$counter]['item'] = $purchaseorderitem->POI_number;
		        		$orders_array[$counter]['sku'] = $purchaseorderitem->SKU->SKU_name;
		        		$orders_array[$counter]['amount'] = $purchaseorderitem->POI_amount;
		        		$orders_array[$counter]['receiving_start'] = 'Not Received Yet';
		        		$orders_array[$counter]['receiving_end'] = 'Not Received Yet';
		        		$orders_array[$counter]['treated'] = '';
		        		$orders_array[$counter]['picking_start'] = '';
		        		$orders_array[$counter]['picking_end'] = '';
		        		$orders_array[$counter]['shipping'] = '';
		        		$orders_array[$counter]['completed'] = 'NO';

		        		$receivingjobs = ReceivingJob::where('RCJ_OWN_id',$owner->id)->where('RCJ_POI_id',$purchaseorderitem->id)->orderBy('created_at', 'asc')->get();
		        		if(count($receivingjobs) > 0)
		        		{
		        			$orders_array[$counter]['receiving_start'] = Carbon::parse($receivingjobs[0]['created_at'])->toDateString();

		        			$orders_array[$counter]['receiving_end'] = Carbon::parse($receivingjobs[count($receivingjobs)-1]['created_at'])->toDateString();
		        			if(Carbon::parse($receivingjobs[count($receivingjobs)-1]['created_at'])->toDateString() <= $end_date)
		        				$orders_array[$counter]['completed'] = 'YES';
		        		}

		        		$counter++;      			        		
		        	}
	        	}	        	
	        }

	        foreach ($issueorders as $key => $issueorder) {
	        	if($issueorder->Client->id = $client->id)
	        	{
	        		
	        		$issueorderitems = $issueorder->IssueItem;
		        	foreach ($issueorderitems as $key => $issueorderitem) {
		        		$orders_array[$counter]['order'] = $issueorder->ISO_number;
		        		$orders_array[$counter]['client']= $issueorder->Client->CLI_name;
		        		$orders_array[$counter]['site'] = $issueorder->Site->SITE_name;
		        		$orders_array[$counter]['type'] = 'OUT';	
		        		$orders_array[$counter]['item'] = $issueorderitem->ISI_number;
		        		$orders_array[$counter]['sku'] = $issueorderitem->SKU->SKU_name;
		        		$orders_array[$counter]['amount'] = $issueorderitem->ISI_amount;
		        		$orders_array[$counter]['receiving_start'] = '';
		        		$orders_array[$counter]['receiving_end'] = '';
		        		$orders_array[$counter]['treated'] = 'Not Treated Yet';
		        		$orders_array[$counter]['picking_start'] = 'Not Picked Yet';
		        		$orders_array[$counter]['picking_end'] = 'Not Picked Yet';
		        		$orders_array[$counter]['shipping'] = 'Not Shipped Yet';
		        		$orders_array[$counter]['completed'] = 'NO';

		        		$pickrequest =  $issueorderitem->PickRequest;
		        		if($pickrequest != null)
	        				$orders_array[$counter]['treated'] = Carbon::parse($pickrequest->created_at)->toDateString();


		        		$pickjobs = PickJob::where('PJ_OWN_id',$owner->id)->whereIn('PJ_PRI_id', (PickRequestItem::where('PRI_OWN_id',$owner->id)->where('PRI_PR_id',$pickrequest->id)->lists('id')->toArray()))->orderBy('created_at', 'asc')->get();
		        		if(count($pickjobs) > 0)
		        		{
		        			$orders_array[$counter]['picking_start'] = Carbon::parse($pickjobs[0]['created_at'])->toDateString();

		        			$orders_array[$counter]['picking_end'] = Carbon::parse($pickjobs[count($pickjobs)-1]['created_at'])->toDateString();
		        		}

		        		$shipping = $issueorderitem->Shipping;
		        		if($shipping != null)
		        		{
		        			$orders_array[$counter]['shipping'] = Carbon::parse($shipping->created_at)->toDateString();
		        			if(Carbon::parse($shipping->created_at)->toDateString() <= $end_date)
		        				$orders_array[$counter]['completed'] = 'YES';
		        		}

		        		$counter++;      			        		
		        	}
	        	}
	        }
	    }
	    else
	    {

	        foreach ($purchaseorders as $key => $purchaseorder) {
	        	
        		$purchaseorderitems = $purchaseorder->podItems;
	        	foreach ($purchaseorderitems as $key => $purchaseorderitem) {
	        		$orders_array[$counter]['order'] = $purchaseorder->PO_number;
	        		$orders_array[$counter]['client']= $purchaseorder->Client->CLI_name;
	        		$orders_array[$counter]['site'] = $purchaseorder->Site->SITE_name;
	        		$orders_array[$counter]['type'] = 'IN';	
	        		$orders_array[$counter]['item'] = $purchaseorderitem->POI_number;
	        		$orders_array[$counter]['sku'] = $purchaseorderitem->SKU->SKU_name;
	        		$orders_array[$counter]['amount'] = $purchaseorderitem->POI_amount;
	        		$orders_array[$counter]['receiving_start'] = 'Not Received Yet';
	        		$orders_array[$counter]['receiving_end'] = 'Not Received Yet';
	        		$orders_array[$counter]['treated'] = '';
	        		$orders_array[$counter]['picking_start'] = '';
	        		$orders_array[$counter]['picking_end'] = '';
	        		$orders_array[$counter]['shipping'] = '';
	        		$orders_array[$counter]['completed'] = 'NO';

	        		$receivingjobs = ReceivingJob::where('RCJ_OWN_id',$owner->id)->where('RCJ_POI_id',$purchaseorderitem->id)->orderBy('created_at', 'asc')->get();
	        		if(count($receivingjobs) > 0)
	        		{
	        			$orders_array[$counter]['receiving_start'] = Carbon::parse($receivingjobs[0]['created_at'])->toDateString();

	        			$orders_array[$counter]['receiving_end'] = Carbon::parse($receivingjobs[count($receivingjobs)-1]['created_at'])->toDateString();

	        			if(Carbon::parse($receivingjobs[count($receivingjobs)-1]['created_at'])->toDateString() <= $end_date)
		        				$orders_array[$counter]['completed'] = 'YES';
	        		}

	        		$counter++;      			        		
	        	}	        	
	        }

	        foreach ($issueorders as $key => $issueorder) {
	        		
        		$issueorderitems = $issueorder->IssueItem;
	        	foreach ($issueorderitems as $key => $issueorderitem) {
	        		$orders_array[$counter]['order'] = $issueorder->ISO_number;
	        		$orders_array[$counter]['client']= $issueorder->Client->CLI_name;
	        		$orders_array[$counter]['site'] = $issueorder->Site->SITE_name;
	        		$orders_array[$counter]['type'] = 'OUT';
	        		$orders_array[$counter]['item'] = $issueorderitem->ISI_number;
	        		$orders_array[$counter]['sku'] = $issueorderitem->SKU->SKU_name;
	        		$orders_array[$counter]['amount'] = $issueorderitem->ISI_amount;
	        		$orders_array[$counter]['receiving_start'] = '';
	        		$orders_array[$counter]['receiving_end'] = '';
	        		$orders_array[$counter]['treated'] = 'Not Treated Yet';
	        		$orders_array[$counter]['picking_start'] = 'Not Picked Yet';
	        		$orders_array[$counter]['picking_end'] = 'Not Picked Yet';
	        		$orders_array[$counter]['shipping'] = 'Not Shipped Yet';
	        		$orders_array[$counter]['completed'] = 'NO';

	        		$pickrequest =  $issueorderitem->PickRequest;
	        		if($pickrequest != null)
	        		{
        				$orders_array[$counter]['treated'] = Carbon::parse($pickrequest->created_at)->toDateString();

        				$pickjobs = PickJob::where('PJ_OWN_id',$owner->id)->whereIn('PJ_PRI_id', (PickRequestItem::where('PRI_OWN_id',$owner->id)->where('PRI_PR_id',$pickrequest->id)->lists('id')->toArray()))->orderBy('created_at', 'asc')->get();
		        		if(count($pickjobs) > 0)
		        		{
		        			$orders_array[$counter]['picking_start'] = Carbon::parse($pickjobs[0]['created_at'])->toDateString();

		        			$orders_array[$counter]['picking_end'] = Carbon::parse($pickjobs[count($receivingjobs)-1]['created_at'])->toDateString();
		        		}
	        		}

	        		$shipping = $issueorderitem->Shipping;
	        		if($shipping != null)
	        		{
	        			$orders_array[$counter]['shipping'] = Carbon::parse($shipping->created_at)->toDateString();

	        			if(Carbon::parse($shipping->created_at)->toDateString() <= $end_date)
		        				$orders_array[$counter]['completed'] = 'YES';
	        		}

	        		$counter++;      			        		
	        	}
	        }
	    }

	    return $this->respond($orders_array);

    }

    public function getStockReport(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin()  && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to View This Page']);
        }

        $v = Validator::make($request->all(),[
            'day'=>'date_format:"Y-m-d"'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        if($request->has('day'))
        {
        	$today = Carbon::now()->toDateString();
	        if($request->day > $today)
	        {
	            return $this->badRequest(['Selected Day is Invalid']);
	        } 
	        $day = $request->day; 
        }
        else
        	$day = '8888-01-01';

        $receivingjobs = ReceivingJob::where('RCJ_OWN_id',$owner->id)->where('created_at','<',Carbon::parse($day)->addDays(1))->get();

        $shippings = Shipping::where('SHIP_OWN_id',$owner->id)->where('created_at','<',Carbon::parse($day)->addDays(1))->get();

        $unitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_amount','>',0)->get();

        $skus = SKU::where('SKU_OWN_id',$owner->id)->get();

        $sku_stock_array = array();


        if($request->has('CLI_name'))
        {
        	$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
	        if($client == null)
	            return $this->badRequest(['Client Does not Exist']);
	        //return $client;

	        $counter = 0;

	        foreach ($skus as $key => $sku) {

	        	if($sku->Client->id == $client->id)
	        	{
	        		$sku_stock_array[$counter]['SKU_name'] = $sku->SKU_name;
	        		$sku_stock_array[$counter]['CLI_name'] = $sku->Client->CLI_name;
	        		$counter_sit = 0;
	        		foreach ($owner->Site as $key => $site) {
	        			$sku_stock_array[$counter]['site'][$counter_sit]['name'] = $site->SITE_name;
	        			$sku_stock_array[$counter]['site'][$counter_sit]['amount'] = 0;
	        			foreach ($receivingjobs as $key => $receivingjob) {
	        				//return $receivingjob->PurchaseOrderItems->PurchaseOrder;
		        			if($receivingjob->PurchaseOrderItems->PurchaseOrder->Site->id == $site->id && $receivingjob->PurchaseOrderItems->PurchaseOrder->Client->id == $client->id && $receivingjob->PurchaseOrderItems->SKU->SKU_name == $sku->SKU_name)
		        			{
		        				$sku_stock_array[$counter]['site'][$counter_sit]['amount'] = intval($sku_stock_array[$counter]['site'][$counter_sit]['amount']) + intval($receivingjob->RCJ_amount);
		        			}
		        		}

		        		// foreach ($unitloads as $key => $unitload) {
	        			// 	//return $receivingjob->PurchaseOrderItems->PurchaseOrder;
		        		// 	if($unitload->Site->id == $site->id && $unitload->Client->id == $client->id && $unitload->SKU->SKU_name == $sku->SKU_name)
		        		// 	{
		        		// 		$sku_stock_array[$counter]['site'][$counter_sit]['amount'] = intval($sku_stock_array[$counter]['site'][$counter_sit]['amount']) + intval($unitload->UL_amount);
		        		// 	}
		        		// }

		        		foreach ($shippings as $key => $shipping) {
		        			if($shipping->Site->id == $site->id && $shipping->IssueItem->IssueOrder->Client->id == $client->id && $shipping->IssueItem->SKU->SKU_name == $sku->SKU_name)
		        			{
		        				$sku_stock_array[$counter]['site'][$counter_sit]['amount'] = intval($sku_stock_array[$counter]['site'][$counter_sit]['amount']) - intval($shipping->IssueItem->ISI_amount);
		        			}
		        		}
		        		$counter_sit++;
	        		}
	        		$counter++;
	        		
	        	}
	        	
	        }	        
        }


        else
        {
	        $counter = 0;
	        foreach ($skus as $key => $sku) {

	        	$sku_stock_array[$counter]['SKU_name'] = $sku->SKU_name;
        		$sku_stock_array[$counter]['CLI_name'] = $sku->Client->CLI_name;
        		$counter_sit = 0;
        		foreach ($owner->Site as $key => $site) {
        			$sku_stock_array[$counter]['site'][$counter_sit]['name'] = $site->SITE_name;
        			$sku_stock_array[$counter]['site'][$counter_sit]['amount'] = 0;
        			foreach ($receivingjobs as $key => $receivingjob) {
	        			if($receivingjob->PurchaseOrderItems->Site->id == $site->id && $receivingjob->PurchaseOrderItems->SKU->SKU_name == $sku->SKU_name)
	        			{
	        				$sku_stock_array[$counter]['site'][$counter_sit]['amount'] = intval($sku_stock_array[$counter]['site'][$counter_sit]['amount']) + intval($receivingjob->RCJ_amount);

	        			}
	        		}

	        		// foreach ($unitloads as $key => $unitload) {
	        		// 		//return $receivingjob->PurchaseOrderItems->PurchaseOrder;
		        	// 		if($unitload->Site->id == $site->id && $unitload->SKU->SKU_name == $sku->SKU_name)
		        	// 		{
		        	// 			$sku_stock_array[$counter]['site'][$counter_sit]['amount'] = intval($sku_stock_array[$counter]['site'][$counter_sit]['amount']) + intval($unitload->UL_amount);
		        	// 		}
		        	// 	}


	        		foreach ($shippings as $key => $shipping) {
	        			if($shipping->Site->id == $site->id && $shipping->IssueItem->SKU->SKU_name == $sku->SKU_name)
	        			{
	        				$sku_stock_array[$counter]['site'][$counter_sit]['amount'] = intval($sku_stock_array[$counter]['site'][$counter_sit]['amount']) - intval($shipping->IssueItem->ISI_amount);

	        			}
	        		}
	        		$counter_sit++;
        		}        		
        		$counter++;	        	
	        }
	               
        }
        return $this->respond($sku_stock_array);
    }

    public function getMinTransactionReport(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to View This Page']);
        }

        $v = Validator::make($request->all(),[
            'start_date'=>'required|date_format:"Y-m-d"',
            'end_date'=>'required|date_format:"Y-m-d"'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $today = Carbon::now()->toDateString();
        if($request->start_date > $today)
        {
            return $this->badRequest(['Start Date is Invalid']);
        } 
        if($request->end_date > $today)
        {
            return $this->badRequest(['End Date is Invalid']);
        }
        if($request->end_date < $request->start_date)
        {
            return $this->badRequest(['End Date is Before Start Date']);
        }
        $purchaseorders = PurchaseOrder::where('PO_OWN_id',$owner->id)->where('created_at','>=',$request->start_date)->where('created_at','<',Carbon::parse($request->end_date)->addDays(1))->get();
        $receivingjobs = ReceivingJob::where('RCJ_OWN_id',$owner->id)->where('created_at','>=',$request->start_date)->where('created_at','<',Carbon::parse($request->end_date)->addDays(1))->get();
        $storagejobs = StorageJob::where('STJ_OWN_id',$owner->id)->where('created_at','>=',$request->start_date)->where('created_at','<',Carbon::parse($request->end_date)->addDays(1))->get();
        $issueorders = IssueOrder::where('ISO_OWN_id',$owner->id)->where('created_at','>=',$request->start_date)->where('created_at','<',Carbon::parse($request->end_date)->addDays(1))->get();
        $pickrequests = PickRequest::where('PR_OWN_id',$owner->id)->where('created_at','>=',$request->start_date)->where('created_at','<',Carbon::parse($request->end_date)->addDays(1))->get();
        $pickjobs = PickJob::where('PJ_OWN_id',$owner->id)->where('created_at','>=',$request->start_date)->where('created_at','<',Carbon::parse($request->end_date)->addDays(1))->get();
        $shippings = Shipping::where('SHIP_OWN_id',$owner->id)->where('created_at','>=',$request->start_date)->where('created_at','<',Carbon::parse($request->end_date)->addDays(1))->get();


        $date = $request->start_date;
        $transactions_array = array();
        $counter = 0;
        while ($date < $request->end_date) {
        	$transactions_array[$counter]['date'] = $date;
        	$transactions_array[$counter]['purchaseorders'] = 0;
        	$transactions_array[$counter]['receivingjobs'] = 0;
        	$transactions_array[$counter]['storagejobs'] = 0;
        	$transactions_array[$counter]['issueorders'] = 0;
        	$transactions_array[$counter]['pickrequests'] = 0;
        	$transactions_array[$counter]['pickjobs'] = 0;
        	$transactions_array[$counter]['shippings'] = 0;

        	if($request->has('CLI_name') && $request->has('SKU_name') && $request->has('SITE_name'))
	        {
	        	$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
		        if($client == null)
		            return $this->badRequest(['Client Does not Exist']);
		        $sku = SKU::where('SKU_OWN_id',$owner->id)->where('SKU_name',$request['SKU_name'])->first();
		        if($sku == null)
		            return $this->badRequest(['SKU Does not Exist']);
		        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
		        if($site == null)
		            return $this->badRequest(['Site Does not Exist']);

		        foreach ($purchaseorders as $key => $purchaseorder) {
		        	if($purchaseorder->Client->id == $client->id && $purchaseorder->Site->id == $site->id && Carbon::parse($purchaseorder->created_at)->toDateString() == $date)
		        		foreach ($purchaseorder->podItems as $key => $purchaseorderitem) {
		        			if($purchaseorderitem->SKU->id = $sku->id)
		        			{
		        				$transactions_array[$counter]['purchaseorders'] = intval($transactions_array[$counter]['purchaseorders']) + 1;
		        				break;
		        			}
		        		}		        			
		        }
		        foreach ($receivingjobs as $key => $receivingjob) {
		        	if($receivingjob->PurchaseOrderItems->PurchaseOrder->Client->id == $client->id && $receivingjob->PurchaseOrderItems->PurchaseOrder->Site->id == $site->id && Carbon::parse($receivingjob->created_at)->toDateString() == $date)
		        		if($receivingjob->PurchaseOrderItems->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['receivingjobs'] = intval($transactions_array[$counter]['receivingjobs']) + 1;
	        				break;
	        			}		        			
		        }
		        foreach ($storagejobs as $key => $storagejob) {
		        	if($storagejob->Unitload->Client->id == $client->id && $storagejob->Unitload->Site->id == $site->id && Carbon::parse($storagejob->created_at)->toDateString() == $date)
		        		if($storagejob->Unitload->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['storagejobs'] = intval($transactions_array[$counter]['storagejobs']) + 1;
	        				break;
	        			}
		        		
		        }
		        foreach ($issueorders as $key => $issueorder) {
		        	if($issueorder->Client->id == $client->id && $issueorder->Site->id == $site->id && Carbon::parse($issueorder->created_at)->toDateString() == $date)
		        		foreach ($issueorder->IssueItem as $key => $issueorderitem) {
		        			if($issueorderitem->SKU->id == $sku->id)
		        			{
		        				$transactions_array[$counter]['issueorders'] = intval($transactions_array[$counter]['issueorders']) + 1;
		        				break;
		        			}
		        		}
		        		
		        }
		        foreach ($pickrequests as $key => $pickrequest) {
		        	if($pickrequest->IssueItem->IssueOrder->Client->id == $client->id && $pickrequest->IssueItem->IssueOrder->Site->id == $site->id && Carbon::parse($pickrequest->created_at)->toDateString() == $date)
		        		if($pickrequest->IssueItem->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['pickrequests'] = intval($transactions_array[$counter]['pickrequests']) + 1;
	        				break;
	        			}		        		
		        }
		        foreach ($pickjobs as $key => $pickjob) {
		        	if($pickjob->Unitload->Client->id == $client->id && $pickjob->Unitload->Site->id == $site->id && Carbon::parse($pickjob->created_at)->toDateString() == $date)
		        		if($pickjob->Unitload->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['pickjobs'] = intval($transactions_array[$counter]['pickjobs']) + 1;
	        				break;
	        			}
		        		
		        }
		        foreach ($shippings as $key => $shipping) {
		        	if($shipping->IssueItem->IssueOrder->Client->id == $client->id && $shipping->IssueItem->IssueOrder->Site->id == $site->id && Carbon::parse($shipping->created_at)->toDateString() == $date)
		        		if($shipping->IssueItem->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['shippings'] = intval($transactions_array[$counter]['shippings']) + 1;
	        				break;
	        			}		        		
		        }
		    }

        	elseif($request->has('CLI_name') && !$request->has('SKU_name') && $request->has('SITE_name'))
	        {
	        	$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
		        if($client == null)
		            return $this->badRequest(['Client Does not Exist']);
		        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
		        if($site == null)
		            return $this->badRequest(['Site Does not Exist']);

		        foreach ($purchaseorders as $key => $purchaseorder) {
		        	if($purchaseorder->Client->id == $client->id && $purchaseorder->Site->id == $site->id && Carbon::parse($purchaseorder->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['purchaseorders'] = intval($transactions_array[$counter]['purchaseorders']) + 1;	        			
		        }
		        foreach ($receivingjobs as $key => $receivingjob) {
		        	if($receivingjob->PurchaseOrderItems->PurchaseOrder->Client->id == $client->id && $receivingjob->PurchaseOrderItems->PurchaseOrder->Site->id == $site->id && Carbon::parse($receivingjob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['receivingjobs'] = intval($transactions_array[$counter]['receivingjobs']) + 1;	        					        			
		        }
		        foreach ($storagejobs as $key => $storagejob) {
		        	if($storagejob->Unitload->Client->id == $client->id && $storagejob->Unitload->Site->id == $site->id && Carbon::parse($storagejob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['storagejobs'] = intval($transactions_array[$counter]['storagejobs']) + 1;
	        				
		        		
		        }
		        foreach ($issueorders as $key => $issueorder) {
		        	if($issueorder->Client->id == $client->id && $issueorder->Site->id == $site->id && Carbon::parse($issueorder->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['issueorders'] = intval($transactions_array[$counter]['issueorders']) + 1;		        				
		        		
		        }
		        foreach ($pickrequests as $key => $pickrequest) {
		        	if($pickrequest->IssueItem->IssueOrder->Client->id == $client->id && $pickrequest->IssueItem->IssueOrder->Site->id == $site->id && Carbon::parse($pickrequest->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['pickrequests'] = intval($transactions_array[$counter]['pickrequests']) + 1;
	        					        		
		        }
		        foreach ($pickjobs as $key => $pickjob) {
		        	if($pickjob->Unitload->Client->id == $client->id && $pickjob->Unitload->Site->id == $site->id && Carbon::parse($pickjob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['pickjobs'] = intval($transactions_array[$counter]['pickjobs']) + 1;
	        				
		        		
		        }
		        foreach ($shippings as $key => $shipping) {
		        	if($shipping->IssueItem->IssueOrder->Client->id == $client->id && $shipping->IssueItem->IssueOrder->Site->id == $site->id && Carbon::parse($shipping->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['shippings'] = intval($transactions_array[$counter]['shippings']) + 1;
	        					        		
		        }
		    }

        	elseif($request->has('CLI_name') && $request->has('SKU_name') && !$request->has('SITE_name'))
	        {
	        	$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
		        if($client == null)
		            return $this->badRequest(['Client Does not Exist']);
		        $sku = SKU::where('SKU_OWN_id',$owner->id)->where('SKU_name',$request['SKU_name'])->first();
		        if($sku == null)
		            return $this->badRequest(['SKU Does not Exist']);

		        foreach ($purchaseorders as $key => $purchaseorder) {
		        	if($purchaseorder->Client->id == $client->id && Carbon::parse($purchaseorder->created_at)->toDateString() == $date)
		        		foreach ($purchaseorder->podItems as $key => $purchaseorderitem) {
		        			if($purchaseorderitem->SKU->id == $sku->id)
		        			{
		        				$transactions_array[$counter]['purchaseorders'] = intval($transactions_array[$counter]['purchaseorders']) + 1;
		        				break;
		        			}
		        		}		        			
		        }
		        foreach ($receivingjobs as $key => $receivingjob) {
		        	if($receivingjob->PurchaseOrderItems->PurchaseOrder->Client->id == $client->id && Carbon::parse($receivingjob->created_at)->toDateString() == $date)
		        		if($receivingjob->PurchaseOrderItems->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['receivingjobs'] = intval($transactions_array[$counter]['receivingjobs']) + 1;
	        				break;
	        			}		        			
		        }
		        foreach ($storagejobs as $key => $storagejob) {
		        	if($storagejob->Unitload->Client->id == $client->id && Carbon::parse($storagejob->created_at)->toDateString() == $date)
		        		if($storagejob->Unitload->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['storagejobs'] = intval($transactions_array[$counter]['storagejobs']) + 1;
	        				break;
	        			}
		        		
		        }
		        foreach ($issueorders as $key => $issueorder) {
		        	if($issueorder->Client->id == $client->id && Carbon::parse($issueorder->created_at)->toDateString() == $date)
		        		foreach ($issueorder->IssueItem as $key => $issueorderitem) {
		        			if($issueorderitem->SKU->id == $sku->id)
		        			{
		        				$transactions_array[$counter]['issueorders'] = intval($transactions_array[$counter]['issueorders']) + 1;
		        				break;
		        			}
		        		}
		        		
		        }
		        foreach ($pickrequests as $key => $pickrequest) {
		        	if($pickrequest->IssueItem->IssueOrder->Client->id == $client->id && Carbon::parse($pickrequest->created_at)->toDateString() == $date)
		        		if($pickrequest->IssueItem->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['pickrequests'] = intval($transactions_array[$counter]['pickrequests']) + 1;
	        				break;
	        			}		        		
		        }
		        foreach ($pickjobs as $key => $pickjob) {
		        	if($pickjob->Unitload->Client->id == $client->id && Carbon::parse($pickjob->created_at)->toDateString() == $date)
		        		if($pickjob->Unitload->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['pickjobs'] = intval($transactions_array[$counter]['pickjobs']) + 1;
	        				break;
	        			}
		        		
		        }
		        foreach ($shippings as $key => $shipping) {
		        	if($shipping->IssueItem->IssueOrder->Client->id == $client->id && Carbon::parse($shipping->created_at)->toDateString() == $date)
		        		if($shipping->IssueItem->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['shippings'] = intval($transactions_array[$counter]['shippings']) + 1;
	        				break;
	        			}		        		
		        }
		    }

        	elseif($request->has('CLI_name') && !$request->has('SKU_name') && !$request->has('SITE_name'))
	        {
	        	$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
		        if($client == null)
		            return $this->badRequest(['Client Does not Exist']);

		        foreach ($purchaseorders as $key => $purchaseorder) {
		        	if($purchaseorder->Client->id == $client->id && Carbon::parse($purchaseorder->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['purchaseorders'] = intval($transactions_array[$counter]['purchaseorders']) + 1;
		        				        			
		        }
		        foreach ($receivingjobs as $key => $receivingjob) {
		        	if($receivingjob->PurchaseOrderItems->PurchaseOrder->Client->id == $client->id && Carbon::parse($receivingjob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['receivingjobs'] = intval($transactions_array[$counter]['receivingjobs']) + 1;
	        					        			
		        }
		        foreach ($storagejobs as $key => $storagejob) {
		        	if($storagejob->Unitload->Client->id == $client->id && Carbon::parse($storagejob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['storagejobs'] = intval($transactions_array[$counter]['storagejobs']) + 1;
		        		
		        }
		        foreach ($issueorders as $key => $issueorder) {
		        	if($issueorder->Client->id == $client->id && Carbon::parse($issueorder->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['issueorders'] = intval($transactions_array[$counter]['issueorders']) + 1;
		        				
		        		
		        }
		        foreach ($pickrequests as $key => $pickrequest) {
		        	if($pickrequest->IssueItem->IssueOrder->Client->id == $client->id && Carbon::parse($pickrequest->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['pickrequests'] = intval($transactions_array[$counter]['pickrequests']) + 1;
	        					        		
		        }
		        foreach ($pickjobs as $key => $pickjob) {
		        	if($pickjob->Unitload->Client->id == $client->id && Carbon::parse($pickjob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['pickjobs'] = intval($transactions_array[$counter]['pickjobs']) + 1;
		        		
		        }
		        foreach ($shippings as $key => $shipping) {
		        	if($shipping->IssueItem->IssueOrder->Client->id == $client->id &&Carbon::parse($shipping->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['shippings'] = intval($transactions_array[$counter]['shippings']) + 1;
	        					        		
		        }
		    }

        	elseif(!$request->has('CLI_name') && $request->has('SKU_name') && $request->has('SITE_name'))
	        {
		        $sku = SKU::where('SKU_OWN_id',$owner->id)->where('SKU_name',$request['SKU_name'])->first();
		        if($sku == null)
		            return $this->badRequest(['SKU Does not Exist']);
		        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
		        if($site == null)
		            return $this->badRequest(['Site Does not Exist']);

		        foreach ($purchaseorders as $key => $purchaseorder) {
		        	if($purchaseorder->Site->id == $site->id && Carbon::parse($purchaseorder->created_at)->toDateString() == $date)
		        		foreach ($purchaseorder->podItems as $key => $purchaseorderitem) {
		        			if($purchaseorderitem->SKU->id == $sku->id)
		        			{
		        				$transactions_array[$counter]['purchaseorders'] = intval($transactions_array[$counter]['purchaseorders']) + 1;
		        				break;
		        			}
		        		}		        			
		        }
		        foreach ($receivingjobs as $key => $receivingjob) {
		        	if($receivingjob->PurchaseOrderItems->PurchaseOrder->Site->id == $site->id && Carbon::parse($receivingjob->created_at)->toDateString() == $date)
		        		if($receivingjob->PurchaseOrderItems->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['receivingjobs'] = intval($transactions_array[$counter]['receivingjobs']) + 1;
	        				break;
	        			}		        			
		        }
		        foreach ($storagejobs as $key => $storagejob) {
		        	if($storagejob->Unitload->Site->id == $site->id && Carbon::parse($storagejob->created_at)->toDateString() == $date)
		        		if($storagejob->Unitload->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['storagejobs'] = intval($transactions_array[$counter]['storagejobs']) + 1;
	        				break;
	        			}
		        		
		        }
		        foreach ($issueorders as $key => $issueorder) {
		        	if($issueorder->Site->id == $site->id && Carbon::parse($issueorder->created_at)->toDateString() == $date)
		        		foreach ($issueorder->IssueItem as $key => $issueorderitem) {
		        			if($issueorderitem->SKU->id == $sku->id)
		        			{
		        				$transactions_array[$counter]['issueorders'] = intval($transactions_array[$counter]['issueorders']) + 1;
		        				break;
		        			}
		        		}
		        		
		        }
		        foreach ($pickrequests as $key => $pickrequest) {
		        	if($pickrequest->IssueItem->IssueOrder->Site->id == $site->id && Carbon::parse($pickrequest->created_at)->toDateString() == $date)
		        		if($pickrequest->IssueItem->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['pickrequests'] = intval($transactions_array[$counter]['pickrequests']) + 1;
	        				break;
	        			}		        		
		        }
		        foreach ($pickjobs as $key => $pickjob) {
		        	if($pickjob->Unitload->Site->id == $site->id && Carbon::parse($pickjob->created_at)->toDateString() == $date)
		        		if($pickjob->Unitload->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['pickjobs'] = intval($transactions_array[$counter]['pickjobs']) + 1;
	        				break;
	        			}
		        		
		        }
		        foreach ($shippings as $key => $shipping) {
		        	if($shipping->IssueItem->IssueOrder->Site->id == $site->id && Carbon::parse($shipping->created_at)->toDateString() == $date)
		        		if($shipping->IssueItem->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['shippings'] = intval($transactions_array[$counter]['shippings']) + 1;
	        				break;
	        			}		        		
		        }
		    }

        	elseif(!$request->has('CLI_name') && !$request->has('SKU_name') && $request->has('SITE_name'))
	        {
		        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
		        if($site == null)
		            return $this->badRequest(['Site Does not Exist']);

		        foreach ($purchaseorders as $key => $purchaseorder) {
		        	if($purchaseorder->Site->id == $site->id && Carbon::parse($purchaseorder->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['purchaseorders'] = intval($transactions_array[$counter]['purchaseorders']) + 1;
		        				        			
		        }
		        foreach ($receivingjobs as $key => $receivingjob) {
		        	if($receivingjob->PurchaseOrderItems->PurchaseOrder->Site->id == $site->id && Carbon::parse($receivingjob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['receivingjobs'] = intval($transactions_array[$counter]['receivingjobs']) + 1;
	        					        			
		        }
		        foreach ($storagejobs as $key => $storagejob) {
		        	if($storagejob->Unitload->Site->id == $site->id && Carbon::parse($storagejob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['storagejobs'] = intval($transactions_array[$counter]['storagejobs']) + 1;
	        				
		        		
		        }
		        foreach ($issueorders as $key => $issueorder) {
		        	if($issueorder->Site->id == $site->id && Carbon::parse($issueorder->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['issueorders'] = intval($transactions_array[$counter]['issueorders']) + 1;
		        		
		        }
		        foreach ($pickrequests as $key => $pickrequest) {
		        	if($pickrequest->IssueItem->IssueOrder->Site->id == $site->id && Carbon::parse($pickrequest->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['pickrequests'] = intval($transactions_array[$counter]['pickrequests']) + 1;
	        					        		
		        }
		        foreach ($pickjobs as $key => $pickjob) {
		        	if($pickjob->Unitload->Site->id == $site->id && Carbon::parse($pickjob->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['pickjobs'] = intval($transactions_array[$counter]['pickjobs']) + 1;
		        		
		        }
		        foreach ($shippings as $key => $shipping) {
		        	if($shipping->IssueItem->IssueOrder->Site->id == $site->id && Carbon::parse($shipping->created_at)->toDateString() == $date)
		        			$transactions_array[$counter]['shippings'] = intval($transactions_array[$counter]['shippings']) + 1;
	        					        		
		        }
		    }

        	elseif(!$request->has('CLI_name') && $request->has('SKU_name') && !$request->has('SITE_name'))
	        {
		        $sku = SKU::where('SKU_OWN_id',$owner->id)->where('SKU_name',$request['SKU_name'])->first();
		        if($sku == null)
		            return $this->badRequest(['SKU Does not Exist']);

		        foreach ($purchaseorders as $key => $purchaseorder) {
		        	if(Carbon::parse($purchaseorder->created_at)->toDateString() == $date)
		        		foreach ($purchaseorder->podItems as $key => $purchaseorderitem) {
		        			if($purchaseorderitem->SKU->id == $sku->id)
		        			{
		        				$transactions_array[$counter]['purchaseorders'] = intval($transactions_array[$counter]['purchaseorders']) + 1;
		        				break;
		        			}
		        		}		        			
		        }
		        foreach ($receivingjobs as $key => $receivingjob) {
		        	if(Carbon::parse($receivingjob->created_at)->toDateString() == $date)
		        		if($receivingjob->PurchaseOrderItems->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['receivingjobs'] = intval($transactions_array[$counter]['receivingjobs']) + 1;
	        				break;
	        			}		        			
		        }
		        foreach ($storagejobs as $key => $storagejob) {
		        	if(Carbon::parse($storagejob->created_at)->toDateString() == $date)
		        		if($storagejob->Unitload->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['storagejobs'] = intval($transactions_array[$counter]['storagejobs']) + 1;
	        				break;
	        			}
		        		
		        }
		        foreach ($issueorders as $key => $issueorder) {
		        	if(Carbon::parse($issueorder->created_at)->toDateString() == $date)
		        		foreach ($issueorder->IssueItem as $key => $issueorderitem) {
		        			if($issueorderitem->SKU->id == $sku->id)
		        			{
		        				$transactions_array[$counter]['issueorders'] = intval($transactions_array[$counter]['issueorders']) + 1;
		        				break;
		        			}
		        		}
		        		
		        }
		        foreach ($pickrequests as $key => $pickrequest) {
		        	if(Carbon::parse($pickrequest->created_at)->toDateString() == $date)
		        		if($pickrequest->IssueItem->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['pickrequests'] = intval($transactions_array[$counter]['pickrequests']) + 1;
	        				break;
	        			}		        		
		        }
		        foreach ($pickjobs as $key => $pickjob) {
		        	if(Carbon::parse($pickjob->created_at)->toDateString() == $date)
		        		if($pickjob->Unitload->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['pickjobs'] = intval($transactions_array[$counter]['pickjobs']) + 1;
	        				break;
	        			}
		        		
		        }
		        foreach ($shippings as $key => $shipping) {
		        	if(Carbon::parse($shipping->created_at)->toDateString() == $date)
		        		if($shipping->IssueItem->SKU->id == $sku->id)
	        			{
	        				$transactions_array[$counter]['shippings'] = intval($transactions_array[$counter]['shippings']) + 1;
	        				break;
	        			}		        		
		        }
		    }
		    else
		    {
		        foreach ($purchaseorders as $key => $purchaseorder) {
		        	if(Carbon::parse($purchaseorder->created_at)->toDateString() == $date)
		        		$transactions_array[$counter]['purchaseorders'] = intval($transactions_array[$counter]['purchaseorders']) + 1;	
		        }
		        foreach ($receivingjobs as $key => $receivingjob) {
		        	if(Carbon::parse($receivingjob->created_at)->toDateString() == $date)
		        		$transactions_array[$counter]['receivingjobs'] = intval($transactions_array[$counter]['receivingjobs']) + 1;	
		        }
		        foreach ($storagejobs as $key => $storagejob) {
		        	if(Carbon::parse($storagejob->created_at)->toDateString() == $date)
		        		$transactions_array[$counter]['storagejobs'] = intval($transactions_array[$counter]['storagejobs']) + 1;
		        }
		        foreach ($issueorders as $key => $issueorder) {
		        	if(Carbon::parse($issueorder->created_at)->toDateString() == $date)
		        		$transactions_array[$counter]['issueorders'] = intval($transactions_array[$counter]['issueorders']) + 1;
		        }
		        foreach ($pickrequests as $key => $pickrequest) {
		        	if(Carbon::parse($pickrequest->created_at)->toDateString() == $date)
		        		$transactions_array[$counter]['pickrequests'] = intval($transactions_array[$counter]['pickrequests']) + 1;
		        }
		        foreach ($pickjobs as $key => $pickjob) {
		        	if(Carbon::parse($pickjob->created_at)->toDateString() == $date)
		        		$transactions_array[$counter]['pickjobs'] = intval($transactions_array[$counter]['pickjobs']) + 1;
		        }
		        foreach ($shippings as $key => $shipping) {
		        	if(Carbon::parse($shipping->created_at)->toDateString() == $date)
		        		$transactions_array[$counter]['shippings'] = intval($transactions_array[$counter]['shippings']) + 1;
		        }
		    }
		    $counter++;
        	$date = Carbon::parse($date)->addDays(1)->toDateString();
        }

        return $this->respond($transactions_array);
    }

    public function getOrderReport(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to View This Page']);
        }

        $v = Validator::make($request->all(),[
            'start_date'=>'date_format:"Y-m-d"',
            'end_date'=>'date_format:"Y-m-d"',
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $today = Carbon::now()->toDateString();
        if($request->has('start_date'))
        {
        	$today = Carbon::now()->toDateString();
	        if($request->start_date > $today)
	        {
	            return $this->badRequest(['Selected Day is Invalid']);
	        } 
	        $start_date = $request->start_date; 
        }
        else
        	$start_date = '1111-01-01';

        if($request->has('end_date'))
        {
        	$today = Carbon::now()->toDateString();
	        if($request->end_date > $today)
	        {
	            return $this->badRequest(['Selected Day is Invalid']);
	        } 
	        $end_date = $request->end_date; 
        }
        else
        	$end_date = '8888-01-01';
        
        if($end_date < $start_date)
        {
            return $this->badRequest(['End Date is Before Start Date']);
        }

        $purchaseorders = PurchaseOrder::where('PO_OWN_id',$owner->id)->where('created_at','>=',$start_date)->where('created_at','<',Carbon::parse($end_date)->addDays(1))->get();

        $issueorders = IssueOrder::where('ISO_OWN_id',$owner->id)->where('created_at','>=',$start_date)->where('created_at','<',Carbon::parse($end_date)->addDays(1))->get();

        $orders_array = array();
        $counter = 0;

        if($request->has('CLI_name'))
        {
        	$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['CLI_name'])->first();
	        if($client == null)
	            return $this->badRequest(['Client Does not Exist']);

	        foreach ($purchaseorders as $key => $purchaseorder) {
	        	if($purchaseorder->Client->id == $client->id)
	        	{
	        		$purchaseorderitems = $purchaseorder->podItems;
		        	foreach ($purchaseorderitems as $key => $purchaseorderitem) {
		        		if($purchaseorderitem->POI_receivedamount < $purchaseorderitem->POI_amount)
		        		{
		        			$orders_array[$counter]['order'] = $purchaseorder->PO_number;
	        				$orders_array[$counter]['orderitems'] = $purchaseorder->podItems;
			        		$orders_array[$counter]['client']= $purchaseorder->Client->CLI_name;
			        		$orders_array[$counter]['site'] = $purchaseorder->Site->SITE_name;
			        		$orders_array[$counter]['type'] = 'IN';	
			        		$counter++;
			        		break;
		        		}		        			        		
		        	}
	        	}	        	
	        }

	        foreach ($issueorders as $key => $issueorder) {
	        	if($issueorder->Client->id = $client->id)
	        	{
	        		$issueorderitems = $issueorder->IssueItem;
	        		foreach ($issueorderitems as $key => $issueorderitem) {
	        			if($issueorderitem->ISI_shipped < $issueorderitem->ISI_amount)
	        			{
	        				$orders_array[$counter]['order'] = $issueorder->ISO_number;
        					$orders_array[$counter]['orderitems'] = $issueorder->IssueItem;
			        		$orders_array[$counter]['client']=$issueorder->Client->CLI_name;
			        		$orders_array[$counter]['site'] = $issueorder->Site->SITE_name;
			        		$orders_array[$counter]['type'] = 'OUT';	
			        		$counter++;
			        		break;	 
	        			}
	        		}
	        	}
	        }
	    }
	    else
	    {
	    	foreach ($purchaseorders as $key => $purchaseorder) {
	        	$purchaseorderitems = $purchaseorder->podItems;
	        	foreach ($purchaseorderitems as $key => $purchaseorderitem) {
	        		if($purchaseorderitem->POI_receivedamount < $purchaseorderitem->POI_amount)
	        		{
	        			$orders_array[$counter]['order'] = $purchaseorder->PO_number;
	        			$orders_array[$counter]['orderitems'] = $purchaseorder->podItems;
		        		$orders_array[$counter]['client']= $purchaseorder->Client->CLI_name;
		        		$orders_array[$counter]['site'] = $purchaseorder->Site->SITE_name;
		        		$orders_array[$counter]['type'] = 'IN';	
		        		$counter++;
		        		break;
	        		}		        			        		
	        	}	        	
	        }

	        foreach ($issueorders as $key => $issueorder) {
	        	$issueorderitems = $issueorder->IssueItem;
        		foreach ($issueorderitems as $key => $issueorderitem) {
        			if($issueorderitem->ISI_shipped < $issueorderitem->ISI_amount)
        			{
        				$orders_array[$counter]['order'] = $issueorder->ISO_number;
        				$orders_array[$counter]['orderitems'] = $issueorder->IssueItem;
		        		$orders_array[$counter]['client']=$issueorder->Client->CLI_name;
		        		$orders_array[$counter]['site'] = $issueorder->Site->SITE_name;
		        		$orders_array[$counter]['type'] = 'OUT';	
		        		$counter++;	
		        		break; 
        			}
        		}
	        }
	    }

	    return $this->respond($orders_array);

    }

    public function getAllData()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to View This Page']);
        }
        $clients = $owner->Client;
        $skus = $owner->SKU;
        $sites = $owner->Site;

        $Data = array();

        $client_names = array();
        $counter = 0;
        foreach ($clients as $key => $client) {
            $client_names[$counter] = $client->CLI_name;
            $counter++;
        }

        $site_names = array();
    	$counter = 0;
    	foreach ($sites as $key => $site) {
    		$site_names[$counter] = $site->SITE_name;
    		$counter++;
    	}

    	$sku_names = array();
    	$counter = 0;
    	foreach ($skus as $key => $sku) {
    		$sku_names[$counter] = $sku->SKU_name;
    		$counter++;
    	}

        $Data['client_names'] = $client_names;
    	$Data['site_names'] = $site_names;
    	$Data['sku_names'] = $sku_names;

    	return $this->respond($Data);

    }
}
