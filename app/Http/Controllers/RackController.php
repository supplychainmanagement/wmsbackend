<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\Zone;
use App\Site;
use App\Rack;
use App\StorageLocation;
use App\StorageLocationType;
use App\FunctionalArea;
use DB;

class RackController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }
    /**
		 * View Racks
		 * @param none
		 * @return Racks Based on Permissions
		 */
    public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$RackData =array();

        $rackdata = array();
        if($user->isSystemClient()){
            $RackData['systemClient']=true;
            $RackData['StorageLocationType'] = $owner->StorageLocationType->all();
            $RackData['FunctionalArea'] = $owner->FunctionalArea->all();
            if($user->isSystemAdmin())
        	{
        		$zones = $owner->Zone;
        		$clients = $owner->Client;
        		$sites = $owner->Site;
        		$racks = $owner->Rack;
        		$RackData['zones']=$zones->all();
        		$counter =0;
                foreach ($racks as $key => $value) {
                    $rackdata[$counter]['rack'] = $value;
                    $rackdata[$counter]['zone_name'] = $value->zone->ZONE_name;
                    $rackdata[$counter]['site_name'] = $value->zone->site->SITE_name;
                    $counter++;
                }
//        		$RackData['racks']=$racks->all();
                $RackData['racks']=$rackdata;
        		$RackData['clients']=$clients->all();
        		$RackData['sites']= $sites->all();
        		$RackData['permission'] = true;
				$RackData['siteBased']=false;
				$RackData['role'] = "systemAdmin";
                $RackData['storagelocationtypes'] = $owner->StorageLocationType->all();
                $RackData['functionalareas'] = $owner->FunctionalArea->all();
        	}
        	elseif($user->isSiteAdmin())
        	{
        		$site = $user->Site;
        		$zones = $site->Zone;
                $racks = array();
                foreach ($zones as $key => $zone) {
                    $racksInZone = Rack::where('RACK_ZONE_id',$zone->id)->get();
                    foreach ($racksInZone as $key => $rackInZone) {
                        array_push($racks,$rackInZone);
                    }
                }
//        		$RackData['racks']=$racks->all();
                $RackData['sites'] = [$site];
        		$clients = $owner->Client;
        		$RackData['zones']=$zones->all();
        		$RackData['clients']=$clients->all();
                $counter =0;
                foreach ($racks as $key => $value) {
                    $rackdata[$counter]['rack'] = $value;
                    $rackdata[$counter]['zone_name'] = $value->zone->ZONE_name;
                    $rackdata[$counter]['site_name'] = $value->zone->site->SITE_name;
                    $counter++;
                }
        		$RackData['racks']=$rackdata;
                $RackData['storagelocationtypes'] = $owner->StorageLocationType->all();
                $RackData['functionalareas'] = $owner->FunctionalArea->all();
        		$RackData['permission'] = true;
				$RackData['siteBased'] = true;
				$RackData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$site = $user->Site;
        		$zones = $site->Zone;
                $racks = array();
                foreach ($zones as $key => $zone) {
                    $racksInZone = Rack::where('RACK_ZONE_id',$zone->id)->get();
                    foreach ($racksInZone as $key => $rackInZone) {
                        array_push($racks,$rackInZone);
                    }
                }
//        		$RackData['racks']=$racks->all();
                $counter =0;
                foreach ($racks as $key => $value) {
                    $rackdata[$counter]['rack'] = $value;
                    $rackdata[$counter]['zone_name'] = $value->zone->ZONE_name;
                    $rackdata[$counter]['site_name'] = $value->zone->site->SITE_name;
                    $counter++;
                }
        		$RackData['racks']=$rackdata;
                $RackData['storagelocationtypes'] = $owner->StorageLocationType->all();
                $RackData['functionalareas'] = $owner->FunctionalArea->all();
                $RackData['sites'] =[$site];
        		$RackData['zones']=$zones->all();
        		$RackData['permission'] = false;
				$RackData['siteBased'] = true;
                $RackData['clients']=$owner->Client->all();
				$RackData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$site = $user->Site;
				$zones = $site->Zone;
				$racks = array();
                foreach ($zones as $key => $zone) {
                    $racksInZone = Rack::where('RACK_ZONE_id',$zone->id)->get();
                    foreach ($racksInZone as $key => $rackInZone) {
                        array_push($racks,$rackInZone);
                    }
                }
//				$RackData['racks']=$racks->all();
                $RackData['sites'] =[$site];
                $RackData['clients']=$owner->Client->all();
                $counter =0;
                foreach ($racks as $key => $value) {
                    $rackdata[$counter]['rack'] = $value;
                    $rackdata[$counter]['zone_name'] = $value->zone->ZONE_name;
                    $rackdata[$counter]['site_name'] = $value->zone->site->SITE_name;
                    $counter++;
                }
				$RackData['racks']=$rackdata;
                $RackData['storagelocationtypes'] = $owner->StorageLocationType->all();
                $RackData['functionalareas'] = $owner->FunctionalArea->all();
				$RackData['zones']=$zones->all();
				$RackData['permission'] = false;
				$RackData['siteBased'] = true;
				$RackData['role'] ="labour";
			}
        }
        
   	 	return $this->respond($RackData);
        
        
    }
    /**
     * Create new Rack
     * @param Attributes of Rack to be Created
     * @return Approval of Creation
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'RACK_name'=>'required','ZONE_name'=>'required','RACK_column'=>'required|integer|min:1',
            'RACK_row'=>'required|integer|min:1','RACK_depth'=>'required|integer|min:1','STL_Type_name'=>'required','FA_name'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $name = $request['RACK_name'];
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        if(!$user->isSystemAdmin())
        {
             $siteid = $user->site->id;
            $zone = Zone::where('ZONE_OWN_id',$owner->id)->where('ZONE_name',$request->ZONE_name)->where('ZONE_SITE_id',$siteid)->first();
        }
        else if($user->isSystemAdmin())
        {
            if($request->has('SITE_name'))
            {
                 $sitename = $request['SITE_name'];
                $siteid = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$sitename)->first()->id;
                $zone = Zone::where('ZONE_OWN_id',$owner->id)->where('ZONE_name',$request->ZONE_name)->where('ZONE_SITE_id',$siteid)->first();
            }
            else
            {
                return $this->badRequest(['Site name Not Sent']);
            }
           
        }
        $zone_id = $zone->id;
        $site = $zone->Site;

        $site_id = $site->id;
        
        if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin())))
            return $this->unauthorized(["you don't have the permission to update a rack"]);
        else
        {
            // if($user->isSystemAdmin())
            // {
            //     if(!$request->has('SITE_name'))
            //     {
            //         return $this->badRequest(['Site is Missing']);
            //     }else{
            //         $site_name = $request['SITE_name'];
            //         $site = Site::where('SITE_OWN_id',$owner->id)->
            //         where('SITE_name',$site_name)->first();
            //         //return $site;
            //         $zone = Zone::where('ZONE_OWN_id',$owner->id)->
            //         where('ZONE_SITE_id',$site->id)->first();
            //         if($zone == null)
            //             return $this->badRequest(["this zone doesn't exsit in this site"]);
            //         else
            //         {
            //             $site_id = $site->id;
            //             $zone_id = $zone->id;
            //         }
            //     }
            // }
            // elseif($user->isSiteAdmin())
            // {
            //     $site = $user->site;
            //     $site_id = $site->id;
            //     $zone = Zone::where('ZONE_OWN_id',$owner->id)->
            //         where('ZONE_SITE_id',$site->id)->first();
            //     $zone_id = $zone->id;
            // }
            //return $site_id;
            if(!$this->isUniqueness($name, $owner->id,$site_id,0))return $this->badRequest(['Rack name already Exists']);

            DB::beginTransaction();

            $rack = new Rack();
            $rack->RACK_name = $name;
            $rack->RACK_OWN_id = $owner->id;
            $rack->RACK_ZONE_id = $zone_id;
            $rack->RACK_row =$request['RACK_row'];
            $rack->RACK_depth=$request['RACK_depth'];
            $rack->RACK_column=$request['RACK_column'];


            if($rack->save())
            {
                $noStorageLocations  = $request['RACK_row']*$request['RACK_depth']*$request['RACK_column'];
                for($i=0;$i<$rack->RACK_column;$i++)
                {
                    for ($j=0; $j <$rack->RACK_row ; $j++) { 
                        for ($k=0; $k <$rack->RACK_depth ; $k++) { 

                            $storageLocation = new StorageLocation();
                            $storageLocation->STL_allocation = 0;
                            $storageLocation->STL_STL_type_id = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->where('STL_Type_name',$request['STL_Type_name'])->first()->id;
                            $storageLocation->STL_FA_id = FunctionalArea::where('FA_OWN_id',$owner->id)
                            ->where('FA_name',$request['FA_name'])->first()->id;
                            $storageLocation->STL_OWN_id = $owner->id;
                            $storageLocation->STL_RACK_id = $rack->id;
                            $storageLocation->STL_SITE_id = $rack->Zone->Site->id;
                            $storageLocation->STL_name = $rack->RACK_name.'_'.($i+1).'_'.($j+1).'_'.($k+1);
                            if(!$storageLocation->save())
                            {
                                DB::rollBack();
                                return $this->internalError(['Storage location Creation has Failed']);
                            }


                        }
                    }
                    
                }
                DB::commit();
                return $this->resourceCreated(['Rack Successfully Created with the storage location inside it']);
            }else{
                return $this->internalError(['Rack Creation has Failed']);
            }
        }
        
    }

    /**
     * Update Rack
     * @param Rack id to be Updated & New Attributes
     * @return Update Approval
     */
    public function update(Request $request)
    {
        
        $v = Validator::make($request->all(),[
            'id' => 'required|exists:Rack,id',
            'RACK_column'=>'integer|min:1','RACK_row'=>'integer|min:1','RACK_depth'=>'integer|min:1'
        ]);

        $id = intval($request['id']);
        if($v->fails()) return $this->badRequest($v->errors()->all());
        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;
        $name = $request['RACK_name'];
        if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin())))
            return $this->unauthorized(["you don't have the permission to update a rack"]);
        $zoneName = $request['RACK_zone'];
        $rackCol = $request['RACK_column'];
        $rackRow = $request['RACK_row'];
        $rackDepth = $request['RACK_depth'];


        $rack = Rack::findOrFail($id);
        $site_id = $rack->zone->site->id;
        if(sizeof($rack->storagelocation)>0)
        {
            if (($rack->RACK_column != $rackCol) || ($rack->RACK_row != $rackRow) ||($rack->RACK_depth!=$rackDepth) ||($zoneName != $rack->zone->ZONE_name))
                return $this->badRequest(["Sorry u can't edit row col depth or zone attributes except after deleting every storage location inside it"]);
            else{

                # ANA SHAYEF EN HENA EL MAFROOD NA3MEL STORAGE LOCATIONS GEDEDA
                $rack->RACK_column =$rackCol;
                $rack->RACK_row =$rackRow;
                $rack->RACK_depth =$rackDepth;
                $rack->RACK_ZONE_id = 
                Zone::where('ZONE_name',$zoneName)->where('ZONE_SITE_id',$site_id)->where('ZONE_OWN_id',$owner_id);
            }
        }
        $rack->RACK_name = $name;
        if(!$this->isUniqueness($name,$owner_id,$site_id,$id))return $this->badRequest(['Rack Name Already Exists']);

        if($rack->save())
        {
            return $this->respond(['Rack Successfully Updated']);
        }else{
            return $this->internalError(['Rack Update Failed']);
        }


    }
    public function destroy(Request $request)
    {
        $v = Validator::make(['id'=>$request['id']],[
            'id'=>'integer|exists:Rack,id'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());
        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;
        if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin())))
            return $this->unauthorized(["you don't have the permission to update a rack"]);
        $id = intval($request['id']);
        $rack = Rack::findOrFail($id);
        if(sizeof($rack->storagelocation)>0)
            return $this->badRequest(['This Rack has storage locations inside it ']);
        if($rack->delete())
        {
            return $this->respond(['Rack Deleted Succesfully']);
        }else{
            return $this->internalError(['Rack Delete Failed']);
        }
    }
    /**
     * View Properties of Rack to be Updated
     * @param Rack id
     * @return Rack Properties (Attributes)
     */
    public function rackprop (Request $request)
    {
        
        $v = Validator::make(['id'=> $request['id']],[
            'id' => 'integer|exists:Rack,id'
        ]);

        if($v->fails()) return $this->badRequest($v->errors()->all());
        $id = intval($request['id']);

        $user = \Illuminate\Support\Facades\Request::user();
        $rack = Rack::where('id',$id)->first();
        $zone = $rack->zone;
        $zones = $rack->zone->site->zone;
        $admin = false;
        $site = false;
        if($user->isSystemAdmin())
        {
            $admin = true;
            $site = false;

        }else if($user->isSiteAdmin())
        {
            $admin = false;
            $site = true;
        }else
        {
            $admin = false;
            $site = false;
        }
        $respond = array();
        $respond['rack'] = $rack;
        $respond['rack_zone'] = $zone;
        $respond['admin'] = $admin;
        $respond['zones'] = $zones;
        json_encode($respond);
        return $this->respond($respond);

    }


    public function isUniqueness($name,$owner_id,$site_id,$id)
    {
        $field = array('id'=>$id);
        $site = Site::where('id',$site_id)->first();
        $zones = $site->zone;
        foreach ($zones as $key => $value) {
            $racks = $value->rack;
            if(sizeof($racks)>0)
            {
                foreach ($racks as $key => $valueRack) {
                if($valueRack['RACK_name'] == $name && $valueRack['id'] != $id)
                    return false;
                }
            }
            
        }
        return true;
    }
    public function getZonesBySiteName(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        //return $request['SITE_name'];
        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request->SITE_name)->first();
        if($site == null)
            return $this->badRequest(['Site Does not Exist']);

        $zones = $site->zone;
        return $this->respond([$zones]); 
    }
    public function hatem_fillStorageLocations(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        $racks = $owner->rack;
        foreach ($racks as $key => $rack) {
        $noRows = $rack->RACK_row;
            $noCols = $rack->RACK_column;
            $noDepth = $rack->RACK_depth;
            for($i=0;$i<$noCols;$i++)
            {
                for ($j=0; $j <$noRows; $j++) { 
                    for ($k=0; $k <$noDepth; $k++) { 

                        $storageLocation = new StorageLocation();
                        $storageLocation->STL_allocation = 0;
                        $storageLocation->STL_STL_type_id = 7;
                        $storageLocation->STL_FA_id = 13;
                        $storageLocation->STL_OWN_id = $owner->id;
                        $storageLocation->STL_RACK_id = $rack->id;
                        $storageLocation->STL_SITE_id = 20;
                        $storageLocation->STL_name = $rack->RACK_name.'_'.($i+1).'_'.($j+1).'_'.($k+1);
                        if(!$storageLocation->save())
                        {
                            return $this->internalError(['Storage location Creation has Failed']);
                        }
                    }
                }
            }
        }
    }
}
