<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use DB;
use App\Strategy;
use App\Client;
use App\Site;
use App\Customer;
use App\IssueOrder;
use App\IssueItem;
use App\SKU;
use App\Unitload;
use App\CapacityUnitload;
use App\PickRequest;
use App\PickRequestItem;
use App\PickJob;


class PortalPickRequestController extends ApiController
{   
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    // public function getMatchedStorageUnitloads(Request $request)
    // {
    //  $user = \Illuminate\Support\Facades\Request::user();
    //     $owner = $user->Client->Owner;
    //     if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
    //     {
    //         return $this->unauthorized(['You do not have Permission to make this Operation']);
    //     }

    //     $v = Validator::make($request->all(),[
    //         'ISI_number'=>'required'
    //     ]);

    //     if($v->fails())return $this->badRequest($v->errors()->all());

    //     $issueorderitem = IssueItem::where('ISI_OWN_id',$owner->id)->where('ISI_number',$request['ISI_number'])->first();
    //     if($issueorderitem == null)
    //         return $this->badRequest(['Issue Order Item Does not Exist']);

    //     $matchedunitloads_names = array();
    //     if($receivingjob->PurchaseOrderItems->POI_productiondate != null)
    //     {
    //         $lifetime = intval($receivingjob->PurchaseOrderItems->SKU->SKU_lifeTime);
    //         $expiryDate= $receivingjob->PurchaseOrderItems->POI_productiondate->addDays($lifetime);
    //         $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
    //             $receivingjob->PurchaseOrderItems->Site->id)->where('UL_SKU_id',$receivingjob->PurchaseOrderItems->SKU->id)->whereIn('UL_expiryDate',[$expiryDate,null])->get();
    //         if(count($matchedunitloads) > 0)
    //         {
    //             $counter = 0;
    //             foreach ($matchedunitloads as $key => $matchedunitload) {
    //                 if($matchedunitload->StorageLocation->FunctionalArea->FA_GoodsIn)
    //                 {
    //                     $matchedunitloads_names[$counter] = $matchedunitload->UL_name;
    //                     $counter++;
    //                 }
    //             }
    //         }
    //     }
    //     else
    //     {
    //         $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
    //             $receivingjob->PurchaseOrderItems->Site->id)->where('UL_SKU_id',$receivingjob->PurchaseOrderItems->SKU->id)->where('UL_expiryDate',null)->get();
    //         if(count($matchedunitloads) > 0)
    //         {
    //             $counter = 0;
    //             foreach ($matchedunitloads as $key => $matchedunitload) {
    //                 if($matchedunitload->StorageLocation->FunctionalArea->FA_GoodsIn 
    //                     && $matchedunitload->SKU->SKUType->id == $receivingjob->PurchaseOrderItems->SKU->SKUType->id)
    //                 {
    //                     $matchedunitloads_names[$counter] = $matchedunitload->UL_name;
    //                     $counter++;
    //                 }
    //             }
    //         }
    //     }

    //     return $this->respond($matchedunitloads_names);
    // }

    public function getStorageUnitloads(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'ISI_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $issueorderitem = IssueItem::where('ISI_OWN_id',$owner->id)->where('ISI_number',$request['ISI_number'])->first();
        if($issueorderitem == null)
            return $this->badRequest(['Issue Order Item Does not Exist']);

        $matchedunitloads_names = array();
        //return $issueorderitem->Site->id;
        $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
            $issueorderitem->Site->id)->where('UL_SKU_id',$issueorderitem->SKU->id)->get();
        //return $matchedunitloads[1];
        if(count($matchedunitloads) > 0)
        {
            $counter = 0;
            foreach ($matchedunitloads as $key => $matchedunitload) {
                if($matchedunitload->StorageLocation->FunctionalArea->FA_Storage 
                    && $matchedunitload->SKU->SKUType->id == $issueorderitem->SKU->SKUType->id)
                {
                    if($matchedunitload->UL_amount <= 0)
                            continue;
                    $matchedunitloads_names[$counter]['unitload'] = $matchedunitload->UL_name;
                    $matchedunitloads_names[$counter]['route'] = ($matchedunitload->StorageLocation->Rack->Zone->ZONE_name).' => '.($matchedunitload->StorageLocation->Rack->RACK_name).' => '.($matchedunitload->StorageLocation->STL_name);
                    $counter++;
                }
            }
        }

        return $this->respond($matchedunitloads_names);
    }


    public function setMatchedStorageUnitloads(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'ISI_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $issueorderitem = IssueItem::where('ISI_OWN_id',$owner->id)->where('ISI_number',$request['ISI_number'])->first();
        if($issueorderitem == null)
            return $this->badRequest(['Issue Order Item Does not Exist']);

        $required_amount = $issueorderitem->ISI_amount;

        $matchedunitloads_names = array();
        $counter = 0;
        $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
            $issueorderitem->Site->id)->where('UL_SKU_id',$issueorderitem->SKU->id)->Where(function ($query){
                        $query->whereNotNull('UL_expiryDate')->orwhere('UL_expiryDate','<>','');
                })->where('UL_expiryDate','>',Carbon::now()->addDays(1)->toDateString())
        ->orderBy('UL_expiryDate', 'asc')->get();
        if(count($matchedunitloads) > 0)
        {
            foreach ($matchedunitloads as $key => $matchedunitload) {
                if($matchedunitload->StorageLocation->FunctionalArea->FA_Storage 
                    && $matchedunitload->SKU->SKUType->id == $issueorderitem->SKU->SKUType->id)
                {
                    if($matchedunitload->UL_amount <= 0)
                        continue;

                    $matchedunitloads_names[$counter]['UL_name']= $matchedunitload->UL_name;
                    if($matchedunitload->UL_amount >= $required_amount)
                    {
                        $matchedunitloads_names[$counter]['UL_amount']= $required_amount;
                        $required_amount = 0;
                        $counter++;
                        break;
                    }
                    else
                    {
                        $matchedunitloads_names[$counter]['UL_amount']= $matchedunitload->UL_amount;
                        $required_amount = $required_amount - ($matchedunitload->UL_amount);
                    }
                    $counter++;
                }
            }
        }

        if($required_amount > 0)
        {
            $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
                $issueorderitem->Site->id)->where('UL_SKU_id',$issueorderitem->SKU->id)
            ->Where(function ($query){
                            $query->whereNull('UL_expiryDate')->orwhere('UL_expiryDate','');
                    })->get();
            if(count($matchedunitloads) > 0)
            {
                foreach ($matchedunitloads as $key => $matchedunitload) {
                    if($matchedunitload->StorageLocation->FunctionalArea->FA_Storage 
                        && $matchedunitload->SKU->SKUType->id == $issueorderitem->SKU->SKUType->id)
                    {
                        if($matchedunitload->UL_amount <= 0)
                            continue;

                        $matchedunitloads_names[$counter]['UL_name']= $matchedunitload->UL_name;
                        if($matchedunitload->UL_amount >= $required_amount)
                        {
                            $matchedunitloads_names[$counter]['UL_amount']= $required_amount;
                            break;
                        }
                        else
                        {
                            $matchedunitloads_names[$counter]['UL_amount']= $matchedunitload->UL_amount;
                            $required_amount = $required_amount - ($matchedunitload->UL_amount);
                        }
                        $counter++;
                    }
                }
            }
        }        

        return $this->respond($matchedunitloads_names);
    }



    public function getMatchedGoodsOutUnitloads(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'ISI_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $issueorderitem = IssueItem::where('ISI_OWN_id',$owner->id)->where('ISI_number',$request['ISI_number'])->first();
        if($issueorderitem == null)
            return $this->badRequest(['Issue Order Item Does not Exist']);

        $matchedunitloads_names = array();
        $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
            $issueorderitem->Site->id)->where('UL_SKU_id',$issueorderitem->SKU->id)->get();
        if(count($matchedunitloads) > 0)
        {
            $counter = 0;
            foreach ($matchedunitloads as $key => $matchedunitload) {
                //return $matchedunitload;
                if($matchedunitload->StorageLocation->FunctionalArea->FA_GoodsOut 
                    && $matchedunitload->SKU->SKUType->id == $issueorderitem->SKU->SKUType->id && $matchedunitload->UL_allocation < 100)
                {
                    $matchedunitloads_names[$counter]['unitload'] = $matchedunitload->UL_name;
                    $matchedunitloads_names[$counter]['route'] = ($matchedunitload->StorageLocation->Rack->Zone->ZONE_name).' => '.($matchedunitload->StorageLocation->Rack->RACK_name).' => '.($matchedunitload->StorageLocation->STL_name);
                    $counter++;
                }
            }
        }

        return $this->respond($matchedunitloads_names);
    }


    public function submitPickRequest(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'ISI_number'=>'required',
            'Unitloads_amount_storage_array'=>'required',
            'Unitloads_amount_goodsout_array'=>'required',
            'PR_date' =>'date_format:"Y-m-d"'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $issueorderitem = IssueItem::where('ISI_OWN_id',$owner->id)->where('ISI_number',$request['ISI_number'])->first();
        if($issueorderitem == null)
            return $this->badRequest(['Issue Order Item Does not Exist']);

        if($issueorderitem->ISI_state != 'new')
            return $this->badRequest(['Issue Order Item has already been Treated']);

        DB::beginTransaction();

        $pickrequest = new PickRequest();
        $pickrequest->PR_number = 'PR_'.((PickRequest::where('PR_OWN_id',$owner->id)->count())+1); 
        $pickrequest->PR_OWN_id = $owner->id;
        $pickrequest->PR_state = 'new';
        //$pickrequest->PR_date = $request->PR_date;
        $pickrequest->PR_date = '2016-08-11 15:44:15';
        $pickrequest->PR_ISI_id = $issueorderitem->id;
        $pickrequest->PR_SITE_id = $issueorderitem->IssueOrder->Site->id;
        if(!$pickrequest->save())
        {
            DB::rollBack();
            return $this->internalError(['Pick Request Creation has Failed']);
        }

        $issueorderitem->ISI_state = 'treated';

        if(!$issueorderitem->save())
        {
            DB::rollBack();
            return $this->internalError(['Issue Order Item Updating has Failed']);
        }

        if(count($request->Unitloads_amount_storage_array) == 0)
        {
            DB::rollBack();
            return $this->badRequest(['Unitloads Storage Array is Empty']);
        }

        if(count($request->Unitloads_amount_goodsout_array) == 0)
        {
            DB::rollBack();
            return $this->badRequest(['Unitloads GoodsOut Array is Empty']);
        }

        $picked_amount = $issueorderitem->ISI_amount;
        $stored_amount = 0;

        $unitloads_for_strategy = array();
        $strategy_counter = 0;
        $items_counter = 0;
        foreach ($request->Unitloads_amount_storage_array as $key => $unitload_amount_pair) 
        {
            if (!array_key_exists("unitload_name",$unitload_amount_pair))
            {
                DB::rollBack();
                return $this->badRequest(['Unitload name Index Error']);
            }
            if (!array_key_exists("unitload_amount",$unitload_amount_pair))
            {
                DB::rollBack();
                return $this->badRequest(['Unitload amount Index Error']);
            }
            
            $unitload_name = $unitload_amount_pair['unitload_name'];
            $unitload_amount = $unitload_amount_pair['unitload_amount'];

            $unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$unitload_name)->first();
            if($unitload == null)
            {
                DB::rollBack();
                return $this->badRequest(['Unitload Does not Exist']);
            }

            $unitloads_for_strategy[$strategy_counter] = $unitload;
            $strategy_counter++;

            if($unitload_amount <= 0)
            {
                DB::rollBack();
                return $this->badRequest(['Amount is Invalid']);
            }


            if($unitload->SKU->id != $issueorderitem->SKU->id )
            {
                DB::rollBack();
                return $this->badRequest(['Not the Same SKU is Invalid']);
            }

            if($unitload->SKU->SKUType->id != $issueorderitem->SKU->SKUType->id )
            {
                DB::rollBack();
                return $this->badRequest(['Not the Same SKU Type is Invalid']);
            }

            if(!$unitload->StorageLocation->FunctionalArea->FA_Storage)
            {
                DB::rollBack();
                return $this->badRequest(['Unitload is Not in Storage Area']);
            }

            if($unitload->Site->id != $issueorderitem->IssueOrder->Site->id)
            {
                DB::rollBack();
                return $this->badRequest(['Unitload is Not in the Same Site']);
            }

            $picked_amount = $picked_amount - $unitload_amount;

            // $unitload->UL_amount = ($unitload->UL_amount) - $unitload_amount;
            // $unitload->UL_reserved = ($unitload->UL_reserved) + $unitload_amount;
            // if(!$unitload->save())
            //     return $this->internalError(['Unitload Updating has Failed']);

            $pickrequestitem = new PickRequestItem();
            $pickrequestitem->PRI_number = $pickrequest->PR_number.'_OUT_'.(($pickrequest->PickRequestItem->count())+1+$items_counter);
            $pickrequestitem->PRI_amount = $unitload_amount;
            $pickrequestitem->PRI_type = 'picking';
            $pickrequestitem->PRI_UL = $unitload->id;
            $pickrequestitem->PRI_PR_id = $pickrequest->id;
            $pickrequestitem->PRI_OWN_id = $owner->id;
            $pickrequestitem->PRI_SITE_id = $unitload->Site->id;
            if(!$pickrequestitem->save())
            {
                DB::rollBack();
                return $this->internalError(['Receiving Job Unitload Creation has Failed']);
            }            
            $items_counter++;

        }

        if($picked_amount > 0)
        {
            DB::rollBack();
            return $this->badRequest(['Picked Amount Does not Fulfill Issue Order Amount']);
        }

        elseif($picked_amount < 0)
        {
            DB::rollBack();
            return $this->badRequest(['Picked Amount is Larger than Issue Order Amount']);
        }

        $expiryDate_attr = array();
        foreach ($unitloads_for_strategy as $key => $row)
        {
            $expiryDate_attr[$key] = $row['UL_expiryDate'];
        }
        array_multisort($expiryDate_attr, SORT_ASC, $unitloads_for_strategy);

        if($issueorderitem->Strategy != null)
        {
            if($issueorderitem->Strategy->STRAT_name == 'First_Expired_First_Out')
            {
                // $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
                //     $issueorderitem->Site->id)->where('UL_SKU_id',$issueorderitem->SKU->id)
                // ->orderBy('UL_expiryDate', 'asc')->get();
                // $matchedunitloads_array = array();
                // if(count($matchedunitloads) > 0)
                // {
                //     $counter = 0;
                //     foreach ($matchedunitloads as $key => $matchedunitload) {
                //         if($matchedunitload->StorageLocation->FunctionalArea->FA_Storage 
                //             && $matchedunitload->SKU->SKUType->id == $issueorderitem->SKU->SKUType->id)
                //         {
                //             if($matchedunitload->UL_amount <= 0)
                //                 continue;
                //             else
                //             {
                //                 $matchedunitloads_array[$counter]= $matchedunitload;
                //                 $counter++;
                //             }
                //         }
                //     }
                // }


                $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
                    $issueorderitem->Site->id)->where('UL_SKU_id',$issueorderitem->SKU->id)->Where(function ($query){
                                $query->whereNotNull('UL_expiryDate')->orwhere('UL_expiryDate','<>','');
                        })->where('UL_expiryDate','>',Carbon::now()->addDays(1)->toDateString())
                ->orderBy('UL_expiryDate', 'asc')->get();
                $matchedunitloads_array = array();
                $counter = 0;                
                if(count($matchedunitloads) > 0)
                {
                    foreach ($matchedunitloads as $key => $matchedunitload) {
                        if($matchedunitload->StorageLocation->FunctionalArea->FA_Storage 
                            && $matchedunitload->SKU->SKUType->id == $issueorderitem->SKU->SKUType->id)
                        {
                            if($matchedunitload->UL_amount <= 0)
                                continue;

                            else
                            {
                                $matchedunitloads_array[$counter]= $matchedunitload;
                                $counter++;
                            }
                        }
                    }
                }

                $matchedunitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',
                    $issueorderitem->Site->id)->where('UL_SKU_id',$issueorderitem->SKU->id)
                ->Where(function ($query){
                                $query->whereNull('UL_expiryDate')->orwhere('UL_expiryDate','');
                        })->get();
                if(count($matchedunitloads) > 0)
                {
                    foreach ($matchedunitloads as $key => $matchedunitload) {
                        if($matchedunitload->StorageLocation->FunctionalArea->FA_Storage 
                            && $matchedunitload->SKU->SKUType->id == $issueorderitem->SKU->SKUType->id)
                        {
                            if($matchedunitload->UL_amount <= 0)
                                continue;

                            else
                            {
                                $matchedunitloads_array[$counter]= $matchedunitload;
                                $counter++;
                            }
                        }
                    }
                } 

                if((count($matchedunitloads) <= 0))
                {
                    DB::rollBack();
                    return $this->badRequest(['No Unitload Match Order Strategy']);
                }
                if(count($unitloads_for_strategy) > count($matchedunitloads_array))
                {
                    DB::rollBack();
                    return $this->badRequest(['Number of Unitloads Selected violate Order Strategy']);
                }
                $counter = 0;
                foreach ($unitloads_for_strategy as $key => $unitload_for_strategy) {
                    $matchedunitload = $matchedunitloads_array[$counter];
                    
                    if($unitload_for_strategy->UL_expiryDate > ($matchedunitload->UL_expiryDate))
                    {
                        DB::rollBack();
                        return $this->badRequest(['Unitloads violate Order Strategy']);
                    } 
                    else
                    {
                        $unitload_for_strategy->UL_amount = ($unitload->UL_amount) - $unitload_amount;
                        $unitload_for_strategy->UL_reserved = ($unitload->UL_reserved) + $unitload_amount;
                        if(!$unitload_for_strategy->save())
                        {
                            DB::rollBack();
                            return $this->internalError(['Unitload Updating has Failed']);
                        }
                    }                   
                }

            }

        }
        $items_counter = 0;

        foreach ($request->Unitloads_amount_goodsout_array as $key => $unitload_amount_pair)
        {
            if (!array_key_exists("unitload_name",$unitload_amount_pair))
            {
                DB::rollBack();
                return $this->badRequest(['Unitload name Index Error']);
            }
            if (!array_key_exists("unitload_amount",$unitload_amount_pair))
            {
                DB::rollBack();
                return $this->badRequest(['Unitload amount Index Error']);
            }
            $unitload_name = $unitload_amount_pair['unitload_name'];
            $unitload_amount = $unitload_amount_pair['unitload_amount'];
            $unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$unitload_name)->first();
            if($unitload == null)
            {
                DB::rollBack();
                return $this->badRequest(['Unitload Does not Exist']);
            }

            if($unitload_amount < 0)
            {
                DB::rollBack();
                return $this->badRequest(['Amount is Invalid']);
            }


            if($unitload->SKU->id != $issueorderitem->SKU->id )
            {
                DB::rollBack();
                return $this->badRequest(['Not the Same SKU is Invalid']);
            }

            if($unitload->SKU->SKUType->id != $issueorderitem->SKU->SKUType->id )
            {
                DB::rollBack();
                return $this->badRequest(['Not the Same SKU Type is Invalid']);
            }

            if(!$unitload->StorageLocation->FunctionalArea->FA_GoodsOut)
            {
                DB::rollBack();
                return $this->badRequest(['Unitload is Not in Goods Out Area']);
            }

            if($unitload->Site->id != $issueorderitem->IssueOrder->Site->id)
            {
                DB::rollBack();
                return $this->badRequest(['Unitload is Not in the Same Site']);
            }

            $old_unitload_amount = intval($unitload->UL_amount) + intval($unitload->UL_reserved);

            $capacity = CapacityUnitload::where('CU_OWN_id',$owner->id)->where('CU_ULT_Type_id',$unitload->UnitloadType->id)->where('CU_SKU_Type_id',$issueorderitem->SKU->SKUType->id)->first();

            if($capacity == null)
            {
                DB::rollBack();
                return $this->badRequest(['Unitload Capacity Rule Not Found']);
            }

            $allocation = $capacity->CU_allocation;

            $max_amount=($old_unitload_amount + intval($unitload_amount))*intval($allocation);

            if($max_amount > 100)
            {
                DB::rollBack();
                return $this->badRequest(['Unitload Capacity is Not Enough']);
            }

            $stored_amount = $stored_amount + $unitload_amount;

            $unitload->UL_reserved = ($unitload->UL_reserved) + $unitload_amount;
            $unitload->UL_allocation = ($unitload->UL_allocation) +(intval($unitload_amount) * intval($allocation));
            if(!$unitload->save())
                return $this->internalError(['Unitload Updating has Failed']);

            $pickrequestitem = new PickRequestItem();
            $pickrequestitem->PRI_number = $pickrequest->PR_number.'_IN_'.(($pickrequest->PickRequestItem->count())+1+$items_counter);
            $pickrequestitem->PRI_amount = $unitload_amount;
            $pickrequestitem->PRI_type = 'storing';
            $pickrequestitem->PRI_UL = $unitload->id;
            $pickrequestitem->PRI_PR_id = $pickrequest->id;
            $pickrequestitem->PRI_OWN_id = $owner->id;
            $pickrequestitem->PRI_SITE_id = $unitload->Site->id;
            if(!$pickrequestitem->save())
            return $this->internalError(['Receiving Job Unitload Creation has Failed']);
            $items_counter++;
        }

        if($stored_amount > $issueorderitem->ISI_amount)
        {
            DB::rollBack();
            return $this->badRequest(['Stored Amount is Larger than Issue Order Amount']);
        }

        elseif($stored_amount < $issueorderitem->ISI_amount)
        {
            DB::rollBack();
            return $this->badRequest(['Stored Amount is Smaller than Issue Order Amount']);
        }
        DB::commit();
        return $this->resourceCreated(['Pick Request Successfully Submitted']);

    }        


    public function getAllPickRequests()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        $pickrequests = $owner->PickRequest;
        $pickrequest_names = array();
        $counter = 0;
        foreach ($pickrequests as $key => $pickrequest) {
            $pickrequestitems = $pickrequest->PickRequestItem;
            foreach ($pickrequestitems as $key => $pickrequestitem) {
                if($pickrequestitem->PickJob == null)
                {
                    $pickrequest_names[$counter] = $pickrequest->PR_number;
                    $counter++;
                    break;
                }
            }            
        }
        return $pickrequest_names;  
    }

    public function getPickRequestByName(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'PR_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $pickrequest = PickRequest::where('PR_OWN_id',$owner->id)->where('PR_number',$request['PR_number'])->first();
        if($pickrequest == null)
            return $this->badRequest(['Pick Request Does not Exist']);

        $pickrequestitems = $pickrequest->PickRequestItem;
        $pickrequestitems_data = array();
        $counter = 0;
        foreach ($pickrequestitems as $key => $pickrequestitem) {
            if($pickrequestitem->PickJob == null)
            {
                $pickrequestitems_data[$counter]['pickrequestitem'] = $pickrequestitem;
                $pickrequestitems_data[$counter]['UL_name'] = $pickrequestitem->Unitload->UL_name;
                $pickrequestitems_data[$counter]['SKU_name'] = $pickrequestitem->Unitload->SKU->SKU_name;
                $counter++;
            }            
        }

        $Data = array();
        $Data['PR_date'] = $pickrequest->PR_date;
        $Data['ISI_number'] = $pickrequest->IssueItem->ISI_number;
        $Data['SITE_name'] = $pickrequest->Site->SITE_name;
        $Data['pickrequestitems_data'] = $pickrequestitems_data;

        return $this->respond($Data);
    }

    // public function submitPickRequestItem(Request $request)
    // {
        
    // }

    public function submitPickJob(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'PRI_number'=>'required',
            'PJ_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $pickrequestitem = PickRequestItem::where('PRI_OWN_id',$owner->id)->where('PRI_number',$request['PRI_number'])->first();
        if($pickrequestitem == null)
            return $this->badRequest(['Pick Request Item Does not Exist']);

        if($pickrequestitem->PickJob != null)
            return $this->badRequest(['There is an already Pick Job for This Pick Request Item']);

        DB::beginTransaction();        

        $pickjob = new PickJob();
        $pickjob->PJ_number = $request->PJ_number;
        $pickjob->PJ_amount = $pickrequestitem->PRI_amount;
        $pickjob->PJ_UL = $pickrequestitem->Unitload->id;
        $pickjob->PJ_PRI_id = $pickrequestitem->id;
        $pickjob->PJ_OWN_id = $owner->id;
        $pickjob->PJ_SITE_id = $pickrequestitem->Site->id;

        if(!$pickjob->save())
        {
            DB::rollBack();
            return $this->internalError(['Pick Job Creation has Failed']);
        }
        $issueorderitem = $pickrequestitem->PickRequest->IssueItem;
        $unitload = $pickrequestitem->Unitload;

        $capacity = CapacityUnitload::where('CU_OWN_id',$owner->id)->where('CU_ULT_Type_id',$unitload->UnitloadType->id)->where('CU_SKU_Type_id',$issueorderitem->SKU->SKUType->id)->first();

        if($capacity == null)
        {
            DB::rollBack();
            return $this->badRequest(['Unitload Capacity Rule Not Found']);
        }

        $allocation = $capacity->CU_allocation;

        if($pickrequestitem->PRI_type == 'picking')
        {
            $unitload->UL_reserved = ($unitload->UL_reserved) - ($pickjob->PJ_amount);
            $unitload->UL_allocation = ($unitload->UL_allocation) - (intval($pickjob->PJ_amount) * intval($allocation));
            if(!$unitload->save())
            {
                DB::rollBack();
                return $this->internalError(['Unitload Updating has Failed']);
            }
        }

        else
        {
            $issueorderitem->ISI_picked = ($issueorderitem->ISI_picked) + $pickjob->PJ_amount;
            if(!$issueorderitem->save())
            {
                DB::rollBack();
                return $this->internalError(['Issue Order Item Updating  has Failed']);
            }

            $unitload->UL_reserved = ($unitload->UL_reserved) - ($pickjob->PJ_amount);
            $unitload->UL_amount = ($unitload->UL_amount) + ($pickjob->PJ_amount);
            if(!$unitload->save())
            {
                DB::rollBack();
                return $this->internalError(['Unitload Updating has Failed']);
            }
        }

        DB::commit();
        return $this->resourceCreated(['Pick Job Successfully Created']);

    }



    public function UnitloadAmountByName(Request $request)
    {
        $v = Validator::make($request->all(),[
            'UL_name'=>'required'
        ]);

        if($v->fails())return $this->badRequest(['Unitload Name is Required']);
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        $unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$request['UL_name'])->first();
        if($unitload == null)return $this->badRequest(['No Unitload Found With This Name']);
        $amount = $unitload->UL_amount;
        return $this->respond([$amount]);
    }


    public function UnitloadFreeCapacityByName(Request $request)
    {
        $v = Validator::make($request->all(),[
            'UL_name'=>'required'
        ]);
        if($v->fails())return $this->badRequest(['Unitload Name is Required']);
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        $unitload = Unitload::where('UL_OWN_id',$owner->id)->where('UL_name',$request['UL_name'])->first();
        if($unitload == null)return $this->badRequest(['No Unitload Found With This Name']);
        $sku = $unitload->SKU;
        if($sku == null)return $this->badRequest(['This Unitload Has No SKU Assigned To It']);
        if($unitload->UnitloadType == null)return $this->badRequest(['This Unitload Has No Type']);
        if($sku->SKUType == null)return $this->badRequest(['The SKU in This Unitload Has No Type']);

        $allocation = CapacityUnitload::where('CU_OWN_id',$owner->id)->where('CU_ULT_TYPE_id',$unitload->UnitloadType->id)
            ->where('CU_SKU_Type_id',$sku->SKUType->id)->first()->CU_allocation;
        if($allocation == null)return $this->badRequest(['The SKU Type in This Unitload Type Has No Allocation']);
        $response = null;
        if($allocation == 0)
            return $this->respond([100000]);
        if($unitload->UL_amount == 0 ||$unitload->UL_amount == null)
        {
            $response = 100/$allocation;
            return $this->respond([$response]);
        }
        $response = (100-($unitload->UL_allocation))/($allocation);
        return $this->respond([$response]);
    }
    public function pickRequestIndex()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
         if($user->isSystemAdmin())
        {
            $pickrequests = $owner->PickRequest;
        }
        elseif(!$user->isSystemAdmin())
        {
            $pickrequests = $user->site->PickRequest;
        }
        $pickrequests_array = array();
        $counter = 0;
        foreach ($pickrequests as $key => $pickrequest) {
            $pickrequests_array[$counter]['pickrequest'] = $pickrequest;
            if($pickrequest->IssueItem != null)
                $pickrequests_array[$counter]['issueitem'] = $pickrequest->IssueItem->ISI_number;
            else
                $pickrequests_array[$counter]['issueitem'] = 'not set';
            if($pickrequest->Site != null)
                $pickrequests_array[$counter]['site'] = $pickrequest->Site->SITE_name;
            else
                $pickrequests_array[$counter]['site'] = 'not set';
            $counter++;
        }
        return $this->respond($pickrequests_array);
    }
    public function pickRequestItemIndex()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
         if($user->isSystemAdmin())
        {
            $pickrequestitems = $owner->PickRequestItem;
        }
        elseif(!$user->isSystemAdmin())
        {
            $pickrequestitems = $user->site->PickRequestItem;
        }
        
        $pickrequestitems_array = array();
        $counter = 0;
        foreach ($pickrequestitems as $key => $pickrequestitem) {
            $pickrequestitems_array[$counter]['pickrequestitem'] = $pickrequestitem;
            if($pickrequestitem->PickRequest != null)
                $pickrequestitems_array[$counter]['pickrequest'] = $pickrequestitem->PickRequest->PR_number;
            else
                $pickrequestitems_array[$counter]['pickrequest'] = 'not set';
            if($pickrequestitem->Site != null)
                $pickrequestitems_array[$counter]['site'] = $pickrequestitem->Site->SITE_name;
            else
                $pickrequestitems_array[$counter]['site'] = 'not set';
            if($pickrequestitem->Unitload != null)
                $pickrequestitems_array[$counter]['unitload'] = $pickrequestitem->Unitload->UL_name;
            else
                $pickrequestitems_array[$counter]['unitload'] = 'not set';
            $counter++;
        }
        return $this->respond($pickrequestitems_array);
    }
    public function pickJobIndex()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
         if($user->isSystemAdmin())
        {
            $pickjobs = $owner->PickJob;
        }
        elseif(!$user->isSystemAdmin())
        {
            $pickjobs = $user->site->PickJob;
        }
        $pickjobs = $owner->PickJob;
        $pickjobs_array = array();
        $counter = 0;
        foreach ($pickjobs as $key => $pickjob) {
            $pickjobs_array[$counter]['pickjob'] = $pickjob;
            if($pickjob->PickRequestItem != null)
                $pickjobs_array[$counter]['pickrequestitem'] = $pickjob->PickRequestItem->PRI_number;
            else
                $pickjobs_array[$counter]['pickrequestitem'] = 'not set';
            if($pickjob->Site != null)
                $pickjobs_array[$counter]['site'] = $pickjob->Site->SITE_name;
            else
                $pickjobs_array[$counter]['site'] = 'not set';
            if($pickjob->Unitload != null)
                $pickjobs_array[$counter]['unitload'] = $pickjob->Unitload->UL_name;
            else
                $pickjobs_array[$counter]['unitload'] = 'not set';
            $counter++;
        }
        $pickRequests = $this->getAllPickRequests();
        $return_array = array(
            'pickjobs'=>$pickjobs_array,
            'pickrequests'=>$pickRequests
        );
        return $this->respond($return_array);
    }


    public function generatePickJobName()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $receivingjob_name = 'PJ_'.((PickJob::where('PJ_OWN_id',$owner->id)->count())+1);

        $time_start = microtime(true) * 10000;
        $receivingjob_name = $receivingjob_name.'_'. $time_start;
        return $this->respond([$receivingjob_name]);
    }
    public function getPickRequestForPrint(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

         $v = Validator::make($request->all(),[
            'PR_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $pickrequest = PickRequest::where('PR_OWN_id',$owner->id)->where('PR_number',$request['PR_number'])->first();
        if($pickrequest == null)
            return $this->badRequest(['Pick Request Does not Exist']);

        $pickrequestitems = $pickrequest->PickRequestItem;
        $pickrequestoutitems_data = array();
        $pickrequestinitems_data = array();
        $counterout = 0;
        $counterin=0;
        foreach ($pickrequestitems as $key => $pickrequestitem) {
            if($pickrequestitem->PRI_type == 'picking')
            {
                $pickrequestoutitems_data[$counterout]['pickrequestitem'] = $pickrequestitem;
                $pickrequestoutitems_data[$counterout]['UL_name'] = $pickrequestitem->Unitload->UL_name;
                $pickrequestoutitems_data[$counterout]['SKU_name'] = $pickrequestitem->Unitload->SKU->SKU_name;
                 $pickrequestoutitems_data[$counterout]['UL_route'] = $pickrequestitem->Unitload->unitloadRoute();
                 $pickrequestoutitems_data[$counterout]['SKU_number'] = $pickrequestitem->Unitload->SKU->SKU_item_no;
                $counterout++;
            }
            elseif($pickrequestitem->PRI_type == 'storing')
            {
                $pickrequestinitems_data[$counterin]['pickrequestitem'] = $pickrequestitem;
                $pickrequestinitems_data[$counterin]['UL_name'] = $pickrequestitem->Unitload->UL_name;
                $pickrequestinitems_data[$counterin]['SKU_name'] = $pickrequestitem->Unitload->SKU->SKU_name;
                $pickrequestinitems_data[$counterin]['SKU_number'] = $pickrequestitem->Unitload->SKU->SKU_item_no;
                 $pickrequestinitems_data[$counterin]['UL_route'] = $pickrequestitem->Unitload->unitloadRoute();
                $counterin++;
            }                 
        }

        $Data = array();
        $Data['pickrequest'] = $pickrequest;
        $Data['ISI_number'] = $pickrequest->IssueItem->ISI_number;
        $Data['ISO_number'] =$pickrequest->IssueItem->issueorder->ISO_number;
        $Data['SITE_name'] = $pickrequest->Site->SITE_name;
        $Data['pickrequestoutitems_data'] = $pickrequestoutitems_data;
        $Data['pickrequestinitems_data'] = $pickrequestinitems_data;

        return $this->respond($Data);
    }
}
