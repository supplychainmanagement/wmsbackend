<?php

/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/16/2016
 * Time: 9:18 AM
 */



namespace App\Http\Controllers;

use App\FunctionalArea;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Unitload;
use App\Client;
use App\StorageLocation;
use App\UnitloadType;
use App\SKU;
use App\Rack;
use App\StorageLocationType;
use App\Site;
use Illuminate\Pagination\Paginator;
class StorageLocationController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }


    public function index()
    {

        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        $response = array();
        $StorageLocationData =array();
        if($user->isSystemClient()){
            $StorageLocationData['systemClient']=true;
            if($user->isSystemAdmin())
            {
                $locationdata = array();

                $Types = $owner->StorageLocationType;
                $typeData = array();
                $counter = 0;
                foreach($Types as $Type)
                {
                    $typeData[$counter] = $Type->STL_Type_name;
                    $counter++;
                }
                $response['Types'] = $typeData;


                $Sites = $owner->Site;
                $siteData = array();
                $counter = 0;
                foreach($Sites as $site)
                {
                    $siteData[$counter] = $site->SITE_name;
                    $counter++;
                }

                $response['Sites'] = $siteData;

                $FunctionalAreas = $owner->FunctionalArea;
                $FAData = array();
                $counter = 0;
                foreach($FunctionalAreas as $FA)
                {
                    $FAData[$counter] = $FA->FA_name;
                    $counter++;
                }
                $response['FunctionalAreas'] = $FAData;
            }
            elseif($user->isSiteAdmin())
            {
                $locationdata = array();
                $Types = $owner->StorageLocationType;
                $typeData = array();
                $counter = 0;
                foreach($Types as $Type)
                {
                    $typeData[$counter] = $Type->STL_Type_name;
                    $counter++;
                }
                $response['Types'] = $typeData;

                $Sites = $user->client->Site;
                $siteData = array();
                $counter = 0;
                foreach($Sites as $site)
                {
                    $siteData[$counter] = $site->SITE_name;
                    $counter++;
                }
                $response['Sites'] = $siteData;

                $FunctionalAreas = $owner->FunctionalArea;
                $FAData = array();
                $counter = 0;
                foreach($FunctionalAreas as $FA)
                {
                    $FAData[$counter] = $FA->FA_name;
                    $counter++;
                }
                $response['FunctionalAreas'] = $FAData;
            }
            else
            {
                $locationdata = array();
                 $response['Types'] = array();
                 $response['Sites'] = array();
                 $response['FunctionalAreas'] = array();
                $response['Locations']=$locationdata;
            }
        }

        return $this->respond($response);
    }
    public function storageLocationPagination(Request $request)
    {
        $limit = $request->limit;
        $startfrom = $request->startfrom;
        $page = intval($startfrom/$limit)+1;
        $search = $request->search;
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        $response = array();
        $StorageLocationData =array();
        if($user->isSystemClient()){
            $StorageLocationData['systemClient']=true;
            if($user->isSystemAdmin())
            {
                if($search == null)
                {
                    $currentPage = $page;
                    Paginator::currentPageResolver(function () use ($currentPage) {
                        return $currentPage;
                    });
                    $locations = $owner->StorageLocation()->orderBy('id')->paginate($limit);
                }
                else
                {
                    $functionalAreasForSearch = FunctionalArea::where('FA_OWN_id',$owner->id)->where('FA_name','like','%'.$search.'%')->lists('id')->toArray();
                    $rackForSearch =  Rack::where('RACK_OWN_id',$owner->id)->where('RACK_name','like','%'.$search.'%')->lists('id')->toArray();
                    $typeForSearch = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->where('STL_Type_name','like','%'.$search.'%')->lists('id')->toArray();
                    $currentPage = $page;
                    Paginator::currentPageResolver(function () use ($currentPage) {
                        return $currentPage;
                    });

                    $locations = StorageLocation::where('STL_OWN_id',$owner->id)->where(function($query) use ($search,$functionalAreasForSearch,$rackForSearch,$typeForSearch){
                        $query->where('STL_name','like','%'.$search.'%')
                        ->orwhereIn('STL_FA_id',$functionalAreasForSearch)
                        ->orwhereIn('STL_RACK_id',$rackForSearch)
                        ->orwhereIn('STL_STL_Type_id',$typeForSearch)
                        ->orwhere('STL_allocation','like','%'.$search.'%')
                        ->orwhere('STL_stocktakingDate','like','%'.$search.'%')
                        ->orwhere('id','like','%'.$search.'%');
                    })->paginate($limit);
                    //return $search;
                }

                $locationdata = array();
                $counter = 0;
                foreach ($locations as $key =>$location) {
                    $locationdata[$counter]['id'] = $location->id;
                    $locationdata[$counter]['STL_name'] = $location->STL_name;
                    $locationdata[$counter]['STL_status'] = $location->STL_status;
                    $locationdata[$counter]['STL_FA'] = $location->FunctionalArea->FA_name;
                    $locationdata[$counter]['STL_allocation'] = $location->STL_allocation;
                    $locationdata[$counter]['STL_type'] = $location->StorageLocationType->STL_Type_name;
                    $locationdata[$counter]['STL_stocktakingDate'] = $location->STL_stocktakingDate;
                    $locationdata[$counter]['STL_RACK'] = Rack::where('id',$location->STL_RACK_id)->first()->RACK_name;
                    $locationdata[$counter]['STL_serialNumber'] = $location->STL_serialNumber;
                    $counter++;
                }
                $response['Locations']=$locationdata;

                $Types = $owner->StorageLocationType;
                $typeData = array();
                $counter = 0;
                foreach($Types as $Type)
                {
                    $typeData[$counter] = $Type->STL_Type_name;
                    $counter++;
                }
                $response['Types'] = $typeData;


                $Sites = $owner->Site;
                $siteData = array();
                $counter = 0;
                foreach($Sites as $site)
                {
                    $siteData[$counter] = $site->SITE_name;
                    $counter++;
                }

                $response['Sites'] = $siteData;

                $FunctionalAreas = $owner->FunctionalArea;
                $FAData = array();
                $counter = 0;
                foreach($FunctionalAreas as $FA)
                {
                    $FAData[$counter] = $FA->FA_name;
                    $counter++;
                }
                $response['FunctionalAreas'] = $FAData;
            }
            elseif($user->isSiteAdmin())
            {
                $site = $user->site;
                if($search == null)
                {
                    $currentPage = $page;
                    Paginator::currentPageResolver(function () use ($currentPage) {
                        return $currentPage;
                    });
                   $locations = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_SITE_id',$site->id)->paginate($limit);
                }
                else
                {
                    $functionalAreasForSearch = FunctionalArea::where('FA_OWN_id',$owner->id)->where('FA_name','like','%'.$search.'%')->lists('id')->toArray();
                    $rackForSearch =  Rack::where('RACK_OWN_id',$owner->id)->where('RACK_name','like','%'.$search.'%')->lists('id')->toArray();
                    $typeForSearch = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->where('STL_Type_name','like','%'.$search.'%')->lists('id')->toArray();
                    $currentPage = $page;
                    Paginator::currentPageResolver(function () use ($currentPage) {
                        return $currentPage;
                    });

                    $locations = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_SITE_id',$site->id)->where(function($query) use ($search,$functionalAreasForSearch,$rackForSearch,$typeForSearch){
                        $query->where('STL_name','like','%'.$search.'%')
                        ->orwhereIn('STL_FA_id',$functionalAreasForSearch)
                        ->orwhereIn('STL_RACK_id',$rackForSearch)
                        ->orwhereIn('STL_STL_Type_id',$typeForSearch)
                        ->orwhere('STL_allocation','like','%'.$search.'%')
                        ->orwhere('STL_stocktakingDate','like','%'.$search.'%')
                        ->orwhere('id','like','%'.$search.'%');
                    })->paginate($limit);
                }
                $locationdata = array();
                $counter = 0;
                
                foreach ($locations as $key =>$location) {
                    $locationdata[$counter]['id'] = $location->id;
                    $locationdata[$counter]['STL_name'] = $location->STL_name;
                    $locationdata[$counter]['STL_status'] = $location->STL_status;
                    $locationdata[$counter]['STL_FA'] = $location->FunctionalArea->FA_name;
                    $locationdata[$counter]['STL_allocation'] = $location->STL_allocation;
                    $locationdata[$counter]['STL_type'] = $location->StorageLocationType->STL_Type_name;
                    $locationdata[$counter]['STL_stocktakingDate'] = $location->STL_stocktakingDate;
                    $locationdata[$counter]['STL_RACK'] = Rack::where('id',$location->STL_RACK_id)->first()->RACK_name;
                    $locationdata[$counter]['STL_serialNumber'] = $location->STL_serialNumber;

                    $counter++;
                }

                $response['Locations']=$locationdata;
                $Types = $owner->StorageLocationType;
                $typeData = array();
                $counter = 0;
                foreach($Types as $Type)
                {
                    $typeData[$counter] = $Type->STL_Type_name;
                    $counter++;
                }
                $response['Types'] = $typeData;

                $Sites = $user->client->Site;
                $siteData = array();
                $counter = 0;
                foreach($Sites as $site)
                {
                    $siteData[$counter] = $site->SITE_name;
                    $counter++;
                }
                $response['Sites'] = $siteData;

                $FunctionalAreas = $owner->FunctionalArea;
                $FAData = array();
                $counter = 0;
                foreach($FunctionalAreas as $FA)
                {
                    $FAData[$counter] = $FA->FA_name;
                    $counter++;
                }
                $response['FunctionalAreas'] = $FAData;
            }
            else
            {
                $site = $user->site;
                if($search == null)
                {
                    $currentPage = $page;
                    Paginator::currentPageResolver(function () use ($currentPage) {
                        return $currentPage;
                    });
                    $locations = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_SITE_id',$site->id)->paginate($limit);
                }
                else
                {
                    $functionalAreasForSearch = FunctionalArea::where('FA_OWN_id',$owner->id)->where('FA_name','like','%'.$search.'%')->lists('id')->toArray();
                    $rackForSearch =  Rack::where('RACK_OWN_id',$owner->id)->where('RACK_name','like','%'.$search.'%')->lists('id')->toArray();
                    $typeForSearch = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->where('STL_Type_name','like','%'.$search.'%')->lists('id')->toArray();
                    $currentPage = $page;
                    Paginator::currentPageResolver(function () use ($currentPage) {
                        return $currentPage;
                    });

                    $locations = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_SITE_id',$site->id)->where(function($query) use ($search,$functionalAreasForSearch,$rackForSearch,$typeForSearch){
                        $query->where('STL_name','like','%'.$search.'%')
                        ->orwhereIn('STL_FA_id',$functionalAreasForSearch)
                        ->orwhereIn('STL_RACK_id',$rackForSearch)
                        ->orwhereIn('STL_STL_Type_id',$typeForSearch)
                        ->orwhere('STL_allocation','like','%'.$search.'%')
                        ->orwhere('STL_stocktakingDate','like','%'.$search.'%')
                        ->orwhere('id','like','%'.$search.'%');
                    })->paginate($limit);
                }
                $locationdata = array();
                $counter = 0;
                
                foreach ($locations as $key =>$location) {
                    $locationdata[$counter]['id'] = $location->id;
                    $locationdata[$counter]['STL_name'] = $location->STL_name;
                    $locationdata[$counter]['STL_status'] = $location->STL_status;
                    $locationdata[$counter]['STL_FA'] = $location->FunctionalArea->FA_name;
                    $locationdata[$counter]['STL_allocation'] = $location->STL_allocation;
                    $locationdata[$counter]['STL_type'] = $location->StorageLocationType->STL_Type_name;
                    $locationdata[$counter]['STL_stocktakingDate'] = $location->STL_stocktakingDate;
                    $locationdata[$counter]['STL_RACK'] = Rack::where('id',$location->STL_RACK_id)->first()->RACK_name;
                    $locationdata[$counter]['STL_serialNumber'] = $location->STL_serialNumber;

                    $counter++;
                }
                 $response['Types'] = array();
                 $response['Sites'] = array();
                 $response['FunctionalAreas'] = array();
                $response['Locations']=$locationdata;
            }
        }

        return $this->respondWithPagination($locations,$response);
    }
    public function create(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        if($user->isLabour())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'STL_name'=>'required',
            'STL_FA' => 'required',
            'STL_RACK'=>'required',
            'STL_Type' => 'required',
            'STL_SITE'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $owner = $user->client->owner;
        $location = new StorageLocation();
        $location->STL_name = $request->STL_name;
        $location->STL_FA_id = FunctionalArea::where('FA_name',$request->STL_FA)->where('FA_OWN_id',$owner->id)->first()->id;
        $location->STL_RACK_id = Rack::where('RACK_name',$request->STL_RACK)->where('RACK_OWN_id',$owner->id)->first()->id;
        $location->STL_STL_Type_id = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->where('STL_Type_name',$request->STL_Type)
            ->first()->id;

        if($request->has('STL_stocktakingDate'))
            $location->STL_stocktakingDate = $request->STL_stocktakingDate;
        $location->STL_allocation = 0;
        $location->STL_status = 'New';
        $location->STL_OWN_id = $owner->id;
        $location->STL_SITE_id = Site::where('SITE_name',$request->STL_SITE)->where('SITE_OWN_id',$owner->id)->first()->id;

        if($location->save())
            return $this->resourceCreated(['Storage location Successfully Created']);
        else{
            return $this->internalError(['Storage location Creation has Failed']);
        }

    }

    public function prop(Request $request)
    {
        $v = Validator::make(['id'=> $request['id']],[
            'id' => 'integer|exists:Storage_Location,id'
        ]);

        if($v->fails()) return $this->badRequest($v->errors()->all());
        $id = intval($request['id']);

        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        $location = StorageLocation::findOrFail($id);
        $respond = array();

        if($user->isSystemAdmin())
        {
            $types = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->get();
            $functionalAreas = FunctionalArea::where('FA_OWN_id',$owner->id)->get();
            $sites = Site::where('SITE_OWN_id',$owner->id)->get();
            $racks = Rack::where('RACK_OWN_id',$owner->id)->get();
            $respond['types'] = $types;
            $respond['functionalAreas'] = $functionalAreas;
            $respond['sites'] = $sites;
            $respond['racks'] = $racks;
            $respond['STL_name'] = $location->STL_name;
            $respond['STL_status'] = $location->STL_status;
            $respond['STL_stocktakingDate'] = $location->STL_stocktakingDate;
            $respond['STL_allocation'] = $location->STL_allocation;
            $respond['STL_FA'] = FunctionalArea::where('FA_OWN_id',$owner->id)->
                where('id',$location->STL_FA_id)->first()->FA_name;
            $respond['STL_RACK'] = Rack::where('id',$location->STL_RACK_id)->first()->RACK_name;
            $respond['STL_Type'] = $location->StorageLocationType->STL_Type_name;
//            $respond['STL_Type'] = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->
//            where('id',$location->STL_Type_id)->first()->STL_Type_name;

        }else
        {
            $types = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->get();
            $functionalAreas = FunctionalArea::where('FA_OWN_id',$owner->id)->get();
            $sites = $user->site;
            $racks = $user->site->rack;
            $respond['types'] = $types;
            $respond['functionalAreas'] = $functionalAreas;
            $respond['sites'] = $sites;
            $respond['racks'] = $racks;
            $respond['STL_name'] = $location->STL_name;
            $respond['STL_status'] = $location->STL_status;
            $respond['STL_stocktakingDate'] = $location->STL_stocktakingDate;
            $respond['STL_allocation'] = $location->STL_allocation;
            $respond['STL_FA'] = FunctionalArea::where('FA_OWN_id',$owner->id)->
            where('id',$location->STL_FA_id)->first()->FA_name;
//            $respond['STL_Type'] = StorageLocationType::where('STL_Type_OWN_id',$owner->id)->
//            where('id',$location->STL_Type_id)->first()->STL_Type_name;
            $respond['STL_RACK'] = Rack::where('id',$location->STL_RACK_id)->first()->RACK_name;
            $respond['STL_Type'] = $location->StorageLocationType->STL_Type_name;
        }
        if($user->isSystemAdmin() || $user->isSiteAdmin())
            $respond['permission'] = true;
        else
            $respond['permission'] =false;
        return $this->respond($respond);
    }

    public function update(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        if($user->isLabour())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'id' => 'required|exists:Storage_Location,id',
            'STL_name'=>'required',
            'STL_Rack' => 'required',
            'STL_FA' => 'required',
            'STL_Type' => 'required',
        ]);

        $id = intval($request['id']);
        if($v->fails()) return $this->badRequest($v->errors()->all());
        $owner_id = $user->client->owner->id;
        $location = StorageLocation::findOrFail($id);
        if($location->STL_allocation>0)
           return $this->badRequest(['The Location Selected is not Empty Please Empty it up first before updating']);
        if($request->has('STL_Status'))
            $location->STL_Status = $request->STL_Status;
        $location->STL_FA_id = FunctionalArea::getIdByName('FA',$request->STL_FA);
        $location->STL_STL_Type_id = StorageLocationType::getIdByName('STL_Type',$request->STL_Type);

        if($location->save())
        {
            return $this->respond(['Location Successfully Updated']);
        }else{
            return $this->internalError(['Location Update Failed']);
        }
    }

    public function delete(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        if($user->isLabour()||$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make(['id'=>$request['id']],[
            'id'=>'integer|exists:Storage_Location,id'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());
        $id = intval($request['id']);
        $location = StorageLocation::findOrFail($id);

        if($location->STL_allocation>0)
           return $this->badRequest(['The Location Selected is not Empty Please Empty it up first before Deleting']);

        if($location->delete())
        {
            return $this->respond(['Location Deleted Succesfully']);
        }else{
            return $this->internalError(['Location Delete Failed']);
        }
    }
    

}


