<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Unitload;
use App\Client;
use App\StorageLocation;
use App\UnitloadType;
use App\SKU;
use App\Site;
use App\Zone;
use App\Rack;
use App\FunctionalArea;
use App\CapacityLocation;
use App\CapacityUnitload;
use DB;

class UnitloadController extends ApiController
{
	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }


    public function index()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$UnitloadData =array();
        if($user->isSystemClient()){
			$UnitloadData['systemClient']=true;
        	if($user->isSystemAdmin())
        	{
        		$unitloads = $owner->Unitload;
        		$unitloaddata = array();
        		$counter = 0;
        		foreach ($unitloads as $unitload) {
        			$client_name = $unitload->Client->CLI_name;
        			$sku_name = $unitload->SKU->SKU_name;
        			$unitloadType_name = $unitload->UnitloadType->ULT_Type_name;
					$storagelocation_name = $unitload->StorageLocation->STL_name;
					$unitloaddata[$counter]['unitload'] = $unitload;
        			$unitloaddata[$counter]['client_name'] = $client_name;
        			$unitloaddata[$counter]['storagelocation_name'] = $storagelocation_name;
        			$unitloaddata[$counter]['sku_name'] = $sku_name;
        			$unitloaddata[$counter]['unitloadType_name'] = $unitloadType_name;
        			$counter++;
        			
        		}
        		$UnitloadData['unitloads']=$unitloaddata;
        		$all_clients = $owner->Client;
        		$all_storagelocations = $owner->StorageLocation->take(10);
        		$all_skus = $owner->SKU;
        		$all_unitload_types = $owner->UnitloadType;
        		$UnitloadData['all_clients']=$all_clients->all();
        		$UnitloadData['all_sites']=$owner->site;
        		$UnitloadData['all_skus']=$all_skus->all();
        		$UnitloadData['all_unitload_types']=$all_unitload_types->all();
        		$UnitloadData['permission'] = true;
				$UnitloadData['siteBased']=false;
        	}
        	elseif($user->isSiteAdmin() || $user->isCheifOfLabours())
        	{
        		$unitloads = $owner->Unitload;
        		$unitloaddata = array();
        		$counter = 0;
        		foreach ($unitloads as $key =>$unitload) {
        			$client_name = $unitload->Client->CLI_name;
        			$storagelocation_name = $unitload->StorageLocation->STL_name;
        			$sku_name = $unitload->SKU->SKU_name;
        			$unitloadType_name = $unitload->UnitloadType->ULT_Type_name;
        			$unitloaddata[$counter]['unitload'] = $unitload;
        			$unitloaddata[$counter]['client_name'] = $client_name;
        			$unitloaddata[$counter]['storagelocation_name'] = $storagelocation_name;
        			$unitloaddata[$counter]['sku_name'] = $sku_name;
        			$unitloaddata[$counter]['unitloadType_name'] = $unitloadType_name;
        			$counter++;
        			
        		}
        		$UnitloadData['unitloads']=$unitloaddata;
        		$all_clients = $owner->Client;
        		$all_storagelocations = $user->Site->StorageLocation->take(10);
        		$all_skus = $owner->SKU;
        		$all_unitload_types = $owner->UnitloadType;
        		$UnitloadData['all_clients']=$all_clients->all();
        		$UnitloadData['all_sites']=[$user->site];
        		$UnitloadData['all_skus']=$all_skus->all();
        		$UnitloadData['all_unitload_types']=$all_unitload_types->all();
        		$UnitloadData['permission'] = true;
				$UnitloadData['siteBased']=true;
        	}
        	else
			{
				$unitloads = $owner->Unitload;
        		$unitloaddata = array();
        		$counter = 0;
        		foreach ($unitloads as $key =>$unitload) {
        			$client_name = $unitload->Client->CLI_name;
        			$storagelocation_name = $unitload->StorageLocation->STL_name;
        			$sku_name = $unitload->SKU->SKU_name;
        			$unitloadType_name = $unitload->UnitloadType->ULT_Type_name;
        			$unitloaddata[$counter]['unitload'] = $unitload;
        			$unitloaddata[$counter]['client_name'] = $client_name;
        			$unitloaddata[$counter]['storagelocation_name'] = $storagelocation_name;
        			$unitloaddata[$counter]['sku_name'] = $sku_name;
        			$unitloaddata[$counter]['unitloadType_name'] = $unitloadType_name;
        			$counter++;
        			
        		}
        		$UnitloadData['unitloads']=$unitloaddata;
        		$UnitloadData['all_clients']=array();
        		$UnitloadData['all_skus']=array();
        		$UnitloadData['all_unitload_types']=array();
        		$UnitloadData['all_sites']=array();
        		$UnitloadData['permission'] = false;
				$UnitloadData['siteBased']=true;
			}
        }
        

       	 	//if(sizeof($UnitloadData['unitloads']) > 0){
                return $this->respond($UnitloadData);
            // }else{
            //     return $this->respondNoContent();
            // }
    }

    public function create(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	if($user->isLabour())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'UL_name'=>'required',
			'UL_STL' => 'required',
			'UL_CLI' => 'required',
			'UL_SKU' => 'required',
			'UL_ULT_Type' => 'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$owner = $user->client->owner;

		if(!Unitload::isUniqueName('UL','client',$request->UL_name,0))return $this->badRequest(['Unitload name already Exists']);
		$client_id = Client::getIdByName('CLI',$request->UL_CLI);
		if($client_id == -1)
			return $this->badRequest(['Client does not Exist']);
		// $sku_id = SKU::getIdByName('SKU',$request->UL_SKU);
		// if($sku_id == -1)
		// 	return $this->badRequest(['SKU does not Exist']);
		$sku = SKU::where('SKU_OWN_id',$owner->id)->where('SKU_name',$request->UL_SKU)->first();
		if($sku == null)
			return $this->badRequest(['SKU does not Exist']);
		// $storagelocation_id = StorageLocation::getIdByName('STL',$request->UL_STL);
		// if($storagelocation_id == -1)
		// 	return $this->badRequest(['StorageLocation does not Exist']);
		$storagelocation = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_name',$request->UL_STL)->first();
		if($storagelocation == null)
			return $this->badRequest(['Storage Location does not Exist']);
		$unitloadtype_id = UnitloadType::getIdByName('ULT_Type',$request->UL_ULT_Type);
		if($unitloadtype_id == -1)
			return $this->badRequest(['Unitload Type does not Exist']);

		$capacity = CapacityLocation::where('CL_OWN_id',$owner->id)->where('CL_STL_Type_id',$storagelocation->StorageLocationType->id)->where('CL_ULT_Type_id',$unitloadtype_id)->first();
		if($capacity == null)
			return $this->badRequest(['Capacity Rule for Location does not Exist']);

		$allocation = $capacity->CL_allocation;

		$max_allocation = 100 - intval($storagelocation->STL_allocation);

		//return $this->badRequest(['Allocation: '.$allocation.' & '.$max_allocation.'Only']);

		if($allocation > $max_allocation)
			return $this->badRequest(['No Enought Capacity in Storage Location']);

		$capacityUnitload = CapacityUnitload::where('CU_OWN_id',$owner->id)->where('CU_SKU_Type_id',$sku->SKUType->id)->where('CU_ULT_Type_id',$unitloadtype_id)->first();
		if($capacityUnitload == null)
			return $this->badRequest(['Capacity Rule for Unitload does not Exist']);

		$allocationUnitload = $capacityUnitload->CU_allocation;

		if((intval($allocationUnitload) * intval($request->UL_amount)) > 100)
			return $this->badRequest(['No Enought Capacity in Unitload']);

		$storagelocation->STL_allocation = intval($storagelocation->STL_allocation) + $allocation;

		DB::beginTransaction();

		if(!$storagelocation->save())
		{
			DB::rollBack();
			return $this->internalError(['Failed to Update Storage Location Allocation']);
		}

		$unitload = new Unitload();
		$unitload->UL_name = $request->UL_name;
		$unitload->UL_OWN_id = $owner->id;
		$unitload->UL_status = $request->UL_status;
		$unitload->UL_serialNumber = $request->UL_serialNumber;
		$unitload->UL_STL_id = $storagelocation->id;
		$unitload->UL_allocation = (intval($allocationUnitload) * intval($request->UL_amount));
		$unitload->UL_reserved = 0;
		$unitload->UL_CLI_id = $client_id;
		$unitload->UL_SITE_id = $storagelocation->Site->id;
		$unitload->UL_amount = 0;
		$unitload->UL_SKU_id = $sku->id;
		$unitload->UL_stocktakingDate = $request->UL_stocktakingDate;
		$unitload->UL_expiryDate = $request->UL_expiryDate;
		$unitload->UL_ULT_Type_id = $unitloadtype_id;			

		if($unitload->save())
		{
			DB::commit();
			return $this->resourceCreated(['Unitload Successfully Created']);
		}else{
			DB::rollBack();
			return $this->internalError(['Unitload Creation has Failed']);
		}

    }

    public function prop(Request $request)
    {
    	$v = Validator::make(['id'=> $request['id']],[
			'id' => 'integer|exists:Unitload,id'
		]);

		if($v->fails()) return $this->badRequest($v->errors()->all());
		$id = intval($request['id']);

		$user = \Illuminate\Support\Facades\Request::user();
		$owner = $user->Client->Owner;
		$unitload = Unitload::findOrFail($id);
		$respond = array();
		$permission = false;
		$siteBased = false;
		$respond['all_clients']=array();
		$respond['all_storagelocations']=array();
		$respond['all_skus']=array();
		$respond['all_unitload_types']=array();
		if($user->isSystemAdmin())
		{
			$permission = true;
			$all_clients = $owner->Client;
    		$all_skus = $owner->SKU;
    		$all_unitload_types = $owner->UnitloadType;
    		$respond['all_clients']=$all_clients->all();
    		$respond['all_storagelocations']=[$unitload->storagelocation];
    		$respond['all_skus']=$all_skus->all();
    		$respond['all_unitload_types']=$all_unitload_types->all();

		}elseif($user->isSiteAdmin() || $user->isCheifOfLabours())
        {
            $permission = true;
            $siteBased = true;
            $all_clients = $owner->Client;
    		$all_skus = $owner->SKU;
    		$all_unitload_types = $owner->UnitloadType;
    		$respond['all_clients']=$all_clients->all();
    		$respond['all_storagelocations']=[$unitload->storagelocation];
    		$respond['all_skus']=$all_skus->all();
    		$respond['all_unitload_types']=$all_unitload_types->all();

        }
		$respond['permission'] = $permission;
		$respond['siteBased'] = $siteBased;

		$client_name = $unitload->Client->CLI_name;
		$storagelocation_name = $unitload->StorageLocation->STL_name;
		$sku_name = $unitload->SKU->SKU_name;
		$unitloadType_name = $unitload->UnitloadType->ULT_Type_name;
		$respond['unitload'] = $unitload;
		$respond['unitload_client'] = $client_name;
		$respond['unitload_storagelocation'] = $storagelocation_name;
		$respond['unitload_sku'] = $sku_name;
		$respond['unitload_unitloadType'] = $unitloadType_name;

		json_encode($respond);
		return $this->respond($respond);
    }

    public function update(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->Client->Owner;
    	if($user->isLabour())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

    	$v = Validator::make($request->all(),[
			'id' => 'required|exists:Unitload,id',
			'UL_name'=>'required',
			'UL_STL' => 'required',
			'UL_allocation'=>'required',
			'UL_CLI' => 'required',
			'UL_amount' => 'required',
			'UL_SKU' => 'required',
			'UL_ULT_Type' => 'required'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());

		$owner_id = $user->client->owner->id;

		if(!Unitload::isUniqueName('UL','client',$request->UL_name,$id))return $this->badRequest(['Unitload name already Exists']);

		$client_id = Client::getIdByName('CLI',$request->UL_CLI);
		if($client_id == -1)
			return $this->badRequest(['Client does not Exist']);
		$sku = SKU::where('SKU_OWN_id',$owner->id)->where('SKU_name',$request->UL_SKU)->first();
		if($sku == null)
			return $this->badRequest(['SKU does not Exist']);
		$storagelocation = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_name',$request->UL_STL)->first();
		if($storagelocation == null)
			return $this->badRequest(['Storage Location does not Exist']);
		$unitloadtype_id = UnitloadType::getIdByName('ULT_Type',$request->UL_ULT_Type);
		if($unitloadtype_id == -1)
			return $this->badRequest(['Unitload Type does not Exist']);


		$capacity = CapacityLocation::where('CL_OWN_id',$owner->id)->where('CL_STL_Type_id',$storagelocation->StorageLocationType->id)->where('CL_ULT_Type_id',$unitloadtype_id)->first();
		if($capacity == null)
			return $this->badRequest(['Capacity Rule for Location does not Exist']);

		$allocation = $capacity->CL_allocation;

		$max_allocation = 100 - intval($storagelocation->STL_allocation);

		if($allocation > $max_allocation)
			return $this->badRequest(['No Enought Capacity in Storage Location']);

		$capacityUnitload = CapacityUnitload::where('CU_OWN_id',$owner->id)->where('CU_SKU_Type_id',$sku->SKUType->id)->where('CU_ULT_Type_id',$unitloadtype_id)->first();
		if($capacityUnitload == null)
			return $this->badRequest(['Capacity Rule for Unitload does not Exist']);

		$allocationUnitload = $capacityUnitload->CU_allocation;

		if((intval($allocationUnitload) * intval($request->UL_amount)) > 100)
			return $this->badRequest(['No Enought Capacity in Unitload']);

		$storagelocation->STL_allocation = intval($storagelocation->STL_allocation) + $allocation;

		if(!$storagelocation->save())
			return $this->internalError(['Failed to Update Storage Location Allocation']);

		$unitload = Unitload::findOrFail($id);
		$unitload->UL_name = $request->UL_name;
		$unitload->UL_OWN_id = $owner_id;
		$unitload->UL_status = $request->UL_status;
		$unitload->UL_serialNumber = $request->UL_serialNumber;
		$unitload->UL_STL_id = $storagelocation->id;
		$unitload->UL_allocation = (intval($allocationUnitload) * intval($request->UL_amount));
		$unitload->UL_CLI_id = $client_id;
		$unitload->UL_amount = $request->UL_amount;
		$unitload->UL_SKU_id = $sku->id;
		$unitload->UL_stocktakingDate = $request->UL_stocktakingDate;
		$unitload->UL_expiryDate = $request->UL_expiryDate;
		$unitload->UL_ULT_Type_id = $unitloadtype_id;

		if($unitload->save())
		{
			return $this->respond(['Unitload Successfully Updated']);
		}else{
			return $this->internalError(['Unitload Update Failed']);
		}
    }

    public function delete(Request $request)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner= $user->Client->Owner;
    	if($user->isLabour())
    	{
    		return $this->unauthorized(['You do not have Permission to make this Operation']);
    	}

		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Unitload,id'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());
		$id = intval($request['id']);
		$unitload = Unitload::findOrFail($id);

		if($unitload->UL_amount != null && $unitload->UL_amount > 0)
		{
			return $this->badRequest(['Unitload selected is not Empty']);
		}

		$capacity = CapacityLocation::where('CL_OWN_id',$owner->id)->where('CL_STL_Type_id',$unitload->StorageLocation->StorageLocationType->id)->where('CL_ULT_Type_id',$unitload->UnitloadType->id)->first();
		if($capacity == null)
			return $this->badRequest(['Capacity Rule for Location does not Exist']);

		$allocation = $capacity->CL_allocation;

		DB::beginTransaction();

		$storagelocation = $unitload->StorageLocation;
		$storagelocation->STL_allocation = intval($storagelocation->STL_allocation) - $allocation;

		if(!$storagelocation->save())
		{
			DB::rollBack();
			return $this->internalError(['Storage Location Updating Failed']);
		}

		if($unitload->delete())
		{
			DB::commit();
			return $this->respond(['Unitload Deleted Succesfully']);
		}else{
			DB::rollBack();
			return $this->internalError(['Unitload Delete Failed']);
		}
    }
    public function getZones(Request $request)
    {
 	 	$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }
         $v = Validator::make($request->all(),[
            'SITE_name'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
        if($site == null)
            return $this->badRequest(['Site Does not Exist']);

        $zones = Zone::where('ZONE_OWN_id',$owner->id)->where('ZONE_SITE_id',$site->id)->lists('ZONE_name')->toArray();
        return $this->respond($zones);
        
    }

    public function getRacks(Request $request)
    {
 	 	$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }
         $v = Validator::make($request->all(),[
            'SITE_name'=>'required',
             'ZONE_name'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
        if($site == null)
            return $this->badRequest(['Site Does not Exist']);

         $zone = Zone::where('ZONE_OWN_id',$owner->id)->where('ZONE_SITE_id',$site->id)->where('ZONE_name',$request['ZONE_name'])->first();
        if($zone == null)
            return $this->badRequest(['Zone Does not Exist']);

        $Racks = Rack::where('RACK_OWN_id',$owner->id)->where('RACK_ZONE_id',$zone->id)->lists('RACK_name')->toArray();
        return $this->respond($Racks);
        
    }

    public function getLocations(Request $request)
    {
 	 	$user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }
         $v = Validator::make($request->all(),[
            'SITE_name'=>'required',
         	'ZONE_name'=>'required',
         	'RACK_name'=>'required',
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request['SITE_name'])->first();
        if($site == null)
            return $this->badRequest(['Site Does not Exist']);

         $zone = Zone::where('ZONE_OWN_id',$owner->id)->where('ZONE_SITE_id',$site->id)->where('ZONE_name',$request['ZONE_name'])->first();
        if($zone == null)
            return $this->badRequest(['Zone Does not Exist']);

        $rack = Rack::where('RACK_OWN_id',$owner->id)->where('RACK_ZONE_id',$zone->id)->where('RACK_name',$request['RACK_name'])->first();
        if($rack == null)
            return $this->badRequest(['Rack Does not Exist']);
        if(!$request->has('type'))
        {
        	 $locations = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_RACK_id',$rack->id)->lists('STL_name')->toArray();
    	}

    	elseif($request->has('type') && $request->type == 'storage')
    	{
    		 $locations = StorageLocation::where('STL_OWN_id',$owner->id)->where('STL_RACK_id',$rack->id)->whereIn('STL_FA_id', (FunctionalArea::where('FA_OWN_id',$owner->id)->where('FA_Storage',1)->lists('id')->toArray()))->lists('STL_name')->toArray();
    	}
       
        return $this->respond($locations);
        
    }
}
