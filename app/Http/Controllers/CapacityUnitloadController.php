<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\CapacityLocation;
use App\UnitloadType;
use App\StorageLocationType;
use App\SKUType;
use App\CapacityUnitload;

class CapacityUnitloadController extends ApiController
{
     public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$CapacityUnitloadData =array();
    	$capacityunitloaddata =array();
        if($user->isSystemClient()){
			$CapacityUnitloadData['systemClient']=true;
			$capacities = $owner->capacityunitload;
			$counter =0;
    		foreach ($capacities as $key => $value) {
    			$capacityunitloaddata[$counter]['capacity'] = $value;
    			//return $value;
    			$capacityunitloaddata[$counter]['CU_ULT_Type_name'] = 
    				UnitloadType::where('id',$value['CU_ULT_Type_id'])->first()->ULT_Type_name;
    			$capacityunitloaddata[$counter]['CU_SKU_Type_name'] = SKUType::where('id',$value['CU_SKU_Type_id'])->first()->SKU_Type_name;
    			$counter++;
    		}
        	$CapacityUnitloadData['CapacityUnitload']= $capacityunitloaddata;
        	$CapacityUnitloadData['ULT'] = $owner->unitloadtype;
        	$CapacityUnitloadData['SKUT'] = $owner->skutype;
        	if($user->isSystemAdmin() || $user->isSiteAdmin())
        	{
        		
        		$CapacityUnitloadData['permission'] = true;
				$CapacityUnitloadData['siteBased']=false;
				if($user->isSystemAdmin())
					$CapacityUnitloadData['role'] = "systemAdmin";
				elseif($user->isSiteAdmin())
					$CapacityUnitloadData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$CapacityUnitloadData['permission'] = true;
				$CapacityUnitloadData['siteBased'] = true;
				$CapacityUnitloadData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$CapacityUnitloadData['permission'] = false;
				$CapacityUnitloadData['siteBased'] = true;
				$CapacityUnitloadData['role'] ="labour";
			}
        }
       	 	//if(sizeof($CapacityUnitloadData['CapacityUnitload']) > 0){
                return $this->respond($CapacityUnitloadData);
            // }else{
            //     return $this->respondNoContent();
            // }
    }

    /**
	* Create new Capacity Unitload
	* @param Attributes of Capacity Unitload to be Created
	* @return Approval of Creation
	*/

    public function store(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'CU_ULT_Type_name'=>'required|exists:Unitload_Type,ULT_Type_name','CU_SKU_Type_name'=>'required|exists:SKU_Type,SKU_Type_name',
			'CU_allocation'=>'required|numeric'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$unitloadTypeName = $request['CU_ULT_Type_name'];
		$skutypename = $request['CU_SKU_Type_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Capacity Location"]);
		else
		{
			$owner = $user->client->owner;
			$unitloadtype =UnitloadType::where('ULT_Type_name',$unitloadTypeName)->where('ULT_Type_OWN_id',$owner->id)->first();
			$skutype=SKUType::where('SKU_Type_name',$skutypename)->where('SKU_Type_OWN_id',$owner->id)->first();
			if($unitloadtype == null || $skutype == null)
				return $this->badRequest(["This Unitloadtype or this SKU Type doesn't Exists"]);
			if(!$this->isUniqueness($unitloadTypeName,$skutypename, $owner->id,0))return $this->badRequest(['Capacity Unitload Exists']);

			$capacityunitload = new CapacityUnitload();
			$capacityunitload->CU_ULT_Type_id = $unitloadtype->id;
			$capacityunitload->CU_SKU_Type_id = $skutype->id;
			$capacityunitload->CU_OWN_id = $owner->id;
			$capacityunitload->CU_allocation = $request['CU_allocation'];
			if($capacityunitload->save())
			{
				return $this->resourceCreated(['Capacity Unitload Successfully Created']);
			}
			else{
				return $this->internalError(['Capacity Unitload Creation has Failed']);
			}
		}
	}


	public function update(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'id' => 'required|exists:Capacity_Unitload,id'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create Capacity Unitload"]);
		else
		{
			$unitloadtypename = $request['ULT_Type_name'];
			$skutypename = $request['SKU_Type_name'];
			$allocation = $request['allocation'];

			$capacityunitload = CapacityUnitload::findOrFail($id);
			if($this->checkIfExists($capacityunitload))
				return $this->badRequest(['There is already a unitloadtype assigned to  SKU Type using this rule']);
			$unitloadtype =UnitloadType::where('ULT_Type_name',$unitloadtypename)->where('ULT_Type_OWN_id',$owner_id)->first();
			$skutype=SKUType::where('SKU_Type_name',$skutypename)->where('SKU_Type_OWN_id',$owner_id)->first();
			if($unitloadtype == null || $unitloadtype == null)
				return $this->badRequest(["This Unitloadtype or this SKU Type doesn't Exists"]);


			$capacityunitload->CU_ULT_Type_id = $unitloadtype->id;
			$capacityunitload->CU_SKU_Type_id = $skutype->id;
			$capacityunitload->CU_OWN_id = $owner_id;
			$capacityunitload->CU_allocation = $allocation;

			if(!$this->isUniqueness($unitloadtypename,$skutypename,$owner_id,$id))return $this->badRequest(['Capacity Unitload Already Exists']);

			if($capacityunitload->save())
			{
				return $this->respond(['Capacity Unitload Successfully Updated']);
			}else{
				return $this->internalError(['Capacity Unitload Update Failed']);
			}
		}
	}

	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Capacity_Unitload,id'
		]);

		$user = \Illuminate\Support\Facades\Request::user();

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Capacity Unitload"]);
		else
		{
			if($v->fails())return $this->badRequest($v->errors()->all());
			$id = intval($request['id']);
			$capacityunitload = CapacityUnitload::findOrFail($id);
			if($this->checkIfExists($capacityunitload))
				return $this->badRequest(['There is already an SKU assigned to  UnitLoad Type using this rule']);
			if($capacityunitload->delete())
			{
				return $this->respond(['Capacity Unitload Deleted Succesfully']);
			}else{
				return $this->internalError(['Capacity Unitload Delete Failed']);
			}
		}
	}

	public function isUniqueness($unitloadTypeName,$skutypename,$owner_id,$id)
	{
		$field = array('id'=>$id);
		$ult_id = UnitloadType::where('ULT_Type_name',$unitloadTypeName)->where('ULT_Type_OWN_id',$owner_id)->first()->id;
		$skut_id =SKUType::where('SKU_Type_name',$skutypename)
		->where('SKU_Type_OWN_id',$owner_id)->first()->id;
		$capacityunitload = CapacityUnitload::where('CU_ULT_Type_id',$ult_id)->where('CU_SKU_Type_id',$skut_id)->whereNotIn('id',$field)->first();
		if($capacityunitload!=null)return false;
		else return true;
	}
	public function checkIfExists($capacityunitload)
	{
		$unitloads = $capacityunitload->unitloadtype->unitload;
		$skus = $capacityunitload->skutype->sku;
		foreach ($unitloads as $key => $unitload) {
			foreach ($skus as $key => $sku) {
				if($unitload->UL_SKU_id == $sku->id)
					return true;
			}
		}
		return false;
	}
}
