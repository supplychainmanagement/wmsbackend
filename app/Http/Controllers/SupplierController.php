<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/9/2016
 * Time: 10:08 AM
 */
namespace App\Http\Controllers;

use App\Http\Requests;
use Validator;
use App\Supplier;
use App\Client;
use App\SKU;
use Illuminate\Http\Request;

class SupplierController extends ApiController
{


    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }


    /**
     * Retrieve All Suppliers
     * @param None
     * @return All Suppliers Attributes
     */
    public function index()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        $suppliers = array();
        $skus = array();
        if($user->isSystemClient())
        {
            if($user->isSystemAdmin())
            {
                $suppliers['suppliers'] = $owner->Supplier->all();
                $suppliers['skus'] = SKU::where('SKU_OWN_id',$owner->id)->get();
                $suppliers['role'] = "systemAdmin";
                $suppliers['permission'] = true;
                $suppliers['siteBased'] = false;

            }elseif($user->isSiteAdmin())
            {
                $suppliers['role'] = "siteAdmin";
                $suppliers['permission'] = true;
                $suppliers['siteBased'] = true;
                $suppliers['suppliers'] = $owner->Supplier->all();
                $suppliers['skus'] = SKU::where('SKU_OWN_id',$owner->id)->get();

            }elseif($user->isCheifOfLabours())
            {
                $suppliers['role'] = "cheifOfLabours";
                $suppliers['siteBased'] = true;
                $suppliers['permission'] = false;
                $suppliers['suppliers'] = $owner->Supplier->all();
                $suppliers['skus'] = SKU::where('SKU_OWN_id',$owner->id)->get();

            }elseif($user->isLabour())
            {
                $suppliers['role'] = "labour";
                $suppliers['siteBased'] = true;
                $suppliers['permission'] = false;
                $suppliers['suppliers'] = $owner->Supplier->all();
                $suppliers['skus'] = SKU::where('SKU_OWN_id',$owner->id)->get();

            }
        }

        return $this->respond($suppliers);

    }

    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'SUP_name'=>'required',
        ]);
        if($v->fails())return $this->badRequest(['Missing Inputs']);
    //        $v2 = Validator::make($request->all(),[
    //            'SUP_name'=> 'unique_with:Supplier,SUP_OWN_id'
    //        ]);
    //        if($v2->fails())return $this->badRequest(['This Supplier Already Exists']);
        $user = \Illuminate\Support\Facades\Request::user();
        if(!$user->isSystemAdmin())return $this->unauthorized(['Unauthorized Access']);
        $supplier_name = $request['SUP_name'];
        $phone = null;
        $address = null;
        $email = null;
        if($request->has('SUP_phone')) $phone = $request['SUP_phone'];
        if($request->has('SUP_address')) $address = $request['SUP_address'];
        if($request->has('SUP_email')) $email = $request['SUP_email'];


        $supplier = new Supplier();
        $supplier->SUP_NAME = $supplier_name;
        $supplier->SUP_email = $email;
        $supplier->SUP_address = $address;
        $supplier->SUP_phone = $phone;
        $supplier->SUP_OWN_id = $user->client->owner->id;

        if($supplier->save())
        {
            if($request->has('SUP_skus'))
            {
                for($i = 0;$i<sizeof($request['SUP_skus']);$i++)
                {
                    $sku_id = SKU::where('SKU_name',$request['SUP_skus'][$i])->first()->id;
                    $supplier->SUPSKU()->attach($sku_id);
                }
            }
            return $this->resourceCreated(['Supplier Created Successfully']);
        }
        return $this->internalError(['Supplier Creation Failed']);

    }

    public function destroy(Request $request)
    {
        $v = Validator::make($request->all(),[
            'id' => 'required',
        ]);
        if($v->fails())return $this->badRequest(['Missing Index']);
        $id = intval($request['id']);
        $user = \Illuminate\Support\Facades\Request::user();
        if(!$user->isSystemAdmin())return $this->unauthorized(['Unauthorized Access']);
        $supplier = Supplier::findOrFail($id);
        if($supplier->delete())return $this->respond(['Supplier Deleted Successfully']);
        return $this->internalError(['Supplier Deleting Failed']);
    }

    public function update(Request $request)
    {
        $v = Validator::make($request->all(),[
            'SUP_name' => 'required',
            'id'=>'required|exists:Supplier,id'
        ]);
        if($v->fails())return $this->badRequest(['Missing Inputs']);
        $supplier_name = $request['SUP_name'];
        $id = intval($request['id']);

        $supplier = Supplier::findOrFail($id);
        $owner_id = \Illuminate\Support\Facades\Request::user()->client->owner->id;
        $user = \Illuminate\Support\Facades\Request::user();
        if(!$user->isSystemAdmin())return $this->unauthorized(['Unauthorized Access']);
        $supplier->SUP_name = $supplier_name;

        if($request->has('SUP_phone'))$supplier->SUP_phone = $request['SUP_phone'];
        if($request->has('SUP_address'))$supplier->SUP_address = $request['SUP_address'];
        if($request->has('SUP_email'))$supplier->SUP_email = $request['SUP_email'];
        if($request->has('SUP_skus'))
        {
            $supplier->SUPSKU()->detach();
            for($i=0;$i<sizeof($request['SUP_skus']);$i++)
            {
                $sku_id = SKU::where('SKU_name',$request['SUP_skus'][$i])->where('SKU_OWN_id',$owner_id)->first()->id;
                $supplier->SUPSKU()->attach($sku_id);
            }
        }

        if(Supplier::where('SUP_name',$supplier_name)->where('SUP_OWN_id',$owner_id)->whereNotIn('id',[$id])->exists())
            return $this->badRequest(['This Supplier Already Exists']);

        if($supplier->save())return $this->respond(['Supplier Updated Successfully']);
        return $this->internalError(['Updating Supplier Failed']);


    }

    public function supplierProp(Request $request)
    {
        $v = Validator::make($request->all(),[
            'id'=>'required|exists:Supplier,id',
        ]);
        if($v->fails())return $this->badRequest(['Missing Index']);
        $id = intval($request['id']);
        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;
        $site = false;
        $admin = false;
        $Supplier = null;
        $skus = null;
        if($user->isSystemClient())
        {
            if($user->isSystemAdmin())
            {
                $admin = true;
                $site = false;
                $Supplier = Supplier::findOrFail($id);
                $skus = SKU::where('SKU_OWN_id',$owner_id)->get();
            }else
            {
                $admin = false;
                $site = true;
                $Supplier = Supplier::findOrFail($id);
                $skus = SKU::where('SKU_OWN_id',$owner_id)->get();
            }
        }
        $respond = array();
        $respond['admin'] = $admin;
        $respond['site'] = $site;
        $respond['supplier'] = $Supplier;
        $respond['skus'] = $skus;
        $respond['supplier_skus'] = $Supplier->SUPSKU->all();
        json_encode($respond);
        return $this->respond($respond);
    }
}