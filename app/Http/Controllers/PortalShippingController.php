<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use DB;
use Carbon\Carbon;
use App\IssueItem;
use App\Unitload;
use App\Shipping;
use App\CapacityUnitload;

class PortalShippingController extends ApiController
{
	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function generateShipJobName()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $receivingjob_name = 'SHIP_'.((Shipping::where('SHIP_OWN_id',$owner->id)->count())+1);

        $time_start = microtime(true) * 10000;
        $receivingjob_name = $receivingjob_name.'_'. $time_start;
        return $this->respond([$receivingjob_name]);
    }


    public function ship(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if(!$user->isSystemAdmin() && !$user->isSiteAdmin() && !$user->isCheifOfLabours())
        {
            return $this->unauthorized(['You do not have Permission to make this Operation']);
        }

        $v = Validator::make($request->all(),[
            'ISI_number'=>'required',
            'SHIP_date'=>'required|date_format:"Y-m-d"',
            'SHIP_shippingAgency'=>'required',
            'SHIP_number'=>'required'
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $issueorderitem = IssueItem::where('ISI_OWN_id',$owner->id)->where('ISI_number',$request['ISI_number'])->first();
        if($issueorderitem == null)
            return $this->badRequest(['Issue Item Does not Exist']);

        if($issueorderitem->ISI_shipped == $issueorderitem->ISI_amount)
           return $this->badRequest(['Selected Issue Item has already been shipped']);

        $pickrequestitems = $issueorderitem->PickRequest->PickRequestItem; 
        if($issueorderitem->ISI_picked < $issueorderitem->ISI_amount)
            return $this->badRequest(['Selected Issue Item has not been totally picked yet']);

        $today = Carbon::now()->toDateString();
        if($request->SHIP_date < $today)
        {
            return $this->badRequest(['Shipping Date is Invalid']);
        } 

        DB::beginTransaction(); 

        $shipping =  new Shipping();
        $shipping->SHIP_OWN_id = $owner->id;
        $shipping->SHIP_shippingAgency = $request->SHIP_shippingAgency;
        $shipping->SHIP_date = $request->SHIP_date;
        $shipping->SHIP_ISI_id = $issueorderitem->id;
        $shipping->SHIP_SITE_id = $issueorderitem->IssueOrder->Site->id;
        $shipping->SHIP_number = $request->SHIP_number;
        $shipping->SHIP_amount = $issueorderitem->ISI_amount; 

        if(!$shipping->save())
        {
            DB::rollBack();
            return $this->internalError(['Shipping Creation has Failed']);
        }

        $issueorderitem->ISI_shipped=($issueorderitem->ISI_shipped) + $shipping->SHIP_amount;
        if(!$issueorderitem->save())
        {
            DB::rollBack();
            return $this->internalError(['Issue Order Item Updating has Failed']);
        }

        foreach ($pickrequestitems as $key => $pickrequestitem) {

            if($pickrequestitem->PRI_type == 'storing')
            {
                $unitload = $pickrequestitem->Unitload;
                $capacity = CapacityUnitload::where('CU_OWN_id',$owner->id)->where('CU_ULT_Type_id',$unitload->UnitloadType->id)->where('CU_SKU_Type_id',$issueorderitem->SKU->SKUType->id)->first();

                if($capacity == null)
                {
                    DB::rollBack();
                    return $this->badRequest(['Unitload Capacity Rule Not Found']);
                }

                $allocation = $capacity->CU_allocation;

                $unitload->UL_amount = ($unitload->UL_amount) - ($pickrequestitem->PRI_amount);
                $unitload->UL_allocation = ($unitload->UL_allocation) - (intval($pickrequestitem->PRI_amount) * intval($allocation));
                if(!$unitload->save())
                {
                    DB::rollBack();
                    return $this->internalError(['Unitload Updating has Failed']);
                }
            }

        }       

        DB::commit();
        return $this->resourceCreated(['Shipping Successfully Created']);

    }
}
