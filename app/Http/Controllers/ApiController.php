<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
class ApiController extends Controller
{
    protected $statusCode = 200;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        $this->statusCode = 200;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respondWithStatusCode($statusCode,$data=[]){
        return Response::json(['errors' => $data],$statusCode,[]);

    }

    public function respond(array $data,array $headers = []){
        return Response::json($data,$this->statusCode,$headers);
    }


    public function respondWithPagination(Paginator $paginator,$data){
        $returnArray = ["data" => $data , "paginator" => [
            'total_count'  => $paginator->total(),
            'total_pages'  => ceil($paginator->total() / $paginator->perPage()),
            'current_page' => $paginator->currentPage(),
            'limit'        => $paginator->perPage()
        ] ];

        return $this->respond($returnArray);
    }

    public function respondWithError($errors = []){
        return $this->respond([
            'errors' => $errors
        ]);
    }

    public function respondNoContent(){
        return $this->setStatusCode(\Illuminate\Http\Response::HTTP_NO_CONTENT)->respond([]);
    }

    public function badRequest($errors = []){
        return $this->setStatusCode(\Illuminate\Http\Response::HTTP_BAD_REQUEST)->respondWithError($errors);
    }

    public function internalError($errors = []){
        return $this->setStatusCode(\Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($errors);
    }

    public function requestAccepted($data = ['Accepted']){
        return $this->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED)->respond($data);
    }

    public function resourceCreated($data = ['Created']){
        return $this->setStatusCode(\Illuminate\Http\Response::HTTP_CREATED)->respond($data);
    }

    public function unauthorized($data = ['Unauthorized Access']){
        return $this->setStatusCode(\Illuminate\Http\Response::HTTP_UNAUTHORIZED)->respond($data);
    }

    public function authorizeRequest($item)
    {
        if(Request::user()->id != $item->user()->getResults()->id){
            return false;
        }else{
            return true;
        }
    }
}
