<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\Site;
use App\FunctionalArea;

class FunctionalAreaController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$FunctionalAreaData =array();
        if($user->isSystemClient()){
			$FunctionalAreaData['systemClient']=true;
        	if($user->isSystemAdmin() || $user->isSiteAdmin())
        	{
        		$FunctionalAreaData['functionalArea']= $owner->functionalarea;
        		$FunctionalAreaData['permission'] = true;
				$FunctionalAreaData['siteBased']=false;
				if($user->isSystemAdmin())
					$FunctionalAreaData['role'] = "systemAdmin";
				elseif($user->isSiteAdmin())
					$FunctionalAreaData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$FunctionalAreaData['functionalArea']= $owner->functionalarea;
        		$FunctionalAreaData['permission'] = true;
				$FunctionalAreaData['siteBased'] = true;
				$FunctionalAreaData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$FunctionalAreaData['functionalArea']= $owner->functionalarea;
				$FunctionalAreaData['permission'] = false;
				$FunctionalAreaData['siteBased'] = true;
				$FunctionalAreaData['role'] ="labour";
			}
        }
       	 	return $this->respond($FunctionalAreaData);
            
        
    }
    /**
	* Create new FunctionalArea
	* @param Attributes of Functional Area to be Created
	* @return Approval of Creation
	*/

    public function store(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'FA_name'=>'required','FA_GoodsIn'=>'required'
			,'FA_GoodsOut'=>'required','FA_Storage'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$name = $request['FA_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Functional area"]);
		else
		{
			$owner = $user->client->owner;
			if(!$this->isUniqueness($name, $owner->id,0))return $this->badRequest(['Functional Area name already Exists']);

			$functionalArea = new FunctionalArea();
			$functionalArea->FA_name = $name;
			$functionalArea->FA_OWN_id = $owner->id;
			$functionalArea->FA_GoodsIn =$request['FA_GoodsIn'];
			$functionalArea->FA_GoodsOut=$request['FA_GoodsOut'];
			$functionalArea->FA_Storage=$request['FA_Storage'];

			if($functionalArea->save())
			{
				return $this->resourceCreated(['Functional Area Successfully Created']);
			}
			else{
				return $this->internalError(['Functional Area Creation has Failed']);
			}
		}
	}

	/**
	 * Update Functional Area
	 * @param Functional Area id to be Updated & New Attributes
	 * @return Update Approval
	 */
	public function update(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'id' => 'required|exists:Functional_Area,id','FA_name'=>'required','FA_GoodsIn'=>'required'
			,'FA_GoodsOut'=>'required','FA_Storage'=>'required'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Functional area"]);
		else
		{
			$name = $request['FA_name'];

			$functionalArea = FunctionalArea::findOrFail($id);
			if(sizeof($functionalArea->storagelocation)>0 ||sizeof($functionalArea->bin)>0)
				return $this->badRequest(['There is already a storage location or bin assigned to this functional area']);
			$functionalArea->FA_name = $name;
			$functionalArea->FA_OWN_id = $owner_id;
			$functionalArea->FA_GoodsIn =$request['FA_GoodsIn'];
			$functionalArea->FA_GoodsOut=$request['FA_GoodsOut'];
			$functionalArea->FA_Storage=$request['FA_Storage'];

			if(!$this->isUniqueness($name,$owner_id,$id))return $this->badRequest(['Functional  area with this Name Already Exists']);

			if($functionalArea->save())
			{
				return $this->respond(['Functional area Successfully Updated']);
			}else{
				return $this->internalError(['Functional area  Update Failed']);
			}
		}


	}

	/**
	 * Delete FA
	 * @param FA
	 * @return Delete Approval
	 */
	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Functional_Area,id'
		]);

		$user = \Illuminate\Support\Facades\Request::user();

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Functional area"]);
		else
		{
			if($v->fails())return $this->badRequest($v->errors()->all());
			$id = intval($request['id']);
			$functionalArea = FunctionalArea::findOrFail($id);
			if(sizeof($functionalArea->storagelocation)>0 ||sizeof($functionalArea->bin)>0)
				return $this->badRequest(['There is already a storage location or bin assigned to this functional area']);
			if($functionalArea->delete())
			{
				return $this->respond(['Functional Area Deleted Succesfully']);
			}else{
				return $this->internalError(['Functional Area Delete Failed']);
			}
		}
	}


	public function isUniqueness($name,$owner_id,$id)
	{
		$field = array('id'=>$id);
		$functionalArea = FunctionalArea::where('FA_name',$name)
		->where('FA_OWN_id',$owner_id)->whereNotIn('id',$field)->first();
		if($functionalArea!=null)return false;
		else return true;
	}
}
