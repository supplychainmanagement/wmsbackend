<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\Site;
use App\SKUType;

class SKUTypeController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;

    	$SKUTypeData =array();
        if($user->isSystemClient()){
			$UnitloadTypeData['systemClient']=true;
        	if($user->isSystemAdmin() || $user->isSiteAdmin())
        	{
        		$SKUTypeData['SKUType']= $owner->SKUType;
        		$SKUTypeData['permission'] = true;
				$SKUTypeData['siteBased']=false;
				if($user->isSystemAdmin())
					$SKUTypeData['role'] = "systemAdmin";
				elseif($user->isSiteAdmin())
					$SKUTypeData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$SKUTypeData['SKUType']= $owner->SKUType;
        		$SKUTypeData['permission'] = true;
				$SKUTypeData['siteBased'] = true;
				$SKUTypeData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$SKUTypeData['SKUType']= $owner->SKUType;
				$SKUTypeData['permission'] = false;
				$SKUTypeData['siteBased'] = true;
				$SKUTypeData['role'] ="labour";
			}
        }
        	//return $owner->SKUType;
       	 	return $this->respond($SKUTypeData);
            
    }

    /**
	* Create new SKUType
	* @param Attributes of SKUType to be Created
	* @return Approval of Creation
	*/

    public function store(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'SKU_Type_name'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$name = $request['SKU_Type_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a SKUType"]);
		else
		{
			$owner = $user->client->owner;
			if(!$this->isUniqueness($name, $owner->id,0))return $this->badRequest(['SKUType already Exists']);

			$sKUType = new SKUType();
			$sKUType->SKU_Type_name = $name;
			$sKUType->SKU_Type_OWN_id = $owner->id;
			if($sKUType->save())
			{
				return $this->resourceCreated(['SKUType Successfully Created']);
			}
			else{
				return $this->internalError(['SKUType Creation has Failed']);
			}
		}
	}

	/**
	 * Update SKUType
	 * @param SKUType id to be Updated & New Attributes
	 * @return Update Approval
	 */
	public function update(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'id' => 'required|exists:SKU_Type,id','SKU_Type_name'=>'required'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create SKUType"]);
		else
		{
			$name = $request['SKU_Type_name'];

			$sKUType = SKUType::findOrFail($id);
			// if(sizeof($storageLocationType->storagelocation)>0)
			// 	return $this->badRequest(['There is already a storage location assigned to this Storage Location Type']);
			$sKUType->SKU_Type_name = $name;
			$sKUType->SKU_Type_OWN_id = $owner_id;

			if(!$this->isUniqueness($name,$owner_id,$id))return $this->badRequest(['SKUType with this Name Already Exists']);

			if($sKUType->save())
			{
				return $this->respond(['SKUType Successfully Updated']);
			}else{
				return $this->internalError(['SKUType Update Failed']);
			}
		}
	}

	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:SKU_Type,id'
		]);

		$user = \Illuminate\Support\Facades\Request::user();

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a SKUType"]);
		else
		{
			if($v->fails())return $this->badRequest($v->errors()->all());
			$id = intval($request['id']);
			$sKUType = SKUType::findOrFail($id);
			if(sizeof($sKUType->SKU)>0)
				return $this->badRequest(['There is already an SKU assigned to this SKUType']);
			if($sKUType->delete())
			{
				return $this->respond(['SKUType Deleted Succesfully']);
			}else{
				return $this->internalError(['SKUType Delete Failed']);
			}
		}
	}


	public function isUniqueness($name,$owner_id,$id)
	{
		$field = array('id'=>$id);
		$sKUType = SKUType::where('SKU_Type_name',$name)
		->where('SKU_Type_OWN_id',$owner_id)->whereNotIn('id',$field)->first();
		if($sKUType!=null)return false;
		else return true;
	}
}
