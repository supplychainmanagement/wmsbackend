<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\Site;
use App\UnitloadType;

class UnitloadTypeController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$UnitloadTypeData =array();
        if($user->isSystemClient()){
			$UnitloadTypeData['systemClient']=true;
        	if($user->isSystemAdmin() || $user->isSiteAdmin())
        	{
        		$UnitloadTypeData['unitloadType']= $owner->UnitloadType;
        		$UnitloadTypeData['permission'] = true;
				$UnitloadTypeData['siteBased']=false;
				if($user->isSystemAdmin())
					$UnitloadTypeData['role'] = "systemAdmin";
				elseif($user->isSiteAdmin())
					$UnitloadTypeData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$UnitloadTypeData['unitloadType']= $owner->UnitloadType;
        		$UnitloadTypeData['permission'] = true;
				$UnitloadTypeData['siteBased'] = true;
				$UnitloadTypeData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$UnitloadTypeData['unitloadType']= $owner->UnitloadType;
				$UnitloadTypeData['permission'] = false;
				$UnitloadTypeData['siteBased'] = true;
				$UnitloadTypeData['role'] ="labour";
			}
        }
       	 	return $this->respond($UnitloadTypeData);
            
    }

    /**
	* Create new Unitloadtype
	* @param Attributes of Unitloadtype to be Created
	* @return Approval of Creation
	*/

    public function store(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'ULT_Type_name'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$name = $request['ULT_Type_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Unitloadtype"]);
		else
		{
			$owner = $user->client->owner;
			if(!$this->isUniqueness($name, $owner->id,0))return $this->badRequest(['Unitloadtype already Exists']);

			$unitloadType = new UnitloadType();
			$unitloadType->ULT_Type_name = $name;
			$unitloadType->ULT_Type_OWN_id = $owner->id;
			if($unitloadType->save())
			{
				return $this->resourceCreated(['Unitloadtype Successfully Created']);
			}
			else{
				return $this->internalError(['Unitloadtype Creation has Failed']);
			}
		}
	}

	/**
	 * Update Unitloadtype
	 * @param Unitloadtype id to be Updated & New Attributes
	 * @return Update Approval
	 */
	public function update(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'id' => 'required|exists:Unitload_Type,id','ULT_Type_name'=>'required'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create Unitloadtype"]);
		else
		{
			$name = $request['ULT_Type_name'];

			$unitloadType = UnitloadType::findOrFail($id);
			// if(sizeof($storageLocationType->storagelocation)>0)
			// 	return $this->badRequest(['There is already a storage location assigned to this Storage Location Type']);
			$unitloadType->ULT_Type_name = $name;
			$unitloadType->ULT_Type_OWN_id = $owner_id;

			if(!$this->isUniqueness($name,$owner_id,$id))return $this->badRequest(['Unitloadtype with this Name Already Exists']);

			if($unitloadType->save())
			{
				return $this->respond(['Unitloadtype Successfully Updated']);
			}else{
				return $this->internalError(['Unitloadtype Update Failed']);
			}
		}


	}


	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Unitload_Type,id'
		]);

		$user = \Illuminate\Support\Facades\Request::user();

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Unitloadtype"]);
		else
		{
			if($v->fails())return $this->badRequest($v->errors()->all());
			$id = intval($request['id']);
			$unitloadType = UnitloadType::findOrFail($id);
			if(sizeof($unitloadType->Unitload)>0)
				return $this->badRequest(['There is already a Unitload assigned to this Unitloadtype']);
			if($unitloadType->delete())
			{
				return $this->respond(['Unitloadtype Deleted Succesfully']);
			}else{
				return $this->internalError(['Unitloadtype Delete Failed']);
			}
		}
	}


	public function isUniqueness($name,$owner_id,$id)
	{
		$field = array('id'=>$id);
		$unitloadType = UnitloadType::where('ULT_Type_name',$name)
		->where('ULT_Type_OWN_id',$owner_id)->whereNotIn('id',$field)->first();
		if($unitloadType!=null)return false;
		else return true;
	}
}
