<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Strategy;

class StrategyController extends ApiController
{

	public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
    	$strategies = Strategy::all();
        $strategies = json_decode($strategies,true);
    	return $this->respond($strategies);
    }

    public function create(Request $request)
    {
    	return $this->unauthorized(['You do not have Permission to make this Operation']);
    }

    public function prop(Request $request)
    {
    	return $this->unauthorized(['You do not have Permission to make this Operation']);
    }

    public function update(Request $request)
    {
    	return $this->unauthorized(['You do not have Permission to make this Operation']);
    }

    public function delete(Request $request)
    {
    	return $this->unauthorized(['You do not have Permission to make this Operation']);
    }
}
