<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\RFQ;

class RFQController extends ApiController
{
    public function createRFQ(Request $request)
    {
    	$v = Validator::make($request->all(), [
            'RFQ_companyName' => 'required',
            'RFQ_name' => 'required',
            'RFQ_ERPPlatform' => 'required',
            'RFQ_numberOfEmployees' => 'required',
            'RFQ_email' => 'required',
            'RFQ_phone' => 'required',
            'RFQ_noOfSites' => 'required',
            'RFQ_noOfOperationsPerMonth' => 'required'
        ]);

        if ($v->fails()) 
            return $this->badRequest($v->errors()->all());


        $rfq = new RFQ();
		$rfq->RFQ_companyName = $request->RFQ_companyName;
		$rfq->RFQ_name = $request->RFQ_name;
		$rfq->RFQ_ERPPlatform = $request->RFQ_ERPPlatform;
		$rfq->RFQ_numberOfEmployees = $request->RFQ_numberOfEmployees;
		$rfq->RFQ_email = $request->RFQ_email;
		$rfq->RFQ_phone = $request->RFQ_phone;
		$rfq->RFQ_noOfSites = $request->RFQ_noOfSites;
		$rfq->RFQ_noOfOperationsPerMonth = $request->RFQ_noOfOperationsPerMonth;		

		if($rfq->save())
		{
			return $this->resourceCreated(['RFQ Successfully Created']);
		}else{
			return $this->internalError(['RFQ Creation has Failed']);
		}
    }
}
