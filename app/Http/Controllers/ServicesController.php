<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/16/2016
 * Time: 5:01 PM
 */
namespace App\Http\Controllers;

use App\FunctionalArea;
use App\Zone;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Unitload;
use App\Client;
use App\StorageLocation;
use App\UnitloadType;
use App\SKU;
use App\Rack;
use App\StorageLocationType;
use App\Site;

class ServicesController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function getZoneBySiteName(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        $site = Site::where('SITE_OWN_id',$owner->id)->where('SITE_name',$request->SITE_name)->first();
        $zones = $site->zone;
        $zones = json_decode($zones,true);
        return $this->respond($zones);
    }

    public function getRackByZoneName(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        $zone = Zone::where('ZONE_name',$request->ZONE_name)->where('ZONE_OWN_id',$owner->id)->first();
        $racks = $zone->Rack;
        $racks = json_decode($racks,true);
        return $this->respond($racks);
    }
}