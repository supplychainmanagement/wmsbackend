<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\Site;

class SiteController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }
    /**
	 * View Sites
	 * @param none
	 * @return Site Based on Permissions
	 */
	public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$SiteData =array();

			$SiteData['systemClient']=true;
        	if($user->isSystemAdmin())
        	{
        		$clients = $owner->Client;
        		$sites = $owner->Site;
        		$available_clients = array();
        		foreach ($clients as $key => $value) {
        		   if($value['CLI_systemClient'] != 1)
        		   {
        		   	 $client_site = $value->CLISite;
        		   	 if(sizeOf($client_site) == 0)
        		   	 {
        		   	 	array_push($available_clients,$value);
        		   	 }	
        		   }
        		}
        		$SiteData['clients']=$available_clients;
        		$SiteData['sites']= $sites->all();
        		$SiteData['permission'] = true;
				$SiteData['siteBased']=false;
				$SiteData['role'] = "systemAdmin";
        	}
        	elseif($user->isSiteAdmin())
        	{
        		$site = $user->Site;
                $SiteData['sites'] = [$site];
        		$clients = $owner->Client;
        		$SiteData['clients']=$owner->Client->all();
        		$SiteData['permission'] = false;
				$SiteData['siteBased'] = true;
				$SiteData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$site = $user->Site;
                $SiteData['sites'] =[$site];
        		$SiteData['permission'] = false;
				$SiteData['siteBased'] = true;
                $SiteData['clients']=$owner->Client->all();
				$SiteData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$site = $user->Site;
                $SiteData['sites'] =[$site];
                $SiteData['clients']=$site->client;
				$SiteData['permission'] = false;
				$SiteData['siteBased'] = true;
				$SiteData['role'] ="labour";
			}

       	 	if(sizeof($SiteData['sites']) > 0){
                return $this->respond($SiteData);
            }else{
                return $this->respondNoContent();
            }

        
    }

    /**
	* Create new Site
	* @param Attributes of Site to be Created
	* @return Approval of Creation
	*/

    public function store(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'SITE_name'=>'required',
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$name = $request['SITE_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		if(!($user->isSystemClient() &&$user->isSystemAdmin()))
			return $this->unauthorized(["you don't have the permission to create a site"]);
		else
		{
			$client_id = $user->client->id;
			$owner = $user->client->owner;
			if(!$this->isUniqueness($name, $owner->id,0))return $this->badRequest(['Site name already Exists']);

			$site = new Site();
			$site->SITE_name = $name;
			$site->SITE_OWN_id = $owner->id;


			if($site->save())
			{
				if($request->has('SITE_client')){
					for($i=0;$i<sizeof($request['SITE_client']);$i++) {
						$client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_name',$request['SITE_client'][$i])->first();
						$client_site = $client->CLISite;
						if(sizeof($client_site)>0)
							return $this->badRequest(['Site Created But Client already has a site attached to it']);
						else
							$site->client()->attach($client->id);
					}
				}
				return $this->resourceCreated(['Site Successfully Created']);
			}
			else{
				return $this->internalError(['Site Creation has Failed']);
			}
		}
		


	}
	/*
	 * Update SITE
	 * @param SITE id to be Updated & New Attributes
	 * @return Update Approval
	 */
	public function update(Request $request)
	{
		
		$v = Validator::make(['id'=>$request['id']],[
			'id' => 'required|exists:Site,id'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;

		if(!($user->isSystemClient() &&$user->isSystemAdmin()))
			return $this->unauthorized(["you don't have the permission to update a site"]);
		else
		{
			$name = $request['SITE_name'];

			$site = Site::findOrFail($id);
			$site->SITE_name = $name;
			$site->SITE_OWN_id = $owner_id;

			if(!$this->isUniqueness($name,$owner_id,$id))return $this->badRequest(['Site Name Already Exists']);
			if(!$request->has('CLI_name'))$site->client()->detach();
			else{
				$site->client()->detach();
				for($i = 0;$i<sizeof($request['CLI_name']);$i++)
				{
					$client_id = Client::where('CLI_name',$request['CLI_name'][$i])->where('CLI_OWN_id',$owner_id)->first()->id;
					$site->client()->attach($client_id);
				}
			}


			if($site->save())
			{
				return $this->respond(['Site Successfully Updated']);
			}else{
				return $this->internalError(['Site Update Failed']);
			}
		}


	}
	/*
	 * Delete SITE
	 * @param SITE
	 * @return Delete Approval
	 */
	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Site,id'
		]);

		$user = \Illuminate\Support\Facades\Request::user();

		if(!($user->isSystemClient() &&$user->isSystemAdmin()))
			return $this->unauthorized(["you don't have the permission to update a site"]);
		else
		{
			if($v->fails())return $this->badRequest($v->errors()->all());
			$id = intval($request['id']);
			$site = Site::findOrFail($id);
			if(sizeof($site->zone)>0)
				return $this->badRequest(['This Site Contains Zones please delete them first']);
			if(sizeof($site->IssueOrder)>0)
				return $this->badRequest(['This Site Contains Issue Orders please delete them first']);
			if(sizeof($site->IssueOrderItem)>0)
				return $this->badRequest(['This Site Contains Issue Order Items please delete them first']);
			if(sizeof($site->StorageJob)>0)
				return $this->badRequest(['This Site Contains Storage Jobs please delete them first']);
			if(sizeof($site->PickJob)>0)
				return $this->badRequest(['This Site Contains Pick Jobs please delete them first']);
			if(sizeof($site->PickRequest)>0)
				return $this->badRequest(['This Site Contains Pick Requests please delete them first']);
			if(sizeof($site->PickRequestItem)>0)
				return $this->badRequest(['This Site Contains Pick Request Items please delete them first']);
			if(sizeof($site->ReceivingJob)>0)
				return $this->badRequest(['This Site Contains Receiving Jobs please delete them first']);
			if(sizeof($site->ReceivingJobUnitload)>0)
				return $this->badRequest(['This Site Contains Receiving Job Unitloads please delete them first']);
			if(sizeof($site->PurchaseOrderItems)>0)
				return $this->badRequest(['This Site Contains Purchase Order Items please delete them first']);
			if(sizeof($site->StorageLocation)>0)
				return $this->badRequest(['This Site Contains Storage Locations please delete them first']);
			if(sizeof($site->Unitload)>0)
				return $this->badRequest(['This Site Contains Unitloads please delete them first']);
			if(sizeof($site->User)>0)
				return $this->badRequest(['This Site Contains Users please delete them first']);
			if(sizeof($site->Shipping)>0)
				return $this->badRequest(['This Site Contains Shippings please delete them first']);
			if(sizeof($site->PurchaseOrder)>0)
				return $this->badRequest(['This Site Contains Purchase Orders please delete them first']);
			$site->client()->detach();
			if($site->delete())
			{
				return $this->respond(['Site Deleted Succesfully']);
			}else{
				return $this->internalError(['Site Delete Failed']);
			}
		}
	}
	/**
	 * View Properties of Site to be Updated
	 * @param Site id
	 * @return Site Properties (Attributes)
	 */
	public function siteprop (Request $request)
	{
		
		$v = Validator::make(['id'=> $request['id']],[
			'id' => 'integer|exists:Site,id'
		]);

		if($v->fails()) return $this->badRequest($v->errors()->all());
		$id = intval($request['id']);

		$user = \Illuminate\Support\Facades\Request::user();
        //return $id;
		$sites = Site::where('id',$id)->first();
		$clients = array();
		$attached = array();
		$admin = false;
		$site = false;

		$clientsArray =$user->client->owner->client->all();
		foreach($clientsArray as $client)
		{
			if($client['CLI_systemClient'] !=1 )array_push($clients,$client);
		}

		if($user->isSystemAdmin())
		{
			$admin = true;
			$site = false;

		}else if($user->isSiteAdmin() )
		{
			$admin = false;
			$site = true;
		}else
        {
            $admin = false;
            $site = true;
        }
		$respond = array();
			$attachedClients = $sites->Client;
			foreach($attachedClients as $attachedClient)
			{
				array_push($attached,$attachedClient['CLI_name']);
			}

		$respond['site'] = $site;
		$respond['admin'] = $admin;
		$respond['clients'] = $clients;
		$respond['sites'] = $sites;
		$respond['attached'] = $attached;
		json_encode($respond);
		return $this->respond($respond);

	}

    public function isUniqueness($name,$owner_id,$id)
	{
		$field = array('id'=>$id);
		$site = Site::where('SITE_name',$name)
		->where('SITE_OWN_id',$owner_id)->whereNotIn('id',$field)->first();
		if($site!=null)return false;
		else return true;
	}
}
