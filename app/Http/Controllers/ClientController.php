<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/4/2016
 * Time: 1:49 PM
 */
namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Client;
use App\Provider;
use App\Customer;
use DB;

class ClientController extends ApiController{

    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }


    /**
     * View All Clients
     * @param None
     * @return Clients, Types(System Clients)
     */
    public function index()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;
        $clients = array();
        if($user->isSystemClient())
        {
            if($user->isSystemAdmin())
            {
                $clients['role'] = "systemAdmin";
                $clients['siteBased'] = false;
                $clients['permission'] = true;
                $clients['clients'] = Client::where('CLI_OWN_id',$owner_id)->get();
            }elseif($user->isSiteAdmin())
            {
                $clients['role'] = "siteAdmin";
                $clients['siteBased'] = true;
                $clients['permission'] = false;
                $site_id = $user->site->id;
                $cli = Client::where('CLI_OWN_id',$owner_id)->get();
                $counter =0;
                foreach($cli as $client)
                {
                    $sites = $client->site;
                    foreach ($sites as $site) {
                        if($site->id == $site_id)
                        {
                            $clients['clients'][$counter] = $client;
                            $counter++;
                        }
                    }
                    
                }
            }elseif($user->isCheifOfLabours())
            {
                $clients['role'] = "cheifOfLabours";
                $clients['siteBased'] = true;
                $clients['permission'] = false;
                $site_id = $user->site->id;
                $cli = Client::where('CLI_OWN_id',$owner_id)->get();
                $counter =0;
                foreach($cli as $client)
                {
                    $sites = $client->site;
                    foreach ($sites as $site) {
                        if($site->id == $site_id)
                        {
                            $clients['clients'][$counter] = $client;
                            $counter++;
                        }
                    }
                }
            }elseif($user->isLabour())
            {
                $clients['role'] = "labour";
                $clients['siteBased'] = true;
                $clients['permission'] = false;
                $site_id = $user->site->id;
                $cli = Client::where('CLI_OWN_id',$owner_id)->get();
                $counter =0;
                foreach($cli as $client)
                {
                    $sites = $client->site;
                    foreach ($sites as $site) {
                        if($site->id == $site_id)
                        {
                            $clients['clients'][$counter] = $client;
                            $counter++;
                        }
                    }
                }
            }

            return $this->respond($clients);
            
        }
    }


    /**
     * Create new Client
     * @param Client Attributes
     * @return Creation Approval
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'CLI_name' => 'required',
            'CLI_phone' => 'numeric',
            'CLI_email'=>'email'
        ]);

        if($v->fails())return $this->badRequest(['Missing Inputs']);

        $user = \Illuminate\Support\Facades\Request::user();
        if(!$user->isSystemAdmin())return $this->unauthorized(['Unauthorized Access']);

        $name = $request['CLI_name'];
        $phone = null;
        $address = null;
        $email = null;
        if($request->has('CLI_phone'))$phone = $request['CLI_phone'];
        if($request->has('CLI_address'))$address = $request['CLI_address'];
        if($request->has('CLI_email'))$email = $request['CLI_email'];
        DB::beginTransaction();
        $client = new Client();
        $client->CLI_name = $name;
        $client->CLI_OWN_id = $user->Client->Owner->id;
        $client->CLI_phone = $phone;
        $client->CLI_address = $address;
        $client->CLI_email = $email;

        if($client->save())
        {
            $customer = new Customer();
            $customer->CST_name = $client->CLI_name."_selfCustomer";
            $customer->CST_email = $email;
            $customer->CST_phone = $phone;
            $customer->CST_address = $address;
            $customer->CST_OWN_id = $user->client->owner->id;
            
             // Create Provider
            $provider = new Provider();
            $provider->PRV_name = $client->CLI_name."_selfProvider";
            $provider->PRV_email = $email;
            $provider->PRV_phone = $phone;
            $provider->PRV_address = $address;
            $provider->PRV_OWN_id = $user->client->owner->id;
            if($customer->save() && $provider->save())
            {
                $provider->client()->attach($client->id);
                $customer->client()->attach($client->id);
                DB::commit();
                return $this->resourceCreated(['Client Created Successfully']);
            }
            else
            {
                DB::rollBack();
                return $this->internalError(['Client Creation has Failed']);
            }
        }
        else
        {
            DB::rollBack();
            return $this->internalError(['Client Creation has Failed']);
        }
    }


    /**
     * Update Client
     * @param Client id and new Attributes
     * @return Approval
     */

    public function update(Request $request)
    {
        $v = Validator::make($request->all(),[
            'id'=>'required|exists:Client,id',
            'CLI_name'=>'required'
        ]);

        if($v->fails())return $this->badRequest(['Missing Inputs']);
        $user = \Illuminate\Support\Facades\Request::user();
        if(!$user->isSystemAdmin())return $this->unauthorized(['Unauthorized Access']);

        $id = intval($request['id']);
        $client = Client::findOrFail($id);
        $client->CLI_name = $request['CLI_name'];
        if($request->has('CLI_phone'))$client->CLI_phone = $request['CLI_phone'];
        if($request->has('CLI_email'))$client->CLI_email = $request['CLI_email'];
        if($request->has('CLI_address'))$client->CLI_address = $request['CLI_address'];

        if($client->save())return $this->respond(['Client has been Updated Successfully']);
        return $this->internalError(['Client Updating Failed']);
    }


    /**
     * Delete Client
     * @param Client id
     * @return Approval
     */
    public function destroy(Request $request)
    {
        $v = Validator::make($request->all(),[
           'id'=>'required|exists:Client,id'
        ]);
        if($v->fails())return $this->badRequest(['Wrong Index']);
        $user = \Illuminate\Support\Facades\Request::user();
        if(!$user->isSystemAdmin())return $this->unauthorized(['Unauthorized Access']);
        $id = intval($request['id']);
        $client = Client::findOrFail($id);
        $providers = $client->CLIProvider;
        $customers =  $client->CLICustomer;
        $unitloads  = $client->unitload;
        $skus =$client->SKU;
        $users =$client->User;
        $purchaseorders = $client->PurchaseOrder;
        $pois =$client->PurchaseOrderItems;
        $iso = $client->IssueOrder;
        //return $providers.$customers;
        //return $this->internalError($providers.$customers);
        if(count($providers) > 0 || count($customers) > 0 || count($unitloads) > 0 || count($skus) > 0 
            || count($users) > 0 || count($purchaseorders) > 0 || count($pois) > 0 || count($iso) > 0)
        {
            $this->badRequest(['There is an attribute associated with this client delete them first']);
        }

        //if()
        
        if($client->delete())
            {
                $client->site()->detach();
                return $this->respond(['Client has Been Deleted Successfully']);
            }
        return $this->internalError(['Internal Server Error']);
    }


    /**
     * Show Attirbutes for selected Client via Ajax
     * @param Client id
     * @return Client's Attributes
     */
    public function clientProp(Request $request)
    {
        $v = Validator::make($request->all(),[
            'id'=>'required|exists:Client,id',
        ]);

        if($v->fails())return $this->badRequest(['Wrong Index']);
        $id = intval($request['id']);
        $client = Client::findOrFail($id);
        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->client->owner->id;
        $site = false;
        $admin = false;
        $client = null;
        if($user->isSystemClient())
        {
            if($user->isSystemAdmin())
            {
                $site = false;
                $admin = true;
                $client = Client::findOrFail($id);
            }elseif($user->isSiteAdmin())
            {
                $site = true;
                $admin = true;
                $client = Client::findOrFail($id);
            }else{
                $site = true;
                $admin = false;
                $client = Client::findOrFail($id);
            }
        }

        $respond = array();
        $respond['site'] = $site;
        $respond['admin'] = $admin;
        $respond['client'] = $client;
        return $this->respond($respond);
    }
}