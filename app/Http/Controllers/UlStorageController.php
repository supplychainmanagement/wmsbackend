<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\ReceivingJobUnitload;
use App\StorageLocation;
use App\Unitload;
use App\StorageJob;
use App\SKU ;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\RoboVics\Transformers;
use App\RoboVics\Transformers\PurchaseOrderTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use RuntimeException;
use DB;
use App\ReceivingJob;
class UlStorageController extends ApiController
{

    public function __construct(PurchaseOrderTransformer $podTransformer)
    {
        parent::__construct();
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function submitStorageJob (Request $request){

        /*
        * Submit a new Receiving Job
        * @param Attributes of RCJ_Id, POI_Id, Amount,
         * ItemSerial, unitLoadSerial, ItemQualityStatus, ItemsProductionDate.
        * @return Approval of Creation
        */

        $v = Validator::make($request->all(),[
            'stj_serial'=>'required|min:0',
            'stl_serial'=>'required',
            'ul_serial'=>'required',
        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $stj_serial= $request['stj_serial'];
        $stl_serial = $request['stl_serial'];
        $ul_serial = $request['ul_serial'];

        $user = \Illuminate\Support\Facades\Request::user();
        $site_id = $user->site->id ;
        $owner_id = $user->site->owner->id;

        /*Not allowed yet operations for business clients*/
        if(!$user->isSystemClient())
        {
            return $this->respondWithError('User is not authorized');
        }



        DB::beginTransaction();

        try {
            // Add the main Data for the Receiving Object itself.

            $ul = Unitload::where('UL_serialNumber', $ul_serial)->get();
            //validate unitload related Data.
            if ($ul->count()==0){
                throw new ModelNotFoundException('No such Unit Load');
            }else
                $ul = $ul[0];

            $stl = StorageLocation::where('STL_serialNumber', $stl_serial)->get();
            //validate unitload related Data.
            if ($stl->count()==0){
                throw new ModelNotFoundException('No Such Storage Location');
            }else
                $stl = $stl[0];

            $newAllocation=null  ; // will be retruned as a parameter.
            $response = $this->validateStorageLocation ($stl,$ul,$newAllocation);
            if($response){
                return $response;
            }
            if(!($ul->CanBeGoodsIn())){

                DB::rollback();
                return $this->respondWithStatusCode(503,'Unit load is not inside Goods in Area');
            }

            //Add the STJ to the Database

            $stj = new StorageJob();
            $stj->STJ_name= $stj_serial;
            $stj->STJ_UL_id =$ul->id;
            $stj->STJ_STL_id =$stl->id ;
            $stj->STJ_SITE_id = $site_id;
            $stj->STJ_OWN_id = $owner_id;
            $stj->save();

            //Update the unitload & Storage Location data.
            $ul->UL_STL_id=  $stl->id;
            $stl->STL_allocation= $newAllocation;

            $stl->save();
            $ul->save() ;

            DB::commit();
            return $this->resourceCreated(['Storage Job has been Successfully Created']);

        }catch(ModelNotFoundException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            return $this->respondWithStatusCode(501,$e->getMessage());
        }
        catch(ValidationException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            return $this->internalError(['Storage Job has Failed1']);
        } catch(\Exception $e)
        {
            DB::rollback();
            return $this->internalError(['Storage Job has Failed2',$e->getMessage()]);
        }
        }

    private function validateStorageLocation ($stl,$ul,&$newAllocation){
        if(!($stl->FunctionalArea->isStorageArea())){
            DB::rollback();
            return $this->respondWithStatusCode(504,'Storage Location is not dedicated for storage');
        }
        $newAllocation = $stl->fitOneMoreUL($ul);
        if(!$newAllocation){
            DB::rollback();
            return $this->respondWithStatusCode(502,' Not enough space in this StorageLocation Or this unit Load cannot be filled.');

        }

    }

    public function generateUniqueStorageJobId(){
        $labor_id =\Illuminate\Support\Facades\Request::user()->id;
        $index = intval($labor_id);

        // Basic Validation.
        if ($index < 0 ){
            return $this->badRequest('index is not valid');
        }
        $time_start = microtime(true) * 10000;
        $unique_string = $labor_id. $time_start;
        return $this->respond((array)$unique_string);
    }


}
