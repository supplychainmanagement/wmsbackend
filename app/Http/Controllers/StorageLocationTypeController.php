<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\Site;
use App\StorageLocationType;

class StorageLocationTypeController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$StorageLocationTypeData =array();
        if($user->isSystemClient()){
			$StorageLocationTypeData['systemClient']=true;
        	if($user->isSystemAdmin() || $user->isSiteAdmin())
        	{
        		$StorageLocationTypeData['storageLocationType']= $owner->StorageLocationType;
        		$StorageLocationTypeData['permission'] = true;
				$StorageLocationTypeData['siteBased']=false;
				if($user->isSystemAdmin())
					$StorageLocationTypeData['role'] = "systemAdmin";
				elseif($user->isSiteAdmin())
					$StorageLocationTypeData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$StorageLocationTypeData['storageLocationType']= $owner->StorageLocationType;
        		$StorageLocationTypeData['permission'] = true;
				$StorageLocationTypeData['siteBased'] = true;
				$StorageLocationTypeData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$StorageLocationTypeData['storageLocationType']= $owner->StorageLocationType;
				$StorageLocationTypeData['permission'] = false;
				$StorageLocationTypeData['siteBased'] = true;
				$StorageLocationTypeData['role'] ="labour";
			}
        }
       	 	return $this->respond($StorageLocationTypeData);
    }

    /**
	* Create new Storage location type
	* @param Attributes of Storage location type to be Created
	* @return Approval of Creation
	*/

    public function store(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'STL_Type_name'=>'required'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$name = $request['STL_Type_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Storage location type"]);
		else
		{
			$owner = $user->client->owner;
			if(!$this->isUniqueness($name, $owner->id,0))return $this->badRequest(['Storage location type already Exists']);

			$StorageLocationTypeData = new StorageLocationType();
			$StorageLocationTypeData->STL_Type_name = $name;
			$StorageLocationTypeData->STL_Type_OWN_id = $owner->id;
			if($StorageLocationTypeData->save())
			{
				return $this->resourceCreated(['Storage location type Successfully Created']);
			}
			else{
				return $this->internalError(['Storage location type Creation has Failed']);
			}
		}
	}

	/**
	 * Update Storage Location Type
	 * @param Storage Location Type id to be Updated & New Attributes
	 * @return Update Approval
	 */
	public function update(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'id' => 'required|exists:Storage_Location_Type,id','STL_Type_name'=>'required'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Storage Location Type"]);
		else
		{
			$name = $request['STL_Type_name'];

			$storageLocationType = StorageLocationType::findOrFail($id);
			// if(sizeof($storageLocationType->storagelocation)>0)
			// 	return $this->badRequest(['There is already a storage location assigned to this Storage Location Type']);
			$storageLocationType->STL_Type_name = $name;
			$storageLocationType->STL_Type_OWN_id = $owner_id;

			if(!$this->isUniqueness($name,$owner_id,$id))return $this->badRequest(['Storage Location Type with this Name Already Exists']);

			if($storageLocationType->save())
			{
				return $this->respond(['Storage Location Type Successfully Updated']);
			}else{
				return $this->internalError(['Storage Location Type Update Failed']);
			}
		}


	}

	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Storage_Location_Type,id'
		]);

		$user = \Illuminate\Support\Facades\Request::user();

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin())))
			return $this->unauthorized(["you don't have the permission to Delete a Storage Location Type"]);
		else
		{
			if($v->fails())return $this->badRequest($v->errors()->all());
			$id = intval($request['id']);
			$storageLocationType = StorageLocationType::findOrFail($id);
			if(sizeof($storageLocationType->storagelocation)>0)
				return $this->badRequest(['There is already a storage location assigned to this Storage Location Type']);
			if($storageLocationType->delete())
			{
				return $this->respond(['Storage Location Type Deleted Succesfully']);
			}else{
				return $this->internalError(['Storage Location Type Delete Failed']);
			}
		}
	}

	public function isUniqueness($name,$owner_id,$id)
	{
		$field = array('id'=>$id);
		$StorageLocationTypeData = StorageLocationType::where('STL_Type_name',$name)
		->where('STL_Type_OWN_id',$owner_id)->whereNotIn('id',$field)->first();
		if($StorageLocationTypeData!=null)return false;
		else return true;
	}
}
