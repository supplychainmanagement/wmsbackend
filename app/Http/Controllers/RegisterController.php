<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/9/2016
 * Time: 2:05 PM
 */
namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Owner;
use App\Client;
use App\Provider;
use App\Customer;
use App\User;
use App\Role;
use App\Site;
use Hash;
use Auth;
use Crypt;
use DB;

class RegisterController extends ApiController
{
    public function checkSubdomain(Request $request)
    {
        $warehousename = $request->warehousename;
        $owner = Owner::where('OWN_warehouseName',$warehousename)->where('OWN_enable',1)->first();
        if($owner == null)
            return $this->unauthorized(['Wrong Subdomain']);
        else
            return $this->respond(['Subdomain Active']);
    }
    public function register(Request $request)
    {

        $v = Validator::make($request->all(),[
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email|unique:Owner,OWN_email',
            'phone' => 'numeric',
            'warehouseName' => 'required|alpha|unique:Owner,OWN_warehouseName',
            'password'=>'required'
        ]);
        if($v->fails())
        {
            return $this->badRequest($v->errors()->all());
        }
        $email = $request['email'];
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        $warehouseName = $request['warehouseName'];
        $phone = "";
        $address = "";
        $username = $request['username'];
        $password = $request['password'];

        if($request->has('phone'))
        {
            $phone = $request['phone'];
        }
        if($request->has('address'))
        {
            $address = $request['address'];
        }
        DB::beginTransaction();
        $owner = new Owner();
        $owner->OWN_email = $email;
        $owner->OWN_firstName = $firstName;
        $owner->OWN_lastName = $lastName;
        $owner->OWN_warehouseName = $warehouseName;
        $owner->OWN_address = $address;
        $owner->OWN_phone = $phone;
        $owner->OWN_enable = 1;
        $owner->OWN_warehouseType = 0;


        if($owner->save())
        {

            //Create System Client
            $client = new Client();
            $client->CLI_name = $firstName." ".$lastName;
            $client->CLI_phone = $phone;
            $client->CLI_address = $address;
            $client->CLI_email = $email;
            $client->CLI_OWN_id = Owner::where('OWN_warehouseName',$warehouseName)->first()->id;
            $client->CLI_systemClient = 1;
            if($client->save())
            {
                //Create User
                $user = new User();
                $user->USR_name = $username;
                $user->password =  Hash::make($password);
                $user->USR_email = $email;
                $user->USR_CLI_id = Client::where('CLI_name',$firstName." ".$lastName)->first()->id;
                // Create Customer
                $customer = new Customer();
                $customer->CST_name = $client->CLI_name."_selfCustomer";
                $customer->CST_email = $email;
                $customer->CST_phone = $phone;
                $customer->CST_address = $address;
                $customer->CST_OWN_id = $owner->id;
                
                 // Create Provider
                $provider = new Provider();
                $provider->PRV_name = $client->CLI_name."_selfProvider";
                $provider->PRV_email = $email;
                $provider->PRV_phone = $phone;
                $provider->PRV_address = $address;
                $provider->PRV_OWN_id = $owner->id;
               

                //

                if($user->save() && $customer->save() && $provider->save())
                {
                    //Finally
                    $user->Role()->attach(1);
                    $provider->client()->attach($client->id);
                    $customer->client()->attach($client->id);
                    DB::commit();
                    return $this->resourceCreated(['Warehouse Added Successfully']);
                }
                else{
                    DB::rollBack();
                    return $this->internalError(['User Registration Failed']);
                }
            }else{
                DB::rollBack();
                return $this->internalError(['System Client Registration Failed']);
            }

        }else{
            DB::rollBack();
            return $this->internalError(['Warehouse Registration Failed']);
        }


    }


    /*
         * User Login
         * @param username and password
         * @return Login Succeed or Failure
         */

    public function login(Request $request)
    {
        $v = Validator::make($request->all(),[
            'username'=>'required',
            'password'=>'required',
            'warehousename'=>'required'
        ]);

        if($v->fails())
        {
            return $this->badRequest([$v->errors()->all()]);
        }

        $username = $request['username'];
        $password = $request['password'];
        $auth_header = base64_encode($username . ':' . $password);
        $encrypted_header = Crypt::encrypt($auth_header);
        $owner = Owner::where('OWN_warehouseName',$request['warehousename'])->first();

        if($owner != null  || sizeof($owner)>0)
        {
             $client = Client::where('CLI_OWN_id',$owner->id)->where('CLI_systemClient',1)->first();
             if($client != null  || sizeof($client)>0)
             {
                 if(Auth::attempt(['USR_name'=>$username, 'password'=>$password,'USR_CLI_id'=>$client->id]))
                {
                    $role = 'other';
                    if(Auth::user()->isSystemAdmin())
                        $role = 'system_admin';
                    elseif(Auth::user()->isSiteAdmin())
                        $role = 'site_admin';
                    elseif(Auth::user()->isCheifOfLabours())
                        $role = 'chief_of_labors';
                    elseif(Auth::user()->isLabour())
                        $role = 'labor';
                    return $this->respond([
                        'Authorization' => $encrypted_header,
                        'user_role' => $role,
                        'system_client' =>Auth::user()->isSystemClient(),
                        'warehouseName'=>$owner->OWN_warehouseName,
                        'warehouseEnable'=>$owner->OWN_enable,
                        'name'=>$request['username'],
                    ]);
                }
                else
                {
                    return $this->badRequest(['Wrong Username or Password']);
                }
             }
             else
             {
                 return $this->badRequest(['System CLient For this Subdomain doens\'t exist']);
             }
            
        }
        else
        {
            return $this->badRequest(['This Subdomain Doesn\'t exsist']);

        }
    }

}