<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Owner;
use App\CapacityLocation;
use App\UnitloadType;
use App\StorageLocationType;

// Final

class CapacityLocationController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function index()
    {
		
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$CapacityLocationData =array();
    	$capacitylocationdata =array();
        if($user->isSystemClient()){
			$CapacityLocationData['systemClient']=true;
			$capacities = $owner->capacitylocation;
			$counter =0;
    		foreach ($capacities as $key => $value) {
    			$capacitylocationdata[$counter]['capacity'] = $value;
    			//return $value;
    			$capacitylocationdata[$counter]['CL_ULT_Type_name'] = 
    				UnitloadType::where('id',$value['CL_ULT_Type_id'])->first()->ULT_Type_name;
    			$capacitylocationdata[$counter]['CL_STL_Type_name'] = StorageLocationType::where('id',$value['CL_STL_Type_id'])->first()->STL_Type_name;
    			$counter++;
    		}
        	$CapacityLocationData['CapacityLocation']= $capacitylocationdata;
        	$CapacityLocationData['ULT'] = $owner->unitloadtype;
        	$CapacityLocationData['STLT'] = $owner->storagelocationtype;
        	if($user->isSystemAdmin() || $user->isSiteAdmin())
        	{
        		
        		$CapacityLocationData['permission'] = true;
				$CapacityLocationData['siteBased']=false;
				if($user->isSystemAdmin())
					$CapacityLocationData['role'] = "systemAdmin";
				elseif($user->isSiteAdmin())
					$CapacityLocationData['role'] = "siteAdmin";
        	}
        	elseif($user->isCheifOfLabours())
        	{
        		$CapacityLocationData['permission'] = true;
				$CapacityLocationData['siteBased'] = true;
				$CapacityLocationData['role'] = "chiefOfLabours";
        	}elseif($user->isLabour())
			{
				$CapacityLocationData['permission'] = false;
				$CapacityLocationData['siteBased'] = true;
				$CapacityLocationData['role'] ="labour";
			}
        }
       	 	//if(sizeof($CapacityLocationData['CapacityLocation']) > 0){
                return $this->respond($CapacityLocationData);
            // }else{
            //     return $this->respondNoContent();
            // }
    }

    /**
	* Create new Capacity Location
	* @param Attributes of Capacity Location to be Created
	* @return Approval of Creation
	*/

    public function store(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'CL_ULT_Type_name'=>'required|exists:Unitload_Type,ULT_Type_name','CL_STL_Type_name'=>'required|exists:Storage_Location_Type,STL_Type_name',
			'CL_allocation'=>'required|numeric'
		]);

		if($v->fails())return $this->badRequest($v->errors()->all());

		$unitloadTypeName = $request['CL_ULT_Type_name'];
		$storageLocationTypeName = $request['CL_STL_Type_name'];
		$user = \Illuminate\Support\Facades\Request::user();
		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Capacity Location"]);
		else
		{
			$owner = $user->client->owner;
			$unitloadtype =UnitloadType::where('ULT_Type_name',$unitloadTypeName)->where('ULT_Type_OWN_id',$owner->id)->first();
			$storagelocationtype=StorageLocationType::where('STL_Type_name',$storageLocationTypeName)->where('STL_Type_OWN_id',$owner->id)->first();
			if($storagelocationtype == null || $unitloadtype == null)
				return $this->badRequest(["This Unitloadtype or this Storage Location Type doesn't Exists"]);
			if(!$this->isUniqueness($unitloadTypeName,$storageLocationTypeName, $owner->id,0))return $this->badRequest(['Capacity Location Exists']);

			$capacitylocation = new CapacityLocation();
			$capacitylocation->CL_ULT_Type_id = $unitloadtype->id;
			$capacitylocation->CL_STL_Type_id = $storagelocationtype->id;
			$capacitylocation->CL_OWN_id = $owner->id;
			$capacitylocation->CL_allocation = $request['CL_allocation'];
			if($capacitylocation->save())
			{
				return $this->resourceCreated(['Capacity Location Successfully Created']);
			}
			else{
				return $this->internalError(['Capacity Location Creation has Failed']);
			}
		}
	}


	public function update(Request $request)
	{
		
		$v = Validator::make($request->all(),[
			'id' => 'required|exists:Capacity_Location,id'
		]);

		$id = intval($request['id']);
		if($v->fails()) return $this->badRequest($v->errors()->all());
		$user = \Illuminate\Support\Facades\Request::user();
		$owner_id = $user->client->owner->id;

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create Capacity Location"]);
		else
		{
			$unitloadtypename = $request['ULT_Type_name'];
			$storagelocationtypename = $request['STL_Type_name'];
			$allocation = $request['allocation'];

			$capacitylocation = CapacityLocation::findOrFail($id);
			if($this->checkIfExists($capacitylocation))
				return $this->badRequest(['There is already a unitloadtype assigned to  Storage Location Type using this rule']);
			$unitloadtype =UnitloadType::where('ULT_Type_name',$unitloadtypename)->where('ULT_Type_OWN_id',$owner_id)->first();
			$storagelocationtype=StorageLocationType::where('STL_Type_name',$storagelocationtypename)->where('STL_Type_OWN_id',$owner_id)->first();
			if($storagelocationtype == null || $unitloadtype == null)
				return $this->badRequest(["This Unitloadtype or this Storage Location Type doesn't Exists"]);


			$capacitylocation->CL_ULT_Type_id = $unitloadtype->id;
			$capacitylocation->CL_STL_Type_id = $storagelocationtype->id;
			$capacitylocation->CL_OWN_id = $owner_id;
			$capacitylocation->CL_allocation = $allocation;

			if(!$this->isUniqueness($unitloadtypename,$storagelocationtypename,$owner_id,$id))return $this->badRequest(['Capacity Location Already Exists']);

			if($capacitylocation->save())
			{
				return $this->respond(['Capacity Location Successfully Updated']);
			}else{
				return $this->internalError(['Capacity Location Update Failed']);
			}
		}
	}

	public function destroy(Request $request)
	{
		$v = Validator::make(['id'=>$request['id']],[
			'id'=>'integer|exists:Capacity_Location,id'
		]);

		$user = \Illuminate\Support\Facades\Request::user();

		if(!($user->isSystemClient() &&($user->isSystemAdmin()||$user->isSiteAdmin()||$user->isCheifOfLabours())))
			return $this->unauthorized(["you don't have the permission to create a Capacity Location"]);
		else
		{
			if($v->fails())return $this->badRequest($v->errors()->all());
			$id = intval($request['id']);
			$capacitylocation = CapacityLocation::findOrFail($id);
			if($this->checkIfExists($capacitylocation))
				return $this->badRequest(['There is already a unitloadtype assigned to  Storage Location Type using this rule']);
			if($capacitylocation->delete())
			{
				return $this->respond(['Capacity Location Deleted Succesfully']);
			}else{
				return $this->internalError(['Capacity Location Delete Failed']);
			}
		}
	}

	public function isUniqueness($unitloadTypeName,$storageLocationTypeName,$owner_id,$id)
	{
		$field = array('id'=>$id);
		$ult_id = UnitloadType::where('ULT_Type_name',$unitloadTypeName)->where('ULT_Type_OWN_id',$owner_id)->first()->id;
		$stlt_id =StorageLocationType::where('STL_Type_name',$storageLocationTypeName)
		->where('STL_Type_OWN_id',$owner_id)->first()->id;
		$capacitylocation = CapacityLocation::where('CL_ULT_Type_id',$ult_id)->where('CL_STL_Type_id',$stlt_id)->whereNotIn('id',$field)->first();
		if($capacitylocation!=null)return false;
		else return true;
	}
	public function checkIfExists($capacitylocation)
	{
		$unitloads = $capacitylocation->unitloadtype->unitload;
		$storagelocations = $capacitylocation->storagelocationtype->storagelocation;
		foreach ($unitloads as $key => $unitload) {
			foreach ($storagelocations as $key => $storagelocation) {
				if($unitload->UL_STL_id == $storagelocation->id)
					return true;
			}
		}
		return false;
	}
}
