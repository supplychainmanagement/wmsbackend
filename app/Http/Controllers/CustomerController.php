<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/1/2016
 * Time: 4:57 PM
 */
namespace App\Http\Controllers;

use App\Client;
use App\Customer;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;

class CustomerController extends ApiController
{

    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    /**
     * Create new Customer
     * @param Customer Attributes
     * @return Creation Approval
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'CST_name' => 'required'
        ]);
        if($v->fails())
        {
            return $this->badRequest(['Missing Inputs']);
        }

        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;

        if($user->isCheifOfLabours()||$user->isLabour())
        {
            return $this->unauthorized(['Unauthorized Access']);
        }

        $old_customer = Customer::where('CST_OWN_id',$owner->id)->where('CST_name',$request->CST_name)->get();
        if(sizeof($old_customer) > 0)
        {
            return $this->badRequest(['Customer Name Must Be Unique']);
        }        

        $name = $request['CST_name'];
        $phone = null;
        $address = null;
        $branch = null;
        $email = null;

        if($request->has('CST_phone')) $phone = $request['CST_phone'];
        if($request->has('CST_address')) $address = $request['CST_address'];
        if($request->has('CST_branch')) $branch = $request['CST_branch'];
        if($request->has('CST_email')) $email = $request['CST_email'];

        $clients = $owner->Client;
        $newCustomer = new Customer();
        $newCustomer->CST_name = $name;
        $newCustomer->CST_phone = $phone;
        $newCustomer->CST_address = $address;
        $newCustomer->CST_branch = $branch;
        $newCustomer->CST_email = $email;
        $newCustomer->CST_OWN_id = $owner->id;


        if($newCustomer->save())
        {
            if($request->has('CST_CLI_name'))
            {
                for($i =0;$i<sizeof($request['CST_CLI_name']);$i++)
                {
                    $client_id = Client::getIdByName('CLI',$request['CST_CLI_name'][$i]);
                    $newCustomer->CLient()->attach($client_id);
                }
            }
            return $this->resourceCreated(['Customer Successfully Created']);
        }
        return $this->internalError(['Customer Creation Failed']);

    }

    /**
     * Retrieve Records to be Viewed in Frontend
     * @param none
     * @return Customers' Records
     */
    public function index()
    {
        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->client->owner;
        $customers = array();
        if($user->isSystemClient()) {
            $customers['systemClient'] = true;
            if ($user->isSystemAdmin()) {
                $customers['role'] = "systemClient";
                $customers['permission'] = true;
                $customers['siteBased'] = false;
                $customers['customers'] = $owner->Customer->all();
                $customers['clients'] = $owner->Client->all();
            } elseif ($user->isSiteAdmin()) {
                $customers['role'] = "siteAdmin";
                $customers['permission'] = true;
                $customers['siteBased'] = true;
                $customers['customers'] = $owner->Customer->all();
                $customers['clients'] = $user->site->client;
            } elseif ($user->isCheifOfLabours()) {
                $customers['role'] = "cheifOfLabours";
                $customers['permission'] = false;
                $customers['siteBased'] = true;
                $customers['customers'] = $owner->Customer->all();
                $customers['clients'] = $user->site->client;
            } elseif ($user->isLabour()) {
                $customers['role'] = "labour";
                $customers['permission'] = false;
                $customers['siteBased'] = true;
                $customers['customers'] = $owner->Customer->all();
                $customers['clients'] = $user->site->client;
            }
        }
//        }else{
//            $customers['systemClient'] = false;
//            $client = $user->client;
//            if($user->isSystemAdmin())
//            {
//                $customers['role'] = "systemAdmin";
//                $customers['permission'] = true;
//                $customers['siteBased'] = false;
//                $customers['customers'] = $client->Customer->all();
//                $customers['clients'] = $client;
//            }elseif($user->isSiteAdmin())
//            {
//                $customers['role'] = "siteAdmin";
//                $customers['permission'] = false;
//                $customers['siteBased'] = true;
//                $customers['customers'] = $client->Customer->all();
//                $customers['clients'] = $client;
//            }elseif($user->isCheifOfLabours())
//            {
//                $customers['role'] = "chiefOfLabours";
//                $customers['permission'] = false;
//                $customers['siteBased'] = true;
//                $customers['customers'] = $client->Customer->all();
//                $customers['clients'] = $client;
//            }elseif($user->isLabour())
//            {
//                $customers['role'] = "labour";
//                $customers['permission'] = false;
//                $customers['siteBased'] = true;
//                $customers['customers'] = $client->Customer->all();
//                $customers['clients'] = $client;
//            }
//
//        }

        return $this->respond($customers);
    }

    /**
     * Update Customer Attributes
     * @param Customer New Attributes
     * @return Update Approval
     */
    public function update(Request $request)
    {

        $v = Validator::make($request->all(),[
            'id'=>'required|exists:Customer,id',
            'CST_name'=>'required',
        ]);
        if($v->fails())return $this->badRequest(['Missing Inputs']);

        $user = \Illuminate\Support\Facades\Request::user();
        $owner = $user->Client->Owner;
        if($user->isCheifOfLabours()||$user->isLabour())
        {
            return $this->unauthorized(['Unauthorized Access']);
        }
        $id = intval($request['id']);
        $customer = Customer::findOrFail($id);

        $old_customer = Customer::where('CST_OWN_id',$owner->id)->where('CST_name',$request->CST_name)->where('id','<>',$customer->id)->get();
        if(sizeof($old_customer) > 0)
        {
            return $this->badRequest(['Customer Name Must Be Unique']);
        }  

        $customer->CST_name = $request['CST_name'];
        if($request->has('CST_phone')) $customer->CST_phone = $request['CST_phone'];
        if($request->has('CST_address')) $customer->CST_address = $request['CST_address'];
        if($request->has('CST_email')) $customer->CST_email = $request['CST_email'];
        if($request->has('CST_branch')) $customer->CST_branch = $request['CST_branch'];
        $customer->CLient()->detach();
        if($request->has('CST_CLI'))
        {
            for($i = 0;$i<sizeof($request['CST_CLI']);$i++)
            {
                $customer->CLient()->attach(Client::getIdByName('CLI',$request['CST_CLI'][$i]));
            }
        }

        if(Customer::where('CST_name',$request['CST_name'])->where('CST_branch',$request['CST_branch'])->
            whereNotIn('id',[$id])->exists())
            return $this->badRequest(['This Customer Already Exists']);
        if($customer->save())return $this->respond(['Customer Updated Successfully']);
        return $this->internalError(['Customer Updation Failed']);

    }


    public function destroy(Request $request)
    {
        $v = Validator::make($request->all(),[
        'id' => 'required|exists:Customer,id'
        ]);
        if($v->fails())return $this->badRequest(['Wrong Index']);
        $id = intval($request['id']);
        $customer = Customer::findOrFail($id);
        $customer->client()->detach();
        if($customer->delete())return $this->respond(['Customer is Deleted Successfully']);
        return $this->internalError(['Deleting Customer Failed']);
    }

    public function customerProp(Request $request)
    {
        $v = Validator::make($request->all(),[
            'id'=>'required|exists:Customer,id'
        ]);
        if($v->fails())return $this->badRequest(['Invalid Index']);
        $id = intval($request['id']);
        $user = \Illuminate\Support\Facades\Request::user();
        $site = false;
        $admin = false;
        $clients = null;
        $customer = null;
        $owner_id = $user->client->owner->id;

        if($user->isSystemClient())
        {
            $systemCleint = true;
            if($user->isSystemAdmin())
            {
                $admin = true;
                $site = false;
                $customer = Customer::where('id',$id)->first();
                $clients = Client::where('CLI_OWN_id',$owner_id)->get();
            }else
            {
                $admin = false;
                $site = true;
                $customer = Customer::where('id',$id)->first();
                $clients = $user->site->client;
            }
        }

        $respond = array();
        $respond['site'] = $site;
        $respond['admin'] = $admin;
        $respond['customer'] = $customer;
        $respond['clients'] = $clients;        
        $respond['customer_clients'] = $customer->CLient;
        //return $customer;
        json_encode($respond);
        return $this->respond($respond);
    }


}