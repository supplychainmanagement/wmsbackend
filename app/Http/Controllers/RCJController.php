<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\ReceivingJobUnitload;
use App\Unitload;
use App\SKU ;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\RoboVics\Transformers;
use App\RoboVics\Transformers\PurchaseOrderTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use RuntimeException;
use DB;
use App\ReceivingJob;
class RCJController extends ApiController
{

    public function __construct(PurchaseOrderTransformer $podTransformer)
    {
        parent::__construct();
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }

    public function submitReceivingJob (Request $request){

        /*
        * Submit a new Receiving Job
        * @param Attributes of RCJ_Id, POI_Id, Amount,
         * ItemSerial, unitLoadSerial, ItemQualityStatus, ItemsProductionDate.
        * @return Approval of Creation
        */

        $v = Validator::make($request->all(),[
            'rcj_serial'=>'required|min:0',
            'poi_id'=>'required',
            'amount'=>'required|min:1',
            'item_serial'=>'required',
            'ul_serial'=>'required',
            'item_quality_status'=>'required|min:0|max:2',

        ]);

        if($v->fails())return $this->badRequest($v->errors()->all());

        $rcj_serial= $request['rcj_serial'];
        $poi_id = $request['poi_id'];
        $amount = $request['amount'];
        $item_serial = $request['item_serial'];
        $ul_serial = $request['ul_serial'];
        $item_quality_status = $request['item_quality_status'];
        $item_pod_date = $request['item_pod_date'];


        $user = \Illuminate\Support\Facades\Request::user();
        $owner_id = $user->site->owner->id;
        $site_id = $user->site->id ;


        /*Not allowed yet operations for business clients*/
        if(!$user->isSystemClient())
        {
            return $this->respondWithError('User is not authorized');
        }



        DB::beginTransaction();

        try {
            // Add the main Data for the Receiving Object itself.

            $rcj_job = new ReceivingJob();
            $rcj_job->RCJ_number = $rcj_serial ;
            $rcj_job->RCJ_POI_id = $poi_id ;
            $rcj_job->RCJ_OWN_id = $owner_id;
            $rcj_job->RCJ_SITE_id = $site_id ;
            $rcj_job->RCJ_SITE_id = $site_id ;
            $rcj_job->RCJ_quantity = $amount ;

            //Validate the partial amount of the total Amount of the Purchase order Item.
            $poi = $rcj_job->PurchaseOrderItems;
            if($poi->TotalAmountsubmitted()+$amount > $poi->POI_amount){
                DB::rollback();
                return $this->respondWithStatusCode(501,'Total Amount Exceeds Submittion');
            }

            $rcj_job->save();
            $ul = Unitload::where('UL_serialNumber', $ul_serial)->get();
            $sku = SKU::where('SKU_item_no', $item_serial)->get();

            //Validate SKU related Data.
            if ($sku->count()==0){
                DB::rollback();
                return $this->respondWithStatusCode(508,'No such Item Serial');

            }else $sku = $sku[0];

            if(!($sku->id ==$poi->POI_SKU_id )){
                DB::rollback();
                return $this->respondWithStatusCode(509,' Different item Serial than the original One');
            }
            //validate unitload related Data.
            if ($ul->count()==0){
                throw new ModelNotFoundException('');
            }else
                $ul = $ul[0];

            // Unit load validation

            $response = $this->validateUnitLoad ($ul,$item_serial,$item_quality_status,$amount);
            if($response){
                return $response;
            }
            $response = $this->validateExpiryDate($ul,$item_pod_date,$sku);
            if($response ){
                return $response;
            }
            // Add the new Receiving JoB_UnitLoad Object .
            $rju = new ReceivingJobUnitload();
            $rju->RJU_SKU_id = $poi->POI_SKU_id;
            $rju->RJU_UL_id =$ul->id ;
            $rju->RJU_RCJ_id =$rcj_job->id ;
            $rju->RJU_SITE_id = $site_id;
            $rju->save();

           //Update the unitload data.
            $ul->UL_amount = intval($ul->UL_amount) + intval($amount) ;
            $ul->updateAllocation();
            $ul->save() ;
            DB::commit();
            return $this->resourceCreated(['Receiving Job has Successfully Created']);

        }catch(ModelNotFoundException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            return $this->respondWithStatusCode(501,'No Such unit Load');
        }
        catch(ValidationException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            return $this->internalError(['Receiving Job has Failed1']);
        } catch(\Exception $e)
        {
            DB::rollback();
            return $this->internalError(['Receiving Job has Failed2',$e->getMessage()]);
        }
        }
    public function validateExpiryDate($ul,$item_pod_date,$sku){

        //validate SKu palindrome .
        if ($ul->UL_expiryDate){
            if (!$item_pod_date){
                DB::rollback();
                return $this->respondWithStatusCode(505,'Unit load and Items expiry date missmatch1');
            }else {
                if (!((date($ul->UL_expiryDate)-date ($item_pod_date)) == intval($sku->SKU_lifeTime))){

                    DB::rollback();
                    return $this->respondWithStatusCode(505,'Unit load and Items expiry date missmatch2');
                }
            }
        }else {
            if ($item_pod_date){
                DB::rollback();
                return $this->respondWithStatusCode(505,'Unit load and Items expiry date missmatch3');
            }

        }

        return null;

    }

    public function validateUnitLoad ($ul,$item_serial,$item_quality_status,$amount) {
        if(!($ul->SKU->SKU_item_no == $item_serial)){
            DB::rollback();
            return $this->respondWithStatusCode(503,'This Unit Load has a different Stock unit');
        }
        if(!($ul->CanBeGoodsIn())){

            DB::rollback();
            return $this->respondWithStatusCode(506,'Unit load is not inside Goods in Area');
        }
        if(!($ul->UL_status==$item_quality_status)){

            DB::rollback();
            return $this->respondWithStatusCode(504,' item Contains a different QS Type');
        }
        if(!$ul->canTakeMoreSKU($amount)){
            DB::rollback();
            return $this->respondWithStatusCode(502,' Not enough space in this unit load.');

        }
        return null ;
    }
    public function generateUniqueReceivingJobId($poi_id){

        $index = intval($poi_id);

        // Basic Validation.
        if ($index < 0 ){
            return $this->badRequest('index is not valid');
        }
        $time_start = microtime(true) * 10000;
        $unique_string = $poi_id. $time_start;
        return $this->respond((array)$unique_string);
    }


}
