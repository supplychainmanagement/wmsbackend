<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Client;
use App\Zone;
use App\StorageLocation;
use App\Unitload;
use App\Rack;
use Carbon\Carbon;
use App\ReceivingJob;
use App\Shipping;
use App\SKU;
use App\IssueItem;  
class HomeController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }
    public function getNumberOfClients()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$amount = 0;
    	if($user->isSystemAdmin())
    	{
    		$noClients = Client::where('CLI_OWN_id',$owner->id)->count();
    		$amount = $noClients;
    	}
    	else
    	{
    		$site = $user->site;
    		$noClients = $site->client->count();
    		$amount = $noClients;
    	}
    	$noClientsArray = array('amount'=>$amount);
    	return $noClientsArray;
    }
    public function getEmptyZones()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$noOfEmptyZones = 0;
    	$noOfNonEmptyZones = 0;
            //$zones_ids = array();
            //$noOfzones =0;
            if($user->isSystemAdmin())
            {
                $zones_ids = array();
                $unitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_allocation','>',0)->get();
                
                foreach ($unitloads as $key => $unitload) {
                    $zone_id = $unitload->StorageLocation->Rack->RACK_ZONE_id;
                    if(!in_array($zone_id, $zones_ids))
                        $zones_ids[] = $zone_id;
                }
                $noOfzones = Zone::where('ZONE_OWN_id',$owner->id)->count();
            }
            elseif(!$user->isSystemAdmin())
            {
                $zones_ids = array();
                $site = $user->site;
                $unitloads = Unitload::where('UL_OWN_id',$owner->id)->where('UL_SITE_id',$site->id)->where('UL_allocation','>',0)->get();
                foreach ($unitloads as $key => $unitload) {
                    $zone_id = $unitload->StorageLocation->Rack->RACK_ZONE_id;
                    if(!in_array($zone_id, $zones_ids))
                        $zones_ids[] = $zone_id;
                }
                $noOfzones = Zone::where('ZONE_OWN_id',$owner->id)->where('ZONE_SITE_id',$site->id)->count();
            }
			$data = array(
    			'noOfEmptyZones'=>$noOfzones-sizeof($zones_ids),
    			'noOfNonEmptyZones'=>sizeof($zones_ids)
    		);
    		return $data;
    }
    //new,partially treated,treated
    public function issueOrderCategories()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	
    	$noNew =0;
    	$noPartiallyTreated =0;
    	$noTreated=0;

    	if($user->isSystemAdmin())
    	{
    		$issueOrders = $owner->IssueOrder;
    		foreach ($issueOrders as $key => $issueOrder) {
    		$issueOrderItems = $issueOrder->IssueItem;
    		$countNoOfTreated = 0;
    		foreach ($issueOrderItems as $key => $issueOrderItem) {
    			if($issueOrderItem->ISI_state == 'treated')
    				$countNoOfTreated++;
    		}
    		if($countNoOfTreated == 0)
    			$noNew++;
    		elseif($countNoOfTreated == sizeof($issueOrderItems))
    			$noTreated++;
    		elseif($countNoOfTreated != sizeof($issueOrderItems))
    			$noPartiallyTreated++;
    		}
    	}
    	elseif(!$user->isSystemAdmin())
    	{
    		$site = $user->site;
    		$issueOrders = $site->IssueOrder;
    		foreach ($issueOrders as $key => $issueOrder) {
    		$issueOrderItems = $issueOrder->IssueItem;
    		$countNoOfTreated = 0;
    		foreach ($issueOrderItems as $key => $issueOrderItem) {
    			if($issueOrderItem->ISI_state == 'treated')
    				$countNoOfTreated++;
    		}
    		if($countNoOfTreated == 0)
    			$noNew++;
    		elseif($countNoOfTreated == sizeof($issueOrderItems))
    			$noTreated++;
    		elseif($countNoOfTreated != sizeof($issueOrderItems))
    			$noPartiallyTreated++;
    		}
    	}
    	$data = array(
    		'noNew'=>$noNew,
    		'noTreated'=>$noTreated,
    		'noPartiallyTreated'=>$noPartiallyTreated
    	);
    	return $data;	
    }
    //picked,partiallypicked,shipped,partiallyshipped
    public function issueOrderCategories2()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	
    	$noPicked =0;
    	$noShipped =0;
    	$noPartiallyPicked=0;
    	$noPartiallyShipped =0;
    	if($user->isSystemAdmin())
    	{
    		$issueOrders = $owner->IssueOrder;
    		foreach ($issueOrders as $key => $issueOrder) {
    			$issueOrderItems = $issueOrder->IssueItem;
    			$noPickedInternal = 0;
    			$noShippedInternal = 0;
    			foreach ($issueOrderItems as $key => $issueOrderItem) {
    				if($issueOrderItem->ISI_picked  == $issueOrderItem->ISI_amount)
    					$noPickedInternal++;
    				if($issueOrderItem->ISI_shipped  == $issueOrderItem->ISI_amount)
    					$noShippedInternal++;
    			}
    			if($noShippedInternal == sizeof($issueOrderItems))
    				$noShipped++;
    			elseif($noShippedInternal != sizeof($issueOrderItems))
    				$noPartiallyShipped++;
    			if($noPickedInternal == sizeof($issueOrderItems))
    				$noPicked++;
    			elseif($noPickedInternal != sizeof($issueOrderItems))
    				$noPartiallyPicked++;
    		}
    	}
    	elseif(!$user->isSystemAdmin())
    	{
    		$site = $user->site;
    		$issueOrders = $site->IssueOrder;
    		foreach ($issueOrders as $key => $issueOrder) {
    			$issueOrderItems = $issueOrder->IssueItem;
    			$noPickedInternal = 0;
    			$noShippedInternal = 0;
    			foreach ($issueOrderItems as $key => $issueOrderItem) {
    				if($issueOrderItem->ISI_picked  == $issueOrderItem->ISI_amount)
    					$noPickedInternal++;
    				if($issueOrderItem->ISI_shipped  == $issueOrderItem->ISI_amount)
    					$noShippedInternal++;
    			}
    			if($noShippedInternal == sizeof($issueOrderItems))
    				$noShipped++;
    			elseif($noShippedInternal != sizeof($issueOrderItems))
    				$noPartiallyShipped++;
    			if($noPickedInternal == sizeof($issueOrderItems))
    				$noPicked++;
    			elseif($noPickedInternal != sizeof($issueOrderItems))
    				$noPartiallyPicked++;
    		}
    	}
    	$data = array(
    		'noShipped'=>$noShipped,
    		'noPartiallyShipped'=>$noPartiallyShipped,
    		'noPicked'=>$noPicked,
    		'noPartiallyPicked'=>$noPartiallyPicked
    	);
    	return $data;
    }
    //received partially,received,new
    public function purchaseOrderCategories()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	
    	$noNew =0;
    	$noPartiallyRecieved =0;
    	$noRecieved=0;

    	if($user->isSystemAdmin())
    	{
    		$purchaseOrders = $owner->PurchaseOrder;
    		foreach ($purchaseOrders as $key => $purchaseOrder) {
    		$purchaseOrderItems = $purchaseOrder->podItems;
    		$countNoOfRecieved = 0;
    		foreach ($purchaseOrderItems as $key => $purchaseOrderItem) {
    			if($purchaseOrderItem->POI_state == 'fully_received')
    				$countNoOfRecieved++;
    		}
    		if($countNoOfRecieved == 0)
    			$noNew++;
    		elseif($countNoOfRecieved == sizeof($purchaseOrderItems))
    			$noRecieved++;
    		elseif($countNoOfRecieved != sizeof($purchaseOrderItems))
    			$noPartiallyRecieved++;
    		}
    	}
    	elseif(!$user->isSystemAdmin())
    	{
    		$site = $user->site;
    		$purchaseOrders = $site->PurchaseOrder;
    		foreach ($purchaseOrders as $key => $purchaseOrder) {
    		$purchaseOrderItems = $purchaseOrder->podItems;
    		$countNoOfRecieved = 0;
    		foreach ($purchaseOrderItems as $key => $purchaseOrderItem) {
    			if($purchaseOrderItem->POI_state == 'fully_received')
    				$countNoOfRecieved++;
    		}
    		if($countNoOfRecieved == 0)
    			$noNew++;
    		elseif($countNoOfRecieved == sizeof($purchaseOrderItems))
    			$noRecieved++;
    		elseif($countNoOfRecieved != sizeof($purchaseOrderItems))
    			$noPartiallyRecieved++;
    		}
    	}
    	$data = array(
    		'noNew'=>$noNew,
    		'noRecieved'=>$noRecieved,
    		'noPartiallyRecieved'=>$noPartiallyRecieved
    	);
    	return $data;	
    }
    public function amountSKU()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$skuArray = array();
    	$skus = SKU::where('SKU_OWN_id',$owner->id)->inRandomOrder()->paginate(3);
    	$counter = 0;
    	foreach ($skus as $key => $sku) {
    		$amount = 0;
    		if($user->isSystemAdmin())
    		{
    			$unitloads = $owner->unitload;
                foreach ($unitloads as $key => $unitload) {
                    $amount += $unitload->UL_amount;
                }
    		}
    		elseif(!$user->isSystemAdmin())
    		{
    			$site = $user->site;
				$unitloads = $site->unitload;
				foreach ($unitloads as $key => $unitload) {
                    $amount += $unitload->UL_amount;
                }
    		}
    		$skuArray[$counter]['sku'] =$sku->SKU_name;
    		$skuArray[$counter]['number'] =$sku->SKU_item_no;
    		$skuArray[$counter]['amount']=$amount;
    		$counter++;
    	}
    	return $skuArray;
    }

    public function recevingJobsShippingJobsDate()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$receivingJobsArray= array();
    	$shippingJobsArray = array();
    	if($user->isSystemAdmin())
    	{
    		$receivingJobs = ReceivingJob::where('RCJ_OWN_id',$owner->id)
    		->orderBy('created_at', 'ASC')
		    ->get()
		    ->groupBy(function($date) {
		        return Carbon::parse($date->created_at)->format('Y'); // grouping by months
		    });
		    $shippingJobs = Shipping::where('SHIP_OWN_id',$owner->id)
		    ->orderBy('created_at', 'ASC')
		    ->get()
		    ->groupBy(function($date) {
		        return Carbon::parse($date->created_at)->format('Y'); // grouping by months
		    });
		    $counter = 0;
		    foreach ($receivingJobs as $key => $perMonth) {
		    	$amount = 0;
		    	foreach ($perMonth as $keyrecevingJob => $value) {
		    		$amount += $value['RCJ_amount'];
		    	}
		    	$receivingJobsArray[$counter]['amount']= $amount;
		    	$receivingJobsArray[$counter]['year'] = $key;
		    	$counter++;
		    }
		    $counter = 0;
		    foreach ($shippingJobs as $key => $perMonth) {
		    	$amount = 0;
		    	foreach ($perMonth as $keyshippingJob => $value) {
		    		$amount += IssueItem::where('id',$value['SHIP_ISI_id'])->first()->ISI_amount;
		    	}
		    	$shippingJobsArray[$counter]['amount']= $amount;
		    	$shippingJobsArray[$counter]['year'] = $key;
		    	$counter++;
		    }
    	}
    	elseif(!$user->isSystemAdmin())
    	{
    		$receivingJobs = ReceivingJob::where('RCJ_OWN_id',$owner->id)
    		->where('RCJ_SITE_id',$user->site->id)
		    ->orderBy('created_at', 'ASC')
		    ->get()
		    ->groupBy(function($date) {
		        return Carbon::parse($date->created_at)->format('Y'); // grouping by months
		    });
		    $shippingJobs = Shipping::where('SHIP_OWN_id',$owner->id)
		    ->where('SHIP_SITE_id',$user->site->id)
		    ->orderBy('created_at', 'ASC')
		    ->get()
		    ->groupBy(function($date) {
		        return Carbon::parse($date->created_at)->format('Y'); // grouping by months
		    });
		    $counter =0;
		    foreach ($receivingJobs as $key => $perMonth) {
		    	$amount = 0;
		    	foreach ($perMonth as $keyrecevingJob => $value) {
		    		$amount += $value['RCJ_amount'];
		    	}
		    	$receivingJobsArray[$counter]['amount']= $amount;
		    	$receivingJobsArray[$counter]['year'] = $key;
		    	$counter++;
		    }
		    $counter = 0;
		    foreach ($shippingJobs as $key => $perMonth) {
		    	$amount = 0;
		    	foreach ($perMonth as $keyshippingJob => $value) {
		    		$amount += IssueItem::where('id',$value['SHIP_ISI_id'])->first()->ISI_amount;
		    	}
		    	$shippingJobsArray[$counter]['amount']= $amount;
		    	$shippingJobsArray[$counter]['year'] = $key;
		    	$counter++;
		    }
    	}
    	$data = array(
    		'receivingJobs'=>$receivingJobsArray,
    		'shippingJobs'=>$shippingJobsArray
    	);

	    return $data;
    }
    public function getHomeData()
    {
    	$data = array(
    		'getEmptyZones'=>$this->getEmptyZones(),
    		'issueOrderCategories'=>$this->issueOrderCategories(),
    		'issueOrderCategories2'=>$this->issueOrderCategories2(),
    		'purchaseOrderCategories'=>$this->purchaseOrderCategories(),
    		'recevingJobsShippingJobsDate'=>$this->recevingJobsShippingJobsDate()
    	);
    	return $this->respond($data);
    }
    public function getHomeDataIndex()
    {
    	$data =array(
    		'noClients'=>$this->getNumberOfClients(),
    		'sku'=>$this->amountSKU()
    	);
    	return $this->respond($data);
    }
    public function getRandomSKU()
    {
        return $this->respond($this->amountSKU());
    }
}
