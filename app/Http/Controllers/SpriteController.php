<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Client;
use App\Zone;
use App\StorageLocation;
use App\Unitload;
use App\Site;
use App\Rack;
use Carbon\Carbon;

class SpriteController extends ApiController
{
    public function __construct()
    {
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
    }
    public function getSites()
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
		$sites = null;
		if($user->isSystemAdmin())
		{
			$sites = $owner->site;
			return $this->respond($sites->all());
		}else{
			$sites = [$user->site];
			return $this->respond($sites);
		}


    }
        public function getWarehouseData(Request $request)
    
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$data = array();
    	$unitloads = array();
        if($user->isSystemAdmin()){
        	$sites = $owner->site;
        	if($sites != null)
        	{

        		if($request->sitename == '')
        			$data['site'] = $sites[0];
        		else
        			$data['site'] = Site::where('SITE_name',$request->sitename)->where('SITE_OWN_id',$owner->id)->first();
        		$zones = Zone::where('ZONE_SITE_id',$data['site']->id)->get();
        		$data['zones'] = $zones;
        		for($i=0;$i<sizeof($zones);$i++)
        		{
        			$racks = Rack::where('RACK_ZONE_id',$zones[$i]->id)->get();
        			$data['zones'][$i]['plus_rack'] = $racks;
      //   			for($j=0;$j<sizeof($racks);$j++)
      //   			{
      //   				$storagelocations = StorageLocation::where('STL_RACK_id',$racks[$j]->id)->get();
      //   				$data['zones'][$i]['plus_rack'][$j]['plus_storloc'] = $storagelocations;
      //   				$amountOfAssets=0;
      //   				for($k=0;$k<sizeof($storagelocations);$k++)
						// {
						// 	$unitloads = Unitload::where('UL_STL_id',$storagelocations[$k]->id)->get();
						// 	for($n=0;$n<sizeof($unitloads);$n++)
						// 	{
						// 		$amountOfAssets+=$unitloads[$n]->UL_amount;
						// 	}
						// }
						// $data['zones'][$i]['plus_rack'][$j]['noOfAssets'] = $amountOfAssets;
						// $data['zones'][$i]['plus_rack'][$j]['noOfMissplacedAssets'] =0;
						// $data['zones'][$i]['plus_rack'][$j]['noLocations'] = sizeof($storagelocations);
      //   			}
        		}
            	
        	}
        }
        elseif(!$user->isSystemAdmin()){
        	$data['site'] = $user->site;
            $zones = Zone::where('ZONE_SITE_id',$user->site->id)->get();
        		$data['zones'] = $zones;
        		for($i=0;$i<sizeof($zones);$i++)
        		{
        			$racks = Rack::where('RACK_ZONE_id',$zones[$i]->id)->get();
        			$data['zones'][$i]['plus_rack'] = $racks;
      //   			for($j=0;$j<sizeof($racks);$j++)
      //   			{
      //   				$storagelocations = StorageLocation::where('STL_RACK_id',$racks[$j]->id)->get();
      //   				$data['zones'][$i]['plus_rack'][$j]['plus_storloc'] = $storagelocations;
      //   				$amountOfAssets=0;
      //   				for($k=0;$k<sizeof($storagelocations);$k++)
						// {
						// 	$unitloads = Unitload::where('UL_STL_id',$storagelocations[$k]->id)->get();
						// 	for($n=0;$n<sizeof($unitloads);$n++)
						// 	{
						// 		$amountOfAssets+=$unitloads[$n]->UL_amount;
						// 	}
						// }
						// $data['zones'][$i]['plus_rack'][$j]['noOfAssets'] = $amountOfAssets;
						// $data['zones'][$i]['plus_rack'][$j]['noOfMissplacedAssets'] =0;
						// $data['zones'][$i]['plus_rack'][$j]['noLocations'] = sizeof($storagelocations);

      //   			}
        		}
            
        }
        return $this->respond($data); 

    }
    public function getUnitloadLocation(Request $request)
    {
        $user = \Illuminate\Support\Facades\Request::user();
    	$owner = $user->client->owner;
    	$sitename = $request->sitename;
    	$zonename = $request->zonename;
    	$rackname = $request->rackname;
    	$returnarray = array();
    	$site = Site::where('SITE_name',$sitename)->where('SITE_OWN_id',$owner->id)->first();
    	$zone = Zone::where('ZONE_name',$zonename)->where('ZONE_SITE_id',$site->id)->where('ZONE_OWN_id',$owner->id)->first();
    	$rack = Rack::where('RACK_name',$rackname)->where('RACK_ZONE_id',$zone->id)->where('RACK_OWN_id',$owner->id)->first();

    	$locations  = $rack->storagelocation;
    	//return $locations;
    	for($i=0;$i<sizeof($locations);$i++)
    	{
    		$returnarray[$i]['location'] = $locations[$i];
    		$unitloads = Unitload::where('UL_STL_id',$locations[$i]->id)->where('UL_OWN_id',$owner->id)->get();
    		$returnarray[$i]['unitloads'] = $unitloads;
    	}
        return Response($returnarray);
       
    }
}
