<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\PurchaseOrderItems;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\RoboVics\Transformers;
use App\RoboVics\Transformers\PurchaseOrderTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use RuntimeException;
class PodController extends ApiController
{
    //
    protected   $podTransformer;

    public function __construct(PurchaseOrderTransformer $podTransformer)
    {
        parent::__construct();
        $this->middleware('decrypt');
        $this->middleware('auth.basic');
        $this->podTransformer = $podTransformer;
    }

    public function getPodById($id)
    {
        /*
       *Get All Details of a certain Purchase Order.
       * Tariq S. Shatat
       */
        $index = intval($id);
        // Basic Validation.
        if ($index < 0 ){
              return $this->badRequest('index is not valid');
        }
        // logic validation.
        try {
            $pod = PurchaseOrder::findOrFail($index);
            return $this->respond($this->podTransformer->transformToHandheld($pod));
        } catch (ModelNotFoundException  $e) {
            return $this->respondWithError('No Such Purchase Order');
        }catch (RuntimeException  $e) {//uncaught error
            return $this->respondWithError(['Undefined Error',$e->getMessage()]);
        }
    }

}
