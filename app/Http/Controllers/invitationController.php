<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\InvitationCode;
use App\Owner;
use App\Client;
use App\User;
use App\Customer;
use App\Provider;
use DB;
use Mail;
use Hash;

class invitationController extends ApiController
{
    public function verifyCode(Request $request)
    {
    	$v = Validator::make($request->all(), [
            'code' => 'required|exists:Invitation_Code,INV_code'
        ]);

        if ($v->fails()) 
            return $this->badRequest($v->errors()->all());

        $invitation = InvitationCode::where('INV_code',$request['code'])->first();
        if ($invitation->Owner != null) {
         	return $this->badRequest(['Invitation Code is already in use']);
         } 

     	else
     		return $this->respond(['Invitation Code Verified']);
    }

    public function activateCode(Request $request)
    {

        $v = Validator::make($request->all(),[
            'email' => 'required|email|unique:Owner,OWN_email',
            'warehouseName' => 'required|alpha|unique:Owner,OWN_warehouseName',
            'password'=>'required',
            'username'=>'required',
            'code'=>'required'
        ]);
        if($v->fails())
        {
            return $this->badRequest($v->errors()->all());
        }
        $invitation = InvitationCode::where('INV_code',$request['code'])->first();
        if ($invitation == null) {
         	return $this->badRequest(['Invitation Code does not Exist']);
         } 
        if ($invitation->INV_OWN_id != null) {
         	return $this->badRequest(['Invitation Code is already in use']);
         } 
        $email = $request['email'];
        $warehouseName = $request['warehouseName'];
        $username = $request['username'];
        $password = $request['password'];
        $code = $request['code'];

        DB::beginTransaction();
        $owner = new Owner();
        $owner->OWN_email = $email;
        $owner->OWN_firstName = '';
        $owner->OWN_lastName = '';
        $owner->OWN_warehouseName = $warehouseName;
        $owner->OWN_address = '';
        $owner->OWN_phone = '';
        $owner->OWN_enable = 1;
        $owner->OWN_warehouseType = 0;


        if($owner->save())
        {

            //Create System Client
            $client = new Client();
            $client->CLI_name = 'Invited_Client';
            $client->CLI_email = $email;
            $client->CLI_OWN_id = $owner->id;
            $client->CLI_systemClient = 1;
            if($client->save())
            {
                //Create User
                $user = new User();
                $user->USR_name = $username;
                $user->password =  Hash::make($password);
                $user->USR_email = $email;
                $user->USR_CLI_id = $client->id;
                // Create Customer
                $customer = new Customer();
                $customer->CST_name = $client->CLI_name."_selfCustomer";
                $customer->CST_email = $email;
                $customer->CST_phone = '';
                $customer->CST_address = '';
                $customer->CST_OWN_id = $owner->id;
                
                 // Create Provider
                $provider = new Provider();
                $provider->PRV_name = $client->CLI_name."_selfProvider";
                $provider->PRV_email = $email;
                $provider->PRV_phone = '';
                $provider->PRV_address = '';
                $provider->PRV_OWN_id = $owner->id;
               

                //

                if($user->save() && $customer->save() && $provider->save())
                {
                    //Finally
                    $user->Role()->attach(1);
                    $provider->client()->attach($client->id);
                    $customer->client()->attach($client->id);


                    try {

                    	$data = array( 'email' => $email, 'username' => $username,'password' => $password,'warehouseName' => $warehouseName, 'from' => 'wmsplusplus@gmail.com', 'from_name' => 'The WmsPlusPlus Team' );
            
			            Mail::send( 'emails.send', $data, function( $message ) use ($data)
			            {
			                $message->to( $data['email'] )->from( $data['from'], $data['from_name'] )->subject( 'Welcome to WMS++' );
			            });
	                    // Mail::send('mail', ['user' => $user, 'password' => $password], function ($m) use ($user) {
	                    //     $m->from('wmsplusplus@gmail.com', 'The WmsPlusPlus Team');
	                    //     $m->to($user->USR_email, $user->USR_email)->subject('Welcome to WmsPlusPlus');
	                    // });
	                } catch (\Exception $e) {
	                	DB::rollBack();
	                    return $this->internalError([$e->getMessage()]);
	                }

                    $invitation->INV_status = 'used';
                    $invitation->INV_OWN_id = $owner->id;


                    if(!$invitation->save())
                    {
                    	DB::rollBack();
                    	return $this->internalError(['Invitation Updating Failed']);
                    }
                    DB::commit();
                    return $this->resourceCreated(['Warehouse Added Successfully']);
                }
                else{
                    DB::rollBack();
                    return $this->internalError(['User Registration Failed']);
                }
            }else{
                DB::rollBack();
                return $this->internalError(['System Client Registration Failed']);
            }

        }else{
            DB::rollBack();
            return $this->internalError(['Warehouse Registration Failed']);
        }
    }
}
