<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Owner, Client, User Registration and Login
Route::post('/register','RegisterController@register');
Route::post('/login','RegisterController@login');



Route::get('/zone','ZoneController@index');
Route::post('/zone/create','ZoneController@store');
Route::post('/zone/update','ZoneController@update');
Route::post('/zone/delete','ZoneController@destroy');
Route::post('/zoneprop','ZoneController@zoneprob');

Route::get('/customer','CustomerController@index');
Route::post('/customer/create','CustomerController@store');
Route::post('/customer/update','CustomerController@update');
Route::post('/customer/delete','CustomerController@destroy');
Route::post('/customer/prop','CustomerController@customerProp');

Route::get('/provider','ProviderController@index');
Route::post('/provider/create','ProviderController@store');
Route::post('/provider/update','ProviderController@update');
Route::post('/provider/delete','ProviderController@destroy');
Route::post('/provider/providerprop','ProviderController@providerProp');


Route::get('/supplier','SupplierController@index');
Route::post('/supplier/create','SupplierController@store');
Route::post('/supplier/update','SupplierController@update');
Route::post('/supplier/delete','SupplierController@destroy');
Route::post('/supplier/supplierprop','SupplierController@supplierProp');


Route::get('/user','UserController@index');
Route::post('/user/create','UserController@store');
Route::post('/user/update','UserController@update');
Route::post('/user/delete','UserController@destroy');
Route::post('/user/userprop','UserController@userProp');


Route::get('/client','ClientController@index');
Route::post('/client/create','ClientController@store');
Route::post('/client/update','ClientController@update');
Route::post('/client/delete','ClientController@destroy');
Route::post('/client/clientprop','ClientController@clientProp');


Route::get('/site','SiteController@index');
Route::post('/site/create','SiteController@store');
Route::post('/site/update','SiteController@update');
Route::post('/site/delete','SiteController@destroy');
Route::post('/siteprop','SiteController@siteprop');


/*
 * Receive routes
 * Tariq s. shatat
 * **/

Route::get('/pod_details/{id}','PodController@getPodById');
Route::get('/receivingJob/generate/{poi_id}','RCJController@generateUniqueReceivingJobId');
Route::post('/receivingJob/submit','RCJController@submitReceivingJob');

/*
 * Storage routes
 * Tariq s. shatat
 * **/

Route::get('/storageJob/generate','UlStorageController@generateUniqueStorageJobId');
Route::post('/storageJob/submit','UlStorageController@submitStorageJob');


Route::group(['prefix' => '/sku'], function () {
			    Route::get('/' , ['as' => 'sku.index' ,'uses' => 'SKUController@index']);
			    Route::post('/prop' , 'SKUController@prop');
				Route::post('/create' , 'SKUController@create');
				Route::post('/delete' , 'SKUController@delete');
				Route::post('/update' , array( 'uses' =>'SKUController@update', 'as' => 'formupdate'));
				 
			});

Route::group(['prefix' => '/unitload'], function () {
			    Route::get('/' , ['as' => 'unitload.index' ,'uses' => 'UnitloadController@index']);
			    Route::post('/prop' , 'UnitloadController@prop');
				Route::post('/create' , 'UnitloadController@create');
				Route::post('/delete' , array( 'uses' =>'UnitloadController@delete', 'as' => 'formdelete'));
				Route::post('/update' , array( 'uses' =>'UnitloadController@update', 'as' => 'formupdate'));
				Route::post('/getzones' , 'UnitloadController@getZones');
				Route::post('/getracks' , 'UnitloadController@getRacks');
				Route::post('/getlocations' , 'UnitloadController@getLocations');

			});

Route::group(['prefix' => '/strategy'], function () {
			    Route::get('/' , ['as' => 'strategy.index' ,'uses' => 'StrategyController@index']);
			    Route::post('/prop' , 'StrategyController@prop');
				Route::post('/create' , 'StrategyController@create');
				Route::post('/delete' , array( 'uses' =>'StrategyController@delete', 'as' => 'formdelete'));
				Route::post('/update' , array( 'uses' =>'StrategyController@update', 'as' => 'formupdate'));
				 
			});
Route::get('/rack','RackController@index');
Route::post('/rack/create','RackController@store');
Route::post('/rack/update','RackController@update');
Route::post('/rack/delete','RackController@destroy');
Route::post('/rackprop','RackController@rackprop');



Route::get('/functionalarea','FunctionalAreaController@index');
Route::post('/functionalarea/create','FunctionalAreaController@store');
Route::post('/functionalarea/update','FunctionalAreaController@update');
Route::post('/functionalarea/delete','FunctionalAreaController@destroy');


Route::group(['prefix' => '/storagelocation'], function () {
	Route::get('/' , ['as' => 'storagelocation.index' ,'uses' => 'StorageLocationController@index']);
	Route::post('/prop' , 'StorageLocationController@prop');
	Route::post('/create' , 'StorageLocationController@create');
	Route::post('/delete' , array( 'uses' =>'StorageLocationController@delete', 'as' => 'formdelete'));
	Route::post('/update' , array( 'uses' =>'StorageLocationController@update', 'as' => 'formupdate'));

});


Route::get('/storagelocationtype','StorageLocationTypeController@index');
Route::post('/storagelocationtype/create','StorageLocationTypeController@store');
Route::post('/storagelocationtype/update','StorageLocationTypeController@update');
Route::post('/storagelocationtype/delete','StorageLocationTypeController@destroy');

Route::get('/unitloadtype','UnitloadTypeController@index');
Route::post('/unitloadtype/create','UnitloadTypeController@store');
Route::post('/unitloadtype/update','UnitloadTypeController@update');
Route::post('/unitloadtype/delete','UnitloadTypeController@destroy');


Route::get('/skutype','SKUTypeController@index');
Route::post('/skutype/create','SKUTypeController@store');
Route::post('/skutype/update','SKUTypeController@update');
Route::post('/skutype/delete','SKUTypeController@destroy');


Route::get('/capacitylocation','CapacityLocationController@index');
Route::post('/capacitylocation/create','CapacityLocationController@store');
Route::post('/capacitylocation/update','CapacityLocationController@update');
Route::post('/capacitylocation/delete','CapacityLocationController@destroy');


Route::get('/capacityunitload','CapacityUnitloadController@index');
Route::post('/capacityunitload/create','CapacityUnitloadController@store');
Route::post('/capacityunitload/update','CapacityUnitloadController@update');
Route::post('/capacityunitload/delete','CapacityUnitloadController@destroy');

Route::get('/mismatches','MismatchController@index');
Route::post('/mismatches/create','MismatchController@store');
Route::post('/mismatches/update','MismatchController@update');
Route::post('/mismatches/delete','MismatchController@destroy');




Route::group(['prefix' => '/purchaseorder'], function () {
			    Route::get('/' , 'PortalPurchaseOrderController@purchaseOrderIndex');
				Route::post('/delete' ,'PortalPurchaseOrderController@purchaseOrderDelete');
				Route::post('/update' ,'PortalPurchaseOrderController@purchaseOrderUpdate');			 
			});
Route::group(['prefix' => '/purchaseorderitem'], function () {
			    Route::get('/' , 'PortalPurchaseOrderController@purchaseOrderItemIndex');
			    Route::post('/prop' , 'PortalPurchaseOrderController@prop');
				Route::post('/create' , 'PortalPurchaseOrderController@create');
				Route::post('/delete' , 'PortalPurchaseOrderController@delete');
				Route::post('/update' ,'PortalPurchaseOrderController@update');			 
			});
Route::get('/purchaseorderdata','PortalPurchaseOrderController@getDataForPurchaseOrder');
Route::post('/clientproviders','PortalPurchaseOrderController@getProvidersOfClient');
Route::post('/clientskus','PortalPurchaseOrderController@getSKUsOfClient');
Route::post('/createpurchaseorder','PortalPurchaseOrderController@createPurchaseOrder');
Route::post('/createpurchaseorderitem','PortalPurchaseOrderController@createPurchaseOrderItem');
Route::post('/updatepurchaseorderdate','PortalPurchaseOrderController@updatePurchaseOrderDate');
Route::get('/purchaseordernames','PortalPurchaseOrderController@getAllPurchaseOrdersNames');
Route::get('/purchaseorders','PortalPurchaseOrderController@getAllPurchaseOrders');
Route::post('/purchaseorderbyname','PortalPurchaseOrderController@getPurchaseOrderByName');
Route::post('/purchaseorderbynamecreate','PortalPurchaseOrderController@getPurchaseOrderByNameCreate');
Route::post('/purchaseorderitembyname','PortalPurchaseOrderController@getPurchaseOrderItemByName');




Route::group(['prefix' => '/receivingjob'], function () {
			    Route::get('/' , 'PortalReceivingJobController@receivingJobIndex');
			    Route::post('/prop' , 'PortalReceivingJobController@prop');
				Route::post('/create' , 'PortalReceivingJobController@create');
				Route::post('/delete' , 'PortalReceivingJobController@delete');
				Route::post('/update' ,'PortalReceivingJobController@update');			 
			});
Route::get('/generatereceivingjob','PortalReceivingJobController@generateReceivingJobName');
Route::post('/createreceivingjob','PortalReceivingJobController@createReceivingJob');
Route::post('/receivingjobbyname','PortalReceivingJobController@getReceivingJobByName');
Route::post('/receivingjobmatchedunitloads','PortalReceivingJobController@getMatchedUnitloads');
Route::post('/getunitloadcapacity','PortalReceivingJobController@getMaximumUnitloadCapacity');
Route::post('/checkunitloadcapacity','PortalReceivingJobController@checkUnitloadCapacity');
Route::post('/submitreceivingjob','PortalReceivingJobController@submitReceivingJob');




Route::group(['prefix' => '/storagejob'], function () {
			    Route::get('/' , 'PortalStorageController@storageJobIndex');
			    Route::post('/prop' , 'PortalStorageController@prop');
				Route::post('/create' , 'PortalStorageController@create');
				Route::post('/delete' , 'PortalStorageController@delete');
				Route::post('/update' ,'PortalStorageController@update');			 
			});
Route::get('/generatestoragejob','PortalStorageController@generateStorageJobName');
Route::get('/storagejobdata','PortalStorageController@getDataForStorageJob');
Route::post('/storagejobstrategy','PortalStorageController@getStorageLocationsForStrategy');
Route::post('/createstoragejob','PortalStorageController@createStorageJob');
Route::get('/storagejobs','PortalStorageController@getAllStorageJobs');
Route::post('/submitstoragejob','PortalStorageController@submitStorageJob');




Route::group(['prefix' => '/issueorder'], function () {
			    Route::get('/' , 'PortalIssueOrderController@issueOrderIndex');
			    Route::post('/prop' , 'PortalIssueOrderController@prop');
				Route::post('/create' , 'PortalIssueOrderController@create');
				Route::post('/delete' , 'PortalIssueOrderController@delete');
				Route::post('/update' ,'PortalIssueOrderController@issueOrderUpdate');			 
			});
Route::group(['prefix' => '/issueorderitem'], function () {
			    Route::get('/' , 'PortalIssueOrderController@issueOrderItemIndex');
			    Route::post('/prop' , 'PortalIssueOrderController@prop');
				Route::post('/create' , 'PortalIssueOrderController@create');
				Route::post('/delete' , 'PortalIssueOrderController@delete');
				Route::post('/update' ,'PortalIssueOrderController@issueOrderItemUpdate');			 
			});
Route::get('/issueorderdata','PortalIssueOrderController@getDataForIssueOrder');
Route::post('/clientcustomers','PortalIssueOrderController@getCustomersofClient');
Route::post('/createissueorder','PortalIssueOrderController@createIssueOrder');
Route::post('/createissueorderitem','PortalIssueOrderController@createIssueOrderItem');
Route::get('/issueorders','PortalIssueOrderController@getAllIssueOrders');
Route::post('/issueorderbyname','PortalIssueOrderController@getIssueOrderByName');
Route::post('/issueorderitembyname','PortalIssueOrderController@getIssueOrderItemByName');
Route::post('/unitloadamountbyname','PortalPickRequestController@UnitloadAmountByName');
Route::post('/unitloadfreecapacitybyname','PortalPickRequestController@UnitloadFreeCapacityByName');





Route::group(['prefix' => '/pickrequest'], function () {
			    Route::get('/' , 'PortalPickRequestController@pickRequestIndex');
			    Route::post('/prop' , 'PortalPickRequestController@prop');
				Route::post('/create' , 'PortalPickRequestController@create');
				Route::post('/delete' , 'PortalPickRequestController@delete');
				Route::post('/update' ,'PortalPickRequestController@update');			 
			});
Route::group(['prefix' => '/pickrequestitem'], function () {
			    Route::get('/' , 'PortalPickRequestController@pickRequestItemIndex');
			    Route::post('/prop' , 'PortalPickRequestController@prop');
				Route::post('/create' , 'PortalPickRequestController@create');
				Route::post('/delete' , 'PortalPickRequestController@delete');
				Route::post('/update' ,'PortalPickRequestController@update');			 
			});
Route::group(['prefix' => '/pickjob'], function () {
			    Route::get('/' , 'PortalPickRequestController@pickJobIndex');
			    Route::post('/prop' , 'PortalPickRequestController@prop');
				Route::post('/create' , 'PortalPickRequestController@create');
				Route::post('/delete' , 'PortalPickRequestController@delete');
				Route::post('/update' ,'PortalPickRequestController@update');
				Route::get('/generatepickjobname','PortalPickRequestController@generatePickJobName');
			});
Route::post('/setmatchedstorageunitloads','PortalPickRequestController@setMatchedStorageUnitloads');
Route::post('/matchedstorageunitloads','PortalPickRequestController@getMatchedStorageUnitloads');
Route::post('/storageunitloads','PortalPickRequestController@getStorageUnitloads');
Route::post('/matchedgoodsoutunitloads','PortalPickRequestController@getMatchedGoodsOutUnitloads');
Route::post('/submitpickrequest','PortalPickRequestController@submitPickRequest');
Route::post('/pickrequests','PortalPickRequestController@getAllPickRequests');
Route::post('/getpickrequestbyname','PortalPickRequestController@getPickRequestByName');
Route::post('/submitpickrequestitem','PortalPickRequestController@submitPickRequestItem');
Route::post('/submitpickjob','PortalPickRequestController@submitPickJob');
Route::post('/getpickrequestforprint','PortalPickRequestController@getPickRequestForPrint');



Route::group(['prefix' => '/shipping'], function () {
			    Route::get('/' , 'PortalPickRequestController@shippingIndex');
			    Route::post('/prop' , 'PortalPickRequestController@prop');
				Route::post('/create' , 'PortalPickRequestController@create');
				Route::post('/delete' , 'PortalPickRequestController@delete');
				Route::post('/update' ,'PortalPickRequestController@update');
				Route::get('/generateshipname','PortalShippingController@generateShipJobName');
			});
Route::post('/issueitemsforshipping','PortalShippingController@getAllIssueItemsForShipping');
Route::post('/ship','PortalShippingController@ship');

Route::group(['prefix'=>'/services'],function(){
	Route::post('getzonebysitename','ServicesController@getZoneBySiteName');
	Route::post('getrackbyzonename','ServicesController@getRackByZoneName');
});

Route::group(['prefix'=>'/reports'],function(){
	Route::post('/gettransactionreport','ReportsController@getTransactionReport');
	Route::post('/getstockreport','ReportsController@getStockReport');
	Route::post('/getmintransactionreport','ReportsController@getMinTransactionReport');
	Route::post('/getorderreport','ReportsController@getOrderReport');
	Route::get('/getalldata','ReportsController@getAllData');
});


/*
 * Khaled M.Fathy
 */
Route::get('/pois','PortalPurchaseOrderController@getPurchaseOrderItemData');
Route::post('/checksubdomain','RegisterController@checkSubdomain');
Route::get('/gethomedata','HomeController@getHomeData');
Route::get('/gethomedataindex','HomeController@getHomeDataIndex');
Route::post('/getwarehousedata','SpriteController@getWarehouseData');
Route::post('/getunitloadlocation','SpriteController@getUnitloadLocation');
Route::get('/getsitesmonitoring','SpriteController@getSites');

Route::post('/getzonesbysitename','RackController@getZonesBySiteName');

Route::post('/hatemfillstoragelocation','RackController@hatem_fillStorageLocations');
Route::post('/storagelocationtable','StorageLocationController@storageLocationPagination');
Route::get('/randomskus','HomeController@getRandomSKU');



Route::post('/verifycode','invitationController@verifyCode');
Route::post('/activatecode','invitationController@activateCode');

Route::post('/createrfq','RFQController@createRFQ');