<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IssueOrder extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'ISO_OWN_id');
    }
    public function Strategy()
    {
        return $this->belongsTo('App\Strategy', 'ISO_STRAT_id');
    }
    public function Client()
    {
        return $this->belongsTo('App\Client', 'ISO_CLI_id');
    }
    public function Customer()
    {
        return $this->belongsTo('App\Customer', 'ISO_CST_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\Site', 'ISO_SITE_id');
    }

    public function IssueItem()
    {
        return $this->hasMany('App\IssueItem', 'ISI_ISO_id');
    }

    protected $table = 'Issue_Order';
    protected $dates = ['deleted_at'];
}
