<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SKU extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'SKU_OWN_id');
    }
    public function Client()
    {
        return $this->belongsTo('App\Client', 'SKU_CLI_id');
    }
    public function SKUType()
    {
        return $this->belongsTo('App\SKUType', 'SKU_SKU_Type_id');
    }
    public function Supplier()
    {
        return $this->belongsTo('App\Supplier', 'SKU_SUP_id');
    }

    public function Unitload()
    {
        return $this->hasMany('App\Unitload', 'UL_SKU_id');
    }
    public function SKUSupplier()
    {
        return $this->belongsToMany('App\Supplier','SUP_SKU','SS_SKU_id','SS_SUP_id');
    }
    public function Zone()
    {
        return $this->belongsToMany('App\Zone','SKU_Zone','SZ_SKU_id','SZ_ZONE_id');
    }
    public function ReceivingJobUnitload()
    {
        return $this->hasMany('App\ReceivingJobUnitload', 'RJU_SKU_id');
    }
    public function PurchaseOrderItems()
    {
        return $this->hasMany('App\PurchaseOrderItems', 'POI_SKU_id');
    }
    public function IssueItem()
    {
        return $this->hasMany('App\IssueItem', 'ISI_SKU_id');
    }

    protected $table = 'SKU';
    protected $dates = ['deleted_at'];
}
