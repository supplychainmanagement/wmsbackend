<?php

namespace App;

use Illuminate\Http\Request;
use App\Owner;
use App\Client;


use Illuminate\Database\Eloquent\Model;

class ApiModel extends Model
{
    // public static $model_abbreviation = array(
    //     'bin' => , 
    //     );
    public static function getIdByName($abbreviation,$name)
    {
    	$user = \Illuminate\Support\Facades\Request::user();
    	if($user == null)
    		return -1;
    	$owner_id = $user->Client->Owner->id;
    	$result = static::where($abbreviation.'_OWN_id',$owner_id)->where($abbreviation.'_name',$name)->first();
    	if(count($result) > 0)
    	{
    		$id = $result->id;
    		return $id;
    	}
    	else
    		return -1;
    }

    public static function isUniqueName($abbreviation,$type,$name,$id)
    {
        $user = \Illuminate\Support\Facades\Request::user();
        if($user == null)
            return false;
        $owner_id = $user->Client->Owner->id;
        $field = array('id'=>$id);
        if($type == "client")
        {
            $client_id = $user->Client->id;
            $result = static::where($abbreviation.'_name',$name)->where($abbreviation.'_OWN_id',$owner_id)->where($abbreviation.'_CLI_id',$client_id)->whereNotIn('id',$field)->first();
            if(count($result) > 0)
                return false;
            else
                return true;
        }
    }
}



