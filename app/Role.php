<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends ApiModel
{

	public function User()
    {
        return $this->belongsToMany('App\User','Users_Role','UR_USR_id','UR_ROLE_id');
    }
    protected $table = 'Role';
}
