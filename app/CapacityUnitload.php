<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class CapacityUnitload extends ApiModel
{
    use SoftDeletes;
    public function SKUType()
    {
        return $this->belongsTo('App\SKUType', 'CU_SKU_Type_id');
    }
    public function UnitloadType()
    {
        return $this->belongsTo('App\UnitloadType', 'CU_ULT_Type_id');
    }
    protected $table = 'Capacity_Unitload';
    protected $dates = ['deleted_at'];
}
