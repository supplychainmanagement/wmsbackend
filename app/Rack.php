<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rack extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'RACK_OWN_id');
    }
    public function Zone()
    {
        return $this->belongsTo('App\Zone', 'RACK_ZONE_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\SITE', 'RACK_SITE_id');
    }
    public function StorageLocation()
    {
        return $this->hasMany('App\StorageLocation', 'STL_RACK_id');
    }
    protected $table = 'Rack';
    protected $dates = ['deleted_at'];
}
