<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RFQ extends ApiModel
{
    use SoftDeletes;


    protected $table = 'RFQ';
    protected $dates = ['deleted_at'];
}
