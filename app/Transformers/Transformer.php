<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/1/2016
 * Time: 11:03 AM
 */

namespace App\Transformers;


Abstract class Transformer
{
    public function TransformCollection(array $items){
        return array_map([$this, 'transform'], $items);
    }

    public abstract function transform($item);
    public abstract function transform2($request);
}