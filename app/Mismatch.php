<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mismatch extends ApiModel
{
    use SoftDeletes;
    public function UnitloadTypeFirst()
    {
        return $this->belongsTo('App\UnitloadType', 'MISSMATCH_ULT_Type_id_first');
    }
    public function UnitloadTypeSecond()
    {
        return $this->belongsTo('App\UnitloadType', 'MISSMATCH_ULT_Type_id_second');
    }
    public function StorageLocationType()
    {
        return $this->belongsTo('App\StorageLocationType', 'MISSMATCH_STL_Type_id');
    }
    protected $table = 'Missmatch';
    protected $dates = ['deleted_at'];
}
