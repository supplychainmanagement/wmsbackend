<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unitload extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'UL_OWN_id');
    }
    public function Client()
    {
        return $this->belongsTo('App\Client', 'UL_CLI_id');
    }
    public function StorageLocation()
    {
        return $this->belongsTo('App\StorageLocation', 'UL_STL_id');
    }
    public function SKU()
    {
        return $this->belongsTo('App\SKU', 'UL_SKU_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\Site', 'UL_SITE_id');
    }
    public function UnitloadType()
    {
        return $this->belongsTo('App\UnitloadType', 'UL_ULT_Type_id');
    }

    public function StorageJob()
    {
        return $this->hasMany('App\StorageJob', 'STJ_UL_id');
    }
    public function ReceivingJobUnitload()
    {
        return $this->hasMany('App\ReceivingJobUnitload', 'RJU_UL_id');
    }
    public function PickRequestItem()
    {
        return $this->hasMany('APP\PickRequestItem','PRI_UL');
    }
    public function PickJob()
    {
        return $this->hasMany('APP\PickJob','PJ_UL');
    }
    public function CanBeGoodsIn (){

        return $this->StorageLocation->FunctionalArea->isGoodsInArea();
    }
    public function canTakeMoreSKU($amount){
        $results = CapacityUnitload::where('CU_SKU_Type_id', $this->SKU->SKUType->id)->where('CU_ULT_Type_id', $this->UnitloadType->id)->get();
        if ($results->count()==0) return false ;
        $allocation = $results[0]->CU_allocation;
        if(((intval($amount)+intval($this->UL_amount))*intval($allocation))<=100){
            return true;
        }else
            return false ;
    }
    public function updateAllocation(){
        $results = CapacityUnitload::where('CU_SKU_Type_id', $this->SKU->SKUType->id)->where('CU_ULT_Type_id', $this->UnitloadType->id)->get();
        if ($results->count()==0) return false ;
        $allocation = $results[0]->CU_allocation;
        $this->UL_allocation= intval($this->UL_amount)*intval($allocation);

    }
    public function unitloadRoute(){
        $storagelocation = StorageLocation::where('id',$this->UL_STL_id)->first();
        $rack = Rack::where('id',$storagelocation->STL_RACK_id)->first();
        $zone = Zone::where('id',$rack->RACK_ZONE_id)->first();
        $route = $zone->ZONE_name.'=>'.$rack->RACK_name.'=>'.$storagelocation->STL_name;
        return $route;
    }
    protected $table = 'Unitload';
    protected $dates = ['deleted_at'];
}
