<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zone extends ApiModel
{
    use SoftDeletes;
    public function Owner()
    {
        return $this->belongsTo('App\Owner', 'ZONE_OWN_id');
    }
    public function Client()
    {
        return $this->belongsTo('App\Client', 'ZONE_CLI_id');
    }
    public function Site()
    {
        return $this->belongsTo('App\Site', 'ZONE_SITE_id');
    }
    public function Rack()
    {
        return $this->hasMany('App\Rack','RACK_ZONE_id');
    }
    public function Bin()
    {
        return $this->hasMany('App\Bin','BIN_ZONE_id');
    }
    protected $table = 'Zone';
    protected $dates = ['deleted_at'];
}
