<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('Customer_Client', function (Blueprint $table) {
            $table->bigInteger('CC_CLI_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('CC_CST_id')->unsigned();
            $table->foreign('CC_CST_id')
              ->references('id')->on('Customer')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('CC_CLI_id')
              ->references('id')->on('Client')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Customer_Client');
    }
}
