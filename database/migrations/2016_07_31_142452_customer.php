<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('CST_name');
            $table->string('CST_phone')->nullable();
            $table->string('CST_address')->nullable();
            $table->string('CST_email')->nullable();
            $table->string('CST_branch')->nullable();
            $table->bigInteger('CST_OWN_id');
            $table->foreign('CST_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Customer');
    }
}
