<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Racks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Rack', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('RACK_name');
            $table->bigInteger('RACK_row');
            $table->bigInteger('RACK_column');
            $table->bigInteger('RACK_depth');
            $table->bigInteger('RACK_ZONE_id');
            $table->bigInteger('RACK_OWN_id');
            $table->foreign('RACK_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('RACK_ZONE_id')
              ->references('id')->on('Zone')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Rack');
    }
}
