<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('USR_name');
            $table->softDeletes();
            $table->string('USR_email')->nullable();
            $table->string('password');
            $table->bigInteger('USR_CLI_id');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('USR_CLI_id')
            ->references('id')->on('Client')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('USR_SITE_id')->nullable();
            $table->foreign('USR_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
