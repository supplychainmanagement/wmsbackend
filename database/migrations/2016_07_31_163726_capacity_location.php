<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CapacityLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Capacity_Location', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('CL_STL_Type_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('CL_allocation');
            $table->bigInteger('CL_ULT_Type_id')->unsigned();
            $table->foreign('CL_STL_Type_id')
              ->references('id')->on('Storage_Location_Type')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('CL_ULT_Type_id')
              ->references('id')->on('Unitload_Type')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->bigInteger('CL_OWN_id');
              $table->foreign('CL_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Capacity_Location');
    }
}
