<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StorageJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Storage_Job', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('STJ_name')->nullable();
            $table->string('STJ_state')->nullable();
            $table->bigInteger('STJ_UL_id')->unsigned();
            $table->bigInteger('STJ_STL_id')->unsigned();
            $table->bigInteger('STJ_OWN_id');
            $table->foreign('STJ_UL_id')
              ->references('id')->on('Unitload')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('STJ_STL_id')
              ->references('id')->on('Storage_Location')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('STJ_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('STJ_SITE_id')->nullable();
            $table->foreign('STJ_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Storage_Job');
    }
}
