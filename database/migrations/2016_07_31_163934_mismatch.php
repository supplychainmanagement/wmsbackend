<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mismatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('Missmatch', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('MISSMATCH_ULT_Type_id_first')->unsigned();
            $table->bigInteger('MISSMATCH_ULT_Type_id_second')->unsigned();
            $table->bigInteger('MISSMATCH_STL_Type_id')->unsigned();

            $table->foreign('MISSMATCH_ULT_Type_id_first')
              ->references('id')->on('Unitload_Type')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('MISSMATCH_ULT_Type_id_second')
              ->references('id')->on('Unitload_Type')
              ->onDelete('cascade')->onUpdate('cascade');

              $table->foreign('MISSMATCH_STL_Type_id')
              ->references('id')->on('Storage_Location_Type')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('MISSMATCH_OWN_id');
            $table->foreign('MISSMATCH_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Missmatch');
    }
}
