<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientSiteContract extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Site_Client_Contract', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('SCC_CLI_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('SCC_SITE_id')->unsigned();
            $table->foreign('SCC_CLI_id')
                ->references('id')->on('Client')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('SCC_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Site_Client_Contract');
    }
}
