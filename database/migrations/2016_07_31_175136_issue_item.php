<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IssueItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Issue_Item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('ISI_number');
            $table->string('ISI_state')->nullable();
            $table->date('ISI_shippingDate')->nullable();
            $table->bigInteger('ISI_amount');
            $table->bigInteger('ISI_picked');
            $table->bigInteger('ISI_shipped');
            $table->bigInteger('ISI_ISO_id');
            $table->bigInteger('ISI_SKU_id');
            $table->bigInteger('ISI_OWN_id');
            $table->bigInteger('ISI_STRAT_id');
            $table->foreign('ISI_STRAT_id')
              ->references('id')->on('Strategy')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ISI_ISO_id')
            ->references('id')->on('Issue_Order')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ISI_SKU_id')
            ->references('id')->on('SKU')
            ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('ISI_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('ISI_SITE_id')->nullable();
            $table->foreign('ISI_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Issue_Item');
    }
}
