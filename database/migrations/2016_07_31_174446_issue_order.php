<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IssueOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Issue_Order', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->string('ISO_number');
            $table->string('ISO_status');
            $table->bigInteger('ISO_CLI_id')->nullable();
            $table->bigInteger('ISO_CST_id')->nullable();
            $table->bigInteger('ISO_OWN_id')->nullable();
            $table->foreign('ISO_CLI_id')
              ->references('id')->on('Client')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('ISO_CST_id')
              ->references('id')->on('Customer')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('ISO_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('ISO_SITE_id')->nullable();
            $table->foreign('ISO_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Issue_Order');
    }
}
