<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Strategy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Strategy', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('STRAT_name');
            $table->boolean('STRAT_storage');
            $table->boolean('STRAT_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Strategy');
    }
}
