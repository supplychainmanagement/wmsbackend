<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceivingJobUnitload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Receiving_Job_Unitload', function (Blueprint $table) {
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('RJU_SKU_id')->unsigned();
            $table->bigInteger('RJU_UL_id')->unsigned();
            $table->bigInteger('RJU_RCJ_id')->unsigned();

            $table->foreign('RJU_SKU_id')
              ->references('id')->on('SKU')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('RJU_UL_id')
              ->references('id')->on('Unitload')
              ->onDelete('cascade')->onUpdate('cascade');

              $table->foreign('RJU_RCJ_id')
              ->references('id')->on('Receiving_Job')
              ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('RJU_SITE_id')->nullable();
            $table->foreign('RJU_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Receiving_Job_Unitload');
    }
}
