<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Client extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Client', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('CLI_name');
            $table->boolean('CLI_systemClient')->nullable();
            $table->string('CLI_phone')->nullable();
            $table->string('CLI_address')->nullable();
            $table->string('CLI_email')->nullable();
            $table->unique('CLI_name');
            $table->bigInteger('CLI_OWN_id');
            $table->foreign('CLI_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Client');
    }
}
