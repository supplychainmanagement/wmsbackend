<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PurchaseOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Purchase_Order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('POI_number');
            $table->bigInteger('POI_amount');
            $table->string('POI_state')->nullable();
            $table->bigInteger('POI_receivedamount')->nullable();
            $table->date('POI_productiondate')->nullable();
            $table->bigInteger('POI_PO_id');
            $table->bigInteger('POI_SKU_id');
            $table->bigInteger('POI_PRV_id')->nullable();
            $table->bigInteger('POV_CLI_id')->nullable();
            $table->bigInteger('POI_OWN_id');
            $table->foreign('POI_PO_id')
            ->references('id')->on('Purchase_Order')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('POI_SKU_id')
            ->references('id')->on('SKU')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('POI_PRV_id')
            ->references('id')->on('Provider')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('POV_CLI_id')
            ->references('id')->on('Client')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('POI_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('POI_SITE_id')->nullable();
            $table->foreign('POI_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Purchase_Order_items');
    }
}
