<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CapacityUnitload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Capacity_Unitload', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('CU_SKU_Type_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('CU_allocation');
            $table->bigInteger('CU_ULT_Type_id')->unsigned();
            $table->foreign('CU_SKU_Type_id')
              ->references('id')->on('SKU_Type')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('CU_ULT_Type_id')
              ->references('id')->on('Unitload_Type')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->bigInteger('CU_OWN_id');
              $table->foreign('CU_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Capacity_Unitload');
    }
}
