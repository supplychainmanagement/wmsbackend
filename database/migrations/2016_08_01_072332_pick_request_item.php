<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PickRequestItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pick_Request_Item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('PRI_number');
            $table->bigInteger('PRI_amount');
            $table->boolean('PRI_type');
            $table->bigInteger('PRI_UL');
            $table->bigInteger('PRI_PR_id');
            $table->bigInteger('PRI_OWN_id');
            $table->foreign('PRI_UL')
            ->references('id')->on('Unitload')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('PRI_PR_id')
              ->references('id')->on('Pick_Request')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('PRI_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('PRI_SITE_id')->nullable();
            $table->foreign('PRI_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Pick_Request_Item');
    }
}
