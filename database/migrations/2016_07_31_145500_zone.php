<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Zone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Zone', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->string('ZONE_name');
            $table->bigInteger('ZONE_OWN_id')->unsigned();
            $table->bigInteger('ZONE_SITE_id')->unsigned()->nullable();
            $table->foreign('ZONE_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('ZONE_SITE_id')
              ->references('id')->on('SITE')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Zone');
    }
}
