<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Bin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('BIN_name');
            $table->string('BIN_status')->nullable();
            $table->string('BIN_serialNumber')->nullable();
            $table->bigInteger('BIN_xUnits');
            $table->bigInteger('BIN_yUnits');
            $table->bigInteger('BIN_zUnits');
            $table->bigInteger('BIN_FA_id');
            $table->bigInteger('BIN_allocation');
            $table->bigInteger('BIN_OWN_id');
            $table->bigInteger('BIN_ZONE_id');
            $table->foreign('BIN_ZONE_id')
            ->references('id')->on('ZONE')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('BIN_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('BIN_FA_id')
              ->references('id')->on('Functional_Area')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Bin');
    }
}
