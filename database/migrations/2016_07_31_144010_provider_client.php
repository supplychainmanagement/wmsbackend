<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProviderClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('Provider_Client', function (Blueprint $table) {
            $table->bigInteger('PC_CLI_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('PC_PRV_id')->unsigned();
            $table->foreign('PC_CLI_id')
              ->references('id')->on('Client')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('PC_PRV_id')
              ->references('id')->on('Provider')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Provider_Client');
    }
}
