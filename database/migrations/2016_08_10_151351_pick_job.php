<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PickJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pick_Job', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('PJ_number');
            $table->bigInteger('PJ_amount');
            $table->bigInteger('PJ_UL');
            $table->bigInteger('PJ_PRI_id');
            $table->bigInteger('PJ_OWN_id');
            $table->foreign('PJ_UL')
            ->references('id')->on('Unitload')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('PJ_PRI_id')
              ->references('id')->on('Pick_Request_Item')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('PJ_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('PJ_SITE_id')->nullable();
            $table->foreign('PJ_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Pick_Job');
    }
}
