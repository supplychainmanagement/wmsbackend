<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceivingJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Receiving_Job', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('RCJ_number');
            $table->bigInteger('RCJ_amount');
            $table->bigInteger('RCJ_POI_id');
            $table->bigInteger('RCJ_OWN_id');
            $table->foreign('RCJ_POI_id')
            ->references('POI_id')->on('Purchase_Order_items')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('RCJ_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('RCJ_SITE_id')->nullable();
            $table->foreign('RCJ_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Receiving_Job');
    }
}
