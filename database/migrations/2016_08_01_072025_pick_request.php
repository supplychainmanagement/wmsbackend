<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PickRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('Pick_Request', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('PR_OWN_id');
            $table->string('PR_state')->nullable();
            $table->string('PR_number');
            $table->date('PR_date');
            $table->bigInteger('PR_ISI_id');
            $table->foreign('PR_ISI_id')
              ->references('id')->on('Issue_Item')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('PR_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
             $table->bigInteger('PR_SITE_id')->nullable();
             $table->foreign('PR_SITE_id')
                 ->references('id')->on('Site')
                 ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Pick_Request');
    }
}
