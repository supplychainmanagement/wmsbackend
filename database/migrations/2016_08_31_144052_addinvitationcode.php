<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addinvitationcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Invitation_Code', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->string('INV_code');
            $table->string('INV_status');
            $table->bigInteger('INV_OWN_id')->nullable();
            $table->foreign('INV_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('RFQ', function (Blueprint $table) {
        $table->bigIncrements('id')->unsigned();
        $table->timestamps();
        $table->softDeletes();
        $table->string('RFQ_companyName');
        $table->string('RFQ_name');
        $table->string('RFQ_ERPPlatform');
        $table->string('RFQ_numberOfEmployees');
        $table->string('RFQ_email');
        $table->string('RFQ_phone');
        $table->string('RFQ_noOfSites');
        $table->string('RFQ_noOfOperationsPerMonth');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Invitation_Code');
         Schema::drop('RFQ');
    }
}
