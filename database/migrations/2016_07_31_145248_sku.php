<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('SKU', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('SKU_name');
            $table->bigInteger('SKU_SKU_Type_id');
            $table->bigInteger('SKU_CLI_id')->nullable();
            $table->bigInteger('SKU_SUP_id')->nullable();
            $table->bigInteger('SKU_OWN_id');
            $table->bigInteger('SKU_lifeTime')->nullable();
            $table->bigInteger('SKU_item_no');
            $table->bigInteger('SKU_stackingNumber')->nullable();
            $table->foreign('SKU_SKU_Type_id')
            ->references('id')->on('SKU_Type')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('SKU_CLI_id')
            ->references('id')->on('Client')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('SKU_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('SKU_SUP_id')
            ->references('id')->on('Supplier')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('SKU');
    }
}
