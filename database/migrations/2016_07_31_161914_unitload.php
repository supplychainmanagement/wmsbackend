<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Unitload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Unitload', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('UL_name');
            $table->string('UL_status')->nullable();
            $table->string('UL_serialNumber')->nullable();
            $table->bigInteger('UL_STL_id');
            $table->bigInteger('UL_allocation');
            $table->bigInteger('UL_OWN_id');
            $table->bigInteger('UL_SITE_id');
            $table->bigInteger('UL_reserved')->nullable();
            $table->bigInteger('UL_CLI_id');
            $table->bigInteger('UL_amount');
            $table->bigInteger('UL_SKU_id');
            $table->date('UL_stocktakingDate')->nullable();
            $table->date('UL_expiryDate')->nullable();
            $table->bigInteger('UL_ULT_Type_id');
            $table->foreign('UL_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('UL_SITE_id')
            ->references('id')->on('Site')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('UL_ULT_Type_id')
            ->references('id')->on('Unitload_Type')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('UL_STL_id')
              ->references('id')->on('Storage_Location')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('UL_CLI_id')
              ->references('id')->on('Client')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('UL_SKU_id')
              ->references('id')->on('SKU')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Unitload');
    }
}
