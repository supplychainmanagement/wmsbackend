<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PurchaseOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Purchase_Order', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('PO_OWN_id');
            $table->string('PO_number');
            $table->date('PO_dateOfDelivery')->nullable();
            $table->bigInteger('PO_CLI_id');
            $table->foreign('PO_CLI_id')
              ->references('id')->on('Client')
              ->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('PO_OWN_id')
              ->references('id')->on('Owner')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('PO_SITE_id')->nullable();
            $table->foreign('PO_SITE_id')
                ->references('id')->on('Site')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Purchase_Order');
    }
}
