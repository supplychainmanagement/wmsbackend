<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StorageLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Storage_Location', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('STL_name');
            $table->string('STL_status')->nullable();
            $table->string('STL_serialNumber')->nullable();
            $table->bigInteger('STL_FA_id');
            $table->bigInteger('STL_SITE_id');
            $table->bigInteger('STL_allocation');
            $table->bigInteger('STL_OWN_id');
            $table->date('STL_stocktakingDate')->nullable();
            $table->bigInteger('STL_STL_Type_id');
            $table->bigInteger('STL_RACK_id');
            $table->foreign('STL_RACK_id')
            ->references('id')->on('Rack')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('STL_OWN_id')
            ->references('id')->on('Owner')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('STL_STL_Type_id')
            ->references('id')->on('Storage_Location_Type')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('STL_FA_id')
              ->references('id')->on('Functional_Area')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('STL_SITE_id')
              ->references('id')->on('Site')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Storage_Location');
    }
}
