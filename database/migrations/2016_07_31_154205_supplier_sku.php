<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SupplierSku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SUP_SKU', function (Blueprint $table) {
            $table->bigInteger('SS_SUP_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('SS_SKU_id')->unsigned();
            $table->foreign('SS_SKU_id')
              ->references('id')->on('SKU')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('SS_SUP_id')
              ->references('id')->on('Supplier')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('SUP_SKU');
    }
}
