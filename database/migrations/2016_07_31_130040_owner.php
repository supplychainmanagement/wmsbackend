<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Owner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Owner', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('OWN_email');
            $table->string('OWN_warehouseName');
            $table->boolean('OWN_enable');
            $table->boolean('OWN_warehouseType');
            $table->string('OWN_firstName')->nullable();
            $table->string('OWN_lastName')->nullable();
            $table->string('OWN_address')->nullable();
            $table->string('OWN_phone')->nullable();
            $table->unique('OWN_warehouseName');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Owner');
    }
}
