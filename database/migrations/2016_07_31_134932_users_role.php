<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Users_Role', function (Blueprint $table) {
            $table->bigInteger('UR_USR_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('UR_ROLE_id')->unsigned();
            $table->foreign('UR_USR_id')
              ->references('id')->on('users')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('UR_ROLE_id')
              ->references('id')->on('role')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Users_Role');
    }
}
