<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SkuZone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SKU_Zone', function (Blueprint $table) {
            $table->bigInteger('SZ_SKU_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('SZ_ZONE_id')->unsigned();
            $table->foreign('SZ_SKU_id')
              ->references('id')->on('SKU')
              ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('SZ_ZONE_id')
              ->references('id')->on('Zone')
              ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('SKU_Zone');
    }
}
